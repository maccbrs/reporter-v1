<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //lily
        include_once app_path().'/Http/RouteBinders/lilyRouteBinders.php';
        include_once app_path().'/Http/RouteBinders/dahliaRouteBinders.php';

        $router->bind('headset_id', function($value) {
            $data = \App\Http\Models\Lily\Headset::where('id', $value)->orderBy('created_at','desc')->first(); 
            return $data;
        });

        //prepaid
        include_once app_path().'/Http/RouteBinders/prepaidRouteBinders.php';
        include_once app_path().'/Http/RouteBinders/rosebudRouteBinders.php';

        //primrose
        include_once app_path().'/Http/RouteBinders/primroseRouteBinders.php';
        //sunflow
        require app_path('Http/RouteBinders/sunflowerRouteBinders.php');  

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
} 
