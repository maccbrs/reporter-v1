<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/ 


Route::group(['middleware' => ['web','auth'],'namespace' => 'lily','prefix' => 'lily'], function () {
	include(app_path().'/Http/Routers/lily.php');
});

Route::group(['middleware' => ['web','auth'],'namespace' => 'rosebud','prefix' => 'rosebud'], function () {
	include(app_path().'/Http/Routers/rosebud.php');
});

Route::group(['middleware' => ['web','auth'],'namespace' => 'primrose','prefix' => 'primrose'], function () {
	include_once app_path().'/Http/Routers/primrose.php';
});

Route::group(['middleware' => ['web','auth'],'namespace' => 'dahlia','prefix' => 'dahlia'], function () {
	include_once app_path().'/Http/Routers/dahlia.php';
}); 

Route::group(['middleware' => ['web','auth'],'namespace' => 'prepaid','prefix' => 'prepaid'], function () {
	include(app_path().'/Http/Routers/prepaid.php');
}); 
Route::group(['middleware' => ['web','auth'],'namespace' => 'poppy','prefix' => 'poppy'], function () {
	include(app_path().'/Http/Routers/poppy.php');
}); 
Route::group(['namespace' => 'blockage','prefix' => 'blockage'], function () {
	include(app_path().'/Http/Routers/blockage.php');
}); 

Route::get('/',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@index']);

Route::get('/test',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@test']);

Route::get('/test-eod2',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@eod']);

Route::get('/test-prepaid',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@prepaidtest']);

Route::get('/test-prepaidload',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@prepaidload']);

Route::get('/test-bk',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@backup']);

Route::get('/test-eod',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@test_eod']);

Route::get('/headhunter',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@eodheadhunter']);

Route::get('/updatereserve',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@updatereserve']);

Route::get('/blocakage_test',['middleware' => ['web','auth'],'as' => 'landing','uses' => 'HomeController@blocakage_test']);

Route::post('/error',['middleware' => ['web'],'as' => 'error','uses' => 'HomeController@error']);
Route::auth();


//Route::get('/404', 'ErrorlistController@index');
//Route::resource('/store', 'ErrorlistController@store');

include(app_path().'/Http/Routers/sunflower.php');

