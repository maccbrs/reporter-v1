<?php namespace App\Http\Helpers; 
use DB;

class Getter{

	public function department($type = 'all'){
		switch($type){
			case 'all':
				return DB::connection('rosebud')->table('department')->get();
				break;
			case 'names':
				$x = DB::connection('rosebud')->table('department')->get();
				if($x){
					$a = [];
					foreach ($x as $y => $z) {
						$a[$z->id] = $z->name;
					}
					return $a;
				}
				return false;
				break;			
			case 'name':
				# code...
				break;
		}
	}

	public function requirements($type = 'names'){
		switch ($type) {
			case 'names':
					$x = DB::table('requirements')->get();
					if($x){
						$a = [];
						foreach ($x as $y => $z) {
							$a[$z->id] = $z->name;
						}
						return $a;
					}
					return false;
				break;
		}
	} 

	public function statuses($type = 'chklist'){
		switch ($type) {
			case 'chklist':
				return [
				'1' => 'pending',
				'2' => 'received',
				'3' => 'delivered'
				];
				break;
		}
	}

	public function chklist_field($input,$info){
		$fields = [
			'train_and_trainer_materials' => 1,
			'scripts_and_rebuttals' => 2,
			'knowledge_base_info' => 3,
			'sample_call_recordings' => 4,
			'ivr_recording' => 5,
			'assigned_supervisor' => 6,
			'seat_plan_details' => 7,
			'tfn_did' => 8,
			'client_ftp_access' => 9,
			'ivr_setup' => 10,
			'vm_setup' => 11,
			'magellan_ftp_access' => 12,
			'calling_list_details' => 13,
			'dispo_crc' => 14,
			'url_webform' => 15,
			'web_access' => 18
		];

		$data = [];

		foreach ($input as $key => $value) {
			if(array_key_exists($key, $fields) && $value){
				$data[] = [
					'requirement_id' => $fields[$key],
					'campaign_id' => $info['campaign_id'],
					'users_id' => $info['users_id'],
					'type' =>  $info['type']
				];
			}
		}

		return ($data?$data:false);
	}

	public function test(){
		$x = $this->department();
	}

	public function getByCategory($chklst,$cat){
		$data = false;
		foreach ($chklst as $k => $v) {
			if($v->answer && $v->requirement->category == $cat){
				foreach ($v->answer as $k2 => $v2) {
					$data[] = [
					'name' => $v->requirement->name,
					'content' => $v2->content,
					'date' => $v2->created_at
					];
				}
			}
		}
		return $data;
	}

	public function categories(){
		return [
			'Campaign Files / Documents',
			'Operational Information',
			'Technical Information',
			'Train Requiments',
			'Product Training Date'
		];
	}

	public function access(){
		return [
			'rosebud' => 'Rosebud',
			'lily' => 'Lily'
		];
	}

	public function by_department($i){

		return DB::connection('rosebud')->table('users')->where('department_id',$i)->get();

	}

}