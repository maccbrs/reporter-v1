<?php namespace App\Http\Helpers\prepaid; 

use DB;


class Calls{

	public function __construct(){
		$this->connection = 'prepaid';
	}

	public function current_load($subscriber_id){

		$total = 0;
		$remaining = 0;

		$load = DB::connection($this->connection)->select('
				SELECT * from loads
				WHERE startdate <= CURDATE()
				AND enddate > CURDATE()
				AND subscriber_id = ?
				limit 1
			', [$subscriber_id]);
		//pre($load);
		if($load){

			$calls = ($this->calls($load[0]->id)?$this->calls($load[0]->id):[]);

			if($calls){
				foreach ($calls as $call) {
					$total += $call->seconds;
				}
			}

			$sec = ($load[0]->minutes?$load[0]->minutes*60:0);
			$remaining = ($total?($sec - $total)/60:$sec/60);

			$data = [
			'load' => $load,
			'calls' => $calls,
			'total' => $total,
			'totalmin' => $total/60,
			'remaining' => $remaining
			];
			
			return $data;
		}

		return false;
	}

	public function loads($subscriber_id){
		return DB::connection($this->connection)->select('
				SELECT * from loads
				where subscriber_id = ?
			', [$subscriber_id]);
	}

	public function calls($load_id){
		$details = DB::connection($this->connection)->select('
				SELECT * from load_details
				where load_id = ?
			', [$load_id]);

		$uniqueid = [];
		foreach ($details as $detail) {
			$uniqueid[] = $detail->uniqueid;
		}

		if($uniqueid){
			$res =  DB::connection('192.168.200.132')
			     ->table('vicidial_agent_log')
	     		 ->join('vicidial_closer_log', 'vicidial_closer_log.uniqueid', '=', 'vicidial_agent_log.uniqueid')
	    		 ->select(
	    		 		'vicidial_closer_log.*',
	    		 		'vicidial_agent_log.dispo_Sec',
	    		 		'vicidial_agent_log.talk_Sec',
	    		 		DB::raw('vicidial_agent_log.dispo_Sec+vicidial_agent_log.talk_Sec AS seconds'))
	    		 ->whereIn('vicidial_closer_log.uniqueid', $uniqueid)
	    		 ->whereNotIn('vicidial_closer_log.status', ["DROP","HU","Test"])
	    		 ->get();

			if($res){
				foreach ($res as $k => $v) {
					$res[$k]->seconds = roundsix($v->seconds);
				}
				return $res;
			}	    		 
		}


		return false;
	}

	public function reserved($subscriber_id){
		return DB::connection($this->connection)->select('
				SELECT * from reserved_details
				where subscriber_id = ?
			', [$subscriber_id]);		
	}

	public function reserved_calls($subscriber_id){
		$res = $this->reserved($subscriber_id);
		if(!empty($res)){
			foreach ($res as $v) {
				$uniqueid[] = $v->uniqueid;
			}
			if($uniqueid){
			 $res =  DB::connection('asterisk')
				     ->table('vicidial_agent_log')
		     		 ->join('vicidial_closer_log', 'vicidial_closer_log.uniqueid', '=', 'vicidial_agent_log.uniqueid')
		    		 ->select(
		    		 		'vicidial_closer_log.*',
		    		 		'vicidial_agent_log.dispo_Sec',
		    		 		'vicidial_agent_log.talk_Sec',
		    		 		DB::raw('vicidial_agent_log.dispo_Sec+vicidial_agent_log.talk_Sec AS seconds'))
		    		 ->whereIn('vicidial_closer_log.uniqueid', $uniqueid)
		    		 ->whereNotIn('vicidial_closer_log.status', ["DROP","HU","Test"])
		    		 ->get();

				if($res){
					foreach ($res as $k => $v) {
						$res[$k]->seconds = roundsix($v->seconds);
					}
					return $res;
				}	

			}
		}

		return false;
	}


	public function overage_remaining($subscriber_id){

		$total_reserved = 0;
		$total_used = 0;
		$a = $this->reserved_calls($subscriber_id);
		//pre($a);
		$b =  DB::connection($this->connection)->select('SELECT * from reserved
							where subscriber_id = ?
							', [$subscriber_id]);

		if($b){

			foreach ($b as $bk => $bv) {
				$total_reserved += $bv->minutes*60;
			}
			if($a){
				foreach ($a as $ak => $av) {
					$total_used += $av->seconds;
				}					
			}
			return ($total_reserved - $total_used)/60;

		}

		return 0;

	}

	public function days_left($subscriber_id){
		$load = DB::connection($this->connection)->select('
				SELECT * from loads
				WHERE startdate <= CURDATE()
				AND enddate > CURDATE()
				AND subscriber_id = ?
				limit 1
			', [$subscriber_id]);

		if($load){
			$days = round((strtotime($load[0]->enddate) - time()) / 86400);
			return $days;			
		}

		return '0';

	}


}