<?php namespace App\Http\Helpers\primrose;  

use DateTime; 
use DateTimeZone;
use Config;
class Tables{

  public function not_included(){
    return [
      'test_email','lead_id','phone_number','user','lob','campaign_id','request_status'
    ];
  }

   public function report_data_tbl($data){

      $contents = [];
      $keys_temp = [];

      foreach ($data as $dv):
         $temp_data = $this->mb_clean_campaign_data($dv['contents']);
         $temp_data['date'] = $dv['created_at'];
         $contents[] = $temp_data;
         $keys_temp = array_merge($keys_temp,array_keys($temp_data));
      endforeach;

      $keys = mb_dksort(array_unique($keys_temp),'date');
      $value = [];

      foreach ($contents as $ck => $cv):
          $tempvalue = [];
          foreach ($keys as $key):
              $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
          endforeach;
          $value[] = $tempvalue;
      endforeach;

      $newkeys = array_keys($value);

      return view('primrose.email._table',compact('value','keys'));

   }

   public function email_data_tbl($data,$opt = []){ 

      foreach ($data as $k => $v):
          if(!in_array($k, $this->not_included())):
              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;
          endif;
      endforeach;

      $value_temp['Date'] = (isset($opt->timezone)?$this->convert_tz2(date("Y-m-d H:i:s"),$opt->timezone):date("Y-m-d H:i:s"));
      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');

      return view('primrose.email._table',compact('value','keys'));

   }

   public function email_data_tbl2($data){

      foreach ($data as $k => $v):
          if(!in_array($k, $this->not_included())){
              $value_temp[ucfirst(strtolower(str_replace('_', ' ', $k)))] = $v;
          }
      endforeach;

      $value_temp['Date'] = date('Y-m-d H:i:s' , strtotime('-6 hour', strtotime(date("Y-m-d H:i:s"))));
      $value[0] = $value_temp;
      $keys = mb_dksort(array_keys($value_temp),'date');

      return view('primrose.email._table',compact('value','keys'));

   }


   public function dispo_data_tbl($data){

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1):
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1['status'];    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1['count'];
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
            endforeach;    
      $record1 .= "</table>";
      return $record1;

   }

   public function html_logs($data){

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Description</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1):
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1->status;   
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->status_name;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->count;
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
            endforeach;     
      $record1 .= "</table>";
      return $record1;

   }


      public function html_calllogs($data){

    //    pre($data);

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>Agent ID</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Call Date</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Phone Number</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Campaign</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Disposition</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Talk Time</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Wrap Time</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Wait Time</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Queue Time</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Comment</th>";
      $record1 .= "<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>UniqueID</th></tr>";
      
      foreach ($data as $row1):
     
        $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
        $record1 .= $row1['user'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['call_date'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['phone_number'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['campaign'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['status'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['talktime'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['wraptime'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['waittime'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['queuetime'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['comment'] ;
        $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
        $record1 .= $row1['uniqueid'] ;
        $record1 .= "</td>";
        $record1 .=  "</td></tr>"; 
      endforeach;     
      $record1 .= "</table>";
      return $record1;

   }

    public function html_logs_agent($data,$statusdesc){


      $ctr = 0;
      foreach ($data as $key2 => $value2) :

        $user[$value2->status][$value2->user][]= $value2->campaign_id;

        $userstatus[$value2->status][]= $value2->campaign_id;
        $useragent[$value2->user][]= $value2->campaign_id;
        $total[]= $value2->campaign_id;
        $userlist[] = $value2->user;

      endforeach;


      $userlist = array_unique($userlist);


      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .="<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th>";
      $record1 .="<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Description</th>";
      
      foreach ($userlist as $row1 ):
        
        $record1 .="<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>{$row1}</th>";
         
      endforeach;


      $record1 .="<th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Total</th>";
      $record1 .='</tr>';

          $ctr = 0;

          foreach ($user as $row2 =>$value2):

            $record1 .= "<tr>";
            $record1 .= "<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
            $record1 .= $row2;
            $record1 .= '</td>';
            $record1 .= "<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
            $record1 .= !empty($statusdesc[$row2]) ? $statusdesc[$row2] : " ";
            $record1 .= '</td>';

            foreach ($userlist as $usernamelist ):
            
              $record1 .= "<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .=  !empty($value2[$usernamelist]) ? count($value2[$usernamelist]) : 0;
              $record1 .= "</td>";

            endforeach;  

              $record1 .= "<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;font-weight: bold;'>";
              $record1 .=  !empty($userstatus[$row2]) ? count($userstatus[$row2]) : " ";
              $record1 .= "</td>";
              $record1 .=  "</tr>"; 
              $ctr++;

          endforeach;  

            $record1 .="<tr><th style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;font-weight: bold;'>Total</th>";
            $record1 .= "<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;font-weight: bold;'>";
            $record1 .=  " ";
            $record1 .= "</td>";

          foreach ($userlist as $row1 ):

            $record1 .="<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;font-weight: bold;'>".  count($useragent[$row1]) ."</td>";
         
          endforeach;

            $record1 .= "<td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;font-weight: bold;'>";
            $record1 .=  !empty($userstatus[$row2]) ? count($total) : " ";
            $record1 .= "</td>";
            $record1 .=  "</tr>"; 
            $record1 .= "</table>";
            
      return $record1;

   }

    public function html_logs_blank(){

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'></th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
         
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= "Calls";    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= "0";
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
             
      $record1 .= "</table>";
      return $record1;

   }


   public function html_logs2($data){

      $record1 = "<p>Eod Call Logs</p>";
      $record1 .= "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1):
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1->status;    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->count;
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
            endforeach;     
      $record1 .= "</table>";
      return $record1;

   }

   public function html_data($data){

        $contents = $keys_temp = $value = [];
        $count = 0;

        foreach ($data as $dv):
          $temp_data = $this->mb_clean_campaign_data($dv->contents);
          $temp_data['date'] = $dv->created_at;
          $contents[] = $temp_data;
          $campaign_identifier =  $dv->campaign_id;

          if($campaign_identifier == '583ca9adf1b8b'){

            $arrayhh[0] ='Source';
            $arrayhh[1] = 'ID';
            $arrayhh[2] = 'Job_ID';
            $arrayhh[3] ='Offer_Company_Name';
            $arrayhh[4] = 'Offer_Company_Information';
            $arrayhh[5] ='Offer_Job_Title';
            $arrayhh[6] = 'Offer_Job_Description';
            $arrayhh[7] ='Work_Experience';
            $arrayhh[8] = 'Candidate_Name';
            $arrayhh[9] ='Email';
            $arrayhh[10] = 'COUNTRY_CODE_PHONE';
            $arrayhh[11] ='Phone_Number';
            $arrayhh[12] = 'Current_Location';
            $arrayhh[13] ='Position';
            $arrayhh[14] = 'Salary';

            $arrayhh[15] ='Nationality';
            $arrayhh[16] = 'LatestEmployer';
            $arrayhh[17] ='Offer_Salary_Range';

            $arrayhh[18] = 'Incentives_Bonuses';
            $arrayhh[19] ='Annual_Salary';
            $arrayhh[20] = 'Interested_in_the_role';
            $arrayhh[21] ='Keen_on_other_opportunities';
            $arrayhh[22] = 'Requested_Job_Description';

            $arrayhh[23] = 'Now_or_Call_Back';
            $arrayhh[24] ='Call_Back_Date';
            $arrayhh[25] ='Reason_for_Call_Back';
            $arrayhh[26] ='Other_Reason_for_Callback';
            $arrayhh[27] ='Call_back_Time';
            $arrayhh[28] ='Current_phone_correct';
            $arrayhh[29] = 'Updated_Phone_Number';
            $arrayhh[30] ='Current_email_correct';
            $arrayhh[31] ='Updated_Email_Address';
            $arrayhh[32] = 'Current_company_Correct';
            $arrayhh[33] ='Not_interested_in_company_or_position';
            $arrayhh[34] = 'Not_keen_-_other_reason';
            $arrayhh[35] ='Reason_for_Not_Interested_Company/Position';
            $arrayhh[36] ='Not_Interested_Other_Reason';
            $arrayhh[37] ='Notice_Period';
            $arrayhh[38] ='Work_Permit';
            $arrayhh[39] = 'Pass_Required';
            $arrayhh[40] ='Interview_Schedule';
            $arrayhh[41] = 'How_many_years_of_experience_you_have_with_data_warehousing?_1__Less_than_2_years___2__More_than_2_years';
            $arrayhh[42] = 'Offer_Interview_Schedule';
            $arrayhh[43] = 'date';  

            $keys_temp = $arrayhh;
            
          }


           $keys_temp = array_merge($keys_temp,array_keys($temp_data));

        endforeach;
        

        $keys = $this->mb_dksort(array_unique($keys_temp),'date');

        foreach ($contents as $ck => $cv):


            $tempvalue = [$cv];
            
            foreach ($keys as $key) {

              if($campaign_identifier == '5879861681241'){

                foreach ($tempvalue as $key2 => $value2) {

                  if($key == "CREDIT_CARD_NUMBER#"){

                    if(!empty($value2['CREDIT_CARD_NUMBER#']) ){

                      if(($value2['CREDIT_CARD_NUMBER#']) > 3){
                       
                        $encrypted =  substr($value2['CREDIT_CARD_NUMBER#'], 0, 4) . str_repeat('*', strlen($value2['CREDIT_CARD_NUMBER#']) - 4) ;
                        $cv[$key] = $encrypted;

                      }else{

                        $encrypted =  substr($value2['CREDIT_CARD_NUMBER#'], -1) . str_repeat('*', strlen($value2['CREDIT_CARD_NUMBER#']) - 1);
                        $cv[$key] = $encrypted;
                      }

                    }
                  }
                
                } 
              
              }
              

                $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
                
            }
            $count++;
            $value[] = $tempvalue;

        endforeach;



        $newkeys = array_keys($value);
        return view('primrose.email._table',compact('value','keys','count'));

   }

   public function html_data2($data){

        $contents = $keys_temp = [];

        foreach ($data as $dv):
           $temp_data = $this->mb_clean_campaign_data($dv->contents);
           $temp_data['date'] = $dv->created_at;
           $contents[] = $temp_data;
           $keys_temp = array_merge($keys_temp,array_keys($temp_data));
        endforeach;

        $keys = mb_dksort(array_unique($keys_temp),'date');

        $value = [];
        foreach ($contents as $ck => $cv):

            $tempvalue = [];
            foreach ($keys as $key) {
                $tempvalue[$key] = (isset($cv[$key])?$cv[$key]:'');
            }
            $value[] = $tempvalue;
        endforeach;

        $newkeys = array_keys($value);
        return view('primrose.email._table2',compact('value','keys'));

   }



  public function cleaner($data){
      return $this->mb_clean_campaign_data($data); 
  }

  public function convert_tz($date_time, $from_tz, $to_tz)
  {
    $time_object = new DateTime($date_time, new DateTimeZone($from_tz));
    $time_object->setTimezone(new DateTimeZone($to_tz));
    return $time_object->format('Y-m-d H:i:s');
  }

  public function convert_tz2($date_time,$to_tz)
  {
      $time_object = new DateTime($date_time, new DateTimeZone(Config::get('app.timezone')));
      $time_object->setTimezone(new DateTimeZone($to_tz));
      return $time_object->format('Y-m-d H:i:s');
  }   


  public function timerange($opt){

    $x = json_decode($opt,true);
    $fromTz = 'Asia/Manila';
    $tz = (isset($x['timezone'])?$x['timezone']:'Asia/Manila');
    $from_object = new DateTime((isset($x['from'])?$x['from']:'08:00:00'), new DateTimeZone($fromTz));
    $from_object->modify('-1 day');
    $orig_from = $from_object->format('Y-m-d H:i:s');
    $from_object->setTimezone(new DateTimeZone($tz));
    $to_object = new DateTime((isset($x['to'])?$x['to']:'07:59:59'), new DateTimeZone($fromTz));
    $orig_to = $to_object->format('Y-m-d H:i:s');
    $to_object->setTimezone(new DateTimeZone($tz));    
    return [
      'fr_tz' => $fromTz,
      'to_tz' => $tz,
      'ofr' => $orig_from,
      'fr' => $from_object->format('Y-m-d H:i:s'),
      'oto' => $orig_to,
      'to' => $to_object->format('Y-m-d H:i:s')
    ];

  } 

public function timerange_prod($timezone, $from, $to){

    $fromTz = (isset($timezone)?$timezone:'Asia/Manila');
    $tz = 'Asia/Manila';

    $from_object = new DateTime((isset($from)?$from:'08:00:00'), new DateTimeZone($fromTz));
   
    $orig_from = $from_object->format('Y-m-d H:i:s');
    $from_object->setTimezone(new DateTimeZone($tz));
    $to_object = new DateTime((isset($to)?$to:'07:59:59'), new DateTimeZone($fromTz));
    $orig_to = $to_object->format('Y-m-d H:i:s');
    $to_object->setTimezone(new DateTimeZone($tz));    
    
    return [
      'fr_tz' => $fromTz,
      'to_tz' => $tz,
      'ofr' => $orig_from,
      'fr' => $from_object->format('Y-m-d H:i:s'),
      'oto' => $orig_to,
      'to' => $to_object->format('Y-m-d H:i:s')
    ];

  }

  public function timerange_data($data, $timezone){

    foreach ($data as $k => $v):

      $fromTz = 'Asia/Manila';
      $tz = (isset($timezone)?$timezone:'Asia/Manila');
      $calldate = new DateTime((isset($v['call_date'])?$v['call_date']:'08:00:00'), new DateTimeZone($fromTz));

      $orig_from = $calldate->format('Y-m-d H:i:s');
      $calldate->setTimezone(new DateTimeZone($tz));
      $v['call_date'] = date_format($calldate, 'Y-m-d H:i:s');
      $data[$k]['call_date'] = date_format($calldate, 'Y-m-d H:i:s');

    endforeach;

    return $data;

  }

  public function timerange_drc($opt){

    $x = json_decode($opt,true);
    $fromTz = 'Asia/Manila';
    $tz = (isset($x['timezone'])?$x['timezone']:'Asia/Manila');
    $from_object = new DateTime((isset($x['from'])?$x['from']:'08:00:00'), new DateTimeZone($fromTz));
    //$from_object->modify('-1 day');
    $orig_from = $from_object->format('Y-m-d H:i:s');
    $from_object->setTimezone(new DateTimeZone($tz));
    $to_object = new DateTime((isset($x['to'])?$x['to']:'07:59:59'), new DateTimeZone($fromTz));
    $orig_to = $to_object->format('Y-m-d H:i:s');
    $to_object->setTimezone(new DateTimeZone($tz));    
    return [
      'fr_tz' => $fromTz,
      'to_tz' => $tz,
      'ofr' => $orig_from,
      'fr' => $from_object->format('Y-m-d H:i:s'),
      'oto' => $orig_to,
      'to' => $to_object->format('Y-m-d H:i:s')
    ];
  }

  public function mb_clean_campaign_data($json){
      $data = [];
      if($json != ''):
        $not_included = ['user','lead_id','phone_number','campaign_id','lob','request_status'];
        foreach (json_decode($json) as $k => $v):
            if(!in_array($k, $not_included)):
                $data[$k] = $v;
            endif;
        endforeach;
      endif;
      return $data;
  }

  public function mb_dksort($array, $case){

      if(in_array($case,$array)):
          foreach($array as $key=>$val):
              if($case==$val):

              else:
                  $a[] = $val;
              endif;
          endforeach;
          $a[] = $case;
          return $a;
      endif;
      
      return $array;
      
  }

  public function emailer_parser($json){
    $arr = json_decode($json);
    if(empty($arr)) return '';
    foreach ($arr as $key => $value) {
      $data[] = $value;
    }
    return implode(',', $data);
  }

  public function emailer_parser2($json){
    $arr = json_decode($json);
    if(empty($arr)) return '';
    foreach ($arr as $key => $value) {
      $data[] = $value;
    }
    return $data;
  }

  public function convert_time_drc($data, $timezone){
    
    foreach ($data as $key => $datalist) {
      $tz = new DateTimeZone($timezone);
      $date = new DateTime($datalist->created_at);
      $date->setTimezone($tz);
      $data[$key]->created_at = $date->format('Y-m-d H:i:s');
      $data[$key]->updated_at = $date->format('Y-m-d H:i:s');
    } 
   
    return $data;

  }


   public function bookworm_logs($data){


      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>DISPO Code</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Count</th></tr>";
           foreach ($data as $row1):
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1->status;    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->count;
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
            endforeach;     
      $record1 .= "</table>";
      return $record1;

   }

  public function bookworm_dksort($array, $case){
      pre($array);
      if(in_array($case,$array)):
          foreach($array as $key=>$val):
              if($case==$val):

              else:
                  $a[] = $val;
              endif;
          endforeach;
          $a[] = $case;
          return $a;
      endif;
      
      return $array;
      
  }

  public function bookworm_data($data){
 

      $record1 = "<table style='border-collapse: collapse; margin: auto;' >";  
      $record1 .= "<tr><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center;'>Date</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Full Name</th>
        <th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Phone Number</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Call Back Date</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Call Back Time</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Customer Caller ID</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Magellan Agent</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Customer Concern</th><th style='background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; '>Call For</th></tr>";
           foreach ($data as $row1):
              $record1 .= "<tr><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>"; 
              $record1 .= $row1->Date;    
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->Fname;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->Phone;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->AdditionalInfo1;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->AdditionalInfo2;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->CallerID;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->AgentName;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->Notes;
              $record1 .= "</td><td style='border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;'>";
              $record1 .= $row1->LOB;
              $record1 .= "</td>";
              $record1 .=  "</td></tr>"; 
            endforeach;     
      $record1 .= "</table>";

      return $record1;

   }

}

?>