<?php namespace App\Http\Helpers\eod; 

use Carbon\Carbon;


class Time{

	public function mbformat($str,$obj){

		if(preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([1-2]{1})([0-9]{1}):([0-5]{1})([0-9]{1}):([0-5]{1})([0-9]{1})$/', $str) >= 1):

			$a = Carbon::createFromFormat('Y-m-d H:i:s',$str);
			if($obj->timezone != $obj->timezone2):
				$a->setTimezone($obj->timezone2);
			endif;
			return $a->format('Y-m-d H:i:s');

		endif;
		return $str;
	}

}