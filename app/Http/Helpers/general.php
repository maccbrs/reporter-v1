<?php

function pre($arr,$die = true){

	echo '<pre>';
	if(is_array($arr) || is_object($arr)){
		 print_r($arr);
	}else{
		echo $arr;
	}
	
	if($die) die;
}

function yearArray($year){  
    $range = array(); 
    $start = strtotime($year.'-01-01'); 
    $end = strtotime($year.'-12-31'); 
     
    do{ 
        $range[] = date('Y-m-d',$start); 
        $start = strtotime("+ 1 day",$start); 
    }while ( $start<=$end ); 
     
    return $range; 
} 

function user_color($options){

	$a = json_decode($options);
	if(is_object($a) && isset($a->color)) return $a->color;
	return false;
	
}

function days_left($end_date){
	return round((strtotime($end_date) - time()) / 86400);
}

function days_between($start,$end){
     return floor((strtotime($end) - strtotime(date("y-m-d", strtotime($start))) )/(60*60*24));
}

function addDayswithdate($date,$days){
    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d", $date);
}

function mb_timeformat($date){
	$dateImp = explode('|', date('F|j|Y|g:i a',strtotime($date)));
	return [
		'month' => $dateImp[0],
		'day' => $dateImp[1],
		'year' => $dateImp[2],
		'time' => $dateImp[3]
	];	
}

function addDay($date,$days){
    $date = strtotime("+".$days." days", strtotime($date));
    return  date("Y-m-d H:i:s", $date);
}

function routename($key){

		$route = [
		'lily.index' => 'home',
		'lily.access.show' => 'access',
		'lily.access.edit' => 'edit access',
		'lily.user.index' => 'user',
		'lily.user.add' => 'add user',
		'lily.user.create' => 'create user',
		'lily.user.edit' => 'edit user',
		'lily.user.update' => 'update user',
		'lily.inventory.all' => 'inventory home',
		'lily.inventory.did' => 'inventory did',
		'lily.inventory.did.edit' => 'edit did',
		'lily.inventory.did.update' => 'update did',
		'lily.inventory.headset.index' => 'headset home',
		'lily.inventory.headset.create' => 'create headset',
		'lily.inventory.headset.store' => 'store headset',
		'lily.inventory.headset.show' => 'show headset',
		'lily.tasklist.index' => 'tasklist home',
		'lily.tasklist.create' => 'create tasklist',
		'lily.tasklist.store' => 'store tasklist',
		'lily.tasklist.show' => 'show tasklist',
		'lily.tasklist.edit' => 'edit tasklist',
		'lily.tasklist.update' => 'update tasklist',
		'lily.scheduler.daily' => 'schedule daily',
		'lily.task.index' => 'task home',
		'lily.task.create' => 'create task',
		'lily.task.store' => 'store task',
		'lily.task.doit.show' => 'show do-it',
		'lily.task.doit.update' => 'update do-it',
		'lily.logs.index' => 'logs home'
		];
	return (isset($route[$key])?$route[$key]:$key);
}

function roundsix($n){

	$abs = floor($n/6)*6;
	$mod = (($n%6)>0?6:0);
	return $abs+$mod;

}

///primrose helper only
function pr_options($opt){

	$ret = [];
	if($opt != ''):
		$opt_arr = json_decode($opt,true); //pre($opt_arr);
		foreach ($opt_arr as $v2):
			if(is_array($v2)):
				foreach($v2 as $v):
					$ret[] = $v;
				endforeach;
			else:
				$ret[] = $v2;
			endif;
		endforeach;
	endif; //pre($ret);
	return $ret;
}

function pr_timezone($opt){
	$x = json_decode($opt,true);
	return (isset($x['timezone'])?$x['timezone']:'GMT');	
}

function parse_input($str,$url = null){
		$arr = array(

			"/\[input:([a-zA-Z0-9_ ]*)\]/" => "<input type='text' name='$1' required >",
			"/\[text:([a-zA-Z0-9_ ]*)\]/" => "<textarea rows='4' cols='40' name='$1'></textarea>",
			"/\[submit:([a-zA-Z0-9_ ]*)\]/" => "<button class='btn ' type='submit'>$1</button>",
			"/\[link:board=([a-zA-Z0-9_ ]*):page=([a-zA-Z0-9_ ]*):name=([a-zA-Z0-9_ ]*)\]/" => "<a class='btn sm' href='".$url."/$1/$2'>$3</a>",
			"/\[emailer:([0-9]*):name=([a-zA-Z0-9_ ]*)\]/" => "<a href='".url('/')."/emailer/send/$1' class='btn btn-default'>$2</a>"
		);
		$keys = array_keys($arr);
		$values = array_values($arr);
		return preg_replace($keys, $values, $str);
}

function mb_update_contents($new, $jsonstring = ''){

	$arr = array();
	if($jsonstring != ''){
		$json = json_decode($jsonstring);
		foreach ($json as $k => $v) {
			$arr[$k] = $v;
		}
	}
	$arr[] = $new;
	$data['count'] = count($arr);
	$data['contents'] = json_encode($arr);
	return $data;
}

function mb_view_contents($jsonstring = ''){
	$ret = '';
	if($jsonstring != ''){
		$json = json_decode($jsonstring);
		foreach ($json as $v) {
			$ret .= '- '. $v. '<br>';
		}
	}
	return $ret;
}

function mb_parser($str,$method='r_u'){
	$res = '';
	switch ($method) {
		case 'r_u':
			$res = ucfirst(strtolower(str_replace('_', ' ', $str)));
			break;
	}
	return $res;
}

function mb_allowed_campaign($opt){
		$auth_opt = json_decode($opt);
		if(isset($auth_opt->campaign)){
			$campaigns_allowed = $auth_opt->campaign; 
			return $campaigns_allowed;
		}
		return [];
}
 
function mb_convert_options($json){
	
	$opt = array();
	
	if($json != ''){
	    $opt_arr = json_decode($json);
	    if($opt_arr){
	        foreach ($opt_arr as $k => $v) {
	           $opt[$k] = $v;
	        }
	    }
	}

	return $opt;
}

function mb_clean_campaign_data($json){
	$data = [];

	if($json != ''){
		$input = json_decode($json);
	    $not_included = ['user','lead_id','phone_number','campaign_id','lob','request_status'];
	    foreach ($input as $k => $v) {
	        if(!in_array($k, $not_included)){
	            $data[$k] = $v;
	        }
	    }
	}
    return $data;
}

function mb_dksort($array, $case){
	//pre($array);
    if(in_array($case,$array)){
        
        foreach($array as $key=>$val){
            if($case==$val){

            }else{
                $a[] = $val;
            }
        }
        $a[] = $case;
        return $a;
    }
    	return $array;
    
}

function mb_date($d,$f){
	$date = new DateTime($d);
	return $date->format($f);
}

function mb_parse_report_options($d){

	$data = [];

	if($d != ''){
		$opt = json_decode($d);
		if(isset($opt->reports)){
			$data = $opt->reports;
		}
	}

	return $data;
}

function mb_two_dates(){

	$dates['today'] = date('Y-m-d ').'16:59:59';
	$dates['yesterday'] = date('Y-m-d', strtotime("- 1 day")).' 17:00:00';
	return $dates;
}

function sf_options($opt,$type = 'campaign'){
	
	$ret = [];
	if($opt != ''){
		$opt_arr = json_decode($opt);

		switch ($type) {
			case 'campaign':
					if(isset($opt_arr->campaign)){
						$ret = $opt_arr->campaign;
					}
				break;
			case 'cc':
					if(isset($opt_arr->cc)){
						$ret = $opt_arr->cc;
					}
				break;
			case 'reports':
					if(isset($opt_arr->reports)){
						$a = $opt_arr->reports;
						foreach ($a as $k => $v) {
							$ret[$k] = $v;
						}
					}
				break;								
			default:
				# code...
				break;
		}

	}
	return $ret;
}

function emailer_parser($json){
	$arr = json_decode($json);
	if(empty($arr)) return '';
	foreach ($arr as $key => $value) {
		$data[] = $value;
	}
	return implode(',', $data);
}
function emailer_parser2($json){
	$arr = json_decode($json);
	if(empty($arr)) return '';
	foreach ($arr as $key => $value) {
		$data[] = $value;
	}
	return $data;
}

function validate_email($str){
	$arr = explode(',', $str);
	$b = [];
	foreach ($arr as $v) {
		if(filter_var(trim($v), FILTER_VALIDATE_EMAIL)){
			$b[] = trim($v);
		}
		
	}
	return $b;
}

function email_cc_parser($json){
	switch ($json) {
		case '':
		case '[""]':
		case '[]':
		case 'null':
		return [];
			break;
		default:
		$arr = json_decode($json);
		foreach ($arr as $key => $value) {
			$data[] = $value;
		}
		return $data;
			break;
	}
}

function toccbcc_layout($to){

	$to = ($to != ''?json_decode($to):[]);
	if(!empty($to)):
		if(count($to) > 1):
			$html = '';
			$html .= '<select class="form-control">';
			foreach ($to as $v) {
				$html .= '<option>';
				$html .= $v;
				$html .= '</option>';
			}
			$html .= '</select>';
			return $html;
		else:
			return implode('<br> ', $to);
		endif;
	endif;
	return '';
}

function novalidate($cid){
	$in = [
		'56d7a464f2618'
	];

	if(in_array($cid, $in)){
		echo 'novalidate';
	}
}  

function dynamicParse($json){

    if($json):

        $except = ["Request Status","Phone Number","Lead Id","User"];
        $a = json_decode($json);

        $keys = [];
        $data = [];
        
        if($a):
	        foreach($a as $ak => $av):
	            foreach($av as $bk => $bv):
	                if(!in_array($bk,$except)):
	                    $data[$ak][$bk] = $bv;
	                	if(!in_array($bk,$keys)):
	                		$keys[] = $bk;
	                	endif;
	                endif;
	            endforeach;
	        endforeach;
        endif;
        
        return [
        	'data' => $data,
        	'keys' => $keys
        ];
    endif;
    return false;

} 

function dynamicParse2($Obj){

    if($Obj):
        $except = ["Request Status","request_status","test_email","campaign_id","lob","user","lead_id"];

        $keys = [];
        $data = [];

        foreach($Obj as $ak => $av):
        	$b = json_decode($av->contents,true);
        	foreach ($b as $bk => $bv):
        		if(!in_array($bk, $except)):
        			$data[$ak][$bk] = $bv;
        			if(!in_array($bk,$keys)):
        				$keys[] = $bk;
        			endif;
        		endif;
        	endforeach;
        endforeach;
        return [
        	'data' => $data,
        	'keys' => $keys
        ];
    endif;
    return false;

} 