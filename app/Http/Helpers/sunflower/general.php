<?php 

function parse_input($str,$url = null){
		$arr = array(

			"/\[input:([a-zA-Z0-9_ ]*)\]/" => "<input type='text' name='$1' required >",
			"/\[text:([a-zA-Z0-9_ ]*)\]/" => "<textarea rows='4' cols='40' name='$1'></textarea>",
			"/\[submit:([a-zA-Z0-9_ ]*)\]/" => "<button class='btn ' type='submit'>$1</button>",
			"/\[link:board=([a-zA-Z0-9_ ]*):page=([a-zA-Z0-9_ ]*):name=([a-zA-Z0-9_ ]*)\]/" => "<a class='btn sm' href='".$url."/$1/$2'>$3</a>",
			"/\[emailer:([0-9]*):name=([a-zA-Z0-9_ ]*)\]/" => "<a href='".url('/')."/emailer/send/$1' class='btn btn-default'>$2</a>"
		);
		$keys = array_keys($arr);
		$values = array_values($arr);
		return preg_replace($keys, $values, $str);
}

function mb_update_contents($new, $jsonstring = ''){

	$arr = array();
	if($jsonstring != ''){
		$json = json_decode($jsonstring);
		foreach ($json as $k => $v) {
			$arr[$k] = $v;
		}
	}
	$arr[] = $new;
	$data['count'] = count($arr);
	$data['contents'] = json_encode($arr);
	return $data;
}

function mb_view_contents($jsonstring = ''){
	$ret = '';
	if($jsonstring != ''){
		$json = json_decode($jsonstring);
		foreach ($json as $v) {
			$ret .= '- '. $v. '<br>';
		}
	}
	return $ret;
}

function mb_parser($str,$method='r_u'){
	$res = '';
	switch ($method) {
		case 'r_u':
			$res = ucfirst(strtolower(str_replace('_', ' ', $str)));
			break;
	}
	return $res;
}

function mb_allowed_campaign($opt){
		$auth_opt = json_decode($opt);
		if(isset($auth_opt->campaign)){
			$campaigns_allowed = $auth_opt->campaign;
			return $campaigns_allowed;
		}
		return [];
}
 
function mb_convert_options($json){
	
	$opt = array();
	
	if($json != ''){
	    $opt_arr = json_decode($json);
	    if($opt_arr){
	        foreach ($opt_arr as $k => $v) {
	           $opt[$k] = $v;
	        }
	    }
	}

	return $opt;
}

function mb_clean_campaign_data($json){
	$data = [];

	if($json != ''){
		$input = json_decode($json);
	    $not_included = ['user','lead_id','phone_number','campaign_id','lob','request_status'];
	    foreach ($input as $k => $v) {
	        if(!in_array($k, $not_included)){
	            $data[$k] = $v;
	        }
	    }
	}
    return $data;
}

function pre($arr,$t = true){
	echo '<pre>';
	print_r($arr);

	if($t)die;
}

function mb_dksort($array, $case){
	//pre($array);
    if(in_array($case,$array)){
        
        foreach($array as $key=>$val){
            if($case==$val){

            }else{
                $a[] = $val;
            }
        }
        $a[] = $case;
        return $a;
    }
    	return $array;
    
}

function mb_date($d,$f){
	$date = new DateTime($d);
	return $date->format($f);
}

function mb_parse_report_options($d){

	$data = [];

	if($d != ''){
		$opt = json_decode($d);
		if(isset($opt->reports)){
			$data = $opt->reports;
		}
	}

	return $data;
}

function mb_two_dates(){

	$dates['today'] = date('Y-m-d ').'16:59:59';
	$dates['yesterday'] = date('Y-m-d', strtotime("- 1 day")).' 17:00:00';
	return $dates;
}

function sf_options($opt,$type = 'campaign'){
	
	$ret = [];
	if($opt != ''){
		$opt_arr = json_decode($opt);

		switch ($type) {
			case 'campaign':
					if(isset($opt_arr->campaign)){
						$ret = $opt_arr->campaign;
					}
				break;
			case 'cc':
					if(isset($opt_arr->cc)){
						$ret = $opt_arr->cc;
					}
				break;
			case 'reports':
					if(isset($opt_arr->reports)){
						$a = $opt_arr->reports;
						foreach ($a as $k => $v) {
							$ret[$k] = $v;
						}
					}
				break;								
			default:
				# code...
				break;
		}

	}
	return $ret;
}

function emailer_parser($json){
	$arr = json_decode($json);
	if(empty($arr)) return '';
	foreach ($arr as $key => $value) {
		$data[] = $value;
	}
	return implode(',', $data);
}
function emailer_parser2($json){
	$arr = json_decode($json);
	if(empty($arr)) return '';
	foreach ($arr as $key => $value) {
		$data[] = $value;
	}
	return $data;
}

function validate_email($str){
	$arr = explode(',', $str);
	$b = [];
	foreach ($arr as $v) {
		if(filter_var(trim($v), FILTER_VALIDATE_EMAIL)){
			$b[] = trim($v);
		}
		
	}
	return $b;
}

function email_cc_parser($json){
	switch ($json) {
		case '':
		case '[""]':
		case '[]':
		case 'null':
		return [];
			break;
		default:
		$arr = json_decode($json);
		foreach ($arr as $key => $value) {
			$data[] = $value;
		}
		return $data;
			break;
	}
}

function toccbcc_layout($to){

	$to = ($to != ''?json_decode($to):[]);
	if(!empty($to)):
		if(count($to) > 1):
			$html = '';
			$html .= '<select class="form-control">';
			foreach ($to as $v) {
				$html .= '<option>';
				$html .= $v;
				$html .= '</option>';
			}
			$html .= '</select>';
			return $html;
		else:
			return implode('<br> ', $to);
		endif;
	endif;
	return '';
}

function novalidate($cid){
	$in = [
		'56d7a464f2618'
	];

	if(in_array($cid, $in)){
		echo 'novalidate';
	}
}  
