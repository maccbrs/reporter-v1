<?php 

$router->bind('campaign_id', function($value) {
    $data = \App\Http\Models\rosebud\Campaign::where('id', $value)->with(['checklist','client_status'])->first();  
    return $data;
}); 

$router->bind('rtc_id', function($value) {
    $data = \App\Http\Models\rosebud\RtC::where('id', $value)->with(['requirement','campaign','client_status','user','answer','message'])->first();  
    return $data;
}); 

$router->bind('client_status_id', function($value) {
    if($value){
        $data = \App\Http\Models\rosebud\ClientStatus::where('id', $value)->first();  
        return $data;
    }
    return $value;
}); 

$router->bind('requirement_id', function($value) {
    $data = \App\Http\Models\rosebud\Requirement::where('id', $value)->first(); 
    return $data;
}); 

$router->bind('schedule_id', function($value) {
    $data = \App\Http\Models\rosebud\Schedule::where('id', $value)->first(); 
    return $data;
}); 

$router->bind('cluster_id', function($value) {
    $data = \App\Http\Models\rosebud\Cluster::where('id', $value)->with(['utc'])->first(); 
    return $data;
}); 