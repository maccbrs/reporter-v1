<?php

    $router->bind('board', function($value) {
        return \App\Http\Models\sunflower\Board::where('campaign_id', $value)
        ->where('status',1)
        ->with(['pages' => function ($query){ $query->where('status',1);},'assigned'])->first(); 
    });
    
	$router->bind('page', function($value) {
	    return \App\Http\Models\sunflower\Page::where('id', $value)->where('status',1)->first();
	});

	$router->bind('user', function($value) {
	    return \App\User::where('id', $value)->with(['campaigns'])->first();

	}); 

	$router->bind('board2', function($value) {
	    return \App\Http\Models\sunflower\Board2::where('campaign_id', $value)->where('status',1)->with(['pages'])->first();
	});

	$router->bind('page2', function($value) {
	    return \App\Http\Models\sunflower\Page2::where('id', $value)->where('status',1)->first();
	}); 

	$router->bind('campaign_data', function($value) {
	    return \App\Http\Models\sunflower\CampaignData::where('campaign_id', $value)->where('active',1)->get();
	});  

	$router->bind('campaign_logs', function($value) {
	    return \App\Http\Models\sunflower\Logs::where('campaign_id', $value)->get();
	});
	$router->bind('user_logs', function($value) {
	    return \App\Http\Models\sunflower\Logs::where('users_id', $value)->get();
	});

	$router->bind('emailer_id', function($value) {
	    return \App\Http\Models\sunflower\Emailer::where('id', $value)->first(); 
	});          

	$router->bind('eod_date', function($value) {
	    return \App\Http\Models\sunflower\Eod::where('date', $value)->get(); 
	});  
	$router->bind('eod_id', function($value) {
	    return \App\Http\Models\sunflower\Eod::where('id', $value)->first(); 
	}); 