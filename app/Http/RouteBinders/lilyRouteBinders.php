<?php 

$router->bind('user_id', function($value) {
    $data = \App\User::where('id', $value)->first(); 
    return $data;
}); 

$router->bind('task_id', function($value) {
    $data = \App\Http\Models\Lily\Task::where('id', $value)->orderBy('created_at','desc')->with('tasklist')->first(); 
    return $data;
}); 

$router->bind('rtc_id', function($value) {
    $data = \App\Http\Models\Lily\RtC::where('id', $value)->with(['requirement','campaign','client_status','user','answer','message'])->first();  
    return $data;
}); 

$router->bind('client_status_id', function($value) {
    if($value){
        $data = \App\Http\Models\ClientStatus::where('id', $value)->first();  
        return $data;
    }
    return $value;
}); 

$router->bind('requirement_id', function($value) {
    $data = \App\Http\Models\Lily\Requirement::where('id', $value)->first(); 
    return $data;
}); 

$router->bind('schedule_id', function($value) {
    $data = \App\Http\Models\Lily\Schedule::where('id', $value)->first(); 
    return $data;
}); 

$router->bind('cluster_id', function($value) {
    $data = \App\Http\Models\Lily\Cluster::where('id', $value)->with(['utc'])->first(); 
    return $data;
}); 

$router->bind('did_id', function($value) {
    $data = \App\Http\Models\Lily\Answer::where('answer.id', $value)
             ->select('answer.*','did.vendor','did.date_subscribe','did.campaign','did.status as did_status')
             ->leftJoin('did','answer.id','=','did.answer_id')
             ->first(); 
    return $data;
}); 
                          
$router->bind('headset_id', function($value) {
    $data = \App\Http\Models\Lily\Headset::where('id', $value)->orderBy('created_at','desc')->first(); 
    return $data;
}); 

$router->bind('tasklist_id', function($value) {
    $data = \App\Http\Models\Lily\Tasklist::where('id', $value)->orderBy('created_at','desc')->with('user')->first(); 
    return $data;
}); 

$router->bind('logs_routename', function($value) {
    $data = \App\Http\Models\Lily\Loggers::where('routename', $value)->orderBy('created_at','desc')->with(['users'])->paginate(25); 
    return $data;
});  

$router->bind('logs_id', function($value) {
    $data = \App\Http\Models\Lily\Loggers::where('id', $value)->with(['users'])->first(); 
    return $data;
}); 