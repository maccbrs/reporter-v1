<?php

$router->bind('subscriber_id', function($value) {
    $data = \App\Http\Models\prepaid\Subscriber::where('id', $value)->with(['loads','reserves','notes','active','connected','emailers','outboundloads'])->first(); 
    return ($data?$data: redirect()->route('404'));
});   

$router->bind('connector_id', function($value) {
    $data = \App\Http\Models\prepaid\Connector::where('id', $value)->first(); 
    return ($data?$data: redirect()->route('404'));
});  