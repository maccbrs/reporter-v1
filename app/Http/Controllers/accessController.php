<?php namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;

class accessController extends Controller
{

	public function access(){
		$access = [];
		if(isset(Auth::user()->id)):
	        if(Auth::user()->user_type == 'admin'):
	            $access = ['dahlia','rosebud','primrose','lily','prepaid','poppy'];
	        else:
	            $access = (Auth::user()->access != ''?json_decode(Auth::user()->access,true):[]);
	        endif;
        endif;
        return $access;
	}

	public function routes(){
		$routes = [];
		if(isset(Auth::user()->id)):
			$routes = (Auth::user()->routes != ''?json_decode(Auth::user()->routes,true):[]);
		endif;
		return $routes;
	}

	public function allowed($route){

		$access = $this->access();
		if(in_array($route,$access)):
			return true;
		endif;
			return false;
			
	}


}