<?php namespace App\Http\Controllers\;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use App\Model\CampaignData;
use App\Model\VICI;
Use Carbon\Carbon;
use Mail;
use DB;
class syncController extends Controller
{
	
	
	    public function syncClientDatabase(){ /// Automatic SYNC SERVER From to Jasper Database at 192.168.201.77
			
			
		//$dboardid = json_decode('["59fbb3f53dde9","5a0481d9cfa4d"]',true);	

		
		$Vici_call_closer_log = new \App\Model\VICI;
			
		$CampaignData = new \App\Model\CampaignData;	
		$JasperIcbm = new \App\Model\JasperIcbm;	
		$JasperViciIn = new \App\Model\JasperViciIn;	
		$JasperViciOut = new \App\Model\JasperViciOut;	
		// SELECTING THE LAST CALL RECORD IN VICI LIVE
		$Vici_call_closer_log_lastrecord = $Vici_call_closer_log->orderby('call_date','DESC')->first();				
		$vici_lastcalldate = $Vici_call_closer_log_lastrecord->call_date;
		//SELECTING THE LAST CALL RECORD IN VICI REPORTER
		$JasperViciIn_lastrecord = 	$JasperViciIn->orderby('call_date','DESC')->first();		
		$JasperViciIn_lastcalldate = $JasperViciIn_lastrecord->call_date;
		
		if ($vici_lastcalldate > $JasperViciIn_lastcalldate  )
		{
			$viciNewRecords = $Vici_call_closer_log->whereBetween('call_date', [$JasperViciIn_lastcalldate, $vici_lastcalldate])->orderby('call_date')->get();
			
			foreach($viciNewRecords as $row => $record)
			{
				$JasperViciIn_NewRecord = new \App\Model\JasperViciIn;
				
				$JasperViciIn_NewRecord->closecallid = $record->closecallid;
				$JasperViciIn_NewRecord->lead_id = $record->lead_id;
				$JasperViciIn_NewRecord->list_id = $record->list_id;
				$JasperViciIn_NewRecord->campaign_id = $record->campaign_id;
				$JasperViciIn_NewRecord->call_date = $record->call_date;
				$JasperViciIn_NewRecord->start_epoch = $record->start_epoch;
				$JasperViciIn_NewRecord->end_epoch = $record->end_epoch;
				$JasperViciIn_NewRecord->length_in_sec = $record->length_in_sec;
				$JasperViciIn_NewRecord->status = $record->status;
				$JasperViciIn_NewRecord->phone_code = $record->phone_code;
				$JasperViciIn_NewRecord->phone_number = $record->phone_number;
				$JasperViciIn_NewRecord->user = $record->user;
				$JasperViciIn_NewRecord->comment = $record->comment;
				$JasperViciIn_NewRecord->processed = $record->processed;
				$JasperViciIn_NewRecord->queue_seconds = $record->queue_seconds;
				$JasperViciIn_NewRecord->user_group = $record->user_group;
				$JasperViciIn_NewRecord->xfercallid = $record->xfercallid;
				$JasperViciIn_NewRecord->term_reason = $record->term_reason;
				$JasperViciIn_NewRecord->uniqueid = $record->uniqueid;
				$JasperViciIn_NewRecord->agent_only = $record->agent_only;
				$JasperViciIn_NewRecord->queue_position = $record->queue_position;
				$JasperViciIn_NewRecord->called_count = $record->called_count;
				$JasperViciIn_NewRecord->save();
				//echo $record->call_date;
				//print_r($record);
				
			}
			//echo count($viciNewRecords) . " VICI Inbound has been Added" ;
			
			//pre($viciNewRecords);			
			//echo "Create Record";
			
			
		}
		
		else
		{
			//echo "Your Data is Updated" ;
			//
			
		}
		
		$Vici_call_log = new \App\Model\VICI_OUTBOUND;
		
		// SELECTING THE LAST CALL RECORD IN VICI LIVE
		$Vici_call_log_out_lastrecord = $Vici_call_log->where('status','!=','INCALL')->orderby('call_date','DESC')->first();				
		$vici_out_lastcalldate = $Vici_call_log_out_lastrecord->call_date;
		//SELECTING THE LAST CALL RECORD IN VICI REPORTER
		$JasperViciOut_lastrecord = 	$JasperViciOut->orderby('call_date','DESC')->first();		
		$JasperViciOut_lastcalldate = $JasperViciOut_lastrecord->call_date;
		
		if ($vici_out_lastcalldate > $JasperViciOut_lastcalldate  )
		{
			$viciOutNewRecords = $Vici_call_log_out_lastrecord->where('status','!=','INCALL')->whereBetween('call_date', [$JasperViciOut_lastcalldate, $vici_out_lastcalldate])->orderby('call_date')->get();
			foreach($viciOutNewRecords as $row => $records)
			{
			$JasperViciOut_NewRecord = new \App\Model\JasperViciOut;
			
			
			$JasperViciOut_NewRecord->uniqueid = $records->uniqueid;
			$JasperViciOut_NewRecord->lead_id = $records->lead_id;
			$JasperViciOut_NewRecord->list_id = $records->list_id;
			$JasperViciOut_NewRecord->campaign_id = $records->campaign_id;
			$JasperViciOut_NewRecord->call_date = $records->call_date;
			$JasperViciOut_NewRecord->start_epoch = $records->start_epoch;
			$JasperViciOut_NewRecord->end_epoch = $records->end_epoch;
			$JasperViciOut_NewRecord->length_in_sec = $records->length_in_sec;
			$JasperViciOut_NewRecord->status = $records->status;
			$JasperViciOut_NewRecord->phone_code = $records->phone_code;
			$JasperViciOut_NewRecord->phone_number = $records->phone_number;
			$JasperViciOut_NewRecord->user = $records->user;
			$JasperViciOut_NewRecord->comment = $records->comment;
			$JasperViciOut_NewRecord->processed = $records->processed;
			$JasperViciOut_NewRecord->user_group = $records->user_group;
			$JasperViciOut_NewRecord->term_reason = $records->term_reason;
			$JasperViciOut_NewRecord->alt_dial = $records->alt_dial;
			$JasperViciOut_NewRecord->called_count = $records->called_count;
			$JasperViciOut_NewRecord->save();
			
			
			}
			
			//echo count($JasperViciOut_NewRecord) . " VICI OutBound has been Added" ;
		}
		else
		{
			//echo "VICI OutBound is Updated" ;
			//
			
		}
		
		
		$DboCalldetailViw = new \App\Model\DboCalldetailViw;
		// GET ICBM last CALL
		$ICBM_call_log_out_lastrecord = $DboCalldetailViw->orderby('TerminatedDate','desc')->first();	
		$ICBM_lastCallDate = $ICBM_call_log_out_lastrecord->TerminatedDate;
		//$CallERID  = $ICBM_call_log_out_lastrecord->CallId;
		//GET THE LAST CALL FOR REPORTer		
		$Jasper_LastRecord	=	$JasperIcbm->orderby('TerminatedDate','desc')->first();	
		$Jaster_lastCallDate = date_create($Jasper_LastRecord->terminateddate);
		
		if ($ICBM_lastCallDate > $Jaster_lastCallDate  )
		{$x= 1;
			$DboCalldetailViw_newRecords = $DboCalldetailViw->whereBetween('TerminatedDate', [$Jaster_lastCallDate, $ICBM_lastCallDate])->orderby('TerminatedDate')->with(['wrapup'])->get();
			foreach($DboCalldetailViw_newRecords as $row => $records)
			{
			
			$JasperIcbm_newRecords = new \App\Model\JasperIcbm;
			$JasperIcbm_newRecords->Callid = $records->CallId;
			$JasperIcbm_newRecords->CallType = $records->CallType;
			$JasperIcbm_newRecords->CallDirection = $records->CallDirection;
			$JasperIcbm_newRecords->LineId = $records->LineId;
			$JasperIcbm_newRecords->StationId = $records->StationId;
			$JasperIcbm_newRecords->LocalUserId = $records->LocalUserId;			
			$JasperIcbm_newRecords->AssignedWorkGroup = $records->AssignedWorkGroup;
			$JasperIcbm_newRecords->LocalNumber = $records->LocalNumber;
			$JasperIcbm_newRecords->LocalName = $records->LocalName;
			$JasperIcbm_newRecords->remotenumber = $records->RemoteNumber;
			$JasperIcbm_newRecords->RemoteNumberCountry = $records->RemoteNumberCountry;
			$JasperIcbm_newRecords->RemoteNumberLoComp1 = $records->RemoteNumberLoComp1;
			$JasperIcbm_newRecords->RemoteNumberLoComp2 = $records->RemoteNumberLoComp2;
			$JasperIcbm_newRecords->RemoteNumberFmt = $records->RemoteNumberFmt;
			$JasperIcbm_newRecords->RemoteNumberCallId = $records->RemoteNumberCallId;
			$JasperIcbm_newRecords->RemoteName = $records->RemoteName;
			$JasperIcbm_newRecords->InitiatedDate = $records->InitiatedDate;
			$JasperIcbm_newRecords->InitiatedDateTimeGMT = $records->InitiatedDateTimeGMT;
			$JasperIcbm_newRecords->ConnectedDate = $records->ConnectedDate;
			$JasperIcbm_newRecords->ConnectedDateTimeGMT = $records->ConnectedDateTimeGMT;
			$JasperIcbm_newRecords->TerminatedDate = $records->TerminatedDate;
			$JasperIcbm_newRecords->CallDurationSeconds = $records->CallDurationSeconds;
			$JasperIcbm_newRecords->HoldDurationSeconds = $records->HoldDurationSeconds;
			$JasperIcbm_newRecords->LineDurationSeconds = $records->LineDurationSeconds;
			$JasperIcbm_newRecords->DNIS = $records->DNIS;
			$JasperIcbm_newRecords->CallEventLog = $records->CallEventLog;
			$JasperIcbm_newRecords->tDialing = $records->tDialing;
			$JasperIcbm_newRecords->tIVRWait = $records->tIVRWait;
			$JasperIcbm_newRecords->tAlert = $records->tAlert;
			$JasperIcbm_newRecords->tSuspend = $records->tSuspend;
			$JasperIcbm_newRecords->tConference = $records->tConference;
			$JasperIcbm_newRecords->tExternal = $records->tExternal;
			$JasperIcbm_newRecords->tACW = $records->tACW;
			$JasperIcbm_newRecords->tqueuewait = $records->tQueueWait;
			$JasperIcbm_newRecords->wrapupcode = ($records->wrapup?$records->wrapup->WrapupCode:'-') ; //$records->wrapup->WrapupCode; 
			$JasperIcbm_newRecords->save();
			
			}
			//echo count($DboCalldetailViw_newRecords) . "DATA HAS BEEN ADDED";
		}
		//echo $ICBM_lastCallDate . ' test ICBM ' . $Jaster_lastCallDate ;
		
		
	
		
		
		
       
    }	
	


}

