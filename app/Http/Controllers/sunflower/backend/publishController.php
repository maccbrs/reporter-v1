<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class publishController extends Controller
{

	public function index(){

		$user = Auth::user();
		$updateObj = new \App\Http\Models\sunflower\Updates; 
		$gn = new \App\Http\Controllers\sunflower\backend\generalController;

		if($user->user_type == 'admin'):
			$updates = $updateObj->active()->with(['board'])->orderBy('updated_at','desc')->paginate(15);
		else:
			$campaigns = $gn->options('campaign',$user->options);
			$updates = ($campaigns?$updateObj->active()->whereIn('campaign_id',$campaigns)->with(['board'])->orderBy('updated_at','desc')->paginate(15):[]);
		endif;
		return view('sunflower.updates.index',compact('updates','gn'));

	}

	public function update($board){

	  $boardObj = new \App\Http\Models\sunflower\Board2;
      $newBoard = $boardObj->findOrNew($board->id);
      $newBoard->id = $board->id;
      $newBoard->campaign_id = $board->campaign_id;
      $newBoard->primary_page = $board->primary_page;
      $newBoard->options = $board->options;
      $newBoard->to = $board->to;
      $newBoard->cc = $board->cc;
      $newBoard->from = $board->from;
      $newBoard->lob = $board->lob; 
      $newBoard->campaign = $board->campaign;          
      $newBoard->save();

      $pageObj =  new \App\Http\Models\sunflower\Page2;
      foreach ($board['pages'] as $page){
          $newpage = $pageObj->findOrNew($page->id);
          $newpage->id = $page->id;
          $newpage->board_id = $page->board_id;
          $newpage->campaign_id = $page->campaign_id;
          $newpage->title = $page->title;
          $newpage->template = $page->template;
          $newpage->options = $page->options;
          $newpage->contents = $page->contents;
          $newpage->options = $page->options;
          $newpage->save();
      } 

      $updateObj = new \App\Http\Models\sunflower\Updates; 
      $updateObj->where('campaign_id',$board->campaign_id)->update(['contents' => '','count' => 0]);
      
      return redirect()->back();       

	}

}