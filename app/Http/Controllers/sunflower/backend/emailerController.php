<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class emailerController extends Controller
{

	public function index(){
		
		$eObj = new \App\Http\Models\sunflower\Emailer;
		$gn = new \App\Http\Controllers\sunflower\backend\generalController;
		$emails = $eObj->orderBy('id','desc')->paginate(10);
		return view('sunflower.emailer.index',compact('emails','gn'));
	}

	public function add(){ 
		return view('sunflower.emailer.add');
	}

	public function edit($emails){
		return view('sunflower.emailer.edit',compact('emails'));
	}

	public function create(Request $r){
		
		$emailerObj = new \App\Http\Models\sunflower\Emailer;

		$to_arr = explode(',', $r->input('to'));
		$from_arr = explode(',', $r->input('from'));
		$cc_arr = explode(',', $r->input('cc'));
		$bcc_arr = explode(',', $r->input('bcc'));

		$data = [];

		if(!empty($to_arr)) foreach ($to_arr as $v) { if(filter_var($v, FILTER_VALIDATE_EMAIL)) $data['to'][] = $v; };
		if(!empty($from_arr)) foreach ($from_arr as $v2) { if(filter_var($v2, FILTER_VALIDATE_EMAIL)) $data['from'][] = $v2; };
		if(!empty($cc_arr)) foreach ($cc_arr as $v3) { if(filter_var($v3, FILTER_VALIDATE_EMAIL)) $data['cc'][] = $v3; };
		if(!empty($bcc_arr)) foreach ($bcc_arr as $v4) { if(filter_var($v4, FILTER_VALIDATE_EMAIL)) $data['bcc'][] = $v4; };

		$address['to'] = (isset($data['to'])? json_encode($data['to']): '');
		$address['from'] = (isset($data['from'])? json_encode($data['from']): '');
		$address['cc'] = (isset($data['cc'])? json_encode($data['cc']): '');
		$address['bcc'] = (isset($data['bcc'])? json_encode($data['bcc']): '');
		$address['name'] = $r->input('name');
		$address['subject'] = $r->input('subject');
		$address['content'] = $r->input('content');

		$emailerObj->create($address);
        flash()->success('new email entry created!');
        return redirect()->route('sunflower.emailer.index'); 

	}

	

	public function update(Request $r,$email){
		$email->to = json_encode(explode(',', $r->input('to')));
		$email->from = json_encode(explode(',', $r->input('from'))); 
		$email->cc = json_encode(explode(',', $r->input('cc')));
		$email->bcc = json_encode(explode(',', $r->input('bcc')));
		$email->name = $r->input('name');
		$email->subject = $r->input('subject');
		$email->content = $r->input('content');
		$email->save();
        flash()->success('email updated!');
        return redirect()->route('sunflower.emailer.edit',$email->id); 
	}

	public function destroy($email){
		$email->delete();
		flash()->success('email deleted!');
		return redirect()->back();
	}

	public function send($emailer){
		$emails = $emailer->toArray();
		return view('backend.admin.emailer.send',compact('emails'));
	}



}