<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\sunflower\Board;
use App\Http\Models\sunflower\Board2;
use App\Http\Models\sunflower\Page2;
use App\Http\Models\sunflower\Updates; 
use App\Http\Models\sunflower\Logs;
use App\Http\Models\sunflower\ConCampaigns as CC;

class dashboardController extends Controller
{
    

	public function __construct(Guard $auth,Logs $logs,CC $cc){ 
		$this->auth = $auth;
    $this->logger = $logs;
    $this->concamp = $cc;
	}

  public function index()
  {
    $user = $this->auth->user();
    return view('backend/admin/dashboard/index',compact(['user']));
  }


	public function destroy(){
		$this->auth->logout();
		return redirect()->route('login');
	} 

    public function publish(Board $boardsObj){ 

      $boards = [];

      if(in_array($this->auth->user()->user_type,['admin','operation'])){

        if($this->auth->user()->user_type != 'admin'){
          $asgn = $this->concamp->where('users_id',$this->auth->user()->id)->get()->toArray();
          if(!empty($asgn)){
            foreach ($asgn as $k => $v) {
              $keys[] = $v['campaign_id'];
            }
            $boards =  $boardsObj->mb_updates($keys);
          }
        }else{
          $boards =  $boardsObj->mb_updates([]);
        }   

      }

      return view('backend.admin.publish.index',compact('boards'));

    }

    public function publish_save($board,Board2 $boardObj,Page2 $pageObj,Updates $updates){
    	
      $newBoard = $boardObj->findOrNew($board->id);
      $newBoard->id = $board->id;
      $newBoard->campaign_id = $board->campaign_id;
      $newBoard->primary_page = $board->primary_page;
      $newBoard->options = $board->options;
      $newBoard->to = $board->to;
      $newBoard->cc = $board->cc;
      $newBoard->from = $board->from;
      $newBoard->lob = $board->lob; 
      $newBoard->campaign = $board->campaign;          
      $newBoard->save();

      foreach ($board['pages'] as $page){
          $newpage = $pageObj->findOrNew($page->id);
          $newpage->id = $page->id;
          $newpage->board_id = $page->board_id;
          $newpage->campaign_id = $page->campaign_id;
          $newpage->title = $page->title;
          $newpage->template = $page->template;
          $newpage->options = $page->options;
          $newpage->contents = $page->contents;
          $newpage->options = $page->options;
          $newpage->save();
      }  

      $this->logger->create(['campaign_id' => $board->campaign,'users_id' => $this->auth->user()->id,'contents' => json_encode(['published the board.'])]);
      $updates->where('campaign_id',$board->campaign_id)->update(['contents' => '','count' => 0]);

      flash()->success('Board successfully published!');
      return redirect()->back();

    }
} 