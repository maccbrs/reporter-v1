<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Models\sunflower\Board;
use App\Http\Models\sunflower\Updates;
use App\Http\Models\sunflower\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\sunflower\Logs;


class pageController extends Controller
{

    public function __construct(Logs $logs,Guard $auth){
        $this->auth = $auth;
        $this->logger = $logs;
    }

	public function add($board){ 

		return view('backend.admin.page.add',compact('board')); 
	}

	public function create($id,Request $r){ 

        $this->validate($r, [
            'title' => 'required|max:100',
            'template' => 'required'
        ]); 
        $boardObj = new \App\Http\Models\sunflower\Board;
        $pageObj = new \App\Http\Models\sunflower\Page;
        $board = $boardObj->find($id);

        $input = [
            'title' => $r->input('title'),
            'template' => $r->input('template'),
            'board_id' => $board->id,
            'campaign_id' => $board->campaign_id
        ];
        
        if($pageObj->create($input)){
               $updatesObj = new \App\Http\Models\sunflower\Updates;
               $updatesCont = $updatesObj->getby_campaign($input['campaign_id']);
               $contents_a = (isset($updatesCont->contents)?$updatesCont->contents:'');
               $ups = mb_update_contents($input['title'].' page created',$contents_a);
               $ups['campaign_id'] = $input['campaign_id'];
               $updatesObj->mb_update($input['campaign_id'],$ups['contents'],$ups['count']);
        }

        return redirect()->back();

	}

    public function edit($page){
        return view('sunflower.page.edit',compact('page'));
    } 

    public function update(Request $request,$page){ 
        
        $updatesObj = new \App\Http\Models\sunflower\Updates;
        $pagecontent = [];

        if($page->contents != ''):
            foreach (json_decode($page->contents) as $k=> $v):
                $pagecontent[$k] = $v;
            endforeach;
        endif;

        if ($request->hasFile('logo')):
            $file = $request->file('logo');
            $name = $file->getClientOriginalName(); 
            $pagecontent['logo'] = $name;
            $file->move('uploads',$name);
        endif;

        $pagecontent = [
            'content-r' => $request->input('content-r'),
            'content-l' => $request->input('content-l'),
            'content-2' => $request->input('content-2'),
            'content-3' => $request->input('content-3'),
            'content-4' => $request->input('content-4')
        ];

        $page->contents = json_encode($pagecontent);

        if($request->input('title') != '') $page->title = $request->input('title');
        
        if($request->input('email_subject') != ''){
            switch ($page->options) {
                 case '':
                 case '[]':
                 case '[""]':
                 $options = json_decode('{}');
                     break;
                default:
                 $options = json_decode($page->options);
                    break;
            } 

            $options->email_subject = $request->input('email_subject');
            $page->options = json_encode($options);
        }

        $page->save();

        // //updates
        $updatesCont = $updatesObj->getby_campaign($page->campaign_id);
        $contents_a = (isset($updatesCont->contents)?$updatesCont->contents:'');
        $ups = mb_update_contents($page->title.' page updated',$contents_a);
        $ups['campaign_id'] = $page->campaign_id;
        $updatesObj->mb_update($page->campaign_id,$ups['contents'],$ups['count']);

        return redirect()->back();

    }  

    public function delete($page){

        $page->status = 0;
        $page->save();
        $this->logger->create(['campaign_id' => $page->campaign_id,'users_id' => $this->auth->user()->id,'contents' => json_encode(['delete '.$page->title.' page.'])]);
        flash()->success('page successfully deleted!');
        return redirect()->back();

    }  


}