<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class generalController extends Controller
{

	public function options($type,$str){

		$arr = json_decode($str,true);
		return (isset($arr[$type])?$arr[$type]:false);

	}

	public function last_action($str){
		$arr = json_decode($str,true);
		return ($arr?$arr[count($arr) - 1]:'');
	}

	public function formatter($str,$type = 'br'){

		$result = '';
		if($str != ''):
			$arr = json_decode($str,true);
			switch ($type):
				case 'br':
					foreach ($arr as $v) {
						$result .= $v.'<br>';
					}
					break;
			endswitch;
		endif;
		return $result;
	}

	public function mb_convert_options($json){
		
		$opt = [];
		if($json != ''):
		    $opt_arr = json_decode($json);
		    if($opt_arr):
		        foreach ($opt_arr as $k => $v):
		           $opt[$k] = $v;
		        endforeach;
		    endif;
		endif;
		return $opt;

	}

} 