<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\sunflower\Board;
use App\Http\Models\sunflower\Page;
use App\Http\Models\sunflower\New_campaign as Campaign;
use App\Http\Models\sunflower\Logs;

class campaignController extends Controller
{

  public function __construct(Logs $logs,Guard $auth){
    $this->logger = $logs;
    $this->auth = $auth;
  }

  public function index(Campaign $campaign){

  	$campaigns = $campaign->all();
  	return view('backend.admin.campaign.index',compact('campaigns'));
  }

  public function add(){ 
    return view('backend.admin.campaign.add');
  }

  public function create(Request $request,Campaign $campaign){ 
        $this->validate($request, [
            'campaign_id' => 'required',
            'lob' => 'required|max:255|min:5'
        ]);

        $input = $request->only(['campaign_id','lob']);
        if($campaign->create($input)){
          $this->logger->create(['campaign_id' => $input['campaign_id'],'users_id' => $this->auth->user()->id,'contents' => json_encode(['created the campaign.'])]);
          flash()->success('new campaign created!');
          return redirect()->route('campaign.index');         
        }

        flash()->error('an error occured!');
        return redirect()->back()->withInput();      
  }

  public function edit(){
  	
  }

  public function update(){
  	
  }   

  public function delete(Campaign $campaign, $id){
  	
    $camp0 = $campaign->find($id);
    $camp = $camp0->toArray();
    if($campaign->destroy($id)){
          $cid = $camp['campaign_id'];      
          $this->logger->create(['campaign_id' => $cid,'users_id' => $this->auth->user()->id,'contents' => json_encode(['deleted the campaign.'])]);
          flash()->success('campaign deleted!');
          return redirect()->route('campaign.index');        
    }
    flash()->error('an error occured!');
    return redirect()->back();  

  }

     
}