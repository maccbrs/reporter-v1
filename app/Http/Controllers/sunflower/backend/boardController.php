<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\sunflower\Board;
use App\Http\Models\sunflower\Page;
use App\Http\Models\sunflower\Updates;
use App\Http\Models\sunflower\Logs;
use App\User;
use App\Http\Models\sunflower\Lists;
use App\Http\Models\sunflower\Vct;
use App\Http\Models\sunflower\New_campaign as Campaign;
use App\Http\Models\sunflower\ConCampaigns as CC;

class boardController extends Controller 
{

    public function __construct(Guard $auth,Updates $updates,Board $boardObj,Campaign $campaign,Page $pageObj,Request $request,CC $cc,Logs $logs,Lists $lists, Vct $vici){
        $this->boardObj = $boardObj;
        $this->campaign = $campaign;
        $this->pageObj = $pageObj;
        $this->request = $request;
        $this->auth = $auth;
        $this->updates = $updates;
        $this->name = $auth->user()->name;
        $this->concamp = $cc; 
        $this->logger = $logs; 
        $this->lists = $lists;
        $this->vici = $vici;

    } 

    public function index(){

        if($this->auth->user()->user_type == 'admin'):
            $boards = $this->boardObj->where('status',1)->paginate(10);
        else:
            $boards = $this->boardObj->where('status',1)->whereIn('campaign_id',sf_options($this->auth->user()->options))->paginate(20);
        endif;

        $vici = $this->vici->get();
        $connected = [];

        foreach ($vici as $v):
            if($v->templar_socket != ''):
               $connected = array_merge($connected,json_decode($v->templar_socket,true));
            endif;
        endforeach;

        return view('sunflower.index',compact('boards', 'connected')); 

    }


    public function add(){
        $campaigns = $this->campaign->all();
        return view('sunflower.boards.add',compact('campaigns')); 
    }


    public function edit($board){  
        //pre($board->toArray());
        $opt = mb_convert_options($board->options);
        $emails = [];

        if(!empty($board->assigned)){
            foreach ($board->assigned as $val) { 
               $emails[] = $val['email'];
            }          
        }
        $reports = (isset($opt['reports'])?$opt['reports']:array());
        $btns = (isset($opt['btns'])?$opt['btns']:array());
        return view('sunflower.boards.edit',compact('board','reports','emails','btns')); 

    }
 
    public function save(){ 

        $this->validate($this->request, [
            'campaign' => 'required',
            'lob' => 'required',
            'to' => 'required|max:255|min:5'
        ]);
        
        $post = $this->request->all();
        $post['campaign_id'] = uniqid(); 

        if($this->boardObj->create($post)): 
            $this->_assign_campaigns($this->auth->user()->id,$this->auth->user()->email,$post['campaign_id']);
            $ups = mb_update_contents($post['lob'].' added by '.$this->name.'.');
            $ups['campaign_id'] = $post['campaign_id'];
            $this->updates->create($ups);
            $this->logger->create(['campaign_id' => $post['campaign'],'users_id' => $this->auth->user()->id,'contents' => json_encode(['added the board.'])]);
            flash()->success('new board added!');
            return redirect()->route('sunflower.board.index'); 
        endif;      

    }  


    public function mail($board){

        $emails = [];

        return view('backend.admin.board.mail',compact('board','emails')); 

    }

    public function mail_save($board){

        $data = $this->request->all(); 
        
        if($board->options != ''){
            $options = json_decode($board->options);
        }else{
            $options = json_decode('{}');
        }

        $options->subject = (empty($data['subject'])?'Magellan Reporting':$data['subject']); //die;
        $board->options = json_encode($options); //die;
        $board->to = json_encode(validate_email($data['to'])); //die;
        $board->from = (filter_var(trim($data['from']), FILTER_VALIDATE_EMAIL)?$data['from']:'reports@magellan-solutions.com'); //die;
        $board->cc = json_encode(validate_email($data['cc'])); //die;
        $board->bcc = json_encode(validate_email($data['bcc'])); //die;
        $board->save();
        flash()->success('successfully updated!'); 
        return redirect()->back();
        
    }


    public function update($board,Request $r){ 
        //pre($board->toArray());
        $input = $r->only(['to','cc']);
        $board->to = $r->input('to'); 
        $board->cc = json_encode($r->input('cc'));
        $board->primary_page = $r->input('primary_page');

        $opt = mb_convert_options($board->options);
        $opt['reports'] = $r->input('opt');
        $opt['btns'] = $r->input('btn');
        $board->options = json_encode($opt);
        $board->save();
 
        return redirect()->back();
    }

    public function delete($board){

        if($this->auth->user()->user_type == 'admin'){
            $board->status = 0;
            $this->logger->create(['campaign_id' => $board->campaign_id,'users_id' => $this->auth->user()->id,'contents' => json_encode(['deleted the board.'])]);
            $board->save();
            flash()->success('board successfully deleted!');
        }else{
            flash()->error('you dont have permission to delete this board');
        }
        return redirect()->back();        
    }

     private function _assign_campaigns($uid,$email,$input_camps){
        
        $reports = json_encode($this->lists->reports());
        if(!empty($input_camps)){
            if(is_array($input_camps)){
                $this->concamp->where('users_id',$uid )->delete();
                foreach ($input_camps as $ick => $icv) {
                    if(is_array($icv)){
                        $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $ick,'reports' => json_encode($icv)]);
                    }else{
                        $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $icv,'reports' => $reports]);
                    }
                } 
            }else{

               if($input_camps != '') $this->concamp->create(['email' => $email,'users_id' => $uid,'campaign_id' => $input_camps,'reports' => $reports]);
           
            }           
        }
    }   

    private function _update_campaigns($uid,$email,$input_camps){

        $this->concamp->where('users_id',$uid )->update(['reports' => '']);
        if(!empty($input_camps)){
            foreach ($input_camps as $ick => $icv) {
                $this->concamp
                    ->where('campaign_id',$ick)
                    ->where('users_id',$uid)
                    ->update(['reports' => json_encode($icv)]);
            }            
        }

    }    


} 


