<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class vctController extends Controller
{

	public function index(){

        $vcto = new \App\Http\Models\sunflower\Vct;
        $connectors = $vcto->paginate(10);
		return view('sunflower.connectors.index',compact('connectors'));

	}


	public function create(){ 

        $vicio = new \App\Http\Models\sunflower\Vici;
        $viciClosero = new \App\Http\Models\sunflower\ViciCloser;
        $campaignsObj = new \App\Http\Models\sunflower\Board;
        $dmboards = $campaignsObj->select('campaign_id','lob')->active()->get();
        $outbounds = $vicio->select('campaign_id')->groupBy('campaign_id')->lists('campaign_id')->toArray(); 
        $inbounds = $viciClosero->select('campaign_id')->groupBy('campaign_id')->lists('campaign_id')->toArray();
        return view('sunflower.connectors.create',compact('outbounds','inbounds','dmboards'));

	}	


	public function store(Request $r){
        	
        $this->validate($r, [
            'viciCampaigns' => 'required',
            'dummyCampaigns' => 'required',
            'title' => 'required|max:255|min:5'
        ]);
        $vctObj = new \App\Http\Models\sunflower\Vct;
        $date = date('Y-m-d H:i:s');

        $vctObj->create([
            'inbound' => $r->input('bound'),
            'name' => $r->input('title'),
            'vici_plug' => json_encode($r->input('viciCampaigns')),
            'templar_socket' => json_encode($r->input('dummyCampaigns')),
            'created_at' => $date,
            'updated_at' => $date
        ]);

        return redirect()->route('sunflower.vct.index');

	} 

	public function destroy($id){

        $vctObj = new \App\Http\Models\sunflower\Vct;
		$vctObj->destroy($id);
    	flash()->success('successfully deleted!');
    	return redirect()->back(); 		
	}

 

} 