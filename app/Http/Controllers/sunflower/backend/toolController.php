<?php namespace App\Http\Controllers\sunflower\backend;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Helpers\Tables; 
use App\Model\Blockages;
use App\Model\Logs;
use App\Model\CampaignData;
use DB;

class toolController extends Controller
{



	public function index(){

		$dispo = DB::connection('192.168.200.132') 
			->table('vicidial_campaign_statuses')
			->select('status')
			->groupBy('status') 
			->get();
		return view('sunflower.tool.changecrc',compact('dispo'));
	}

	public function blockage(Request $r){

		$email_tbl = "";
		$campaigns = "";

		$startdate = date("Y-m-d H:i:s", strtotime($r->calldate)); 

		$endTime = date("Y-m-d H:i:s", strtotime($startdate) + (60));

		$conn = 'vicidial';
        
       // $conn2 = 'vicidial2';
                
    	$table = 'vicidial_log';

    	$table2 = 'vicidial_closer_log';
	  
        $vici = DB::connection($conn)->table($table);

        $vici2 = DB::connection($conn)->table($table2);

     //   $viciConn = DB::connection($conn2)->table($table);

      //  $viciConn2 = DB::connection($conn2)->table($table2);

        if(!empty($r->phonenumber)){

	        $logs = (!empty($r->disposition)? $vici->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$vici->where('phone_number', $r->phonenumber));
	        $logs = $logs->get(); 

	        // $logs2 = (!empty($r->disposition)? $viciConn->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$viciConn->where('phone_number', $r->phonenumber));
	        // $logs2 = $logs2->get(); 

	        $logs_closer = (!empty($r->disposition)? $vici2->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$vici2->where('phone_number', $r->phonenumber));
	        $logs_closer = $logs_closer->get();

	        // $logs_closer2 = (!empty($r->disposition)? $viciConn2->where('phone_number', $r->phonenumber)->where('status', $r->disposition):$viciConn2->where('phone_number', $r->phonenumber));
	        // $logs_closer2 = $logs_closer2->get();

	       	
	    }else{

	        $logs = (!empty($r->disposition)? $vici->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$vici->whereBetween('call_date', [$startdate, $endTime]));
	        $logs = $logs->get(); 

	        // $logs2 = (!empty($r->disposition)? $viciConn->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$viciConn->whereBetween('call_date', [$startdate, $endTime]));
	        // $logs2 = $logs2->get();

	        $logs_closer = (!empty($r->disposition)? $vici2->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$vici2->whereBetween('call_date', [$startdate, $endTime]));
	        $logs_closer = $logs_closer->get();

	        // $logs_closer2 = (!empty($r->disposition)? $viciConn2->whereBetween('call_date', [$startdate, $endTime])->where('status', $r->disposition):$viciConn2->whereBetween('call_date', [$startdate, $endTime]));
	        // $logs_closer2 = $logs_closer2->get();

	    }

	        if(empty($logs) && empty($logs2) && empty($logs_closer) && empty($logs_closer2)){

	        	flash()->error('No Calls Found');

	    	}else{

	    		flash()->success('Call Log Successfully Found');

	    	}

	    	$repository = $r->repository;

	    	if(!empty($r->phonenumber)){

	    		$details =  $r->phonenumber;
	    		$label = "Phone Number : ";

	    	}else{

	    		$details =  $startdate . " - " . $endTime; 
	    		$label = "Date Range : ";

	    	}

			$email_tbl = view('email._crc',compact('logs','logs_closer','logs2','logs_closer2', 'conn', 'repository', 'details', 'phonenumber', 'label','conn2'));

			$campaigns = $this->blk->get()->toArray(); 

		$phonenumber = $r->phonenumber; 
		$calldate = $r->call_date;
		$server = $r->repository;

		$dispo = DB::connection('vicidial')
			->table('vicidial_campaign_statuses')
			->select('status')
			->groupBy('status')
			->get();

		return view('backend.admin.tool.index',compact('email_tbl','campaigns','server','calldate', 'phonenumber','dispo'));

	}

	public function update(Request $request ){

		$table = $request['table'];

	    $conn = $request['server'];

		$vici = DB::connection($conn)->table($table);

		$vici->where('uniqueid',$request['uniqueid'])->update(['status' => $request['disposition']]);

		flash()->success('Disposition has been succesfully updated!');

        return redirect()->route('tool.index');

	}
}