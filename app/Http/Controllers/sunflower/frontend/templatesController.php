<?php namespace App\Http\Controllers\sunflower\frontend;
use Illuminate\Http\Request;
use App\Http\Models\sunflower\Board;
use App\Http\Models\sunflower\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\sunflower\Board2; 

class templatesController extends Controller 
{

    public function __construct(Request $r){

    	$base = route('home');
    	$this->viewer = $base.'/live';
    	$this->link = 'live';
        if ($r->is('preview/*')){ $this->viewer = $base.'/preview'; $this->link = 'preview'; }
        if ($r->is('train/*')){ $this->viewer = $base.'/train'; $this->link = 'train'; }
        
    }

	public function index($board,$page){


		$url = $this->viewer;
		$link = $this->link;

		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$bookwormBoards = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b", "57101c7d2183d", "57101d50b098e", "57101d9db2f39", "57101cfd6a2e3", "5714fdb2667d1" );

		$view = 'sunflower.templates.'.$page->template.'.index';
		//echo $view;die;
		return view($view,compact('board','page','pages','url','btns','link','bookwormBoards'));
	}

	public function board($board){

		$gn = new \App\Http\Controllers\sunflower\backend\generalController;
		$url = $this->viewer;
		$link = $this->link;
		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = $gn->mb_convert_options($board['options']); 
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$bookwormBoards = array("570ee783ddf6f", "57101bd74a14d", "57101c17c3a0b", "57101c7d2183d", "57101d50b098e", "57101d9db2f39", "57101cfd6a2e3", "5714fdb2667d1", "571e4d7153706", "571e4e0b94b44" );

		if(empty($pages)){
			$view = 'templates.no-page';
		}else{
			$page = $pages[0];
			if($board->primary_page != ''){
				foreach ($board['pages'] as  $v) {
					if($board->primary_page == $v->id){
						$page = $v;
					}
				}
			}
			$view = 'sunflower.templates.'.$page->template.'.index';
			
		}

		return view($view,compact('board','page','pages','url','btns','link', 'bookwormBoards'));

	}

    
} 