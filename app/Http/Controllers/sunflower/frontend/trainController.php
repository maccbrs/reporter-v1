<?php namespace App\Http\Controllers\sunflower\frontend;
use Illuminate\Http\Request;
use App\Http\Models\sunflower\Board;
use App\Http\Models\sunflower\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\sunflower\Board2; 

class trainController extends Controller
{

   public function __construct(Request $r){

    	$base = route('home');
    	$this->viewer = $base.'/live';
    	$this->link = 'live';
        if ($r->is('preview/*')){ $this->viewer = $base.'/preview'; $this->link = 'preview'; }
        if ($r->is('train/*')){ $this->viewer = $base.'/train'; $this->link = 'train'; }
        
    }


	public function index($board,$page){

		$url = $this->viewer;
		$link = $this->link;

		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		$view = 'templates.'.$page->template.'.index';
		//echo $view;die;
		return view($view,compact('board','page','pages','url','btns','link'));
	}

	public function board($board){

		$url = $this->viewer;
		$link = $this->link;

		$pages = array();
		foreach ($board['pages'] as  $v) {
			$pages[] = $v;
		}

		$opt = mb_convert_options($board['options']);
		$btns = (isset($opt['btns'])?$opt['btns']:[]);

		if(empty($pages)){
			$view = 'templates.no-page';
		}else{
			$page = $pages[0];
			if($board->primary_page != ''){
				foreach ($board['pages'] as  $v) {
					if($board->primary_page == $v->id){
						$page = $v;
					}
				}
			}
			$view = 'templates.'.$page->template.'.index';
			//echo $view;die;
		}

		return view($view,compact('board','page','pages','url','btns','link'));

	}


    public function train(Board2 $boardObj)
    {
        $boards = $boardObj->all();
        return view('frontend.train.train',compact('boards'));
    } 

}