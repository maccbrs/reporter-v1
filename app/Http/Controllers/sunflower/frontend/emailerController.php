<?php namespace App\Http\Controllers\sunflower\frontend;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\AuthController;
use Mail;

class emailerController extends Controller
{
    
   public function __construct(Request $r){
        $this->request = $r;        
    }    


    public function send($emailer)
    { 
        $emails = $emailer;
        return view('frontend.email.send',compact('emails'));
    }

    public function sent($emailer){ 
        
    	$address['to'] = [];
        $address['from'] = []; 
        $address['cc'] = [];
        $address['subject'] = [];

    	$to_arr = json_decode($emailer->to);
    	$from_arr = json_decode($emailer->from);
    	$cc_arr = json_decode($emailer->cc);
        $bcc_arr = json_decode($emailer->bcc);

    	if(!empty($to_arr)) foreach ($to_arr as $v) { if($v != '')$address['to'][$v] = $v; };
    	if(!empty($from_arr)) foreach ($from_arr as $v2) { if($v2 != '')$address['from'][$v2] = $v2; }; 
    	if(!empty($cc_arr)) foreach ($cc_arr as $v3) { if($v3 != '')$address['cc'][$v3] = $v3; };
        if(!empty($bcc_arr)) foreach ($bcc_arr as $v4) { if($v4 != '')$address['bcc'][$v4] = $v4; };
    	
    	$msg = 'message successfully sent!';

       $address['subject'] =  $emailer->subject;
        $address['content'] = $this->request->input('content');

        Mail::send('email.emailer',$address, function ($m) use ($address){
            if(!empty($address['to']) && !empty($address['from'])){
                $m->from($address['from']);
                $m->to($address['to'])->subject($address['subject']);
            }
            if(isset($address['cc'])){ $m->cc($address['cc']);}
            if(isset($address['bcc'])){ $m->bcc($address['bcc']);}
            if(isset($address['subject'])){ $m->subject($address['subject']);}
        });

        flash()->success($msg);
        return redirect()->back();
    }

}