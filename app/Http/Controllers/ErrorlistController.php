<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Errorlist;
use Mail;

class ErrorlistController extends Controller
{

	public function index(){

        return view('404');

    }

    public function store(Request $request){

     	$error = new Errorlist;
     	$error->desc = $request->desc;
     	$error->user_id = $request->user_id;
     	$error->website = $request->website;
     	$error->how_often = $request->how_often;
        $error->type = $request->type;
     	$saved = $error->save();

        //  $user = "kit";

        // Mail::send('emails.report', ['user' => "kit@mailinator.com"], function ($m) use ($user) {
        //     $m->from('kit@mailinator.com', 'Your Application');

        //     $m->to("kit@mailinator.com", "kit")->subject('Your Reminder!');
        // });

     	if($saved){

			flash()->success('Error incident has been reported to NOC Developers.');

		}else{

			flash()->error('Sorry, We cannot deliver your report. Please let NOC be informed about this tru email or ticket.');

		}


     	return redirect('/');

    }
    
}
