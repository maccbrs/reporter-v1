<?php

namespace App\Http\Controllers\rosebud;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Helpers\Getter;
use App\Http\Models\rosebud\Requirement;
use App\Http\Controllers\Controller;
use App\Http\Models\rosebud\Campaign;
use App\Http\Models\rosebud\RtC;
use App\Http\Models\rosebud\Schedule;
use App\User;
use Auth;


class calendarController extends Controller
{ 

	public function launch(Campaign $campaign){
//pre($campaign);
		$arr = [];
		$arr2 = [];

		$camps = $campaign
					->leftJoin('client_status','client_status.campaign_id','=','campaign.id')
					->select('campaign.*','client_status.status as client_status')
					->get()->toArray(); 
		foreach ($camps as $v) {
			$color = ($v['client_status'] == 'closed'?'#0F9D58':'gray');
			$arr2[] = [
			'title' => $v['project_name'],
			'start' => date_format(date_create($v['launch_date']),"Y-m-d"),
			'color'  => $color,
			'url' => 'rosebud/campaign/show/'.$v['id']
			];				
		}	

		$arr = json_encode($arr2);

		return view('rosebud.calendar.launch',compact('arr'));		

	}


	public function request(Guard $auth,RtC $rtc,Requirement $req,Getter $getter,Campaign $campaign){

		$statuses = $getter->statuses();
		$requests = $rtc->leftJoin('requirements', 'requirement_to_campaign.requirement_id','=','requirements.id')
				->leftJoin('client_status', 'client_status.campaign_id','=','requirement_to_campaign.campaign_id')
				->leftJoin('campaign', 'campaign.id','=','requirement_to_campaign.campaign_id')
				->select('requirement_to_campaign.*','requirements.name','requirements.category','requirements.sla','client_status.status as client_status','client_status.due_date','campaign.project_name')
				->where('requirements.department_id',$auth->user()->department_id)
				->where('requirement_to_campaign.status','!=',3)
				->get()->toArray();

		$arr = [];
		$arr2 = [];

		foreach ($requests as $v) {

			$arr2[] = [
			'title' => $v['name'],
			'start' => addDayswithdate($v['due_date'],$v['sla']), 
			'color'  => '#0F9D58',
			'url' => '/rosebud/request/show/'.$v['id']
			];				
		}	

		$arr = json_encode($arr2);
		return view('rosebud.calendar.request',compact('arr'));

	}

	public function my(Schedule $sched){
		$schedules = $sched->where('users_id',Auth::user()->id)->get()->toArray();

		$arr = [];
		$arr2 = [];

		foreach ($schedules as $v) {

			$arr2[] = [
			'title' => $v['title'],
			'start' => date_format(date_create($v['from']),"Y-m-d"),
			'end' => date_format(date_create(addDayswithdate($v['to'],1)),"Y-m-d"),
			'color'  => $v['color'],
			'url' => '/rosebud/schedule/show/'.$v['id']
			];				
		}	

		$arr = json_encode($arr2);
		return view('rosebud.calendar.request',compact('arr'));
	}

}