<?phpnamespace App\Http\Controllers\rosebud;
use App\Http\Controllers\rosebud\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\User;

class userController extends Controller
{

	 public function index(User $user){

	 	$users = $user->paginate(50);
	 	return view('rosebud.user.index',compact('users'));

	 }

	 public function add(){
	 	return view('rosebud.user.add');
	 }

	 public function create(Request $r,User $user){

	 	$data = $r->all();
	 	$data['password'] = bcrypt($r->input('password'));
	 	$option['color'] = $r->input('color');
	 	$option['access'] = array('rosebud');
	 	$data['options'] = json_encode($option);
	 	$user->create($data);

		$logger = new Logger;
		$logger->create($r,$data);

	 	flash()->success('user successfully added!');
	 	return redirect()->route('rosebud.user.index');

	 }

	 public function edit($user){
	 	return view('rosebud.user.edit',compact('user'));
	 }

	 public function update(Request $r, $user){

	 	$data = $r->all();
	 	$options = ($user->options != ''?json_decode($user->options):json_decode("{}"));
	 	$options->color = $r->input('color');
	 	$data['options'] = json_encode($options);
	 	$user->update($data);

		$logger = new Logger;
		$logger->create($r,$data);

	 	flash()->success('record successfully updated');
	 	return redirect()->route('rosebud.user.index');
	 	

	 }


}