<?php namespace App\Http\Controllers\rosebud;

use Illuminate\Http\Request;
use App\Http\Controllers\rosebud\loggerController as Logger;
use App\Http\Requests;
use App\Http\Models\rosebud\Requirement;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Models\rosebud\Campaign;
use App\Http\Helpers\Getter;
use App\Http\Models\rosebud\RtC;
use App\Http\Helpers\Transformer;


class requestController extends Controller
{

	public function index(Guard $auth,RtC $rtc,Requirement $req,Getter $getter){

		$statuses = $getter->statuses();
		$requests = $rtc->leftJoin('requirements', 'requirement_to_campaign.requirement_id','=','requirements.id')
				->leftJoin('client_status', 'client_status.campaign_id','=','requirement_to_campaign.campaign_id')
				->leftJoin('campaign', 'campaign.id','=','requirement_to_campaign.campaign_id')
				->select('requirement_to_campaign.*','requirements.name','requirements.category','requirements.sla','client_status.status as client_status','client_status.due_date','campaign.project_name')
				->where('requirements.department_id',$auth->user()->department_id)
				->where('requirement_to_campaign.status','!=',3)
				->get();
				//pre($requests->toArray());
		return view('rosebud.request.index',compact('requests','statuses'));
	}


	public function calendar(Guard $auth,RtC $rtc,Requirement $req,Getter $getter){

		$arr2 = [];
		$statuses = $getter->statuses();
		$requests = $rtc->leftJoin('requirements', 'requirement_to_campaign.requirement_id','=','requirements.id')
				->leftJoin('client_status', 'client_status.campaign_id','=','requirement_to_campaign.campaign_id')
				->leftJoin('campaign', 'campaign.id','=','requirement_to_campaign.campaign_id')
				->select('requirement_to_campaign.*','requirements.name','requirements.category','requirements.sla','client_status.status as client_status','client_status.due_date','campaign.project_name')
				->where('requirements.department_id',$auth->user()->department_id)
				->get();
			
		if($requests->toArray()):	
			foreach ($requests as $k => $v):
				$arr2[] = [
					'title' => $v->name,
					'start' => addDayswithdate($v->due_date,$v->sla),
					'color' => '#0F9D58',
					'url' => 'test'
				];
			endforeach;
		endif;

		$arr = json_encode($arr2);
		return view('rosebud.request.calendar',compact('arr','statuses'));

	}

	public function show($rtc,Transformer $trans,Getter $getter){ 

		$statuses = $getter->statuses();
		$message = ($rtc->message?$trans->get_message($rtc->message->content):[]);
		return view('rosebud.request.show',compact('rtc','message','statuses'));

	}

    public function status($rtc){// pre($rtc->toArray());

    	return view('rosebud.request.status-update',compact('rtc'));

    }

    public function status_update($rtc,Request $r){ //pre($r->only(['status']));

    	$old = $rtc;
    	$rtc->update($r->only(['status']));

		$logger = new Logger;
		$logger->update($r,$old,$r->only(['status']));

    	flash()->success('status successfully updated!');
    	return redirect()->route('rosebud.request.show',$rtc->id);

    }

}