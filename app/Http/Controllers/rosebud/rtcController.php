<?php namespace App\Http\Controllers\rosebud;
use App\Http\Models\rosebud\Requirement;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\rosebud\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\rosebud\storeChecklist;
use App\Http\Models\rosebud\Campaign;
use App\Http\Models\rosebud\RtC;
use App\Http\Helpers\rosebud\Getter;
use App\User;

class rtcController extends Controller
{

	public function add(Requirement $requirement){ 

		$requirements = $requirement->paginate(50);
		return view('rosebud.rtc.add',compact('requirements'));
	}

	//---------------------------------->
	public function create($campaign,Requirement $req){  

		$requirements = $req->get();
		$id = $campaign->id;
		return view('rosebud.rtc.create',compact('id','requirements'));

	}

	public function show($campaign, Getter $getter){
		//pre($campaign->toArray());
		$requirements = $getter->requirements();
		$checklist = $campaign->checklist->toArray();
		$statuses = $getter->statuses();
		return view('rosebud.rtc.show',compact('checklist','campaign','requirements','statuses'));

	}	

	public function store($campaign,Guard $auth,storeChecklist $r,RtC $rtc){

		$input = $r->all();
		$input['users_id'] = $auth->user()->id;
		$input['campaign_id'] = $campaign->id;
		$rtc->create($input);

		$logger = new Logger;
		$logger->create($r,$input);

		flash()->success('Successfully saved!');
		return redirect()->route('rosebud.rtc.show',$campaign->id);
	}

	public function edit($data,Requirement $req){
		
		$requirements = $req->get(); //pre($data->toArray()); 
		//pre($requirements->toArray());
		return view('rosebud.rtc.edit',compact('data','requirements'));

	}

	public function update($data,storeChecklist $r){

		$old = $data;
		$input = $r->only(['to','requirement_id','remarks']);
		$data->update($input);

		$logger = new Logger;
		$logger->update($r,$old,$input);

		flash()->success('successfully updated!');
		return redirect()->route('rosebud.rtc.show',$data->campaign_id);
		
	}

}    