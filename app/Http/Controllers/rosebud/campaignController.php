<?php

namespace App\Http\Controllers\rosebud;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\rosebud\loggerController as Logger;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\rosebud\Campaign;
use App\Http\Helpers\rosebud\Getter;
use App\Http\Models\rosebud\RtC;
use App\Http\Models\rosebud\ClientStatus; 
use PDF;

class campaignController extends Controller
{ 

	public function index(Campaign $campaign){
		$campaigns = $campaign->with('client_status')->paginate(50);  
		return view('rosebud.campaign.index',compact('campaigns')); 
	}

	public function add(){
		return view('rosebud.campaign.add');
	}

	public function create(Request $r,Campaign $campaign,Getter $getter,Guard $auth,RtC $rtc,ClientStatus $CS){ 

		$input = $r->all(); //pre($input); 
		$input['user_id'] = $auth->user()->id;
		$a = $campaign->create($input);
		
		if($a){

			$logger = new Logger; 
			$logger->create($r,['campaign_id' => $a->id]);

			$CS->insert(['status' => $input['client_status'], 'campaign_id' => $a->id,'created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s'),'due_date' => date('Y-m-d')]);
			$info = [
			'users_id' => $auth->user()->id,
			'campaign_id' => $a->id,
			'type' => 'regular'
			];

			$b = $getter->chklist_field($input,$info);
			$c = [];
			if($input['fst_training_date'] != '') $c[] = ['requirement_id' => 16,'campaign_id' => $info['campaign_id'],'users_id' => $info['users_id'],'type' =>  $info['type'],'to' => $input['fst_training_date']];
			if($input['product_training_date'] != '') $c[] = ['requirement_id' => 17,'campaign_id' => $info['campaign_id'],'users_id' => $info['users_id'],'type' =>  $info['type'],'to' => $input['product_training_date']];
			if($b) $rtc->insert($b);
			if($c) $rtc->insert($c);

		}

		flash()->success('campaign successfully added!');
		return redirect()->route('rosebud.campaign.show',$a->id);

	}

	public function show($campaign, Getter $getter){

		$cats = $getter->categories();

		$categories = $this->_campaignData($cats, $campaign , $getter);

		$requirements = $getter->requirements();
		$statuses = $getter->statuses();

		return view('rosebud.campaign.show',compact('campaign','requirements','statuses','categories'));

	}  

	public function export($campaign, $indicator, Getter $getter){

		$cats = $getter->categories();

		$categories = $this->_campaignData($cats, $campaign , $getter);

		$requirements = $getter->requirements();
		$statuses = $getter->statuses();
	
		if($indicator == 1){ 

			$html = view('rosebud.campaign.pdf_checklist', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();		

		}

		if($indicator == 2){ 
			 
			$html = view('rosebud.campaign.pdf_general_account', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();

		}

		if($indicator == 3){ 
			
			$html = view('rosebud.campaign.pdf_contact_information', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();

		}

		if($indicator == 4){ 

			
			$html = view('rosebud.campaign.pdf_campaign_files', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();

		}

		if($indicator == 5){ 
			
			$html = view('rosebud.campaign.pdf_operational_information', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();

		}

		if($indicator == 6){ 
			
			$html = view('rosebud.campaign.pdf_technical_information', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();

		}

		if($indicator == 7){ 
			
			$html = view('rosebud.campaign.pdf_recruitment_information', compact('campaign','requirements','statuses','categories'))->render();
			$today = date("m/d/y");
			$pdf_title =  $today.'.pdf';
			return PDF::load($html)->filename($pdf_title)->download();

		}

	}

	public function _campaignData($cats, $campaign , $getter ){
		//pre($campaign);
		foreach ($cats as $k => $v){
			$res = $getter->getByCategory($campaign->checklist,$v);
			$categories[$v] = ($res?$res:[]);
			$res = false;
		}

		return $categories;

	}

	public function edit($campaign){
		return view('rosebud.campaign.edit',compact('campaign'));
	}

	public function update($campaign,Request $r){

		//for logs
		$old = $campaign;

		$input = $r->except(['_method','_token']);
		$campaign->update($input);

		$logger = new Logger;
		$logger->update($r,$old,$input);

		flash()->success('sucessfully updated');
		return redirect()->route('rosebud.campaign.show',$campaign->id);
		
	}

	public function edit_client_status($campaign){
		return view('rosebud.campaign.client_status.edit',compact('campaign'));
	}

	public function update_client_status($status,Request $r){

		$input = $r->only(['status']);
		$old = $status;
		$logger = new Logger;

		$input['due_date'] = ($r->input('due_date') != ''?$r->input('due_date'):date('Y-m-d'));

		if($status){
			$status->update($input);
			$logger->update($r,$old,$input);			
			flash()->success('successfully updated!');
		}else{
			$statusObj = new ClientStatus;
			$input['campaign_id'] = $r->input('campaign_id');
			$status = $statusObj->create($input);
			$logger->create($r,$input);
			flash()->success('successfully created!');
		}

		return redirect()->route('rosebud.campaign.show',$status->campaign_id);

	}

} 