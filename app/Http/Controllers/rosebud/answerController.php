<?php

namespace App\Http\Controllers\rosebud;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\rosebud\loggerController as Logger;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\Getter;
use App\Http\Requests\rosebud\storeAnswerReq;
use App\Http\Models\rosebud\Answer;

class answerController extends Controller
{ 

	public function index(){
		echo 1;
	}

	public function create($rtc){

		//pre($rtc->toArray());
		return view('rosebud.answer.create',compact('rtc'));
	}

	public function store(storeAnswerReq $r,Answer $ans){

		$input = $r->only(['rtc_id','content','tag']);
		$ans->create($input);

		$logger = new Logger;
		$logger->create($r,$input);

		flash()->success('successfully posted!');
		return redirect()->route('rosebud.request.show',$input['rtc_id']);
		
	}


}
