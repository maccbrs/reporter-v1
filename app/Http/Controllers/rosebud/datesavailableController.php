<?php

namespace App\Http\Controllers\rosebud;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\rosebud\loggerController as Logger;
use App\Http\Models\rosebud\Department;
use App\Http\Models\rosebud\Dates;
use DB;

class datesavailableController extends Controller
{

	public function __construct(Department $depart){
		$this->depart = $depart;
	}


	public function generateDates(Dates $date){

		die;
		$a = $this->depart->get(); 
		$x = yearArray('2016');

		$container = [];
		foreach ($a as $b){
			foreach ($x as $xk) {
				$container[] = [
					'department_id' => $b->id,
					'status' => 1,
					'date' => $xk
				];
			}

			$date->insert($container);
			$container = [];
		}
		
	}

	public function index(Dates $dates){

		$c = [];
		$x = $dates->where('department_id',1)->get()->toArray();
		foreach ($x as $a) {
			$b = explode('-', $a['date']);
			$m = sprintf("%01d",$b[1]);
			$d = sprintf("%01d",$b[2]);
			$c[$b[0]][$m][$d]['date'] = $a['date'];
			$c[$b[0]][$m][$d]['status'] = $a['status'];
			$c[$b[0]][$m][$d]['id'] = $a['id'];
		}
		//pre($c);
		$dates = $c;
		$year = 2016;
		return view('rosebud.datesavailable.index',compact('dates','year'));
	}



	public function update(Request $request, Dates $date){

		DB::table('dates')->update(['status'=>0]);
		$a = $date->whereIn('id',$request->input('status'));
		$a->update(['status'=>1]);
		flash()->success('dates updated');
		return redirect()->back();
		
	}

}