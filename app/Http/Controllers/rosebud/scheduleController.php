<?php namespace App\Http\Controllers\rosebud;
use App\Http\Controllers\rosebud\loggerController as Logger;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\rosebud\scheduleRequest;
use App\Http\Requests;
use App\Http\Models\rosebud\Dates;
use App\Http\Models\rosebud\Task;
use App\Http\Models\rosebud\Schedule;
use Auth;

class scheduleController extends Controller
{


	public function create(){
		return view('rosebud.schedule.add'); 
	}


	public function store(scheduleRequest $r,Schedule $sched){

		$input = $r->only(['from','to','title','description','color','type']);
		$input['users_id'] = Auth::user()->id;
		$sched->create($input);

		$logger = new Logger;
		$logger->create($r,$input);

		flash()->success('successfully saved!');
		return redirect()->route('rosebud.calendar.my');

	}

	public function show($sched){
		return view('rosebud.schedule.show',compact('sched'));
	}

	public function edit($sched){
		//pre($sched->toArray());
		return view('rosebud.schedule.edit',compact('sched'));
	}
	
	public function update(scheduleRequest $r,$sched){

		$old = $sched;
		$input = $r->only(['from','to','title','description','color','type']);

		$sched->update($input);

		$logger = new Logger;
		$logger->create($r,$old,$input);

		flash()->success('updated saved!');
		return redirect()->route('rosebud.schedule.show',$sched->id);

	}

} 