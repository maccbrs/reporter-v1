<?php namespace App\Http\Controllers\rosebud;
use App\Http\Controllers\rosebud\loggerController as Logger;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\rosebud\Requirement;
use App\Http\Helpers\Getter;
use App\Http\Requests\rosebud\requirementRqst;

class requirementController extends Controller
{

	public function index(Requirement $requirement,Getter $list){

		$departments = $list->department('names');
		$requirements = $requirement->paginate(50);

		return view('rosebud.requirement.index',compact('requirements','departments'));
	}

	public function add(){
		return view('rosebud.requirement.add');
	}

	public function create(requirementRqst $r,Requirement $requirement){

		$input = $r->all();
		$requirement->create($input); 
		flash()->success('Success!');

		$logger = new Logger;
		$logger->create($r,$input);

		return redirect()->route('rosebud.requirement.index');

	} 

	public function edit($requirement){
		return view('rosebud.requirement.edit',compact('requirement'));
	} 

	public function update($req,requirementRqst $r){

		$old = $req;
		$input = $r->only(['name','sla','department_id']);

		$req->update($input);

		$logger = new Logger;
		$logger->update($r,$old,$input);

		flash()->success('successfully updated!');
		return redirect()->route('rosebud.requirement.index');

	}
}