<?php
namespace App\Http\Controllers\rosebud;

use App\Http\Models\rosebud\Requirement;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\rosebud\loggerController as Logger;
use App\Http\Models\rosebud\Task;
use App\Http\Models\rosebud\Campaign;
use App\Http\Models\rosebud\Cluster;
use App\Http\Models\rosebud\User2Cluster;
use App\Http\Requests\rosebud\clusterRequest;
use Illuminate\Http\Request;
use App\User;
use Auth;

class clusterController extends Controller
{


	public function index(Cluster $cl){ 

		$clusters = $cl->get();
		return view('rosebud.cluster.index',compact('clusters'));
	}

	public function create(){ 
		return view('rosebud.cluster.create');
	}

	public function store(clusterRequest $r,Cluster $cluster){

		$x = $cluster->create($r->only(['name']));

		$logger = new Logger;
		$logger->create($r,$r->only(['name']));

		flash()->success('successfully created!');
		return redirect()->route('rosebud.cluster.show',$x->id);

	}

	public function show($cluster){ //pre($cluster->toArray());
		return view('rosebud.cluster.show',compact('cluster'));
	}

	public function user_create($cluster,User $user){

		if($cluster->utc){
				$userIds = [];
			foreach ($cluster->utc as $k => $v) {
				if($v->user){
					$userIds[] =  $v->user->id;
				}
				
			}
			$users = $user->whereNotIn('id',$userIds)->get();
		}else{
			$users = $user->get();
		}
		return view('rosebud.cluster.user.create',compact('cluster','users'));

	}

	public function user_store($cluster,User2Cluster $u2c,Request $r){

	    $this->validate($r, [
	        'users_id' => 'required'
	    ]);

		$data = [
		'users_id' => $r->input('users_id'),
		'clusters_id' => $cluster->id
		];
  
		$u2c->create($data);
		$logger = new Logger;
		$logger->create($r,$data);
				
		flash()->success('successfully added!');
		return redirect()->route('rosebud.cluster.show',$cluster->id);

	}

	public function calendar($cluster){ //pre($cluster->toArray());

		$schedules = [];
		$users = []; 
		foreach ($cluster->utc as $utc){ 
			if($utc->user && $utc->user->schedules && $utc->user->options != ''){
				$colorOpt = json_decode($utc->user->options);
				$users[] = [
				'name' => $utc->user->name,
				'color' => $colorOpt->color
				];
				foreach ($utc->user->schedules as $sched) {
					if($sched->type != 'regular'){
						$schedules[] = [
							'title' => $sched->title,
							'start' => date_format(date_create($sched->from),"Y-m-d"),
							'end' => date_format(date_create($sched->to),"Y-m-d"),
							'color'  => $colorOpt->color
						];
					}	
				}
			}
		}
		$arr = json_encode($schedules);
		return view('rosebud.cluster.calendar',compact('cluster','arr','users'));

	}

}