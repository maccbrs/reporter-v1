<?php

namespace App\Http\Controllers\rosebud;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\rosebud\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\Getter;
use App\Http\Helpers\Transformer;
use App\Http\Requests\rosebud\storeMsgsReq;
use App\Http\Models\rosebud\Message;


class messageController extends Controller
{

	public function create($rtc){
		return view('rosebud.message.create',compact('rtc')); 
	}

	public function store($rtc,storeMsgsReq $r,Transformer $trans,Guard $auth,Message $message){

		$input = $r->only(['content','rtc_id']);

		$existing_msg = ($rtc->message?$rtc->message->toArray():[]);

		$msg = $trans->put_message($existing_msg,$auth->user()->name,$input);

		if($existing_msg){
			$message->find($existing_msg['id'])->update(['content' => $msg]);
			flash()->success('message successfully updated!');
		}else{
			$data = [
				'rtc_id' => $rtc->id,
				'content' => $msg,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
			];
			$message->create($data);

			$logger = new Logger;
			$logger->create($r,$input);

			flash()->success('message successfully posted!');
		}
		
		return redirect()->route('rosebud.request.show',$rtc->id);
		
	}

}