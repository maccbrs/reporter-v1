<?php namespace App\Http\Controllers\dahlia;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\dahlia\loggerController as Logger;
use App\Http\Models\dahlia\RosebudLogs as Logs;

class rosebudController extends Controller
{ 

	public function index(){

		$logsObj = new Logs;

		$routenames = $logsObj->select('routename')->groupBy('routename')->get();

        foreach ($routenames as $route):
            $rn = explode('.', $route->routename);
            if(count($rn) > 1):
	            $arrRoutes[$route->routename]['app'] = $rn[0];
	        	if(count($rn) > 2):
	        		$arrRoutes[$route->routename]['class'] = $rn[1];
	        		if(count($rn) > 3):
	        			$arrRoutes[$route->routename]['method'] = $rn[2].' '.$rn[3];
	        		else:
	        		 	$arrRoutes[$route->routename]['method'] = $rn[2];
	        		endif;
	        	else:
	        		$arrRoutes[$route->routename]['class'] = $rn[0];
	        		$arrRoutes[$route->routename]['method'] = $rn[1];
	            endif;               
            endif;
        endforeach; 

        $finalRoutes = [];
        foreach ($arrRoutes as $k => $v):
        	$finalRoutes[$v['app']][$v['class']]['method'][$k] = $v['method'];
        endforeach;

		return view('dahlia.rosebud.index',compact('finalRoutes'));

	}


	public function viewlogs($routename){

		$logsObj = new Logs;
		$logs = $logsObj->where('routename',$routename)->orderBy('created_at','desc')->paginate(25);
		return view('dahlia.rosebud.viewlogs',compact('logs'));

	}

	public function show($id){
 
 		$logsObj = new Logs;
		$log = $logsObj->where('id',$id)->first();
		$content = json_decode($log['content'],true);
		$type = (isset($log->type)?$log->type:'browse');
		return view('dahlia.rosebud.'.$type,compact('log','content'));
		
	}

}