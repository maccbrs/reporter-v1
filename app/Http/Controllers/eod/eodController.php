<?php namespace App\Http\Controllers\eod;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;


class eodController extends Controller
{

	public function test(){
		$this->eodStep5();
	}

	public function eodStep4(){

	        $EodEmails = new \App\Http\Models\eod\EodEmails;
	        $Time = new \App\Http\Helpers\eod\Time;
	        $eod = $EodEmails->where('visited',0)->first();
	        //$eod = $EodEmails->where('id',303)->first();
	        //pre($eod->toArray());
	        if($eod):

	        	$eod->visited = 1;
		        $eod->save();

		        $a = preg_replace("/\\\\u([0-9a-fA-F]{4})/", "&#x\\1;", json_decode($eod->dialer_data,true));
		        //$b = preg_replace("/\\\\u([0-9a-fA-F]{4})/", "&#x\\1;", json_decode($eod->dboard_data,true));


		        if($a):

		        	$to = json_decode($eod->receiver,true);
		        	$from = json_decode($eod->sender,true);
		        	$cc = json_decode($eod->cc,true);
		        	$bcc = ['webdev@magellan-solutions.com'];

		        	if($to && $from):

				        Mail::send('emails.eod', ['eod' => $eod,'Time' => $Time], function ($m) use ($eod,$to,$from,$cc,$bcc){					            
				            $m->from($from[0], 'EOD Report');
				            $m->to($to)->subject($eod->title.' Eod Report');
				            $m->cc($cc);
				            $m->bcc($bcc);
				        });

				        $eod->sent_time = date("Y-m-d h:i:s");
				        $eod->sent = 1;
				        $eod->save();	

			        endif;		    

		    	endif;


	        endif;

	}
	public function eodStep5(){

	        $EodEmails = new \App\Http\Models\eod\EodEmails;
	        $Time = new \App\Http\Helpers\eod\Time;
	        //$eod = $EodEmails->where('visited',0)->first();
	        $eod = $EodEmails->where('id',303)->first();
	        //pre($eod->toArray());
	        if($eod):
	        	
	        	$eod->visited = 1;
		        $eod->save();

		        $a = json_decode($eod->dialer_data,true);
		        $b = json_decode($eod->dboard_data,true);

		        if($a || $b):

		        	$to = json_decode($eod->receiver,true);
		        	$from = json_decode($eod->sender,true);
		        	$cc= json_decode($eod->cc,true);
		        	$bcc = ['hazel.osias@mailinator.com'];

		        	if($to && $from):

				        Mail::send('emails.eod', ['eod' => $eod,'Time' => $Time], function ($m) use ($eod,$to,$from,$cc,$bcc){					            
				            $m->from($from[0], 'EOD Report');
				            $m->to($to)->subject($eod->title.' Eod Report');
				            $m->cc($cc);
				            $m->bcc($bcc);
				        });

				        $eod->sent = 1;
				        $eod->save();	

			        endif;		    

		    	endif;

	        endif;

	}
	public function testemail(){

	        $EodEmails = new \App\Http\Models\eod\EodEmails;
	        $Time = new \App\Http\Helpers\eod\Time;
	        $eod = $EodEmails->where('id',302)->first();
	        //$eod = $EodEmails->where('id',72)->first();

	        if($eod):
	        	
	        	//$eod->visited = 1;
		        //$eod->save();

		        $a = json_decode($eod->dialer_data,true);
		        $b = json_decode($eod->dboard_data,true);

		        if($a || $b):

		        	//$to = json_decode($eod->receiver,true);
		        	//$from = json_decode($eod->sender,true);
		        	//$cc = json_decode($eod->cc,true);
		        	$to = ['marlon1@mailinator.com','marlon2@mailinator.com'];
		            $from = ['marlon@opsmagellan.com'];
		            $cc = ['marlon3@mailinator.com','marlon4@mailinator.com'];
		            $bcc = ['marlon5@mailinator.com','marlon6@mailinator.com','marlonbbernal@yahoo.com'];
		        	//pre($cc);

		        	if($to && $from):

				        Mail::send('emails.eod', ['eod' => $eod,'Time' => $Time], function ($m) use ($eod,$to,$from,$cc,$bcc){					     // echo 'asdfasdf';die;      
				            $m->from($from[0], 'EOD Report');
				            $m->to($to)->subject($eod->title.' Eod Report');
				            $m->cc($cc);
				            $m->bcc($bcc);
				        });

				        //$eod->sent = 1;
				        //$eod->save();	

			        endif;		    

		    	endif;

	        endif;

	}

public function backup_email(){

	        $EodEmails = new \App\Http\Models\eod\EodEmails;
	        $Time = new \App\Http\Helpers\eod\Time;
	        $eod = $EodEmails->where('testmike',0)->first();
	        //$eod = $EodEmails->where('id',72)->first();
	        if($eod):
	        	
	        	$eod->testmike = 1;
		        $eod->save();

		        $a = json_decode($eod->dialer_data,true);
		        $b = json_decode($eod->dboard_data,true);

		        if($a || $b):

		        	$to = "hazel.osias@magellan-solutions.com";
		        	$from = "webdev@magellan-solutions.com";
		        	

				        Mail::send('emails.eod', ['eod' => $eod,'Time' => $Time], function ($m) use ($eod,$to,$from){					            
				            $m->from($from, 'EOD Report');
				            $m->to($to)->subject($eod->title.' Eod Report');
				            $m->bcc("marc.briones@magellan-solutions.com");				           
				        });

				        $eod->save();	
    

		    	endif;

	        endif;


	}


   //["ryan.alegrado@magellan-solutions.com"]
   //["webdev_report@magellan-solutions.com","operations@magellan-solutions.com","julie.galinato@magellan-solutions.com","april.romasanta@magellan-solutions.com","maryan.anora@magellan-solutions.com","reports@magellan-solutions.com"]     
}

   //["marlon1@mailinator.com"]
   //["marlon2@mailinator.com","marlon3@mailinator.com"]     