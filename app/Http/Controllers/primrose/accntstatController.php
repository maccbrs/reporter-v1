<?php namespace App\Http\Controllers\primrose;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\primrose\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\primrose\Primrose; 
use App\Http\Models\primrose\VICI;
use App\Http\Models\primrose\Viciconn;
use Config;
use DB;

class accntstatController extends Controller 
{ 

	public function occl($type = 'omnitrix'){
		
		$vcon = new Viciconn; 
		$help = new Primrose;
		$restrictions = new Restrictions;

		$timezone = $help->timezones(true); 
		$lists = $vcon->where('type',$type)->where('status',1)->orderby('name','asc')->get();	  
		$from2 = '';
		$to2 = '';

		return view('primrose.accntstat.occl',compact('timezone','lists','from2','to2','type','restrictions'));

	}

	public function occl_post(Request $r,$type = 'omnitrix'){

		ini_set('max_execution_time', -1);
		ini_set('memory_limit', '-1');
		
        $this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'campaigns' => 'required'
        ]);	

		$vcon = new Viciconn;
		$help = new Primrose;
		$restrictions = new Restrictions;
		$params = [];
		$camp = $vcon->where('id',$r->input('campaigns'))->first(); 

		$listname = "";
		if($camp->toArray()):

			$timezone = $help->timezones(true);
			$lists = $vcon->where('type',$type)->where('status','!=',0)->orderby('name','asc')->get();

			$timez = 'Asia/Manila';
			$genCtlr = new \App\Http\Controllers\primrose\generalController;
			$data = $genCtlr->_query($camp,$r->input('from'),$r->input('to'),$timez,Config::get('app.timezone')); 
			$total = $genCtlr->_totals($data);
			$dispo = $genCtlr->dispo($data);
			$dropped = $genCtlr->dropped($data);
			$calls = $genCtlr->calls($data);
			$vendors = $genCtlr->vendor($data);
			if($camp->id == '229'):
			$listname = $genCtlr->listname($data);
			endif;
			//dd($data);
			$params = [
			'from' => $r->input('from'),
			'to' => $r->input('to'),
			'timezone' => $timez,
			'campaigns' => $camp->name
			];
		
		endif;

		$logger = new Logger;
		$logger->report($r,$params);

		//pre($camp->dids);
		return view('primrose.accntstat.occl-results',compact('timezone','lists','total','dispo','dropped','calls','params','type','restrictions', 'vendors','camp', 'listname'));

	}

	public function occl_postajax(Request $request){
		ini_set('max_execution_time', '-1');
		ini_set('memory_limit', '-1');

		$timez = 'Asia/Manila';

		$vcon = new Viciconn;
		$help = new \App\Http\Helpers\primrose\Primrose;
		$controller = new \App\Http\Controllers\primrose\testController;
		$camp = $vcon->where('id',$request->campaigns)->first(); 
		$data = $controller->_query($camp, $request->from, $request->to, $timez, Config::get('app.timezone'));		
		$total = $controller->_totals($data);


	}




} 


