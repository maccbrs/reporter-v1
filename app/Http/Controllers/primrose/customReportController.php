<?php

namespace App\Http\Controllers\primrose;

use Illuminate\Http\Request;
use App\Http\Controllers\primrose\accessController as Restrictions;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\primrose\Viciconn;
use DB;
class customReportController extends Controller
{
    public function index(){
    	$restrictions = new Restrictions;
    	$vcon = new Viciconn; 

    	$campaigns = $vcon->where('type','nomnitrix')->where('status',1)->orderby('name','asc')->get();
    	return view('primrose.custom_report.index',compact('restrictions', 'campaigns'));
    }

    public function generate(Request $request){
    	$restrictions = new Restrictions;
    	$vcon = new Viciconn; 
    	$campaigns = $vcon->where('type','nomnitrix')->where('status',1)->orderby('name','asc')->get();

    	$from = $request->from;
    	$to = $request->to;
    	$camp = $vcon->where('id',$request->campaigns)->first();
    	$data = $this->queryController($camp,$from,$to);

    	return view('primrose.custom_report.index',compact('restrictions', 'campaigns', 'data'));
    }

    public function queryController($camp, $from, $to){
    	$server = '192.168.200.132';
    	$vici = DB::connection($server);

    	$q = $vici->table('vicidial_agent_log')->whereBetween('event_time', [$from,$to])
    				->whereIn('campaign_id',json_decode($camp->dids))->get();
    	
    	$data = [];
    	foreach($q as $key => $val):

    		$data[] = [
    			'date' => $val->event_time,
    			'pause' => $val->pause_sec,
    			'wait' => $val->wait_sec,
    			'talk' => $val->talk_sec,
    			'dispo' => $val->dispo_sec,
    			'dead' => $val->dead_sec,
    			'customer' => ($val->talk_sec - $val->dead_sec),
    			'status' => $val->status,
    			'lead' => $val->lead_id,
    			'campaign' => $val->campaign_id,
    			'pause_code' => $val->sub_status,
    			'type' => $val->comments,
    			'user' => $val->user
    		];

    	endforeach;
    	
    	return $data;

    }
}
