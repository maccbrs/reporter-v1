<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\primrose\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\primrose\Viciconn;
use App\Http\Models\primrose\ClientPortalContent;
use App\Http\Models\laravel\DummyboardUser;
use App\Http\Helpers\primrose\Primrose; 
use Config;
use DB;

class listReporterController extends Controller 
{ 
	public function index(Request $r){ 

		$vcon = new Viciconn;
		$input = $r->all();
		$help = new Primrose;

		$client_portal = new ClientPortalContent;

		$lists = $vcon->where('status',1)->orderby('name','asc')->pluck('name','id');
		
		$restrictions = new Restrictions;
		$timezone = $help->timezones(true);		
		return view('primrose.listreporter.index',compact('timezone','restrictions','lists'));

	}

	public function post(Request $r){

		$vcon = new Viciconn;
		$restrictions = new Restrictions;
		$help = new Primrose;
		$lists = $vcon->where('status',1)->orderby('name','asc')->pluck('name','id');
		$campaign = $r->input('campaign');
		$list_id = $r->input('list_id');
		$searchby = $r->input('searchby');
		$from = $r->input('from');
		$to = $r->input('to');

       if($searchby == 'List'){

			$params = [];
			$timezone = $help->timezones(true);
			$timez = 'Asia/Manila';
			$genCtlr = new \App\Http\Controllers\primrose\generalController;

			$data = $genCtlr->_query_list_id($list_id); 
		
			$total = $genCtlr->_totals($data);
			$dispo = $genCtlr->dispo($data);
			$dropped = $genCtlr->dropped($data);
			$calls = $genCtlr->calls($data);
			$vendors = $genCtlr->vendor($data);

			$params = [

				'from' => $r->input('from'),
				'to' => $r->input('to'),
				'timezone' => $timez,
				'list_id' => $list_id

			];

			$logger = new Logger;
			$logger->report($r,$params);



			return view('primrose.listreporter.post',compact('timezone','lists','total','dispo','dropped','calls','params','type','restrictions', 'vendors'));

		}else{
		
			$params = [];

			$camp = $vcon->where('id',$campaign)->first(); 

			if($camp->toArray()):

				$timezone = $help->timezones(true);
				$lists = $vcon->where('status',1)->orderby('name','asc')->pluck('name','id');

				$timez = 'Asia/Manila';
				$genCtlr = new \App\Http\Controllers\primrose\generalController;

				if($r->input('campaign') != 283){

					$data = $genCtlr->_query_with_listname($camp,$from,$to,$timez,Config::get('app.timezone')); 

					$updated = new ClientPortalContent;
					$updated_list = $genCtlr->clientportal($updated,$data,$from, $to); 
					$row_list = $genCtlr->rowunique($updated_list);

				}else{

					$data = $genCtlr->_query_with_listname1010($camp,$from,$to,$timez,Config::get('app.timezone')); 

				}
				
				
				
				$dummy_user = new DummyboardUser;
				$dummy_users = $dummy_user->pluck('name','id');

				$dropped = $genCtlr->dropped($data);
				$listdetails = $genCtlr->listname($data);
			
				$params = [
				'from' => $r->input('from'),
				'to' => $r->input('to'),
				'timezone' => $timez,
				'campaigns' => $camp->name
				];
			
			endif;

			$logger = new Logger;
			$logger->report($r,$params);

			return view('primrose.listreporter.listname',compact('orig_list','timezone','lists','total','dispo','dropped','calls','params','type','restrictions', 'listdetails','list','updated_list','vendors','row_list','dummy_users'));

		}

	}


	public function agent_break(){  
	
		$campaign_list = $this->campaign('192.168.200.132');
		$users = $this->users(2);
		$test = $this->campaign();
		$help = new Primrose;
		$restrictions = new Restrictions;
		$timezone = $help->timezones(true);	

		return view('primrose.agent.agent_break',compact('timezone','users','restrictions','campaign_list'));
		
	}


	public function agent_break_post(Request $r){
		
		$help = new Primrose;

		$restrictions = new Restrictions;
		$genCtlr = new \App\Http\Controllers\primrose\generalController;
		$campaign_list = $this->campaign('192.168.200.132');
		$timezone = $help->timezones(true);	

		$from = $r->from; 
		$to = $r->to; 
		
		$time = $help->_time($from,$to);
		
		$campaign = $r->get('campaign');



		$campaign_break = $this->campaign_break($campaign,$time['from'],$time['to'],'192.168.200.132');
	
		$results = [
			'from' => $from,
			'to' => $to,
			'campaign' => $campaign,
			'call' => $genCtlr->calls_break($campaign_break),

		];


		$logger = new Logger;
		$logger->report($r,$r->only(['from','to','user'])); 

		return view('primrose.agent.agent_break_result',compact('timezone','results','users','restrictions','campaign_list'));
		 

	}

	private function users($server = 1){

		$conn = '192.168.200.132';
		$users1 = DB::connection($conn)
				->table('vicidial_users')
				->select('user', 'full_name')
				->where('active','Y')
				->groupBy('user')
				->get();

		return $users1;

	}

	private function campaign($io = 'vicidial'){

		$campaign_stat = DB::table('vicidial_campaign_stats')
			->select('campaign_id');

		$campaign_list =  DB::connection($io)
			->table('vicidial_campaigns')
			->select('campaign_id')
            ->orderBy('campaign_id', 'desc')
            ->union($campaign_stat)
            ->get();

	    return $campaign_list;

	}

	private function pausecode($list_id,$f,$t,$io = 'vicidial'){

		$help = new Primrose;
		$a =  DB::connection($io)
				->table('vicidial_agent_log')
				->where('list_id',$list_id)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date', 'desc')
	            ->get();

	    $pausecode = [];     

	    foreach ($a as $av):
	    	if(array_key_exists($av->sub_status, $pausecode)):
	    		$pausecode[$av->sub_status]['count']++;
	    		$pausecode[$av->sub_status]['name'] = $av->sub_status;
	    		$pausecode[$av->sub_status]['pause_sec'] += $av->pause_sec;
	    		$pausecode[$av->sub_status]['talk_sec'] += $av->talk_sec;
	    		$pausecode[$av->sub_status]['wait_sec'] += $av->wait_sec;
	    		$pausecode[$av->sub_status]['dispo_sec'] += $av->dispo_sec;
	    	else:
	    		$pausecode[$av->sub_status] = array();
	    		$pausecode[$av->sub_status]['count'] = 1;
	    		$pausecode[$av->sub_status]['name'] = $av->sub_status;
	    		$pausecode[$av->sub_status]['pause_sec'] = $av->pause_sec;
	    		$pausecode[$av->sub_status]['talk_sec'] = $av->talk_sec;
	    		$pausecode[$av->sub_status]['wait_sec'] = $av->wait_sec;
	    		$pausecode[$av->sub_status]['dispo_sec'] = $av->dispo_sec;
	    	endif;
	    endforeach;

	    $results = array();
	    foreach ($pausecode as $key => $value):
	    	$results[$key] = $value;
	    	$results[$key]['pause_sec'] = (!empty($value['pause_sec'])?$help->sToHms($value['pause_sec']):'0 : 0 : 0');
	    endforeach;
	    return $results;

	}

	private function campaign_break($campaign,$f,$t,$io = 'vicidial'){

		$help = new Primrose;
		$column =  'vicidial_closer_log';

		$a =  DB::connection($io)
			->table($column)
			->whereBetween('call_date', [$f, $t])
			->wherein('campaign_id', [$campaign])
	        ->orderBy('user','asc')
	        ->orderBy('call_date','asc')
	        ->get();

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):

	    	$data[$av->agent_log_id] = $av;
	      	$data[$av->agent_log_id]->call_date = $av->call_date; 
	    	$uids[] = $av->agent_log_id;

	    endforeach;
	  //  pre($a); 
	    return $data;

	}

	private function _query($list_id,$f,$t,$conn = '192.168.200.132',$io){

		$help = new Primrose;
		$column =  'vicidial_closer_log';

		$a =  DB::connection($conn)
				->table($column)
				->where('list_id',$list_id)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):

	    	$data[$av->uniqueid]['closer'] = $av;
	      	$data[$av->uniqueid]['closer']->call_date = $av->call_date; 
	    	$uids[] = $av->uniqueid;

	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv;
	    }


	    return $data;
	}

	private function _query_out($list_id,$f,$t,$conn = '192.168.200.132',$io){

		$help = new Primrose;
		$column = 'vicidial_log';

		$a =  DB::connection($conn)
				->table($column)
				->where('list_id',$list_id)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();

	    $data = [];
	    $uids = [];

	    	 
	    foreach ($a as $av):

	    	$data[$av->uniqueid]['closer'] = $av;
	       	$data[$av->uniqueid]['closer']->call_date = $av->call_date; 
	    	$uids[] = $av->lead_id;

	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('lead_id',$uids)
				->get();
	
	    
	    foreach ($b as $bv):
	    	foreach ($a as $a_list):
	    		if($bv->lead_id == $a_list->lead_id):
	    			$data[$a_list->uniqueid]['agent'] = $bv;
	    		endif;
	    	endforeach;
	    endforeach;

	    	

	    return $data;
	}

	private function _queryBoth($list_id,$f,$t,$conn = '192.168.200.132'){

		$help = new Primrose;

		$x =  DB::connection($conn)
				->table('vicidial_closer_log')
				->where('list_id',$list_id)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();
		$y =  DB::connection($conn)
				->table('vicidial_log')
				->where('list_id',$list_id)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();
	    $a = array_merge($x, $y); 
	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	$data[$av->uniqueid]['closer'] = $av;
	        $data[$av->uniqueid]['closer']->call_date = $av->call_date;
	    	$uids[] = $av->uniqueid;
	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv;
	    }

	    return $data;
	}

	private function _query_agent($user,$f,$t,$tz1,$tz2,$conn = '192.168.200.132',$io = 'in'){

		$help = new Primrose;
		$column ='vicidial_agent_log';

		$a =  DB::connection($conn)
				->table($column)
				->where('user',$user)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	$data[$av->uniqueid]['closer'] = $av;
	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;
	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv;
	    }

	    return $data;
	}


} 