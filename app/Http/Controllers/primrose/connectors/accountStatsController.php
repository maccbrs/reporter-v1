<?php namespace App\Http\Controllers\primrose\connectors;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use App\Http\Controllers\primrose\generalController as Gc;
use Illuminate\Http\Request;
use App\Http\Requests;


class accountStatsController extends Controller 
{ 

	public function account_index($type){
		
		$connectorsObj = new \App\Http\Models\primrose\Viciconn;		
		$connectors = $connectorsObj->where('type',$type)->paginate(15);	
		$restrictions = new Restrictions;
		$count = 0;
		return view('primrose.connectors.accstats.index',compact('connectors','restrictions','count'));

	}

}