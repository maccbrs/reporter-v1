<?php namespace App\Http\Controllers\primrose\connectors;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use App\Http\Controllers\primrose\generalController as Gc;
use Illuminate\Http\Request;
use App\Http\Requests;


class connectorsController extends Controller  
{ 

	public function index(){

	  $restrictions = new Restrictions;
	  return view('primrose.connectors.index',compact('restrictions'));

	} 

	public function board_index(){

	  $gc = new Gc;

	  $restrictions = new Restrictions;
	  $boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
	  $boards = $boardsObj->orderBy('name', 'asc')->paginate(20); //pre($boards->toArray());
	  $help = new \App\Http\Helpers\primrose\Primrose;
	  $timezones = $help->timezones();		  
	  return view('primrose.connectors.board-index',compact('restrictions','boards','gc','timezones'));

	}

	public function destroy($id){
		\App\Http\Models\primrose\Viciconn::destroy($id);
		return redirect()->back();
	}
	//public function 
	
}