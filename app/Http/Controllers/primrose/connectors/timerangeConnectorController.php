<?php namespace App\Http\Controllers\primrose\connectors;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use App\Http\Controllers\primrose\generalController as Gc;
use Illuminate\Http\Request;
use App\Http\Requests;


class timerangeConnectorController extends Controller 
{ 

	public function edit_timerange(){

		$gc = new Gc;
		$help = new \App\Http\Helpers\primrose\Primrose;
		$boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
		$board = $boardsObj->where('id',$id)->first();
		$restrictions = new Restrictions;
		return view('primrose.connectors.board.timerange-edit',compact('restrictions','timezones','timezone','board','gc'));	

	}


}