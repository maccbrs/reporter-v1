<?php namespace App\Http\Controllers\primrose\connectors;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use App\Http\Controllers\primrose\generalController as Gc;
use Illuminate\Http\Request;
use App\Http\Requests;


class boardConnectorController extends Controller 
{ 

	public function edit_timezone($id){

		$gc = new Gc;
		$help = new \App\Http\Helpers\primrose\Primrose;
		$timezones = $help->timezones();
		$boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
		$board = $boardsObj->where('id',$id)->first();
		$timezone = $gc->_options($board->options);
		$restrictions = new Restrictions;
		return view('primrose.connectors.board.timezone-edit',compact('restrictions','timezones','timezone','board'));

	} 

	public function update_timezone(Request $r, $id){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$gc = new Gc;
		$boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
		$board = $boardsObj->where('id',$id)->first();
		if($board->toArray()):
			$board->options = $gc->_update_options($board->options,$r->input('timezone'));
			$board->save();
		endif;
		$timezones = $help->timezones();
		return redirect()->route('primrose.connectors.board-index');

	}

	public function edit_timerange($id){

		$gc = new Gc;
		$help = new \App\Http\Helpers\primrose\Primrose;
		$boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
		$board = $boardsObj->where('id',$id)->first();
		$restrictions = new Restrictions;
		return view('primrose.connectors.board.timerange-edit',compact('restrictions','board','gc'));

	}

	public function update_timerange(Request $r,$id){

		$gc = new Gc;
		$help = new \App\Http\Helpers\primrose\Primrose;
		$boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
		$board = $boardsObj->where('id',$id)->first();
		if($board->toArray()):
			$board->options = $gc->_update_options($board->options,$r->input('from'),'from');
			$board->save();
			$board->options = $gc->_update_options($board->options,$r->input('to'),'to');
			$board->save();			
		endif;
		return redirect()->route('primrose.connectors.board-index');

	}


	public function update_sendtime(Request $r,$id){
		
		$sendtime = date('H:i',strtotime($r->input('sendtime'))); 
		if($sendtime):
			$gc = new Gc;
			$help = new \App\Http\Helpers\primrose\Primrose;
			$boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
			$board = $boardsObj->where('id',$id)->first();
			if($board->toArray()):
				$board->options = $gc->_update_options($board->options,$sendtime,'sendtime');
				$board->save();		
			endif;
		endif;
		return redirect()->route('primrose.connectors.board-index');

	} 

} 