<?php namespace App\Http\Controllers\primrose\campaigns;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\Primrose;

class campaignSettingsController extends Controller 
{ 

	public function timezone($cid){
		//pre($cid->toArray());
		$help = new Primrose;
		$timezone = $this->tz_options($cid->options);
		$timezones = $help->timezones();
		$campaign_id = $cid->campaign_id;
		$restrictions = new Restrictions;
		return view('primrose.campaigns.settings.show',compact('timezone','timezones','campaign_id','restrictions'));

	}
	
	public function timezone_post($cid,Request $r){// pre($cid);
		$cid->options = $this->_tz_update($cid->options,$r->input('timezone'));
		$cid->save();
		flash()->success('successfully saved');
		return redirect()->route('primrose.campaigns.index');
	} 

	private function tz_options($opt){
		$x = json_decode($opt,true);
		return (isset($x['timezone'])?$x['timezone']:'GMT');
	}

	private function _tz_update($opt,$tz){
		$x = json_decode($opt,true);
		$x['timezone'] = $tz;
		return json_encode($x);
	}

}