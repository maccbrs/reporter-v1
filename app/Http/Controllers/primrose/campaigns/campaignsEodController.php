<?php namespace App\Http\Controllers\primrose\campaigns;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Http\Request;
use App\Http\Requests;


class campaignsEodController extends Controller 
{ 

	public function index($campaign_id){


	  $restrictions = new Restrictions;
	  $campaigns = \App\Http\Models\primrose\sunflower\Eod::where('status',1)->where('campaign_id',$campaign_id)->orderBy('created_at','desc')->paginate(13); 
	  //pre($campaigns->toArray());
	  return view('primrose.campaigns.index',compact('campaigns','restrictions'));

	}

	

}

?>