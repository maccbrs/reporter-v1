<?php namespace App\Http\Controllers\primrose\campaigns;

use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\loggerController as Logger;
use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Http\Request;
use App\Http\Requests;


class campaignsController extends Controller 
{ 

	public function index(){

	  $restrictions = new Restrictions;
	  $campaigns = \App\Http\Models\primrose\sunflower\Campaign::where('status',1)->orderBy('created_at','desc')->paginate(13); //pre($campaigns->toArray());
	  return view('primrose.campaigns.index',compact('campaigns','restrictions'));

	} 

	

}
