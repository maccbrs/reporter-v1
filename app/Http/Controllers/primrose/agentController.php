<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\primrose\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\primrose\Primrose; 
use Config;
use DB;

class agentController extends Controller 
{ 

	public function index(Request $r){ 

		$users = $this->users();
		$input = $r->all();
		$help = new Primrose;
		$restrictions = new Restrictions;
		$timezone = $help->timezones(true);		
		return view('primrose.agent.index',compact('timezone','users','restrictions'));

	}

	public function post(Request $r){

		$help = new Primrose;
		$restrictions = new Restrictions;
		$genCtlr = new \App\Http\Controllers\primrose\generalController;

        $this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'user' => 'required'
        ]);	
        
		$from = $r->from; 
		$to = $r->to; 
		
		$time = $help->_time($from,$to);
		$users = $this->users();
		$user = $r->get('user');
		
		$pausecodes = $this->pausecode($user,$time['from'],$time['to'],'192.168.200.132');
		$logData = $this->logcode($user,$time['from'],$time['to'],'192.168.200.132');
		$inData = $this->_query($user,$time['from'],$time['to'],'192.168.200.132','out');
		$outData = $this->_query_out($user,$time['from'],$time['to'],'192.168.200.132','in');
		$bothData = $this->_queryBoth($user,$time['from'],$time['to'],'192.168.200.132');  

		$results = [
			'from' => $time['origFrom'],
			'to' => $time['origTo'],
			'user' => $user,
			'dispo' => $genCtlr->dispo($bothData),
			'stats' => $genCtlr->_totals($inData),
			'stats2' => $genCtlr->_totals($outData), 
			'calls' => $genCtlr->calls($inData),
			'calls2' => $genCtlr->calls_out($outData),
			'pausecodes' => $pausecodes,
			'log_data' => $logData
		];

		$logger = new Logger;
		$logger->report($r,$r->only(['from','to','user'])); 

		return view('primrose.agent.post',compact('results','users','restrictions'));

	}

	public function agent_break(){  
	
		$campaign_list = $this->campaign('192.168.200.132');
		$users = $this->users(2);
		$test = $this->campaign();
		$help = new Primrose;
		$restrictions = new Restrictions;
		$timezone = $help->timezones(true);	

		return view('primrose.agent.agent_break',compact('timezone','users','restrictions','campaign_list'));
		
	}


	public function agent_break_post(Request $r){
		
		$help = new Primrose;

		$restrictions = new Restrictions;
		$genCtlr = new \App\Http\Controllers\primrose\generalController;
		$campaign_list = $this->campaign('192.168.200.132');
		$timezone = $help->timezones(true);	

		$from = $r->from; 
		$to = $r->to; 
		
		$time = $help->_time($from,$to);
		$users = $this->users();
		$campaign = $r->get('campaign');

		$campaign_break = $this->campaign_break($campaign,$time['from'],$time['to'],'192.168.200.132');
	
		$results = [
			'from' => $from,
			'to' => $to,
			'campaign' => $campaign,
			'call' => $genCtlr->calls_break($campaign_break),

		];


		$logger = new Logger;
		$logger->report($r,$r->only(['from','to','user'])); 

		return view('primrose.agent.agent_break_result',compact('timezone','results','users','restrictions','campaign_list'));
		 
	}

	public function agent_log(Request $r){ 

		
		$input = $r->all();
		$help = new Primrose;
		$restrictions = new Restrictions;

		$campaign_list = $this->campaign('192.168.200.132');

		//pre($campaign_list); 
		$timezone = $help->timezones(true);		
		return view('primrose.agent.agent_log',compact('timezone','restrictions','campaign_list'));

	}

	public function agent_log_post(Request $r){

		$help = new Primrose;
		$restrictions = new Restrictions;
		$genCtlr = new \App\Http\Controllers\primrose\generalController;

		$from = $r->from; 
		$to = $r->to; 

		$campaign = $r->input('campaign');

		$campaign_log = $this->campaign_break($campaign,$from,$to,'192.168.200.132');

		$campaign_list = $this->campaign('192.168.200.132');

        $this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'campaign' => 'required'
        ]);	
        
		$logger = new Logger;
		$logger->report($r,$r->only(['from','to','user'])); 

		return view('primrose.agent.agent_log_post',compact('results','users','restrictions','campaign_log','campaign_list'));

	}

	private function users($server = 1){

		$conn = '192.168.200.132';
		$users1 = DB::connection($conn)
				->table('vicidial_users')
				->select('user', 'full_name')
				->where('active','Y')
				->groupBy('user')
				->orderby('full_name','asc')
				->get();

		return $users1;

	}

	private function campaign($io = 'vicidial'){


		$campaign_stat = DB::table('vicidial_campaign_stats')
			->select('campaign_id');

		$campaign_list =  DB::connection($io)
			->table('vicidial_campaigns')
			->select('campaign_id')
            ->union($campaign_stat)
            ->orderBy('campaign_id', 'asc')
            ->pluck('campaign_id');

        $campaign_list2 =  DB::connection('vicidial2')
			->table('vicidial_campaigns')
			->select('campaign_id')
            ->union($campaign_stat)
           //	->orderBy('campaign_id', 'asc')
            ->pluck('campaign_id');

		$result = array_merge($campaign_list, $campaign_list2);
        $result = array_unique($result);

   //     pre($result);

	    return $result;

	}

	private function pausecode($user,$f,$t,$io = 'vicidial'){

		$help = new Primrose;
		$a =  DB::connection($io)
				->table('vicidial_agent_log')
				->where('user',$user)
				->where('sub_status','!=', ' ')
	            ->whereBetween('event_time', [$f, $t])
	            ->orderBy('event_time', 'desc')
	            ->get();

	    $pausecode = [];     

	    foreach ($a as $av):
	    	if(array_key_exists($av->sub_status, $pausecode)):
	    		$pausecode[$av->sub_status]['count']++;
	    		$pausecode[$av->sub_status]['name'] = $av->sub_status;
	    		$pausecode[$av->sub_status]['pause_sec'] += $av->pause_sec;
	    		$pausecode[$av->sub_status]['talk_sec'] += $av->talk_sec;
	    		$pausecode[$av->sub_status]['wait_sec'] += $av->wait_sec;
	    		$pausecode[$av->sub_status]['dispo_sec'] += $av->dispo_sec;
	    	else:
	    		$pausecode[$av->sub_status] = array();
	    		$pausecode[$av->sub_status]['count'] = 1;
	    		$pausecode[$av->sub_status]['name'] = $av->sub_status;
	    		$pausecode[$av->sub_status]['pause_sec'] = $av->pause_sec;
	    		$pausecode[$av->sub_status]['talk_sec'] = $av->talk_sec;
	    		$pausecode[$av->sub_status]['wait_sec'] = $av->wait_sec;
	    		$pausecode[$av->sub_status]['dispo_sec'] = $av->dispo_sec;
	    	endif;
	    endforeach;

	    $results = array();
	    foreach ($pausecode as $key => $value):
	    	$results[$key] = $value;
	    	$results[$key]['pause_sec'] = (!empty($value['pause_sec'])?$help->sToHms($value['pause_sec']):'0 : 0 : 0');
	    endforeach;
	    return $results;

	}

	private function logcode($user,$f,$t,$io = 'vicidial'){


		$a =  DB::connection($io)
				->table('vicidial_agent_log')
				->where('user',$user)
				//->where('sub_status','!=', ' ')
	            ->whereBetween('event_time', [$f, $t])
	            ->orderBy('event_time', 'desc')
	            ->get();

	    return $a;

	}

	private function campaign_break($campaign,$f,$t,$io = 'vicidial'){

		$help = new Primrose;
		$column =  'vicidial_agent_log';




	    if($campaign == '1010'){

	    	$a =  DB::connection('vicidial2')
			->table($column)
			->whereBetween('event_time', [$f, $t])
			->wherein('campaign_id', [$campaign])
	        ->orderBy('event_time','asc')
	        ->get();

	    }else{

	    	$a =  DB::connection($io)
			->table($column)
			->whereBetween('event_time', [$f, $t])
			->wherein('campaign_id', [$campaign])
	        ->orderBy('event_time','asc')
	        ->get();

	    }

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):

	    	$data[$av->agent_log_id] = $av;
	      	$data[$av->agent_log_id]->call_date = $av->event_time; 
	    	$uids[] = $av->agent_log_id;

	    endforeach;
	  //  pre($a); 
	    return $data;

	}


	private function _query($user,$f,$t,$conn = '192.168.200.132',$io){

		$help = new Primrose;
		$column =  'vicidial_closer_log';

		$a =  DB::connection($conn)
				->table($column)
				->where('user',$user)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):

	    	$data[$av->uniqueid]['closer'] = $av;
	      	$data[$av->uniqueid]['closer']->call_date = $av->call_date; 
	    	$uids[] = $av->uniqueid;

	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv;
	    }


	    return $data;
	}

	private function _query_out($user,$f,$t,$conn = '192.168.200.132',$io){

		$help = new Primrose;
		$column = 'vicidial_log';

		$a =  DB::connection($conn)
				->table($column)
				->where('user',$user)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();

	    $data = [];
	    $uids = [];

	    	 
	    foreach ($a as $av):

	    	$data[$av->uniqueid]['closer'] = $av;
	       	$data[$av->uniqueid]['closer']->call_date = $av->call_date; 
	    	$uids[] = $av->lead_id;

	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('lead_id',$uids)
				->get();
	
	    
	    foreach ($b as $bv):
	    	foreach ($a as $a_list):
	    		if($bv->lead_id == $a_list->lead_id):
	    			$data[$a_list->uniqueid]['agent'] = $bv;
	    		endif;
	    	endforeach;
	    endforeach;

	    	

	    return $data;
	}

	private function _queryBoth($user,$f,$t,$conn = '192.168.200.132'){

		$help = new Primrose;

		$x =  DB::connection($conn)
				->table('vicidial_closer_log')
				->where('user',$user)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();
		$y =  DB::connection($conn)
				->table('vicidial_log')
				->where('user',$user)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();
	    $a = array_merge($x, $y); 
	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	$data[$av->uniqueid]['closer'] = $av;
	        $data[$av->uniqueid]['closer']->call_date = $av->call_date;
	    	$uids[] = $av->uniqueid;
	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv;
	    }
	    
	    return $data;
	}

	private function _query_agent($user,$f,$t,$tz1,$tz2,$conn = '192.168.200.132',$io = 'in'){

		$help = new Primrose;
		$column ='vicidial_agent_log';

		$a =  DB::connection($conn)
				->table($column)
				->where('user',$user)
	            ->whereBetween('call_date', [$f, $t])
	            ->orderBy('call_date','desc')
	            ->get();

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	$data[$av->uniqueid]['closer'] = $av;
	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;
	    endforeach;

		$b =  DB::connection($conn)
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv;
	    }

	    return $data;
	}


} 