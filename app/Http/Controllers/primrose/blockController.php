<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\primrose\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\primrose\Primrose; 
use App\Http\Models\primrose\VICI;
use App\Http\Models\primrose\Viciconn;
use Config;
use DB;
use Illuminate\Support\Facades\Mail;

class blockController extends Controller
{ 

	public function index(){ 


		$vcon = new Viciconn;
		$help = new Primrose;
		$restrictions = new Restrictions;

		$timezone = $help->timezones(true);
		$lists = $vcon->wherein('type',['omnitrix','nomnitrix'])->orderby('name','asc')->get();	 

		$from2 = '';
		$to2 = '';
		$type = 'nomnitrix';
		return view('primrose.block.index',compact('timezone','lists','from2','to2','type','restrictions'));

	} 

	public function post(Request $r){

		$this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'campaigns' => 'required'
        ]);	

		$vcon = new Viciconn;
		$help = new Primrose;
		$lists = $vcon->wherein('type',['omnitrix','nomnitrix'])->orderby('name','asc')->get();
		$restrictions = new Restrictions;
		$genCtlr = new \App\Http\Controllers\primrose\generalController;

		$params = [];

		$timezone = $help->timezones(true);
		$timezone2 = json_decode($help->timezones(true)); 

		$camp = $vcon->where('id',$r->input('campaigns'))->first();

		if($camp->toArray()):

			$tmz = [];
			foreach ($timezone2 as $v): $tmz[] = $v->value; endforeach;
			
			$from = explode(' ', $r->input('from'));
			$to = explode(' ', $r->input('to'));

			if(in_array($from[1], $tmz)):
				
				$from2 = $help->convert_tz($from[0].' '.$from[0],$from[1],Config::get('app.timezone')); 
				$to2 = $help->convert_tz($to[0].' '.$to[1],$from[1],Config::get('app.timezone')); 
				$timez = $from[1];

			else:

				$timez = Config::get('app.timezone');
				$from2 = date("Y-m-d H:i:s",strtotime($from[0].' '.$from[1]));
				$to2 = date("Y-m-d H:i:s",strtotime($to[0].' '.$to[1]));
			endif;
			
			$data = $genCtlr->_query($camp,$from2,$to2,$timez,Config::get('app.timezone'));  
		
			$blocks = $genCtlr->block($data);

			$params = [
			'from' => $from[0].' '.$from[0],
			'to' => $to[0].' '.$to[1],
			'timezone' => $timez,
			'campaigns' => $camp->name
			];

		endif;

		date("Y-m-d H:i:s",strtotime($to[0].' '.$to[1]));		

		$logger = new Logger;
		$logger->report($r,$r->only(['from','to','user'])); 
		
		return view('primrose.block.post',compact('timezone','lists','from2','to2','type', 'blocks','restrictions'));

	}

		public function blockage(Request $r){
			// $user = 'Test';
			// $msg = 'test';
			// $emails = ['webdev@magellan-solutions.com','noc@magellan-solutions.com'];

			// Mail::send('emails.reminder', ['msg' => $msg], function ($m) use ($emails) {
	  //           $m->from('hello@app.com', 'Your Application');
	  //            $m->to($emails);
   //              $m->subject('Blockage Report');
	  //       });

		


			$vcon = new \App\Http\Models\primrose\Viciconn;
            $help = new \App\Http\Helpers\primrose\Primrose; 
            $lists = $vcon->wherein('type',['omnitrix','nomnitrix'])->orderby('name','asc')->get();
            $restrictions = new \App\Http\Controllers\primrose\accessController;
            $genCtlr = new \App\Http\Controllers\primrose\generalController;

            $params = [];

            $to2 = date("Y-m-d H:i:s"); 

            $from2 =  date('Y-m-d H:i:s',date(strtotime("-1 day", strtotime($to2 ))));

            $camp = $vcon->where('id','=',52)->first();
            
            
            if($camp->toArray()):

                $timez = Config::get('app.timezone');
                $from2 = date("Y-m-d H:i:s",strtotime($from2));
                $to2 = date("Y-m-d H:i:s",strtotime($to2));
                
                $data = $genCtlr->_blockquery($camp,$from2,$to2); 
            
                $blocks = $genCtlr->block($data);

            endif;  

            $user = 'Kit';
            $param['title'] = 'Blockage';
            // $msg = $blocks;
            // return view('emails.blockage',compact('msg'));
            $emails = ['webdev@magellan-solutions.com', 'gerardo.resano@magellan-solutions.com','julie.galinato@magellan-solutions.com'];

            Mail::send('emails.blockage', ['msg' => $blocks,'user' => $user], function ($m) use ($blocks,$emails)  {
                $m->from('webdev@magellan-solutions.com', 'Magellan Report');
                $m->to($emails);
                $m->subject('Blockage Report');
            });


		}
	


}  