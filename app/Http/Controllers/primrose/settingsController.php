<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\primrose\loggerController as Logger;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\primrose\Primrose;
use App\Http\Models\primrose\VICI;
use App\Http\Models\primrose\Viciconn;

class settingsController extends Controller
{ 

	public function index(){
		$restrictions = new Restrictions;
		return view('primrose.settings.index',compact('restrictions'));
	}

	public function campaign_add(){
		$vici = new VICI;
        $vicidialin = $vici->ingroups('in');
        $vicidialout = $vici->ingroups('out');
        $restrictions = new Restrictions;

        //pre($vicidialout);
		return view('primrose.settings.campaign.add',compact('vicidialin','vicidialout','restrictions'));	
	} 

	public function campaign_store(Request $r){

		$viciconn = new Viciconn;
        $this->validate($r, [
            'name' => 'required',
            'bound' => 'required',
            'vici' => 'required',
            'type' => 'required',
            'timezone' => 'required'
        ]);	

        $input['name'] = $r->input('name');
        $input['bound'] = $r->input('bound');
        $input['type'] = $r->input('type');
        $input['timezone'] = $r->input('timezone'); 
        $input['dids'] = json_encode($r->input('vici')); 
        $input['created_at'] = date("Y-m-d H:i:s");
        $input['updated_at'] = date("Y-m-d H:i:s");	
        


		$viciconn->create($input);
		$logger = new Logger;
		$logger->create($r,$input);



		if($input['type'] == "omnitrix"){


			
			$didinitial =  [];

			$vicilist = json_decode($input['dids']);

			$campaigns = $viciconn->select('id','dids','name','type')
					->whereIn('type',['omnitrix'])
					->get();

		

			foreach ($campaigns  as $key => $value) {


				$did = json_decode($value['dids']);
				foreach ($did as $key2 => $value2) {


					array_push($didinitial,$value2);

				}


			}



			$didinput = json_decode($input['dids']); 
			
			foreach ($didinput as $key3 => $value3) {
 
				array_push($didinitial,$value3);

			}

			$didunique = array_unique ($didinitial);

			$didlist = [];

			foreach ($didunique as $key4 => $value4) {

				array_push($didlist,$value4);
			}


				
			$viciconn->wherein('id',[202])->update(['dids' =>json_encode($didlist)]);	 
			 
		}
		//pre($viciconn);
		flash()->success('successfully created!');

		return back();

	}

	public function campaign_index(){

		 
		$viciconn = new Viciconn;
		$campaigns = $viciconn->select('id','dids','name','type')
			->whereNotIn('type',['productivity'])
			->whereIn('status',['1'])
			->get();
		$vici = new VICI;
        $vicidialin = $vici->ingroups('in');
        $vicidialout = $vici->ingroups('out');
		$restrictions = new Restrictions;
		return view('primrose.settings.campaign.campaign_index',compact('campaigns','restrictions','vicidialin','vicidialout'));
	}

	public function campaign_update(Request $r, $id){

		$viciconn = new Viciconn;
		$dids = json_encode($r['vici']); 

		$update = ['type' => $r['type'],'name' => $r['name'],'dids' => $dids];
		$campaigns = $viciconn->wherein('id',[$id])->update($update);
		
		$vici = new VICI;
        $vicidialin = $vici->ingroups('in');
        $vicidialout = $vici->ingroups('out');
		$restrictions = new Restrictions;
		$campaigns = $viciconn->whereIn('type',[$r['type']])->whereIn('status',['1'])->get();
		return view('primrose.settings.campaign.campaign_index',compact('campaigns','restrictions','vicidialin','vicidialout'));
	}

	public function campaign_edit($id){

	
		$vici = new VICI;
        $vicidialin = $vici->ingroups('in');
        $vicidialout = $vici->ingroups('out');
		$restrictions = new Restrictions;
		$viciconn = new Viciconn;
		$campaigns = $viciconn->wherein('id',[$id])->whereIn('status',['1'])->get();

		return view('primrose.settings.campaign.campaign_edit',compact('campaigns','restrictions','vicidialin','vicidialout'));
	}

	public function campaign_delete($id){

		$viciconn = new Viciconn;
		

		$update = ['status' => 0];
		$campaigns = $viciconn->wherein('id',[$id])->update($update);
		$msg = 'Data successfully removed!';
		flash()->success($msg);
		return back();

	}

	public function campaign_variable(Request $request){
		return 's';
		$viciconn = new Viciconn;
		$vici = new VICI;
        $vicidialin = $vici->ingroups('in');
        $vicidialout = $vici->ingroups('out');
		$restrictions = new Restrictions;
		$campaigns = $viciconn->wherein('type',[$request['key']])->get();
		return view('primrose.settings.campaign.campaign_variable',compact('campaigns','restrictions','vicidialin','vicidialout'));
	}

	public function productivity_index(){

		$viciconn = new Viciconn;
		$restrictions = new Restrictions;
		$campaigns = $viciconn->get();
		return view('primrose.settings.productivity.index',compact('campaigns','restrictions'));

	}

	public function productivity_select_server(){
		$restrictions = new Restrictions;
		return view('primrose.settings.productivity.select-server',compact('restrictions'));
	}

	public function productivity_add(){
		$help = new \App\Http\Helpers\primrose\Primrose;
		$timezones = $help->timezones();
	//	$gc = new \App\Http\Controllers\primrose\generalController;
		//pre($timezones );
		$vici = new VICI;
		$restrictions = new Restrictions;
        $vicidialin = $vici->ingroups('in');
        $vicidialout = $vici->ingroups('out');
		return view('primrose.settings.productivity.add',compact('vicidialout','vicidialin','server','restrictions','timezones','gc'));
		
	}


} 