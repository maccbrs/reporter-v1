<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Mail;
use DB;
use Carbon\Carbon;

class testController extends Controller
{

    public function index(){

        $count = 0;
        $obj = new \App\Http\Models\prepaid\Vici_closer;
        $ldo = new \App\Http\Models\prepaid\Load_details;
        $x = $obj
        ->select('uniqueid')
        // ->whereIn('campaign_id', 
        //     ['Prescripto',
        //     'Priscripto2',
        //     'Priscripto3',
        //     'Priscripto4',
        //     'Priscripto5',
        //     'Priscripto6',
        //     'Priscripto7',
        //     'Priscripto8',
        //     'Priscripto9',
        //     'Priscripto10',
        //     'Priscripto11',
        //     'Priscripto12',
        //     'Priscripto13',
        //     'PrescriptoDID1InOut',
        //     'PrescriptoSPAInOut'
        //     ])

        // ->whereIn('campaign_id', 
        //     [
        //     'OmniOracle'
        //     ]) 

        // ->whereIn('campaign_id', 
        //     [
        //         'OmniMoving',
        //         'OmniMoving2',
        //         'OmniContractor_2',
        //         'OmniContractor_3',
        //         'OmniContractor_4',
        //         'OmniContractor'
        //     ])         

        ->whereIn('campaign_id', 
            [
                'OmniBookMe',
            ])   
         ->whereBetween('call_date', ['2016-06-01 00:00:00','2016-07-01 23:59:59'])->get();

        foreach ($x as $v):
            $r = $ldo->where('uniqueid',$v->uniqueid)->count();
            if(!$r): 
                $ldo->insert(['load_id' => 39,'uniqueid' => $v->uniqueid,'is_reserved' => 0,'phoneno' => '3462230001']);
                $count++; 
            endif;
        endforeach;


            echo $count;

    } 
    
    public function load(){

        die;
        $loadObj = new \App\Http\Models\prepaid\Load;
        $loads = $loadObj->active()->with(['details'])->get();
        $results = []; 
        pre($loads->toArray());
        foreach ($loads as $load):

            $total_sec = 0;

            if($load->details):
                foreach ($load->details as $datail):
                    if($datail->closer && $datail->closer->agent):
                        $total_sec += $datail->closer->agent->talk_sec + $datail->closer->agent->dispo_sec;
                    endif;
                endforeach;
            endif;

            if(($load->minutes*60) <= $total_sec): $load->status = 0; endif;
            $load->used = ($total_sec/60);
            $load->remaining = (($load->minutes*60) - $total_sec)/60;
            $load->save();

        endforeach;

    }



	public function eod(){

            $test = true;
            $done = [];

            $dmboard = new \App\Http\Models\primrose\dummyboard\Board;
            $boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
            $boards = $boardsObj->where('id',49)->get();
            $gc = new \App\Http\Controllers\primrose\generalController;
            $tbl = new \App\Http\Helpers\primrose\Tables;
            $sendtimes = [];
            $current_date = date('Y-m-d');
            $current_time = date('H:i:s'); 
            $tym = date('H:i');
            $text = '';
            $container = [];

            foreach ($boards as $v): //$connector  

              //  if($gc->_options($v->options,'sendtime') == $tym):
               
                            $container[] = $gc->_options($v->options,'sendtime') .' - '. $tym;

                            $html_data = $html_data = '';
                            $logs = $data = $in = $templar_socket = [];
                           
                            if(!in_array($v->templar_socket, $done)):
                                $dates = $tbl->timerange($v->options); 
                                $dates['time'] = $current_time;
                                $board = $dmboard->whereIn('campaign_id',json_decode($v->templar_socket,true))->where('status',1)->first()->toArray();
                                $vici = DB::connection('192.168.200.132')->table(($v->bound == 'out'?'vicidial_log':'vicidial_closer_log'));                                
                                $done[] = $v->templar_socket;

                                if($board):
                                        $opt = json_decode($board['options']);
                                        $in = json_decode($v->vici_plug,true);
                                        $dates['campaigns'] = $in;

                                        if(!empty($in)):
                                            $logs = $vici->select('status',DB::raw('count(*) as count'))
                                                ->whereBetween('call_date', [$dates['fr'],$dates['to']])
                                                ->whereIn('campaign_id',$in)
                                                ->whereNotIn('status',['DROP','INCALL','QUEUE'])
                                                ->groupBy('status')
                                                ->orderBy('status', 'asc')
                                                ->get();  

                                        endif;

                                        if($test):

                                                $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
                                                $board['to'] = ['marlonbbernal@mailinator.com', 'howell.calabia@magellan-solutions.com'];
                                                $board['from'] = 'reporter@magellan-solutions.com';
                                                $board['cc'] = false;
                                                $board['bcc'] = false;
                                        else:
                                                $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
                                                $board['to'] = (!empty($board['to'])?$tbl->emailer_parser2($board['to']):false);
                                                $board['from'] = ($board['from'] != ''?$board['from']:'reporter@magellan-solutions.com');
                                                $board['cc'] = (!empty($board['cc'])?$tbl->emailer_parser2($board['cc']):false);
                                                $board['bcc'] = (!empty($board['bcc'])?$tbl->emailer_parser2($board['bcc']):false);

                                        endif;

                                    

                                        if(!empty($logs)):

                                            $html_logs = $tbl->html_logs($logs);  
                                            $data = DB::connection('dummyboard')->table('campaign_data')
                                                    ->whereIn('campaign_id',json_decode($v->templar_socket,true))
                                                    ->where('contents', 'like', '%"request_status":"live"%')
                                                    ->whereBetween('created_at',[$dates['fr'],$dates['to']])
                                                    ->get();

                                        else:

                                            $html_logs = $tbl->html_logs_blank();  
                                            $data = DB::connection('dummyboard')->table('campaign_data')
                                                    ->whereIn('campaign_id',json_decode($v->templar_socket,true))
                                                    ->where('contents', 'like', '%"request_status":"live"%')
                                                    ->whereBetween('created_at',[$dates['fr'],$dates['to']])
                                                    ->get();
                                            
                                            if(!$test):
                                                DB::connection('dummyboard')->table('eod_reports')->insert([
                                                    'date' => date('Y-m-d '),
                                                    'status' => 0,
                                                    'campaign' => $board['campaign']
                                                ]);
                                            endif;  
                                        endif;


                                        if(!empty($data)):
                                                $html_data = $tbl->html_data($data);
                                            endif;

                                            if(!$board['to']):
                                                    $board['to'] = 'NOC@magellan-solutions.com';
                                                    Mail::send('primrose.email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
                                                       $m->from($board['from'], 'Magellan Reporting Service');
                                                       $m->to($board['to'], $board['lob'])->subject('No Recipient eod!'); 
                                                    }); 
                                            else:

                                                if($v['id'] == 55):

                                                    $board['to'] = array("operations@magellan-solutions.com",   "maryan.anora@magellan-solutions.com", "julie.galinato@magellan-solutions.com");

                                                endif;

                                                Mail::send('primrose.email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
                                                   $m->from($board['from'], 'Magellan Reporting Service');
                                                   $m->to($board['to'], $board['lob'])->subject($board['subject']); 
                                                   if($board['cc'] && !$board['bcc']){
                                                     $m->to($board['to'], $board['lob'])->cc($board['cc'])->subject($board['subject']); 
                                                   }else if(!$board['cc'] && $board['bcc']){
                                                     $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->subject($board['subject']); 
                                                   }else if($board['cc'] && $board['bcc']){
                                                     $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->cc($board['cc'])->subject($board['subject']); 
                                                   }
                                                   
                                                }); 

                                                $status = 1;   

                                        endif;  



                                        if(!$test): 
                                            $cc = ($board['cc']?json_encode($board['cc']):'');
                                            $to = ($board['to']?json_encode($board['to']):'');
                                            $bcc = ($board['bcc']?json_encode($board['bcc']):'');
                                            DB::connection('dummyboard')->table('eod_reports')->insert([
                                                'to' => $to,
                                                'from' => $board['from'],
                                                'cc' => $cc,
                                                'bcc' => $bcc,
                                                'subject' => $board['subject'],
                                                'date' => date('Y-m-d '),
                                                'status' => $status,
                                                'campaign' => $board['campaign'],
                                                'options' => json_encode($dates),
                                                'content' => $html_logs.'<br>'.$html_data
                                            ]);
                                        endif;

                                endif; 

                            endif;                                

            //    endif;
            endforeach;

            DB::connection('mytestdb')->table('eodcron')->insert([
                'title' => 'test cron every 30 minute v4 '.$tym,
                'content' => json_encode($container),
                'date' => date('Y-m-d H:i:s')
            ]);

	}

} 