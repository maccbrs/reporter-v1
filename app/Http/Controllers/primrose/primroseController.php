<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\primrose\Primrose; 
use App\Http\Models\Campaign;
use Auth;

class primroseController extends Controller
{ 

	public function index(Request $r){

		$input = $r->all();
		$help = new Primrose;
		$restrictions = new Restrictions;
		$timezone = $help->timezones(true);
		//pre($restrictions->allowed('primrose.index'));
		return view('primrose.index',compact('timezone','restrictions'));

	}
	

} 