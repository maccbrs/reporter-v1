<?php namespace App\Http\Controllers\primrose;
use DB;

class generalController
{

	public function _query($camp,$f,$t,$tz1,$tz2){

		$help = new \App\Http\Helpers\primrose\Primrose;

		if($camp['server'] == '192.168.200.131'){

			$con = DB::connection('192.168.200.132');

		}else{

			$con = DB::connection('192.168.200.132');
		}

		$a = 	$con
				->table('vicidial_closer_log')
				//->select('lead_id', 'list_id', 'campaign_id', 'call_date', 'phone_number', 'user', 'status', 'queue_seconds', 'uniqueid', 'comments', 'length_in_sec')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();
	    if(!$a):
			$a =  $con
					->table('vicidial_log')
		            ->whereBetween('call_date', [$f, $t])
		            ->whereIn('campaign_id',json_decode($camp->dids))
		            ->get();
	    endif;

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	
	    	$c = $con
				->table('vicidial_list')
				->select('vendor_lead_code', 'lead_id', 'first_name', 'last_name', 'comments', 'email')
	            ->where('lead_id', '=' ,$av->lead_id)
	            ->get();

	        if($camp->dids == '["TOYNK"]'){
	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;
	        	$av->first_name = $c[0]->first_name;
	        	$av->last_name = $c[0]->last_name;
	        	$av->list_comment = $c[0]->comments;
	        	$av->email = $c[0]->email;
	        	
	        }else{

	        if(!empty($c[0]->vendor_lead_code)){

	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;
	        	$av->first_name = $c[0]->first_name;
	        	$av->last_name = $c[0]->last_name;
	        	$av->list_comment = $c[0]->comments;
	        	$av->email = $c[0]->email;
	    		}
	    	}

	    	$data[$av->uniqueid]['closer'] = $av;

	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;
	    endforeach;

		$b =  $con
				->table('vicidial_agent_log')
				//->select('user', 'pause_sec', 'wait_sec', 'talk_sec', 'dispo_sec', 'status', 'comments', 'pause_type', 'uniqueid', 'sub_status')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }

	    return $data;
	}

	public function _blockquery($camp,$f,$t){
		
		$a = DB::connection('192.168.200.132')
				->table('vicidial_closer_log')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();
	    if(!$a):
			$a =  DB::connection('192.168.200.132')
					->table('vicidial_log')
		            ->whereBetween('call_date', [$f, $t])
		            ->whereIn('campaign_id',json_decode($camp->dids))
		            ->get();
	    endif;

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	
	    	$c = DB::connection('192.168.200.132')
				->table('vicidial_list')
				->select('vendor_lead_code', 'lead_id')
	            ->where('lead_id', '=' ,$av->lead_id)
	            ->get();

	        if(!empty($c[0]->vendor_lead_code)){

	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;

	    	}

	    	$data[$av->uniqueid]['closer'] = $av;

	    endforeach;

		$b =  DB::connection('192.168.200.132')
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {

	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }


	    return $data;
	}

	public function _query_list($camp,$f,$t,$list_id){

		$help = new \App\Http\Helpers\primrose\Primrose;

		$a = DB::connection('192.168.200.132')
				->table('vicidial_closer_log')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();
	    if(!$a):
			$a =  DB::connection('192.168.200.132')
					->table('vicidial_log')
		            ->whereBetween('call_date', [$f, $t])
		            ->whereIn('campaign_id',json_decode($camp->dids))
		            ->get();
	    endif;

	   

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	
	    	$c = DB::connection('192.168.200.132')
				->table('vicidial_list')
				->select('vendor_lead_code', 'lead_id')
	            ->where('lead_id', '=' ,$av->lead_id)
	            ->get();



	        if(!empty($c[0]->vendor_lead_code)){

	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;

	    	}


	     
	    	$data[$av->uniqueid]['closer'] = $av;

	    	
	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;

	    endforeach;

		$b =  DB::connection('192.168.200.132')
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }


	    return $data;
	}

	public function _vendorquery($camp,$f,$t,$tz1,$tz2){

		$help = new \App\Http\Helpers\primrose\Primrose;

		$a = DB::connection('192.168.200.132')
				->table('vicidial_closer_log')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();
	    if(!$a):
			$a =  DB::connection('192.168.200.132')
					->table('vicidial_log')
		            ->whereBetween('call_date', [$f, $t])
		            ->whereIn('campaign_id',json_decode($camp->dids))
		            ->get();
	    endif;

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	$data[$av->uniqueid]['closer'] = $av;
	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;
	    endforeach;

		$b =  DB::connection('192.168.200.132')
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }
	    return $data;

	}

	public function _totals($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$abandoned = 0; $handled = 0;
		$l30 = 0; $l60 = 0; $g60 = 0; $handled2 = 0;
		$offered = 0;
		$rate = 0;
		$sl = 0;
		$tt = 0;
		$wt = 0;
		$wait = 0;
		$not_allow_dispo =  array("TEST", "TC","Test");
		$aht = 0;

		foreach ($data as $v): 
			 
			if(!in_array($v['closer']->status, $not_allow_dispo )): 
				if(!in_array($v['closer']->phone_number,$tn)):

					$offered++;

					if(in_array($v['closer']->status,$drops)): 
						$abandoned++;  
					else:
						$handled++;
						$qt = (isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0);
						$l30 = ($qt <= 30) ? $l30 + 1 : $l30;
						$l60 = ($qt <= 60) ? $l60 + 1 : $l60;
						$g60 = ($qt > 60) ? $g60 + 1 : $g60;
						if(isset($v['agent'])):
							$handled2++;
							$tt = $tt + $v['agent']->talk_sec;
							$wt = $wt + $v['agent']->dispo_sec;
							$wait = $wait + $v['agent']->wait_sec;
						endif;
					endif;

				endif;
			endif;

		endforeach;

//		pre($handled2);

		if($offered):
			$rate = round(($handled/$offered)*100,2);
		endif;

		if($handled):
			$sl = round(($l30/$handled)*100,2);
		endif;

		$talktime = $help->sToHms(round($tt,2));
		$wraptime = $help->sToHms(round($wt,2));
		$waittime = $help->sToHms(round($wait,2));

		$sum = $tt + $wt + $wait + $abandoned;


		if($tt != 0){

			$talk_ave =($tt / $handled2);
			$talk_ave = $help->sToHms($talk_ave);

		}else{

			$talk_ave = 0;

		}

		if($wt != 0){

			$wrap_ave = ($wt / $handled2);
			$wrap_ave = $help->sToHms($wrap_ave);

		}else{

			$wrap_ave = 0;
		}

		if($wait != 0){

			$wait_ave = ($wait / $handled2);
			$wait_ave = $help->sToHms($wait_ave);

		}else{

			$wait_ave = 0;
		}

		if($abandoned != 0){

			$drop_ave = round((($abandoned / $sum) * 100),2);

		}else{

			$drop_ave = 0;
		}

		$tt = ($tt == 0) ? 0 : $tt;
		$wt = ($wt == 0) ? 0 : $wt;
		$handled2 = ($handled2 == 0) ? 0 : $handled2;

		if($tt != 0 && $handled2 == 0 && $wt != 0){

		$aht = ($tt / $handled2) + ($wt / $handled2);
		$aht = $aht / $handled2;
		$aht = $help->sToHms($aht);

		}else{
			$aht = 0;
		}

		return [
			'offered' => $offered,
			'handled' => $handled,
			'handled2' => $handled2,
			'abandoned' => $abandoned,
			'answerRate' => $rate,
			'l30' => $l30,
			'l60' => $l60,
			'g60' => $g60,
			'sl' => $sl,
			'talktime' => $talktime,
			'wraptime' => $wraptime,
			'waittime' => $waittime,
			'talktime_ave' => $talk_ave,
			'wraptime_ave' => $wrap_ave,
			'waittime_ave' => $wait_ave,
			'drop_ave' => $drop_ave,
			'handled_ave' => $aht
		];
	}

	public function dispo($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$count = 0;
		$dispo = [];
		$result = [];
		$not_allow_dispo =  array("TC", "TEST CALL", "TEST", "Test");

		//pre($tn); 

		foreach ($data as $v):

			if(!in_array($v['closer']->status, $not_allow_dispo )):
				if(!in_array($v['closer']->phone_number,$tn)):
					$count++;
				//	if(!empty($v['closer']->status['count'])):
						
						if(array_key_exists($v['closer']->status, $dispo)):

							$dispo[$v['closer']->status]['count']++;
							//$dispo[$v['closer']->status]['count']++;
							if(isset($v['agent'])): 
								if(!isset($dispo[$v['closer']->status]['talktime'])): $dispo[$v['closer']->status]['talktime'] = 0; endif;
							    if(!isset($dispo[$v['closer']->status]['wraptime'])): $dispo[$v['closer']->status]['wraptime'] = 0; endif;
								$dispo[$v['closer']->status]['talktime'] += $v['agent']->talk_sec; 
								$dispo[$v['closer']->status]['wraptime'] += $v['agent']->dispo_sec;
							endif;
						else:
							$dispo[$v['closer']->status]['count'] = 1;
						    $dispo[$v['closer']->status]['name'] = $v['closer']->status;
							if(isset($v['agent'])): 
								$dispo[$v['closer']->status]['talktime'] = $v['agent']->talk_sec; 
								$dispo[$v['closer']->status]['wraptime'] = $v['agent']->dispo_sec;
							endif;					
						endif;
					//endif;
				endif;
			endif;
		endforeach;

		$total_wt = 0;
		$total_tt = 0;
		$total_call = 0;
		$total_aht = 0;

		foreach ($dispo as $k => $v):

			$result[$k] = $v;
			$result[$k]['percent'] = round(($v['count']/$count)*100,2).'%';
			$total_call += $v['count'];
			if(isset($v['talktime'])):
				//$help->sToHms(($v['talktime'] + $v['wraptime'])/$v['count']);  
				// $tempAht = ($v['talktime'] + $v['wraptime']) /$v['count']; 
				// $tempAht = round($tempAht, 2);
				$result[$k]['aht'] = $help->sToHms(($v['talktime'] + $v['wraptime'])/$v['count']);  
				$total_tt += round($v['talktime'],2);
				$total_wt += round($v['wraptime'],2);
			endif;
		endforeach;

		if($total_call): $total_aht = ($total_tt + $total_wt)/$total_call; endif;
			
		return [
			'dispo' => $result,
			'total_talktime' => $total_tt,
			'total_wraptime' => $total_wt,
			'total_call' => $total_call,
			'total_aht' => $help->sToHms($total_aht)
		];

	}

	public function orig_list($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tn = $help->tn();
		$result = [];
		$count = 0;

		foreach ($data as $v):

			$result[] = [
				'list_id' => $v['list']->list_id,
				'lead_id' => $v['list']->lead_id,
				'user' => $v['list']->user,
				'first_name' => $v['list']->first_name,
				'last_name' => $v['list']->last_name,
				'phone_number' => $v['list']->phone_number,
				'email' => $v['list']->email
			];
			$count++;
			
		endforeach;

		return [
			'list' => $result,
			'count' => $count
		];

	}

	public function dropped($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests(); 
		$drops = $help->drops();
		$tn = $help->tn();
		$result = [];
		$count = 0;

		foreach ($data as $v):
		
			if(in_array($v['closer']->status, $drops) && !in_array($v['closer']->phone_number, $tn)):
				$result[] = [
					'call_date' => $v['closer']->call_date,
					'campaign' => $v['closer']->campaign_id,
					'phone_number' => $v['closer']->phone_number,
					'status' => $v['closer']->status,
					'lead_id' => $v['closer']->lead_id,
					'list_id' => $v['closer']->list_id,
					'queuetime' => $help->sToHms(isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0),
					'uniqueid' => $v['closer']->uniqueid
				];
				$count++;
			endif;
		endforeach;

		return [
			'result' => $result,
			'count' => $count
		];

	}

	public function calls($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$count = 0;
		$a = [];

		foreach($data as $k => $v):

			// switch ($v['closer']->campaign_id) {
			//     case "OmniCouchPotato2":
			//         $v['closer']->campaign_id = "Monster Blaster";
			//         break;
			//     case "OmniCouchPotato3":
			//          $v['closer']->campaign_id = "Freedom Quit Smoking";
			//         break;
			//     case "OmniCouchPotato4":
			//          $v['closer']->campaign_id = "Febron";
			//         break;
			//     case "OmniStrainee1":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8663452534";
			//         break;
			//     case "OmniStrainee2":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8664603984";
			//         break;
			//     case "OmniStrainee3":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8664612144";
			//         break;
			//     case "OmniStrainee4":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667509031";
			//         break;
			//     case "OmniStrainee5":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667509179";
			//         break;
			//     case "OmniStrainee6":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667510121";
			//         break;
			//     case "OmniStrainee7":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667510537";
			//         break;
			//     case "OmniStrainee8":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667513198";
			//         break;
			//     case "OmniStrainee9":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667513201";
			//         break;
			//     case "OmniStrainee10":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667513630";
			//         break;
			//     case "OmniStrainee11":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667513949";
			//         break;
			//     case "OmniStrainee12":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667514410";
			//         break;
			//     case "OmniStrainee13":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667735997";
			//         break;
			//     case "OmniStrainee14":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667737329";
			//         break;
			//     case "OmniStrainee15":
			//          $v['closer']->campaign_id = $v['closer']->campaign_id . " 8667737621";
			//         break;


			// }

			if(!in_array($v['closer']->status, $drops) && !in_array($v['closer']->phone_number, $tn)):

				$count++;
				$a[] = [
					'user' => (isset($v['closer'])?$v['closer']->user:''),
					'call_date' => $v['closer']->call_date,
					'phone_number' => $v['closer']->phone_number,
					'campaign' => $v['closer']->campaign_id,
					'status' => $v['closer']->status,
					'talktime' => (isset($v['agent'])? $help->sToHms($v['agent']->talk_sec) : $help->sToHms($v['closer']->length_in_sec) ),
					'wraptime' => (isset($v['agent'])?$help->sToHms($v['agent']->dispo_sec):'0 : 0 : 0'),
					'waittime' => (isset($v['agent'])?$help->sToHms($v['agent']->wait_sec):'0 : 0 : 0'),
					'queuetime' => (isset($v['agent'])?$help->sToHms((isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0)):'0 : 0 : 0'), 
					'comment' => $v['closer']->comments,
					'uniqueid' => $v['closer']->uniqueid
				];

			endif;
		endforeach;

		return $a;

	}


	public function calls_out($data){
		
		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$count = 0;
		$a = [];

		foreach($data as $k => $v):
			if(!in_array($v['closer']->status, $drops) && !in_array($v['closer']->phone_number, $tn)):

				$count++;
				$a[] = [
					'user' => (isset($v['closer'])?$v['closer']->user:''),
					'call_date' => $v['closer']->call_date,
					'phone_number' => $v['closer']->phone_number,
					'campaign' => $v['closer']->campaign_id,
					'status' => $v['closer']->status,
					'talktime' => (isset($v['agent'])? $help->sToHms($v['agent']->talk_sec) : $help->sToHms($v['closer']->length_in_sec) ),
					'wraptime' => (isset($v['agent'])?$help->sToHms($v['agent']->dispo_sec):'0 : 0 : 0'),
					'waittime' => (isset($v['agent'])?$help->sToHms($v['agent']->wait_sec):'0 : 0 : 0'),
					'queuetime' => (isset($v['agent'])?$help->sToHms((isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0)):'0 : 0 : 0'), 
					'comment' => $v['closer']->comments,
					'uniqueid' => $v['closer']->uniqueid
				];

			endif;
		endforeach;
		return $a;

	}

	public function calls_break($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$count = 0;
		$a = [];
	
		foreach($data as $k => $v):
			
				$count++;
				$a[] = [
					'user' => (isset($v)?$v->user:''),
					'call_date' => $v->call_date,
					'campaign' => $v->campaign_id,
					'status' => $v->status,
					'substatus' => $v->sub_status,
					'total' => $v->talk_sec,
					'uniqueid' => $v->uniqueid
				];

		endforeach;

		return $a;

	}

	public function vendor($data){

		$help = new \App\Http\Helpers\primrose\Primrose; 
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$count = 0;
		$a = [];

		foreach($data as $k => $v):
			if(!in_array($v['closer']->status, $drops) && !in_array($v['closer']->phone_number, $tn)):
				
				$count++;
				$a[] = [
					'user' => (isset($v['closer'])?$v['closer']->user:''),
					'call_date' => $v['closer']->call_date,
					'phone_number' => $v['closer']->phone_number,
					'campaign' => $v['closer']->campaign_id,
					'status' => $v['closer']->status,
					'talktime' => (isset($v['closer'])? $help->sToHms($v['closer']->length_in_sec) : $help->sToHms($v['agent']->talk_sec)),
					'wraptime' => (isset($v['agent'])?$help->sToHms($v['agent']->dispo_sec):'0 : 0 : 0'),
					'waittime' => (isset($v['agent'])?$help->sToHms($v['agent']->wait_sec):'0 : 0 : 0'),
					'queuetime' => (isset($v['agent'])?$help->sToHms((isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0)):'0 : 0 : 0'), 
					'comment' => $v['closer']->comments,
					'uniqueid' => $v['closer']->uniqueid,
					'lead_id' => $v['closer']->lead_id,
					'vendor_lead_code' => isset($v['closer']->vendor_lead_code) ? $v['closer']->vendor_lead_code: '',
					'list_id' => $v['closer']->list_id,
					'sub_status' => (isset($v['agent']) ? $v['agent']->sub_status : " ")
				];

			endif;
		endforeach;
		return $a;

	}

	public function block($data){

		$help = new \App\Http\Helpers\primrose\Primrose; 
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$test = [];
		$ctr = 1;

		for ($i = 0; $i <= 23; $i++): 

			$hourminus = '-'.$ctr .' hour';

			$hrs[] = [
				'name' => sprintf("%02d", $i),
				'hour' => date("H:i:s"),
				'offered' => 0,
				'handled' => 0,
				'abandoned' => 0,
				'l30' => 0,
				'hour' => date('H',date(strtotime($hourminus, strtotime(date("Y-m-d H:i:s"))))) .':00',
				'date' => date('Y-m-d H:i:s',date(strtotime($hourminus, strtotime(date("Y-m-d H:i:s"))))),
			]; 

		$ctr++;
		endfor;
		
		foreach ($data as $v):

			$datetime = explode(' ', $v['closer']->call_date);
		    $time = explode(':',$datetime[1]);
		    $hr = (int)$time[0]; 
		    
			if(!in_array($v['closer']->status,$tests) && !in_array($v['closer']->phone_number,$tn)):
				
				$hrs[$hr]['date'] =  $v['closer']->call_date  ;	
		    	$hrs[$hr]['hour'] = date('H',date(strtotime( $v['closer']->call_date))).':00'  ;	
				$hrs[$hr]['offered']++;	
				
				if(in_array($v['closer']->status,$drops)): 
					$hrs[$hr]['abandoned']++;  
				else:
					$hrs[$hr]['handled']++;
					if($v['closer']->queue_seconds <= 30): $hrs[$hr]['l30']++;  endif;
				endif;
			endif;
			$ctr ++;
		endforeach;

		foreach ($hrs as $key => $part) {

		    $sort[$part['name']] = strtotime($part['date']);
		}
  
  		array_multisort($sort, SORT_DESC, $hrs);

  
		return $hrs;
	}

	public function _options($opt,$param = 'timezone'){
		$x = json_decode($opt,true);
		switch ($param):
			case 'timezone':
				return (isset($x[$param])?$x[$param]:'GMT');
				break;
			case 'from':
				return (isset($x[$param])?$x[$param]:'07:59:59');
				break;
			case 'to':
				return (isset($x[$param])?$x[$param]:'08:00:00');
				break;
			case 'fr_tz':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'to_tz':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'fr':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'to':
				return (isset($x[$param])?$x[$param]:'');
				break;	
			case 'ofr':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'oto':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'time':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'campaigns':
				return (isset($x[$param])?$x[$param]:'');
				break;
			case 'sendtime': 
				if(isset($x[$param])):
					$time = strtotime($x[$param]); 
					$new_date = date('Y-m-d H:i:s', $time); 
					$hour = date('H', $time); 
					$minute = date('i', $time); 
					
					return $hour.":".$minute ;				
				else:
					return '06:00';
				endif;
				break;																																								
		endswitch;
	}

	public function _update_options($opt,$tz,$param = 'timezone'){
		$x = json_decode($opt,true);
		$x[$param] = $tz;
		return json_encode($x);
	}

	public function _settime(){
		return date('H').':'.(date("i") > 30?'30':'00');		
	}	 

	public function roundsix($n){
		if($n):
			if($n<=30) return 30;
			$a = $n - 30;
			$abs = floor($a/6)*6;
			$mod = (($n%6)>0?6:0);
			return $abs+$mod+30;
		endif;
		return 0;
	}

	// public function roundsix($n){
	// 	$abs = floor($n/6)*6;
	// 	$mod = (($n%30)>0?30:0);
	// 	return $abs+$mod;
	// }

	public function _query_with_listname1010($camp,$f,$t,$tz1,$tz2){

		$help = new \App\Http\Helpers\primrose\Primrose;

		$a =  DB::connection('192.168.200.132')

				->table('vicidial_log')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();
	

	    $data = [];
	    $uids = [];

	 //   pre($a);

	    foreach ($a as $av):

	    	$c = DB::connection('192.168.200.132')
				->table('vicidial_list')
				->select('vicidial_list.vendor_lead_code', 'vicidial_list.lead_id', 'vicidial_lists.list_name')
				->leftJoin('vicidial_lists', 'vicidial_list.list_id', '=', 'vicidial_lists.list_id')
	            ->where('vicidial_list.lead_id', '=' ,$av->lead_id)
	            ->get();
	          
	         if(!empty($c[0]->vendor_lead_code)){

	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;
	        	$av->list_name = $c[0]->list_name;

	    	}

	    	$data[$av->uniqueid]['closer'] = $av;
	    	
	        if($tz1 != $tz2): 

	        	$data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); 

	        endif;
	    	$uids[] = $av->uniqueid;

	    endforeach;

		$b =  DB::connection('192.168.200.132')
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }

	    foreach ($data as $key => $value) {

			$data[$key]['list'] = DB::connection('192.168.200.132')
				->table('vicidial_list')
		        ->where('lead_id','=',$value['closer']->lead_id)
		        ->orderBy('list_id','desc')
		        ->first();
	    }

	    return $data;
	}


	public function _query_with_listname($camp,$f,$t,$tz1,$tz2){

		$help = new \App\Http\Helpers\primrose\Primrose;

		$a = DB::connection('192.168.200.132')
				->table('vicidial_closer_log')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();
	    if(!$a):
			$a =  DB::connection('192.168.200.132')
					->table('vicidial_log')
		            ->whereBetween('call_date', [$f, $t])
		            ->whereIn('campaign_id',json_decode($camp->dids))
		            ->get();
	    endif;

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	
	    	$c = DB::connection('192.168.200.132')
				->table('vicidial_list')
				->select('vicidial_list.vendor_lead_code', 'vicidial_list.lead_id', 'vicidial_lists.list_name')
				->leftJoin('vicidial_lists', 'vicidial_list.list_id', '=', 'vicidial_lists.list_id')
	            ->where('vicidial_list.lead_id', '=' ,$av->lead_id)
	            ->get();
	          
	         if(!empty($c[0]->vendor_lead_code)){

	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;
	        	$av->list_name = $c[0]->list_name;

	    	}

	    	$data[$av->uniqueid]['closer'] = $av;

	    	
	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;

	    endforeach;

		$b =  DB::connection('192.168.200.132')
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }

	    foreach ($data as $key => $value) {

			$data[$key]['list'] = DB::connection('192.168.200.132')
				->table('vicidial_list')
		        ->where('lead_id','=',$value['closer']->lead_id)
		        ->orderBy('list_id','desc')
		        ->first();
	    }

	    return $data;
	}

	public function _query_list_id($list_id,$f = null ,$t = null,$tz1 = null,$tz2 = null){

		$help = new \App\Http\Helpers\primrose\Primrose;

		$a = DB::connection('192.168.200.132')
				->table('vicidial_log')
	            ->whereIn('list_id',[$list_id])
	            ->get();
    
	    if(!$a):
			$a =  DB::connection('192.168.200.132')
					->table('vicidial_log')
		            ->whereIn('list_id',json_decode($list_id))
		            ->get();
	    endif;

	    $data = [];
	    $uids = [];

	    foreach ($a as $av):

	    	if(!empty($av->lead_id)):
	    	
		    	$c = DB::connection('192.168.200.132')
					->table('vicidial_list')
					->leftJoin('vicidial_lists', 'vicidial_list.list_id', '=', 'vicidial_lists.list_id')
					->select('vicidial_list.vendor_lead_code', 'vicidial_list.lead_id', 'vicidial_lists.list_name')
		            ->where('lead_id', '=' ,$av->lead_id)
		            ->get();

		      
		        $av->lead_id2 = $c[0]->lead_id;
		        $av->list_name = $c[0]->list_name;

		    	$data[$av->uniqueid]['closer'] = $av;

		    endif;

	    endforeach;


		$b =  DB::connection('192.168.200.132')
				->table('vicidial_agent_log')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }
	    

	    return $data;
	}

	public function listname($data){

		$help = new \App\Http\Helpers\primrose\Primrose; 
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$count = 0;
		$a = [];
		
		foreach($data as $k => $v):
			if(!in_array($v['closer']->status, $drops) && !in_array($v['closer']->phone_number, $tn)):
				
				$count++;
				$a[] = [
					'user' => (isset($v['closer'])?$v['closer']->user:''),
					'call_date' => $v['closer']->call_date,
					'phone_number' => $v['closer']->phone_number,
					'campaign' => $v['closer']->campaign_id,
					'status' => $v['closer']->status,
					'lead_id' => $v['closer']->lead_id,
					'vendor_lead_code' => isset($v['closer']->vendor_lead_code) ? $v['closer']->vendor_lead_code: '',
					'list_id' => isset($v['closer']->list_id) ? $v['closer']->list_id : '',
					'list_name' => isset($v['closer']->list_name) ? $v['closer']->list_name : '',
					'talktime' => (isset($v['agent'])? $help->sToHms($v['agent']->talk_sec) : $help->sToHms($v['closer']->length_in_sec) ),
					'wraptime' => (isset($v['agent'])?$help->sToHms($v['agent']->dispo_sec):'0 : 0 : 0'),
					'waittime' => (isset($v['agent'])?$help->sToHms($v['agent']->wait_sec):'0 : 0 : 0'),
					'queuetime' => (isset($v['agent'])?$help->sToHms((isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0)):'0 : 0 : 0'), 
					'first_name' => isset($v['closer']->first_name) ? $v['closer']->first_name : '',
					'last_name' => isset($v['closer']->last_name) ? $v['closer']->last_name : '',
					'comments' => isset($v['closer']->list_comment) ? $v['closer']->list_comment : '',
					'email' => isset($v['closer']->email) ? $v['closer']->email : '',
				];

			endif;
		endforeach;
		return $a;
	}

	public function clientportal($model_data,$data,$from,$to){

		$updated = $model_data
			->whereIn('campaign_id',['EBOOK','SOLARW','Sophos','BRISBANE'])
			->whereBetween('created_at', [$from, $to])
			->get(); 

		foreach ($updated as $key => $value) {

			$updated[$key]['content2'] = json_decode($value['content']);
	
		}

	    return $updated;
	}

	public function rowunique($data){

		$row_head = array(); 
	

		foreach ($data as $key => $value) {



			if(!empty($value['content2'])){

				foreach ($value['content2'] as $key2 => $value2) {

					$row_head[] = $key2;
				}
			}

		}

		$row_head = array_unique($row_head);

	    return $row_head;
	}

	
}  



