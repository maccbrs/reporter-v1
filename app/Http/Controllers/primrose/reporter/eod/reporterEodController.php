<?php namespace App\Http\Controllers\primrose\reporter\eod;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\primrose\loggerController as Logger;
use App\Http\Controllers\primrose\generalController as Helper;
use App\Http\Models\primrose\Viciconn;
use App\Http\Helpers\primrose\Primrose; 
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\primrose\sunflower\Eod;
use DB;
use Config;

class reporterEodController extends Controller
{ 

	public function index(){

		$eod = new Eod;
		$restrictions = new Restrictions;
		$lists = $eod->select(DB::raw('count(*) as count, date'))->groupBy('date')->orderBy('date','desc')->paginate(13);
		
		return view('primrose.reporter.eod.index',compact('restrictions','lists'));

	}

	public function eodGenerator(Request $r){

		$test = false;
        $done = [];

        $restrictions = new Restrictions;
        $dmboard = new \App\Http\Models\primrose\dummyboard\Board;
        $boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
        $boards = $boardsObj->orderBy('created_at', 'desc')->wherein('id',[$r->input("campaigns")])->get();
        $gc = new \App\Http\Controllers\primrose\generalController;
        $tbl = new \App\Http\Helpers\primrose\Tables;
        $sendtimes = [];
        $current_date = date('Y-m-d');
        $tym = date('H:i');
        $current_time = date('H:i:s'); 
        $current_hour = date('H:i');

        $precision = 30;
        $timestamp = strtotime($current_hour);
        $precision = 60 * $precision;
        $current_time_hour = date('H:i', round($timestamp / $precision) * $precision);

        $connector = new \App\Http\Models\primrose\Connector;
		$lists = $connector
			->select('name','id')
	        ->orderBy('name', 'asc')
	        ->get(); 
        
        $text = '';
        $container = [];

        foreach ($boards as $v): //$connector   

            $hourset = date("H:i", strtotime($gc->_options($v->options,'sendtime')));

            $precision2 = 30;
            $timestamp2 = strtotime($hourset);
            $precision2 = 60 * $precision2;
            $current_settime = date('H:i', round($timestamp2 / $precision2) * $precision2);
            
                $html_data = $html_data = '';
                $logs = $data = $in = $templar_socket = [];

                if(!in_array($v->templar_socket, $done)):
                    $dates = $tbl->timerange($v->options); 
                    $dates['time'] = $current_time;
                    $board = $dmboard->whereIn('campaign_id',json_decode($v->templar_socket,true))->where('status',1)->first()->toArray();
                    $vici = DB::connection('192.168.200.132')->table(($v->bound == 'out'?'vicidial_log':'vicidial_closer_log'));                                
                    $done[] = $v->templar_socket;
                    $bound = $v->bound == 'out'?'vicidial_log':'vicidial_closer_log';
					
					$dates['fr'] = $r->input('from');
					$dates['to'] = $r->input('to');

                    if($board):

                        $opt = json_decode($board['options']);
                        $in = json_decode($v->vici_plug,true);
                        $dates['campaigns'] = $in;
						
                        $count = $vici
                            ->select(DB::raw('count(*) as count'))
                            ->whereBetween($bound.'.call_date', [$dates['fr'],$dates['to']])
                            ->whereIn($bound.'.campaign_id',$in)
                            ->whereNotIn($bound.'.status',['DROP','INCALL','QUEUE'])
                            ->groupBy($bound.'.status')
                            ->orderBy($bound.'.status', 'asc')
                            ->get();

                        $logs = $vici->leftJoin('vicidial_campaign_statuses', 'vicidial_campaign_statuses.status', '=', $bound.'.status')
                            ->select($bound.'.status','vicidial_campaign_statuses.status_name',DB::raw('count(*) as count'))
                            ->whereBetween($bound.'.call_date', [$dates['fr'],$dates['to']])
                            ->whereIn($bound.'.campaign_id',$in)
                            ->whereNotIn($bound.'.status',['DROP','INCALL','QUEUE'])
                            ->groupBy($bound.'.status')
                            ->orderBy($bound.'.status', 'asc')
                            ->get(); 

                        foreach ($logs as $key => $value) {
                                
                            $logs[$key]->count = $count[$key]->count;
                               
                        }

                          
                     //   if(!empty($logs)):

                            $html_logs = $tbl->html_logs($logs);  
                            $data = DB::connection('dummyboard')->table('campaign_data')
                                ->whereIn('campaign_id',json_decode($v->templar_socket,true))
                                ->where('contents', 'like', '%"request_status":"live"%')
                                ->whereBetween('created_at',[$dates['fr'],$dates['to']])
                                ->get();

                        //    pre($data );

                            if(!empty($data)):

                                $html_data = $tbl->convert_time_drc($data, $dates['to_tz'] );
                                $html_data = $tbl->html_data($data);
                                
                            endif;

                       // endif;  

                    endif; 

                endif;                                

        endforeach;

		return view('primrose.reporter.eod.eodGenerator',compact('html_logs','html_data','lists','restrictions'));

	}

    public function eodGeneratorPage(Request $r){


        $items = false;
        $PageConnectors = new \App\Http\Models\primrose\connectors\PageConnectors;
        $lists = $PageConnectors->get();
        $restrictions = new Restrictions;

        if($r->pageconid):

            $this->validate($r,[
                'from' => 'required',
                'to' => 'required'
            ]);

            $item = $PageConnectors->find($r->pageconid);
            if($item && $item->pageids):

                $CampaignData = new \App\Http\Models\primrose\dummyboard\CampaignData;
                $items = $CampaignData->whereIn('pageid',json_decode($item->pageids,true))->whereBetween('created_at', [$r->from,$r->to])->get();
            endif;

        endif;

        return view('primrose.reporter.eod.eodGeneratorPage',compact('items','restrictions','lists'));

    }


	public function show_eod(){

		$eod = new Eod;
		$restrictions = new Restrictions;
		$lists = $eod->select(DB::raw('count(*) as count, date'))->groupBy('date')->orderBy('date','desc')->paginate(13);
		
		return view('primrose.reporter.eod.index',compact('restrictions','lists'));

	}

	public function date($date){
		$eod = new Eod;
		$restrictions = new Restrictions;
		$data = $eod->where('date',$date)->orderBy('status','desc')->paginate(10);
		//pre($data->toArray());
		return view('primrose.reporter.eod.date',compact('restrictions','data'));
	}

	public function show($id){
		$eod = new Eod;
		$helper = new Helper;
		$restrictions = new Restrictions;
		$data = $eod->where('id',$id)->first();

		return view('primrose.reporter.eod.show',compact('restrictions','data','helper'));
	}

	public function detail($id){

		$restrictions = new Restrictions;
		$helper = new Helper;
		$result = [];
		$eod = new Eod;
		$data = $eod->where('id',$id)->first();
		$campaigns = $helper->_options($data['options'],'campaigns');

		if($campaigns){
			$fr = $helper->_options($data['options'],'fr');
			$to = $helper->_options($data['options'],'to');
			$vici = new \App\Http\Models\primrose\VICI;
			$result = $vici->select('call_date','status','user','campaign_id','uniqueid')
				->whereBetween('call_date',[$fr,$to])
	            ->whereIn('campaign_id',$campaigns)
	            ->whereNotIn('status',['DROP','INCALL','QUEUE'])
	            ->orderBy('call_date', 'asc')
	            ->get(); 
        }
		return view('primrose.reporter.eod.detail',compact('restrictions','result'));

	}

    public function submitted_data(){

        $restrictions = new Restrictions;
        $dummy = new \App\Http\Models\primrose\dummyboard\CampaignData;
        $dummydata = $dummy
            ->where('campaign_id','=','58fe45c2685f8')
           // ->whereBetween('call_date',[$fr,$to])
            ->limit(50)
            ->orderBy('id','desc')
            ->get();

        foreach ($dummydata as $key => $value) {
            $dummydata[$key]['content_data'] = json_decode($value['contents']);
           
        }

        $type = 'nomnitrix';

        $vcon = new Viciconn; 
        $lists = $vcon->where('type',$type)
            ->where('status',1)
            ->orderby('name','asc')
            ->get();    

        return view('primrose.reporter.eod.data',compact('restrictions','dummydata','lists'));
    }

    public function submitted_data_post(Request $r){


        $this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'campaigns' => 'required'
        ]);

        $restrictions = new Restrictions;
        $help = new Primrose;
        $params = [];
        $vcon = new Viciconn; 
        $dummydata = null;
        $dummy_connector= null;
        $sorted_all = array();

        $lists = $vcon->where('type','nomnitrix')
            ->where('status',1)
            ->orderby('name','asc')
            ->get();  

        $camp = $vcon->where('id',$r->input('campaigns'))->first(); 
        $transfer = array();
        $new = array();           
        $all = array();                                            

        if($camp->toArray()):

            $timezone = $help->timezones(true);
        
            $timez = 'Asia/Manila';
            $genCtlr = new \App\Http\Controllers\primrose\generalController;
            $data = $genCtlr->_query($camp,$r->input('from'),$r->input('to'),$timez,Config::get('app.timezone')); 
            $calls = $genCtlr->calls($data);
            $total = $genCtlr->_totals($data);
            $params = [
            'from' => $r->input('from'),
            'to' => $r->input('to'),
            'timezone' => $timez,
            'campaigns' => $camp->name
            ];
        
        endif;

        $dummy_connector = $vcon
            ->select('dummyboard')
            ->where('id',$r->input('campaigns'))
            ->first();



        if(!empty($dummy_connector['dummyboard'])){

            $campaign = json_decode($dummy_connector['dummyboard']);

            pre('$dummy_connector');

            $dummy = new \App\Http\Models\primrose\dummyboard\CampaignData;
            $dummydata = $dummy
                ->whereIn('campaign_id',$campaign)
                ->whereBetween('created_at',[$r->input('from'),$r->input('to')])
                ->orderBy('id','desc')
                ->get();

            

            foreach ($dummydata as $key => $value) {

                $content_decoded = json_decode($value['contents']);
                $dummydata[$key]['content_data'] = $content_decoded;

                $all[] =  $content_decoded;

                if($content_decoded->disposition == 'MEETSCH'){

                    $new[] =  $content_decoded;

               }
                
                if($content_decoded->disposition == 'TRANSFER'){

                    $transfer[] =  $content_decoded;

                }
            }

            $sorted_all = $this->sort($all);

        }

        return view('primrose.reporter.eod.data_post',compact('restrictions','dummydata','lists','calls','total','new','sorted_all','transfer','timezone'));
    }

    public function emailer(){

        $restrictions = new Restrictions;

        $emailer = new \App\Http\Models\primrose\dummyboard\Emailer;

        $emailer_list = $emailer
            ->groupBy('subject')
            ->pluck('subject','id');

        return view('primrose.reporter.eod.emailer',compact('restrictions','emailer_list'));
    }

    public function emailer_post(Request $r){


        $this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'campaign' => 'required'
        ]);

        $restrictions = new Restrictions;
        
        $help = new Primrose;
        $timezone = $help->timezones(true);

        $emailer = new \App\Http\Models\primrose\dummyboard\Emailer;

        $emailer_list = $emailer
            ->groupBy('subject')
            ->pluck('subject','id');

        $emailer_data = $emailer
            ->where('subject','=',$r->input('campaign'))
            ->whereBetween('created_at',[$r->input('from'),$r->input('to')])
            ->get();

        $content_decoded = null;

        foreach ($emailer_data as $key => $value) {
           $content_decoded[] = json_decode($value['content']);
        }

        return view('primrose.reporter.eod.emailer_post',compact('restrictions','emailer_list','content_decoded','timezone','help'));
    }

    public function sort($data){
        $ctr = 0;
        foreach ($data as $key => $value) {
            $ctr ++;
           
            foreach ($value as $key2 => $value2) {
                  if(!empty($key2)){
                    $dummydata['head'][$key2][$ctr] = $value2;
                    $dummydata['content'][$ctr][$key2] = $value2;
                  }
            }

        }

        return $dummydata;
    }


 
}