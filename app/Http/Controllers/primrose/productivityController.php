<?php namespace App\Http\Controllers\primrose;

use App\Http\Controllers\primrose\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\primrose\Primrose; 
use App\Http\Models\primrose\VICI;
use App\Http\Models\primrose\Viciconn;
use Config;
use DB;
use Excel;

class productivityController extends Controller{ 

	public function index(){
		
		$gn = new \App\Http\Controllers\primrose\generalController;
		//echo 'mentainance ongoing';die;
		$help = new Primrose;
		$viciconn = new Viciconn;
		$campaigns = $viciconn->where('type','productivity')->orderby('name','asc')->get();
		$restrictions = new Restrictions;
		$timezone = $help->timezones(true);
		return view('primrose.productivity.index',compact('timezone','restrictions','campaigns')); 

	}  

	public function show(Request $r){ 


		$tbl = new \App\Http\Helpers\primrose\Tables;
		$gn = new \App\Http\Controllers\primrose\generalController;

        $this->validate($r, [
            'from' => 'required',
            'to' => 'required',
            'campaigns' => 'required'
        ]);

        $param = $r->all();
        $data = false;
        $viciconn = new Viciconn;	
        $campaigns = $viciconn->where('type','productivity')->get();
      
		$restrictions = new Restrictions;
        $ids = $viciconn->select('dids','server','timezone','bound')->where('id',$r->input('campaigns'))->first();

        $timezone = $ids['timezone'];
        $from = $r->input('from');
        $to = $r->input('to');
 		
        $dates = $tbl->timerange_prod($timezone,$from,$to); 

        $from = date("Y-m-d H:i", strtotime($dates['fr'])); 
        $to = date("Y-m-d H:i", strtotime($dates['to'])); 
        
        $crc = false;	
        $summary = false;
        $return = false;

        if($ids->toArray()):
        	//pre($ids->toArray());
			$vo = new \App\Http\Models\primrose\ViciCloser;

		    if($ids['bound'] == 'out'):

				$vo = new \App\Http\Models\primrose\ViciOut;

		    endif;
			
			if(isset($param['dispo'])):
				$return = $vo->whereIn('campaign_id',json_decode($ids['dids']))
							->whereBetween('call_date',[$from ,$to])
							->whereNotIn('status', $param['dispo'])
							->with(['agent'])->get();						
			else:
				$return = $vo->whereIn('campaign_id',json_decode($ids['dids']))
							->whereBetween('call_date',[$from ,$to])
							->with(['agent'])->get();
			endif;	

			//pre($return->toArray());
			$return = $tbl->timerange_data($return,$timezone);
	
	    	$dispo = $this->_dispo($return);
	    	$dates = $this->_dates($return);
	   		$summary = $this->_daily_summary($return);
	    	$crc = $this->_daily_crc($return);
		    			    
        endif;
   			

        return view('primrose.productivity.show',compact('summary','crc','return','restrictions','dates', 'campaigns','param','dispo','gn'));

	}


	public function downloadExcel(Request $r)
	{
		$tbl = new \App\Http\Helpers\primrose\Tables;
		$viciconn = new Viciconn;
		$ids = $viciconn->select('dids','server','bound')->where('id',$r->campaigns)->first();	//pre($ids->toArray());

		if($ids):

			$vo = new \App\Http\Models\primrose\ViciCloser;

		    if($ids['bound'] == 'out'):

				$vo = new \App\Http\Models\primrose\ViciOut;

		    endif;

			$carbon = new \Carbon\Carbon;
			$from = $carbon->createFromFormat('Y/m/d H:i',$r->from)->format('Y-m-d H:i:s');
			$to = $carbon->createFromFormat('Y/m/d H:i',$r->to)->format('Y-m-d H:i:s');  
			$ids = $viciconn->select('dids','server','timezone')->where('id',$r->input('campaigns'))->first();

        	$timezone = $ids['timezone'];	


			$dates = $tbl->timerange_prod($timezone,$from,$to); 

			$from = date("Y-m-d H:i", strtotime($dates['fr'])); 
        	$to = date("Y-m-d H:i", strtotime($dates['to'])); 


			if(isset($r->dispo) && $r->dispo != ''):
				
				$exclude = json_decode($r->dispo); 
				$return = $vo->whereIn('campaign_id',json_decode($ids->dids))
							->whereBetween('call_date',[$from,$to])
							->whereNotIn('status' , $exclude)
							->with(['agent'])->get();
			
			else:
				$return = $vo->whereIn('campaign_id',json_decode($ids->dids))
							->whereBetween('call_date',[$from,$to])
							->with(['agent'])->get();	
			endif;	

				$return = $tbl->timerange_data($return,$timezone);
			
		    	$dispo = $this->_dispo($return);
		    	$dates = $this->_dates($return);
		   		$summary = $this->_daily_summary($return);
		    	$crc = $this->_daily_crc($return);
		    	$gn = new \App\Http\Controllers\primrose\generalController;
		  

				Excel::create('data', function($excel) use ($dates,$crc,$dispo,$summary,$return,$gn) {
				    $excel->sheet('Sheet1', function($sheet) use ($dates,$crc,$dispo,$summary,$return,$gn) {

				    	    $sheet->mergeCells('B2:B3');
							$sheet->cell('B2', function($cell){
							    $cell->setValue('CRC');
							    $cell->setBackground('#FFFF99');
							    $cell->setAlignment('center');
							    $cell->setValignment('center');
							});	

				    		$ldates = 'C';
							foreach ($dates as $dk => $dv):
								$sheet->cell($ldates.'2', function($cell) use ($dk){
								    $cell->setValue($dk);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});
								$sheet->cell($ldates.'3', function($cell) use ($dv){
								    $cell->setValue($dv);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});							
								$ldates++;
							endforeach;

							$dinum = 4;
							foreach ($dispo as $dik => $div):
								$sheet->cell('B'.$dinum, function($cell) use ($div){
								    $cell->setValue($div);
								});
								$dinum++;									
							endforeach;

							$crc_n = 4;
							foreach ($crc as $ck => $cv):
								$crc_l = 'C';
								foreach ($cv as $ck2 => $cv2):
									$sheet->cell($crc_l.$crc_n, function($cell) use ($cv2){
									    $cell->setValue($cv2['data']['count']);
									});
									$crc_l++;
								endforeach;
								$crc_n++;
							endforeach;

							$second_group = $crc_n + 2;
							$second_group2 = $second_group +1;
				    	    $sheet->mergeCells('B'.$second_group.':B'.$second_group2);
							$sheet->cell('B'.$second_group, function($cell){
							    $cell->setValue('CRC');
							    $cell->setBackground('#FFFF99');
							    $cell->setAlignment('center');
							    $cell->setValignment('center');
							});	

				    		$ldates2 = 'C';
							foreach ($dates as $dk => $dv):
								$sheet->cell($ldates2.$second_group, function($cell) use ($dk){
								    $cell->setValue($dk);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});
								$sheet->cell($ldates2.$second_group2, function($cell) use ($dv){
								    $cell->setValue($dv);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});							
								$ldates2++;
							endforeach;

							$dinum2 = $second_group2+1;
							foreach ($dispo as $dik => $div):
								$sheet->cell('B'.$dinum2, function($cell) use ($div){
								    $cell->setValue($div);
								});
								$dinum2++;									
							endforeach;

							$crc_n2 = $second_group2+1;
							foreach ($crc as $ck => $cv):
								$crc_l = 'C';
								foreach ($cv as $ck2 => $cv2):
									$sheet->cell($crc_l.$crc_n2, function($cell) use ($cv2){
									    $cell->setValue($cv2['data']['percent'].'%');
									    $cell->setAlignment('right');
									});
									$crc_l++;
								endforeach;
								$crc_n2++;
							endforeach;


							$third_group = $crc_n2 + 2;
							$third_group2 = $third_group + 1;
							$titles = ['Day','Date','Connect Mins','Talk Mins','Wrap Mins','Handle Mins','Mins Billed'];
							$title_l = 'B';

							foreach ($titles as $k => $v):
								$sheet->cell($title_l.$third_group2, function($cell) use($v){
								    $cell->setValue($v);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								    $cell->setValignment('center');
								});	
								$title_l++;
							endforeach;


							$third_group3 = $third_group2 +1;
							foreach ($dates as $dk => $dv):
								$sheet->cell('C'.$third_group3, function($cell) use ($dk){
								    $cell->setValue($dk);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});
								$sheet->cell('B'.$third_group3, function($cell) use ($dv){
								    $cell->setValue($dv);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								});							
								$third_group3++;
							endforeach;

							$third_group4 = $third_group2 +1;
							foreach ($summary as $k => $v):
								$sheet->cell('D'.$third_group4, function($cell) use ($v){
								    $cell->setValue($v['totalm']);
								});
								$sheet->cell('E'.$third_group4, function($cell) use ($v){
								    $cell->setValue($v['talkm']);
								});
								$sheet->cell('F'.$third_group4, function($cell) use ($v){
								    $cell->setValue($v['wrapm']);
								});	
								$sheet->cell('G'.$third_group4, function($cell) use ($v){
								    $cell->setValue($v['totalm']);
								});	
								$sheet->cell('H'.$third_group4, function($cell) use ($v){
								    $cell->setValue($v['totalm']);
								});																														
								$third_group4++;									
							endforeach;

							$fourthGroup = $third_group4 + 2;
							$fourthGroup2 = $third_group4 + 3;
							$agentTitles = ['Agent Name','Call Date','Phone Number','Campaign','Crc','Connect Time','TalkTime','Wraptime'];

							$agent_t = 'B';
							foreach ($agentTitles as $k => $v):
								$sheet->cell($agent_t.$fourthGroup, function($cell) use ($v){
								    $cell->setValue($v);
								    $cell->setBackground('#FFFF99');
								    $cell->setAlignment('center');
								    $cell->setValignment('center');								    
								});
								$agent_t++;	
							endforeach;

							foreach ($return as $k=> $v):
								$sheet->cell('B'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v->user);
								});	
								$sheet->cell('C'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v->call_date);
								});	
								$sheet->cell('D'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v->phone_number);
								});
								$sheet->cell('E'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v->campaign_id);
								});	
								$sheet->cell('F'.$fourthGroup2, function($cell) use ($v){
								    $cell->setValue($v->status);
								});	
								if($v->agent):
									$sheet->cell('G'.$fourthGroup2, function($cell) use ($v,$gn){
										$total = $gn->roundsix($v->agent['talk_sec'] + $v->agent['dispo_sec']);
									    $cell->setValue($total);
									});
									$sheet->cell('H'.$fourthGroup2, function($cell) use ($v,$gn){
									    $cell->setValue($v->agent['talk_sec']);
									});	
									$sheet->cell('I'.$fourthGroup2, function($cell) use ($v){
									    $cell->setValue($v->agent['dispo_sec']);
									});									
								endif;

								$fourthGroup2++;																																										
							endforeach;

				    });
				})->export('xls');

		else:
			return redirect()->back();
		endif;


		pre($ids->toArray());
		pre($r->all());
		//die;
		Excel::create('productivity excelx', function($excel) {
		    $excel->sheet('Excel sheet', function($sheet) {
		        $sheet->setOrientation('landscape');
		    });
		})->export('xls');
		die;

	} 


	private function _dates($data){
		$dates = [];
		if($data):
			foreach ($data as $d):
				$a = explode(' ', $d->call_date);
				if(!array_key_exists($a[0],$dates)):
					$dates[$a[0]] = date("D",strtotime($a[0]));
				endif;
			endforeach;
		endif;
		return $dates;
	}

	private function _daily_summary($data){

		$gn = new \App\Http\Controllers\primrose\generalController;
		$return = [];
		$return2 = [];

		if($data):

			foreach($data as $d):

				$talk = (isset($d->agent->talk_sec)?$gn->roundsix($d->agent->talk_sec):0);
				$wrap = (isset($d->agent->dispo_sec)?$d->agent->dispo_sec:0);

				$time = explode(' ',date("Y m/d H:i:s D",strtotime($d->call_date)));
				$return[$time[1]]['date'] = $time[2];
				$return[$time[1]]['talk'] = (isset($return[$time[1]]['talk'])?$return[$time[1]]['talk'] + $talk : $talk);
				$return[$time[1]]['wrap'] = (isset($return[$time[1]]['wrap'])?$return[$time[1]]['wrap'] + $wrap : $wrap);
				$return[$time[1]]['total'] = (isset($return[$time[1]]['total'])?$return[$time[1]]['total'] + $wrap + $talk : $wrap + $talk);
				$return[$time[1]]['day'] =  $time[3];
			endforeach;

			foreach ($return as $k => $v):
				$return2[$k] = $v;
				$return2[$k]['talkm'] = round($v['talk'] / 60,2);
				$return2[$k]['wrapm'] = round($v['wrap'] / 60,2);
				$return2[$k]['totalm'] = round($v['total'] / 60,2);
			endforeach;

			return $return2;

		endif;

		return false;
	}


	private function _daily_crc($data){

		$return = [];
		$return2 = [];
		$return3 = [];
		$countsArr = [];
		if($data):

			$crc = [];
			$dates = [];

			foreach ($data as $d):
				$time = explode(' ',date("Y m/d H:i:s D",strtotime($d->call_date)));
				if(!in_array($d->status, $crc)):
					$crc[] = $d->status;
				endif;
				if(!in_array($time[1], $dates)):
					$dates[] = ['date' => $time[1],'day' => $time[3]];
				endif;				
			endforeach;

			foreach($data as $d):
				$time = explode(' ',date("Y m/d H:i:s D",strtotime($d->call_date)));
				$return[$time[1].' '.$d->status] = (isset($return[$time[1].' '.$d->status])? $return[$time[1].' '.$d->status] + 1:1);
				$countsArr[$time[1]] = (isset($countsArr[$time[1]])? (int)$countsArr[$time[1]] + 1: 1);
			endforeach;

			foreach ($crc as $c):
				foreach ($dates as $d):
					$count = (isset($return[$d['date'].' '.$c])?$return[$d['date'].' '.$c]:0);
					$return2[$c][$d['date']]['data'] = [
						'count' => $count,
						'day' => $d['day']
					];
					$count = 0;
				endforeach;
			endforeach;

			foreach ($return2 as $kd => $dates):
				foreach ($dates as $k => $v):
					$return3[$kd][$k] = $v;
					$return3[$kd][$k]['data']['percent'] = ($countsArr[$k]?round(($v['data']['count']/$countsArr[$k])*100,2):0.00);
					$return3[$kd][$k]['data']['totalcounts'] = $countsArr[$k];
				endforeach;
			endforeach;

			return $return3;

		endif;

		return false;

	}

	private function _dispo($data){
		$dispo = [];
		foreach ($data as $k => $v):
			(isset($dispo[$v->status])?$dispo[$v->status]++:$dispo[$v->status] = 1);
		endforeach;
		return array_keys($dispo);
	}


}