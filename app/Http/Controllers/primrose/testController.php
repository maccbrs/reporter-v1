<?php namespace App\Http\Controllers\primrose;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Mail;
use DB;
use Carbon\Carbon;
use Excel;

class testController extends Controller
{

    public function test(){

        pre('56567sasda'); 

// $data = array(
//     array('data1', 'data2'),
//     array('data3', 'data4')
// );

// Excel::create('Filename', function($excel) use($data) {

//     // Set the title
//     $excel->setTitle('Our new awesome title');

//     // Chain the setters
//     $excel->setCreator('Maatwebsite')
//           ->setCompany('Maatwebsite');

//     // Call them separately
//     $excel->setDescription('A demonstration to change the file properties');


//     $excel->sheet('First sheet', function($sheet) {
//         // Set background color for a specific cell
//         $sheet->getStyle('A1')->applyFromArray(array(
//             'fill' => array(
//                 'color' => array('rgb' => 'FF0000')
//             )
//         ));
//         $sheet->setPageMargin(array(
//             0.25, 1.30, 0.25, 0.30
//         ));
//         $sheet->loadView('primrose.forms.tbl1');
//     });

//     $excel->sheet('Second sheet', function($sheet) {

//         $sheet->loadView('primrose.forms.tbl2');
//     });

// })->export('xls');

    }

    // public function test(){

    //     pre('s'); 
    // }

    	public function _query($camp,$f,$t,$tz1,$tz2){

		$help = new \App\Http\Helpers\primrose\Primrose;

		if($camp['server'] == '192.168.200.131'){

			$con = DB::connection('192.168.200.132');

		}else{

			$con = DB::connection('192.168.200.132');
		}

		$a = 	$con
				->table('vicidial_closer_log')
				->select('lead_id', 'list_id', 'campaign_id', 'call_date', 'phone_number', 'user', 'status', 'queue_seconds', 'uniqueid')
	            ->whereBetween('call_date', [$f, $t])
	            ->whereIn('campaign_id',json_decode($camp->dids))
	            ->get();

	    if(!$a):
			$a =  $con
					->table('vicidial_log')
		            ->whereBetween('call_date', [$f, $t])
		            ->whereIn('campaign_id',json_decode($camp->dids))
		            ->get();
	    endif;
	    
	    $data = [];
	    $uids = [];

	    foreach ($a as $av):
	    	
	    	$c = $con
				->table('vicidial_list')
				->select('vendor_lead_code', 'lead_id')
	            ->where('lead_id', '=' ,$av->lead_id)
	            ->get();

	        if(!empty($c[0]->vendor_lead_code)){

	        	$av->vendor_lead_code = $c[0]->vendor_lead_code;
	        	$av->lead_id2 = $c[0]->lead_id;

	    	}

	    	$data[$av->uniqueid]['closer'] = $av;

	        if($tz1 != $tz2): $data[$av->uniqueid]['closer']->call_date = $help->convert_tz($av->call_date,$tz1,$tz2); endif;
	    	$uids[] = $av->uniqueid;
	    endforeach;
	 
		$b =  $con
				->table('vicidial_agent_log')
				->select('user', 'pause_sec', 'wait_sec', 'talk_sec', 'dispo_sec', 'status', 'comments', 'pause_type', 'uniqueid')
				->whereIn('uniqueid',$uids)
				->get();
	    
	    foreach ($b as $bv) {
	    	$data[$bv->uniqueid]['agent'] = $bv; 
	    }

	    return $data;
	}

	public function _totals($data){

		$help = new \App\Http\Helpers\primrose\Primrose;
		$tests = $help->tests();
		$drops = $help->drops();
		$tn = $help->tn();
		$abandoned = 0; $handled = 0;
		$l30 = 0; $l60 = 0; $g60 = 0; $handled2 = 0;
		$offered = 0;
		$rate = 0;
		$sl = 0;
		$tt = 0;
		$wt = 0;
		$wait = 0;
		$not_allow_dispo =  array("TEST", "TC","Test");
		$aht = 0;

		foreach ($data as $v): 
			 
			if(!in_array($v['closer']->status, $not_allow_dispo )): 
				if(!in_array($v['closer']->phone_number,$tn)):

					$offered++;

					if(in_array($v['closer']->status,$drops)): 
						$abandoned++;  
					else:
						$handled++;
						$qt = (isset($v['closer']->queue_seconds)?$v['closer']->queue_seconds:0);
						$l30 = ($qt <= 30) ? $l30 + 1 : $l30;
						$l60 = ($qt <= 60) ? $l60 + 1 : $l60;
						$g60 = ($qt > 60) ? $g60 + 1 : $g60;
						if(isset($v['agent'])):
							$handled2++;
							$tt = $tt + $v['agent']->talk_sec;
							$wt = $wt + $v['agent']->dispo_sec;
							$wait = $wait + $v['agent']->wait_sec;
						endif;
					endif;

				endif;
			endif;

		endforeach;

//		pre($handled2);

		if($offered):
			$rate = round(($handled/$offered)*100,2);
		endif;

		if($handled):
			$sl = round(($l30/$handled)*100,2);
		endif;

		$talktime = $help->sToHms(round($tt,2));
		$wraptime = $help->sToHms(round($wt,2));
		$waittime = $help->sToHms(round($wait,2));

		$sum = $tt + $wt + $wait + $abandoned;


		if($tt != 0){

			$talk_ave =($tt / $handled2);
			$talk_ave = $help->sToHms($talk_ave);

		}else{

			$talk_ave = 0;

		}

		if($wt != 0){

			$wrap_ave = ($wt / $handled2);
			$wrap_ave = $help->sToHms($wrap_ave);

		}else{

			$wrap_ave = 0;
		}

		if($wait != 0){

			$wait_ave = ($wait / $handled2);
			$wait_ave = $help->sToHms($wait_ave);

		}else{

			$wait_ave = 0;
		}

		if($abandoned != 0){

			$drop_ave = round((($abandoned / $sum) * 100),2);

		}else{

			$drop_ave = 0;
		}

		$tt = ($tt == 0) ? 0 : $tt;
		$wt = ($wt == 0) ? 0 : $wt;
		$handled2 = ($handled2 == 0) ? 0 : $handled2;

		if($tt != 0 && $handled2 == 0 && $wt != 0){

		$aht = ($tt / $handled2) + ($wt / $handled2);
		$aht = $aht / $handled2;
		$aht = $help->sToHms($aht);

		}else{
			$aht = 0;
		}

		return [
			'offered' => $offered,
			'handled' => $handled,
			//'handled2' => $handled2,
			'abandoned' => $abandoned,
			'answerrate' => $rate,
			'l30' => $l30,
			'l60' => $l60,
			'g60' => $g60,
			'servicelevel' => $sl,
			'totaltalktime' => $talktime,
			'totalwraptime' => $wraptime,
			//'waittime' => $waittime,
			// 'talktime_ave' => $talk_ave,
			// 'wraptime_ave' => $wrap_ave,
			// 'waittime_ave' => $wait_ave,
			// 'drop_ave' => $drop_ave,
			// 'handled_ave' => $aht
		];
	}

} 