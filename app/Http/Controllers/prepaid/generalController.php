<?php namespace App\Http\Controllers\prepaid;


class generalController 
{

	public function dates($data){ 
		$dates = [];
		if($data):
			foreach ($data as $d):
				if($d->closer2):
					$a = explode(' ', $d->closer2['call_date']);
					if(!array_key_exists($a[0],$dates)):
						$dates[$a[0]] = date("D",strtotime($a[0]));
					endif;
				endif;
			endforeach;
		endif; 
		return $dates;
	}

	public function daily_summary($data){

		$return = [];
		$return2 = [];

		if($data):

			foreach($data as $d):

				if($d->closer2):
					$talk = (isset($d->closer2['agent']['talk_sec'])?$d->closer2['agent']['talk_sec']:0);
					$wrap = (isset($d->closer2['agent']['dispo_sec'])?$d->closer2['agent']['dispo_sec']:0);
					$time = explode(' ',date("Y m/d H:i:s D",strtotime($d->closer2['call_date'])));
					$return[$time[1]]['date'] = $time[2];
					$return[$time[1]]['talk'] = (isset($return[$time[1]]['talk'])?$return[$time[1]]['talk'] + $talk : $talk);
					$return[$time[1]]['wrap'] = (isset($return[$time[1]]['wrap'])?$return[$time[1]]['wrap'] + $wrap : $wrap);
					$return[$time[1]]['total'] = (isset($return[$time[1]]['total'])?$return[$time[1]]['total'] + $wrap + $talk : $wrap + $talk);
					$return[$time[1]]['day'] =  $time[3];
				endif;
			endforeach;
			
			foreach ($return as $k => $v):
				$return2[$k] = $v;
				$return2[$k]['talkm'] = round($v['talk'] / 60,2);
				$return2[$k]['wrapm'] = round($v['wrap'] / 60,2);
				$return2[$k]['totalm'] = round($v['total'] / 60,2);
			endforeach;

			return $return2;

		endif;

		return false;
	}

	public function daily_crc($data){ //pre($data->toArray());

		$return = [];
		$return2 = [];
		$return3 = [];
		$countsArr = [];
		if($data):

			$crc = [];
			$dates = [];

			foreach ($data as $d):
				if($d->closer2):
					$time = explode(' ',date("Y m/d H:i:s D",strtotime($d->closer2['call_date'])));
					if(!in_array($d->closer2['status'], $crc)):
						$crc[] = $d->closer2['status'];
					endif;
					if(!in_array($time[1], $dates)):
						$dates[] = ['date' => $time[1],'day' => $time[3]];
					endif;
				endif;				
			endforeach;

			foreach($data as $d):
				$time = explode(' ',date("Y m/d H:i:s D",strtotime($d->closer2['call_date'])));
				$return[$time[1].' '.$d->closer2['status']] = (isset($return[$time[1].' '.$d->closer2['status']])? $return[$time[1].' '.$d->closer2['status']] + 1:1);
				$countsArr[$time[1]] = (isset($countsArr[$time[1]])? (int)$countsArr[$time[1]] + 1: 1);
			endforeach;

			foreach ($crc as $c):
				foreach ($dates as $d):
					$count = (isset($return[$d['date'].' '.$c])?$return[$d['date'].' '.$c]:0);
					$return2[$c][$d['date']]['data'] = [
						'count' => $count,
						'day' => $d['day']
					];
					$count = 0;
				endforeach;
			endforeach;

			foreach ($return2 as $kd => $dates):
				foreach ($dates as $k => $v):
					$return3[$kd][$k] = $v;
					$return3[$kd][$k]['data']['percent'] = ($countsArr[$k]?round(($v['data']['count']/$countsArr[$k])*100,2):0.00);
					$return3[$kd][$k]['data']['totalcounts'] = $countsArr[$k];
				endforeach;
			endforeach;
			
			return $return3;

		endif;

		return false;

	}

	public function formatter($str,$type = 'implode'){

		$result = '';

		if($str != ''):
			switch ($type):
				case 'implode':
					$result = implode(',', json_decode($str,true));
					break;
			endswitch;
		endif;

		return $result;

	}

	public function filter_valid_email($str){

		$emails = [];
		$arr = explode(',',$str);
		foreach ($arr as $v):
			if(filter_var($v, FILTER_VALIDATE_EMAIL)): $emails[] = $v; endif; 
		endforeach;
		return json_encode($emails);

	}

	public function percentage($base,$num){
		return number_format( ($num/$base) * 100, 0 );
	}

	public function notif_type($param){
		$type = 0;
		if($param <= 50) $type = 3;
		if($param <= 30) $type = 2;
		if($param <= 10) $type = 1;
		return $type;
	}

	public function daysleft($date){
		return round((strtotime($date) - time()) / 86400);
	}

	public function mbtimecreate($dt,$format){
		return date_format(date_create_from_format('Y-m-d H:i:s', $dt), $format);
	}

	public function roundsix($n){
		if($n):
			if($n<=30) return 30;
			$a = $n - 30;
			$abs = floor($a/6)*6;
			$mod = (($n%6)>0?6:0);
			return $abs+$mod+30;
		endif;
		return 0;
	}

	public function pre($arr,$die = true){

		echo '<pre>';
		if(is_array($arr) || is_object($arr)){
			 print_r($arr);
		}else{
			echo $arr;
		}
		
		if($die) die;
	}


}