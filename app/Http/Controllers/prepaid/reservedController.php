<?php 
namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\prepaid\reservedCreate;
use App\Http\Models\prepaid\Reserved;


class reservedController extends Controller
{
	public function __construct(){

	}


	public function create($subscriber){

		$restrictions = new Restrictions;
		return view('prepaid.reserved.create',compact('subscriber','restrictions'));

	}

	public function store(reservedCreate $r){

		$Reserved = new \App\Http\Models\prepaid\Reserved;
		$result = $Reserved->where('subscriber_id',$r->input('subscriber_id'))->first();

		if($result):
		
			$result->minutes = $result->minutes + $r->input('minutes');
			$records = ($result->records?json_decode($result->records,true):[]);	

			$records[] = [
				'minutes' => $r->input('minutes'),
				'date' => date('Y-m-d H:i:s')
			];

			$result->remaining = $result->minutes - $result->used;
			$result->records = json_encode($records);
			$result->status = 1;
		
			$result->save();	
		else:
			
			$Reserved->create([
				'subscriber_id' => $r->input('subscriber_id'),
				'minutes' => $r->minutes,
				'used' => 0,
				'remaining' => $r->minutes,
				'status' => 1,
				'records' => json_encode([['minutes' => $r->input('minutes'),'date' => date('Y-m-d H:i:s')]])
			]);		
		endif;

		
		flash()->success('overage successfully added!');
		return redirect()->route('prepaid.subscriber.show',$r->input('subscriber_id'));

	}

	public function edit(){

		$restrictions = new Restrictions;
		return view('prepaid.reserved.edit','restrictions');

	}

	public function update(){

	}

	public function destroy(){

	}	
}