<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\prepaid\loadCreate;
use App\Http\Models\prepaid\Load;
use App\Response;

class cronerController extends Controller
{

	public function loadUpdater(){

		$Load = new \App\Http\Models\prepaid\Load;
		$LoadUpdater = new \App\Http\Models\prepaid\LoadUpdater;
		$help = new \App\Http\Controllers\prepaid\generalController;
		$loads = $Load->active()->with(['details'])->get();
		$changed = 0;

		foreach ($loads as $load):

			$total_sec = 0;

		    if($load->details):
		        foreach ($load->details as $datail):
		            if($datail->closer && $datail->closer->agent):
		                $total_sec += $help->roundsix($datail->closer->agent->talk_sec) + $datail->closer->agent->dispo_sec;
		            endif;
		        endforeach;
		    endif;

		    if($load->used != ($total_sec/60)): $changed++; endif;
		    
		    if(($load->minutes*60) <= $total_sec): $load->status = 0; endif;
		    $load->used = ($total_sec/60);
		    $load->remaining = (($load->minutes*60) - $total_sec)/60;
		    $load->save();

		endforeach;
		
		$LoadUpdater->create(['title' => 'load updater','content' => 'new loads updated']);

	}

}