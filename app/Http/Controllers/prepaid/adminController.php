<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Helpers\prepaid\Calls;
use DB;

class adminController extends Controller
{

	public function __construct(){
		$this->connections = 'prepaid';
	}

	public function index(Calls $c){

		$restrictions = new Restrictions;
        $loadObj = new \App\Http\Models\prepaid\Load;
        $loads = $loadObj->active()->with(['subscriber'])->get();
        $results = $data = []; 
     
        foreach ($loads as $load):

        	$results[] = [
        		'subscriber' => $load->subscriber->client,
        		'remaining' => $load->remaining,
        		'reserved' => !empty($load->subscriber->reserved->remaining) ? $load->subscriber->reserved->remaining : 0,
        		'minutes' => $load->minutes,
        		'started' => $load->startdate,
        		'enddate' => $load->enddate,
        	];
        endforeach;

		usort($results, function($a, $b)
		{
		    return $a['remaining'] - $b['remaining'];
		});
	
		foreach ($results as $result) {
			$data['subscriber'][] = $result['subscriber'];
			$data['remaining'][] = ($result['remaining']) >= 0 ? $result['remaining'] : 0 ;
			$data['reserved'][] = ($result['reserved']) >= 0 ? $result['reserved'] : 0 ;
			$data['startdate'][] = $result['started'];
			$data['enddate'][] = $result['enddate'];
			$data['minutes'][] = $result['minutes'];
		}	
 		
		$label = (isset($data['subscriber'])?json_encode($data['subscriber']):"[]");
		$remaining = (isset($data['remaining'])?json_encode($data['remaining']):"[]");
		$or = (isset($data['reserved'])?json_encode($data['reserved']):"[]"); 
		
		$labellist = json_decode($label);

		$subscription = []; 

		foreach ($labellist as $key => $list) {
			
			$subscription[$key]['subscriber'] = $list;
			$subscription[$key]['load'] = $data['remaining'][$key];
			$subscription[$key]['reserve'] = $data['reserved'][$key];
			$subscription[$key]['startdate'] = $data['startdate'][$key];
			$subscription[$key]['enddate'] =  $data['enddate'][$key];
			$subscription[$key]['minutes'] =  $data['minutes'][$key];
			$subscription[$key]['percentageload'] =   ($data['remaining'][$key] / $data['minutes'][$key]) * 100;
		}

		return view('prepaid.admin.index',compact('label','remaining','or','restrictions','subscription'));

	}


}  