<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\prepaid\Connector;

//requests
use App\Http\Requests\prepaid\connectorCreate;

class connectorController extends Controller 
{

	public function index(){
		$restrictions = new Restrictions;
		$connectors = Connector::with(['subscriber'])->wherein('status',[1])->paginate(40);
		return view('prepaid.connector.index',compact('connectors','restrictions'));
	}

	public function show($connector){
		$restrictions = new Restrictions;
		return view('prepaid.connector.show',compact('connector','restrictions'));
	}

	public function create(){
		$subsObj = new \App\Http\Models\prepaid\Subscriber;
		$subscribers = $subsObj->get();
		$restrictions = new Restrictions;
		return view('prepaid.connector.create',compact('restrictions','subscribers')); 
	}

	public function store(connectorCreate $request){
		$restrictions = new Restrictions;
		$id = Connector::insertGetId($request->only(['subscriber_id','did','status']));
		return redirect()->route('prepaid.connector.show',$id);
	}

	public function edit($connector){

		$subsObj = new \App\Http\Models\prepaid\Subscriber;
		$subscribers = $subsObj->pluck('client','id');
		$restrictions = new Restrictions;
		
		return view('prepaid.connector.edit',compact('connector','restrictions','subscribers')); 

	}

	public function delete($connector){

		$restrictions = new Restrictions;
		$connector->status = 0;
		$connector->save();
		$connectors = Connector::with(['subscriber'])->wherein('status',[1])->paginate(40);
		flash()->success('connector successfully updated');
		return view('prepaid.connector.index',compact('connector','restrictions','connectors'));

	}

	public function update($connector,connectorCreate $request){

		$restrictions = new Restrictions;
		$connector->subscriber_id = $request->input('subscriber_id');
		$connector->did = $request->input('did');
		$connector->save();
		flash()->success('connector successfully updated');
		$connectors = Connector::with(['subscriber'])->wherein('status',[1])->paginate(40);
		
		return view('prepaid.connector.index',compact('connector','restrictions','connectors'));

	}

	public function destroy(){
		
	}
	
}