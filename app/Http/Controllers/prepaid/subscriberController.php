<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use App\TempSubscriber;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\prepaid\Subscriber; 
use App\Http\Helpers\prepaid\Calls;
use App\Http\Helpers\prepaid\Getter;
//requests
use App\Http\Requests\prepaid\subscriberCreate;
//create pagination
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Mail;
use DB;

class subscriberController extends Controller
{

	public function index(){

		$restrictions = new Restrictions;
		$subscribers = Subscriber::where('status',1)->paginate(50);
		return view('prepaid.subscriber.index',compact('subscribers','restrictions'));

	}

	public function index_all(){

		$restrictions = new Restrictions;
		$subscribers = Subscriber::paginate(50);
		return view('prepaid.subscriber.index',compact('subscribers','restrictions'));

	}

	public function show($subscriber){ 

		$reserved_prepaid = $subscriber->toArray()['reserves'];
		//$reserved_prepaid = $subscriber->toArray()['reserves'];
		$notes = $subscriber['notes']; 
		$reservelist = json_decode($reserved_prepaid['records']); 

		if(!empty($reservelist)){
			$ovesum = 0;

			foreach ($reservelist as $key => $value) {
				
				$overage[$key]['sum'] = $ovesum = $ovesum + $value->minutes;
				$overage[$key]['status'] = 'Inactive';
				$overage[$key]['minutes'] = $value->minutes;
				$overage[$key]['date'] = $value->date;
				$overage[$key]['remaining'] = 0;

			}

			$overage[$key]['status'] = 'Active';
			$overage[$key]['remaining'] =  ($reserved_prepaid['remaining']) >= 0 ? $reserved_prepaid['remaining'] : 0 ;
	
		}else{

			$reservelist ="";
		}

		$gn = new \App\Http\Controllers\prepaid\generalController;
		$graphdata = json_encode($this->graph_data($subscriber->active['details']));
		$restrictions = new Restrictions;

		if($subscriber->active['minutes'] != 0){

			$average = ($subscriber->active['remaining']/$subscriber->active['minutes']) * 100; 
		}else{

			$average = 0;
		}

		return view('prepaid.subscriber.show',compact('subscriber','gn','restrictions','graphdata','reserved_prepaid','reservelist','overage','notes','average'));

	}

	public function create(){

		$restrictions = new Restrictions;
		return view('prepaid.subscriber.create',compact('restrictions'));

	}

	public function store(subscriberCreate $request){

		$id = Subscriber::insertGetId($request->only(['client','email', 'alias','status']));

		if($id){

			flash()->success('Subscriber successfully added');

		}else{

			flash()->error('Something went wrong. Please check your input.');
		}

		return redirect()->route('prepaid.subscriber.show',$id);

	}

	public function edit($subscriber){

		$restrictions = new Restrictions;
		return view('prepaid.subscriber.edit',compact('subscriber','restrictions')); 

	}

	public function send($subscriber){

		Mail::send('emails.test', ['name' => '$subscriber'],function ($message)
		{
			$message->to('kit@mailinator.com', 'Your Application')->from('howellkitacalabia@gmail.com')->subject('Welcome!');
		});

	}

	public function update($subscriber,subscriberCreate $request){

		$subscriber->client = $request->input('client');
		$subscriber->email = $request->input('email');
		$subscriber->alias = $request->input('alias');
		$subscriber->prefix = $request->input('prefix');
		$subscriber->save();
		flash()->success('client successfully updated');
		return redirect()->route('prepaid.subscriber.index',$subscriber);
	}

	public function delete($subscriber){
		
		$subscriber->status = 0;

		$subscriber->save();
		flash()->success('client successfully removed');
		return redirect()->route('prepaid.subscriber.index',$subscriber);


	}

	public function graph_data($rows){

		$gn = new \App\Http\Controllers\prepaid\generalController;
		$return = false;
		$data = [];
		if($rows):
			foreach ($rows as $v):
				if($v['closer']):
					$date = $gn->mbtimecreate($v['closer']['call_date'],'Y-m-d');
					if(isset($data[$date])):
						$data[$date]++;
					else:
						$data[$date] = 1;
					endif;
				endif;
			endforeach;

			if($data):
				foreach ($data as $k => $v):
					$return[] = [$k,$v];
				endforeach;
			endif;
		endif;

		return $return;

	}

	
}