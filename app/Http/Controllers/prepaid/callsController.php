<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\helpers\Calls;
use DB;
use PDF;
use Excel;


class callsController extends Controller
{


	public function load($load_id){

		$lo = new \App\Http\Models\prepaid\Load;
		$gnc = new \App\Http\Controllers\prepaid\generalController;
		$gn = new \App\Http\Controllers\primrose\generalController;
		$result = $lo->where('id',$load_id)->with('details2')->first();  //pre($result->toArray());
		$dates = $gnc->dates($result->details2);
   		$summary = $gnc->daily_summary($result->details2);
    	$crc = $gnc->daily_crc($result->details2);	
		$restrictions = new Restrictions;
		return view('prepaid.calls.load',compact('summary','crc','result','restrictions','dates','gn'));		
	} 

	public function reserved($subscriber){

		$restrictions = new Restrictions;

		$calls = [];
		$res = DB::connection('prepaid')->select('
					SELECT * from reserved_details
					where subscriber_id = ?
				', [$subscriber->id]);

		if(!empty($res)){
			foreach ($res as $v) {
				$uniqueid[] = $v->uniqueid;
			}
			if($uniqueid){
			 $calls =  DB::connection('192.168.200.132')
				     ->table('vicidial_agent_log')
		     		 ->join('vicidial_closer_log', 'vicidial_closer_log.uniqueid', '=', 'vicidial_agent_log.uniqueid')
		    		 ->select(
		    		 		'vicidial_closer_log.*',
		    		 		'vicidial_agent_log.dispo_Sec',
		    		 		'vicidial_agent_log.talk_Sec',
		    		 		DB::raw('vicidial_agent_log.dispo_Sec+vicidial_agent_log.talk_Sec AS seconds'))
		    		 ->whereIn('vicidial_closer_log.uniqueid', $uniqueid)
		    		 ->whereNotIn('vicidial_closer_log.status', ["DROP","HU","Test"])
		    		 ->paginate(20);
			}
		}
		return view('prepaid.calls.reserved',compact('calls','restrictions'));

	}

	public function pdf($load_id,$subscriber,$startdate,$enddate){

		$details = DB::connection('prepaid')->select('
				SELECT * from load_details
				where load_id = ?
			', [$load_id]);

		if($details){

			$uniqueid = [];
			foreach ($details as $detail) {
				$uniqueid[] = $detail->uniqueid;
			}

			if($uniqueid){

				$loads = DB::connection('192.168.200.132')
				     ->table('vicidial_agent_log')
		     		 ->join('vicidial_closer_log', 'vicidial_closer_log.uniqueid', '=', 'vicidial_agent_log.uniqueid')
		    		 ->select(
		    		 		'vicidial_closer_log.*',
		    		 		'vicidial_agent_log.dispo_Sec',
		    		 		'vicidial_agent_log.talk_Sec',
		    		 		DB::raw('vicidial_agent_log.dispo_Sec+vicidial_agent_log.talk_Sec AS seconds'))
		    		 ->whereIn('vicidial_closer_log.uniqueid', $uniqueid)
		    		 ->whereBetween('vicidial_closer_log.call_date', [$startdate, $enddate])
		    		 ->whereNotIn('vicidial_closer_log.status', ["DROP","HU","Test"])
		    		 ->paginate(20);
			}
		}

		$subscribers['id'] = $subscriber->id;
		$subscribers['email'] = $subscriber->email;
		$subscribers['client'] = $subscriber->client;

		$html = view('prepaid.calls.pdf', compact('loads','subscribers'))->render();
		$today = date("m/d/y");
		$pdf_title =  $today.'_'.$subscriber['client'].'.pdf';
		return PDF::load($html)->filename($pdf_title)->download();

	}

	public function pdfptool($load_id,$subscriber,$startdate,$enddate){

		$loadDataHolder =  $this->prod_data($load_id,$subscriber,$startdate,$enddate);

		$loadList = $loadDataHolder[0];
		$dates = $loadDataHolder[1]; 
		$subscribers = $loadDataHolder[2];  
		$loads = $loadDataHolder[3];
		$phone_number = $loads[0];

		$datesplit = array_chunk($dates, 15); 

		foreach($datesplit as $keySplit => $dates){
            foreach($dates as $keydate => $date){

                $dataHolder[$keySplit][$keydate]['date'] = $date;
                $dataHolder[$keySplit][$keydate]['day'] =  date('D', strtotime($date));           
            }
        }

        foreach($loadList as $keyload => $load){
        	$loadsplit = array_chunk($load['date'], 15);
        	foreach($loadsplit as $keyload2 => $loads2){
        		
        		foreach($loads2 as $keyload3 => $loads3){
	        	$dataHolder[$keyload2][$keyload3]['count'][$keyload] = count(array_filter($loads3));

	        	}
	        } 
    	}
    	
    	foreach($loadList as $key => $load){

    		$statusHolder[$key]['status'] = $load['status'];
    		$statusHolder[$key]['status_name'] = $load['status_name'];

    	} 

		$html = view('prepaid.calls.pdfptool', compact('loads','subscribers','dates','loadList','dataHolder','statusHolder'))->render();
		$today = date("m/d/y");
		$pdf_title =  $today.'_'.$subscriber['client'].'.pdf';
        
		$paper_orientation = 'landscape';
		$customPaper = 'a4';
	
        PDF::load($html);
        PDF::filename($pdf_title);               
        PDF::setPaper($customPaper,$paper_orientation);
        return PDF::download(); 

	}

	public function prod_data($load_id,$subscriber,$startdate,$enddate){

		if(!empty($subscriber->id)){

			$subscriber = $subscriber->id;
			$identifier = 1;
		}else{
			
			$identifier = 0;
		}

		if(!empty($load_id)){

			$subscriberData = DB::connection('prepaid')
					->table('subscribers')
					->select('*' )
                    ->where('id', '=', $subscriber )
                    ->get();

			$subscriberData = $subscriberData[0];

			$loads = [];
			$load = DB::connection('prepaid')->table('subscribers');
			$details = DB::connection('prepaid')
					->table('load_details')
					->select('*' )
                    ->where('load_id', '=', $load_id )
                    ->get();
			    		             
				if($details){

					$uniqueid = [];
					foreach ($details as $detail) {
						$uniqueid[] = $detail->uniqueid;
				}

				if($uniqueid){
					$loads = DB::connection('192.168.200.132')
					     ->table('vicidial_agent_log')
			     		 ->leftJoin('vicidial_closer_log', 'vicidial_closer_log.uniqueid', '=', 'vicidial_agent_log.uniqueid')
			    		 //->leftJoin('vicidial_campaign_statuses', 'vicidial_campaign_statuses.status', '=', 'vicidial_closer_log.status')
			    		 ->select(
			    		 		'vicidial_closer_log.*',
			    		 		//'vicidial_campaign_statuses.status_name',
			    		 		'vicidial_agent_log.dispo_Sec',
			    		 		'vicidial_agent_log.talk_Sec',
			    		 		DB::raw('vicidial_agent_log.dispo_Sec+vicidial_agent_log.talk_Sec AS seconds'))
			    		 ->whereBetween('vicidial_closer_log.call_date', [$startdate, $enddate])
			    		 ->whereIn('vicidial_closer_log.uniqueid', $uniqueid)
			    		 ->whereNotIn('vicidial_closer_log.status', ["DROP","HU","Test"])
			    		 ->paginate(20);

				}
			}

				// get date range
				$dates = array($startdate);

			    while(end($dates) < $enddate){
			        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
			    }

			    //get status list

			    foreach ($loads as $keyloadlist => $loadlist) {
			    	
			    	$status_list[] = $loadlist->status;
			    	
			    }

			    //get status name
			    foreach ($loads as $keyloadlist => $loadlist) {
			    	
			    	$status_name[] = $loadlist->status;
			    	
			    }

			    if(!empty($status_list) && !empty($status_name)){

				    $status_list = array_unique($status_list); 
				    $status_name = array_unique($status_name); 

				    $ctr = 0;
				    foreach ($status_list as $keystatuslist => $statuslist) {
				    	
				    	$loadList[$ctr]['status'] = $statuslist;
				    	$loadList[$ctr]['status_name'] = $status_name[$keystatuslist];
				    	$ctr++; 
				    }
			    
				    //merge these arrays
				    
				   	foreach ($loads as $keyload => $load) {
					   	$load_date = date('Y-m-d', strtotime($load->call_date));
					    foreach ($loadList as $keystatus => $status) {
					    	if ($status['status'] == $load->status) {

						    	foreach ($dates as $keydate => $date) {	
						    			
						    			
						    		if ($load_date == $date) {

						    			$loadList[$keystatus]['date'][$date][] = $date ;
					    			}else{

					    				$loadList[$keystatus]['date'][$date][] = "";
					    			}
					    		}
				    		}
				    	}
				    }
				}
			}

		if(!empty($loadList)){

			$loadDataHolder[0] = $loadList;
			$loadDataHolder[1] = $dates;
			$loadDataHolder[2] = $subscriberData;
			$loadDataHolder[3] = $loads;

			return $loadDataHolder;

		}else{

			$loadDataHolder = 0;

			return $loadDataHolder;
		}	

	}

	public function excel($load_id,$subscriber,$startdate,$enddate){

		$details = DB::connection('prepaid')->select('
			SELECT * from load_details
				where load_id = ?
			', [$load_id]);

		if($details){

			$uniqueid = [];
			foreach ($details as $detail) {
				$uniqueid[] = $detail->uniqueid;
			}

			if($uniqueid){
				$loads = DB::connection('192.168.200.132')
				     ->table('vicidial_agent_log')
		     		 ->join('vicidial_closer_log', 'vicidial_closer_log.uniqueid', '=', 'vicidial_agent_log.uniqueid')
		    		 ->select(
		    		 		'vicidial_closer_log.*',
		    		 		'vicidial_agent_log.dispo_Sec',
		    		 		'vicidial_agent_log.talk_Sec',
		    		 		DB::raw('vicidial_agent_log.dispo_Sec+vicidial_agent_log.talk_Sec AS seconds'))
		    		 ->whereIn('vicidial_closer_log.uniqueid', $uniqueid)
		    		 ->whereBetween('vicidial_closer_log.call_date', [$startdate, $enddate])
		    		 ->whereNotIn('vicidial_closer_log.status', ["DROP","HU","Test"])
		    		 ->paginate(20);
			}
		}

		$subscribers['id'] = $subscriber->id;
		$subscribers['email'] = $subscriber->email;
		$subscribers['client'] = $subscriber->client;
		$today = date("m/d/y");
		$excel_title =  $today.'_'.$subscriber['client'];
		
		Excel::create($excel_title , function($excel ) use($loads,$subscribers) {

			$excel->sheet('Sheetname', function($sheet) use($loads,$subscribers){

				$asset = $_SERVER["DOCUMENT_ROOT"] ."/";
				$image_location = $asset ."gentella\images\logo2.png";

				$count = count($loads);
				$loadCount =  $count + 19;

				$sheet->mergeCells('C1:G1');
				$sheet->mergeCells('C2:G2');
				$sheet->mergeCells('C3:G3');
				
				$sheet->cells('C1:E1', function($cells){
					$cells->setAlignment('right');
				});

				$sheet->cells('C2:E2', function($cells){
					$cells->setAlignment('right');
				});

				$sheet->cells('C3:E3', function($cells){
					$cells->setAlignment('right');
				});

				$sheet->mergeCells('A5:G5');

				$sheet->cells('A5:E5', function($cells){
					$cells->setBackground('#000000');
					$cells->setFontColor('#FFFFFF');
				});

				$sheet->mergeCells('A7:E7');
				$sheet->mergeCells('A8:E8');
				$sheet->mergeCells('A9:E9');

				$sheet->mergeCells('A11:G11');

				$sheet->cells('A11:E11', function($cells){
					$cells->setBackground('#000000');
					$cells->setFontColor('#FFFFFF');
				});

				$sheet->cells('C33:E33', function($cells){
					$cells->setAlignment('right');
				});

				$subscriber_id =  $subscribers['id'];
				$subscriber_email =  $subscribers['email'];
				$subscriber_client =  $subscribers['client'];

				$sheet->cells('A13:E13', function($cells){

					$cells->setBorder('solid', 'none', 'none', 'solid');

				});

				$columnStart = 'C'.$loadCount;
				$columnEnd = 'G'.$loadCount;

				$sheet->mergeCells($columnStart.':'.$columnEnd);

				$sheet->loadView('prepaid.calls.excel')->with('loads', $loads)->with('subscribers', $subscribers)->with('loadCount', $loadCount);
				
			});


		})->download('xlsx');

	}

	public function excelptool($load_id,$subscriber,$startdate,$enddate){

		$loadDataHolder =  $this->prod_data($load_id,$subscriber,$startdate,$enddate);

		$loadList = $loadDataHolder[0];
		$dates = $loadDataHolder[1]; 
		$subscribers = $loadDataHolder[2];  
		$loads = $loadDataHolder[3];
		$phone_number = $loads[0]; 

		$today = date("m/d/y");

		$excel_title =  $today.'_'.$subscribers->client;
		
		Excel::create($excel_title , function($excel ) use($loads,$subscribers, $dates, $loadList) {

			$excel->sheet('Sheetname', function($sheet) use($loads,$subscribers, $dates, $loadList){

				$asset = $_SERVER["DOCUMENT_ROOT"] ."/";
				$image_location = $asset ."gentella\images\logo2.png";

				$sheet->mergeCells('A1:H1');
				$sheet->mergeCells('A2:H2');
				$sheet->mergeCells('A3:H3');
				$sheet->mergeCells('A5:M5');
				$sheet->mergeCells('A8:H8');
				$sheet->mergeCells('A9:H9');
				$sheet->mergeCells('A7:H7');
				$sheet->mergeCells('A11:M11');

				$sheet->cells('A5:P5', function($cells){
					$cells->setBackground('#000000');
					$cells->setFontColor('#FFFFFF');
				});

				$sheet->cells('A11:P11', function($cells){
					$cells->setBackground('#000000');
					$cells->setFontColor('#FFFFFF');
				});
	
				$sheet->cells('A12:D12', function($cells){
					$cells->setBackground('#1569C7');
				});

				$sheet->cells('A13:D13', function($cells){
					$cells->setBackground('#1569C7');
				});

				$sheet->loadView('prepaid.calls.excelptool')->with('loads', $loads)->with('subscribers', $subscribers)->with('dates', $dates)->with('loadList', $loadList);
				
			});


		})->download('xlsx');

	}

	public function generate(Request $request){
		
		$restrictions = new Restrictions;
		$load_id = $request->load_id;
		$subscribers = $request->subscriber_id;
		$startdate = $request->startdate;
		$enddate = $request->enddate;

		$loadDataHolder =  $this->prod_data($load_id,$subscribers,$startdate,$enddate);

		$loadList = $loadDataHolder[0];
		$dates = $loadDataHolder[1];
		$subscriberData = $loadDataHolder[2];
		$loads = $loadDataHolder[3];

		if(!empty($request->load_id)){

			$subscriberData = DB::connection('prepaid')->select('
					SELECT * from subscribers
					where id = ?
				', [$subscribers]);

			$subscriberData = $subscriberData[0]; 

		}

		if(!empty($loads[0]->status)){

			flash()->success('Call List has been Generated.');

		}else{

			flash()->error('Sorry. There are no calls for this Date Range.');

		}
			$startdate = $request->startdate;
			$enddate = $request->enddate;

			if($loadDataHolder != 0){

				$report_view = view('prepaid.report._report_view',compact('loads','loadList','load_id', 'subscribers', 'subscriberData', 'load_data', 'dates', 'enddate', 'startdate','restrictions'));
			}

				//$report_view = "" ;
		return view('prepaid.calls.load',compact('loads','subscribers','load_id','loadList','report_view','restrictions'));

	}
}