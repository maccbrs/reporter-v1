<?php namespace App\Http\Controllers\prepaid;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\prepaid\userCreate;
use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Pagination\LengthAwarePaginator;

class userController extends Controller
{

	public function index(){

		$user = User::paginate(10);
		$restrictions = new Restrictions;
		return view('prepaid.user.index',compact('user','restrictions'));	

	}

	public function store(userCreate $request){

		$request->created_at = date("Y-m-d H:i:s"); 
		$request->updated_at = date("Y-m-d H:i:s"); 

		$request->password = bcrypt($request->password); 

		$id = User::insertGetId($request->only(['name','email','password', 'created_at', 'updated_at'] ));
		return redirect()->route('prepaid.user.show',$id);

	}
 
	public function show($id){

		$user = User::find($id);
		$restrictions = new Restrictions;
		return view('prepaid.user.show',compact('user','restrictions'));
	}

	public function create(){

		$restrictions = new Restrictions;
		return view('prepaid.user.create','restrictions');
		
	}

	public function edit($id){

		$user = User::find($id);
		$restrictions = new Restrictions;
		return view('prepaid.user.edit',compact('user','restrictions'));	

	}

	public function update(Request $request){

		$user = User::find($request->id);

		$user->name = $request->name;

		$user->email = $request->email;

		$user->password = $request->password;

		$user->save();   
	}

	public function destroy($id){

		$user = User::find($id);

		$user->delete();

	}
}