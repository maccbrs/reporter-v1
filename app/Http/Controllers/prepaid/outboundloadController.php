<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class outboundloadController extends Controller
{

	public function store(Request $r,$subscriber_id){

		$load = new \App\Http\Models\prepaid\OutboundLoad;
		$r["enddate"] = date('Y-m-d', strtotime('+30 days', strtotime($r->startdate)));
		$r["subscriber_id"] = $subscriber_id;
		$load->create($r->all());
		return redirect()->back();

	}

	public function records($id){

		$carbon = new \App\Http\Controllers\prepaid\generalController;
		$restrictions = new Restrictions;
		$load = new \App\Http\Models\prepaid\OutboundLoad;
		$records = $load->where('id',$id)->with(['details'])->first();
		return view('prepaid.outboundload.records',compact('restrictions','records','carbon'));

	}


}