<?php namespace App\Http\Controllers\prepaid;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Auth;

class emailerController extends Controller
{

	public function update(Request $r,$id){

		$gn = new \App\Http\Controllers\prepaid\generalController;
        $this->validate($r, [
            'to' => 'required'
        ]);	

		$eObj = new \App\Http\Models\prepaid\Emailer;
		$emails = $eObj->firstOrCreate(['subscriber_id' => $id]);	
		$emails->cc = $gn->filter_valid_email($r->input('cc'));
		$emails->to = $gn->filter_valid_email($r->input('to'));
		$emails->save();
		return redirect()->back();

	} 

}