<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Mail;
use Carbon\Carbon;
class testController extends Controller
{



    public function index(){

            $loadObj = new \App\Http\Models\prepaid\Load;
           // $loads = $loadObj->with('subscriber')->get();
           // pre($loads->toArray());
            $loads = $loadObj->active()->with('subscriber')->get();
            $consumed = $loadObj->consumed()->with('subscriber')->get();
            $percent = 0;
            $param['title'] = 'Prepaid Loads Status';
            $msg = '';
            $fb50 = $status = $fa50 = [];

            foreach ($loads as $load):

                $percent = round(($load->remaining/$load->minutes)*100);
                $type = 100;
                if($percent <= 50):
                    $fb50[] = [
                        'client' => $load->subscriber->client,
                        'percent' => $percent,
                        'minutes' => $load->minutes,
                        'used' => $load->used,
                        'remaining' => $load->remaining,
                        'start' => $load->startdate,
                        'end' => $load->enddate,
                        'reserved' => ($load->subscriber->reserved['remaining'] <0?0:$load->subscriber->reserved['remaining'])
                    ];
                else:
                    $fa50[] = [
                        'client' => $load->subscriber->client,
                        'percent' => $percent,
                        'minutes' => $load->minutes,
                        'used' => $load->used,
                        'remaining' => $load->remaining,
                        'start' => $load->startdate,
                        'end' => $load->enddate,
                        'reserved' => ($load->subscriber->reserved['remaining'] <0?0:$load->subscriber->reserved['remaining'])
                    ];                    
                endif;

            endforeach; 
            pre($consumed->toArray());
            if($consumed):
                foreach ($consumed as $c):
                    $status[] = [
                        'client' => ($c->subscriber?$c->subscriber->client:'removed'),
                        'minutes' => $c->minutes,
                        'remaining' => ($c->remaining<0?0:$c->remaining),
                        'start' => $c->startdate,
                        'end' => $c->enddate,
                        'reserved' => ($c->subscriber?($c->subscriber->reserved['remaining'] <0?0:$c->subscriber->reserved['remaining']):'removed'),
                        'status' => ($c->remaining < 0?'consumed':'deactivated')                        
                    ];            
                endforeach;  
            endif;


            if($fa50):
                $msg .= '<span style="background-color:yellow">The following are the clients that has load above 50%.</span> <br><br>';
                $msg .= '<table >
                          <tr>
                            <th style="border:1px solid #ebebeb;text-align:left;">Client</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">% left</th> 
                            <th style="border:1px solid #ebebeb;text-align:left;">Load minutes</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load used</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load remaining</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load start date</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load end date</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">reserve</th>
                          </tr>';


                foreach ($fa50 as $v):
                   $msg .= '
                          <tr>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['client'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['percent'].'%</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['minutes'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['used'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['remaining'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['start'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['end'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['reserved'].'</td>
                          </tr>';
                endforeach;
                $msg .= '</table>';

            endif;




            if($fb50):
                $msg .= '<br><span style="background-color:yellow">The following are the clients that has load fell below 50%. </span> <br><br>';
                $msg .= '<table >
                          <tr>
                            <th style="border:1px solid #ebebeb;text-align:left;">Client</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">% left</th> 
                            <th style="border:1px solid #ebebeb;text-align:left;">Load minutes</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load used</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load remaining</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load start date</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load end date</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">reserve</th>
                          </tr>';


                foreach ($fb50 as $v):
                   $msg .= '
                          <tr>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['client'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['percent'].'%</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['minutes'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['used'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['remaining'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['start'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['end'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['reserved'].'</td>
                          </tr>';
                endforeach;
                $msg .= '</table>';

            endif;


            if($status):
                $msg .= '<br><span style="background-color:yellow">The following are the clients that load/s were consumed or deactivated.</span> <br><br>';
                $msg .= '<table >
                          <tr>
                            <th style="border:1px solid #ebebeb;text-align:left;">Client</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load minutes</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load remaining</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load start date</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">Load end date</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">reserve</th>
                            <th style="border:1px solid #ebebeb;text-align:left;">status</th>
                          </tr>';

                foreach ($status as $v):
                   $msg .= '
                          <tr>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['client'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['minutes'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['remaining'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['start'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['end'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['reserved'].'</td>
                            <td style="border:1px solid #ebebeb;text-align:left;">'.$v['status'].'</td>
                          </tr>';
                endforeach;
                $msg .= '</table>';
            endif;

            // Mail::send('emails.reminder2', ['msg' => $msg], function ($m) use ($param){
            //     $m->from('reports@magellan-solutions.com', 'Magellan Solutions');
            //     $m->to(['operations@magellan-solutions.com','maristela.bernardino@magellan-solutions.com'])->subject($param['title']);
            //    // $m->to(['marlon.bernal@mailinator.com'])->subject($param['title']);
            //     $m->bcc(['noc@magellan-solutions.com']);

            // });

    }



	public function index2(){

		$resObj = new \App\Http\Models\prepaid\Reserved;
		$loads = $resObj->with(['details'])->get();// pre($loads->toArray());
		$changed = 0;
		
        foreach ($loads as $load):

            $total_sec = 0;

            if($load->details):
                foreach ($load->details as $datail):
                    if($datail->closer && $datail->closer->agent):
                        $total_sec += $datail->closer->agent->talk_sec + $datail->closer->agent->dispo_sec;
                    endif;
                endforeach;
            endif;

            if($load->used != ($total_sec/60)): $changed++; endif;

            if(($load->minutes*60) <= $total_sec): $load->status = 0; endif;
            $load->used = ($total_sec/60);
            $load->remaining = (($load->minutes*60) - $total_sec)/60;
            $load->updated_at = date('Y-m-d H:i:s');
            $load->save();

        endforeach;


	}

	public function testoutbound(){
        $loadObj = new \App\Http\Models\prepaid\OutboundLoad;
        $loads = $loadObj->active()->with(['details'])->get();

        foreach ($loads as $load):
        	if($load->details):
        		$sec = 0;
	        	foreach ($load->details as $detail):
	        		if($detail['call_details']):
	        			$sec += $detail['call_details']['length_in_sec'];
	        		endif;
	        	endforeach;
	        	if($sec):
	        		$min = $sec/60;
	        		$load->used = $min;
	        		$load->remaining = $load->minutes - $min;
	        		$load->status = ($load->remaining <= 0?0:1);
	        		$load->save();
	        	endif;
        	endif;
        endforeach;		
	}


    public function testoutboundupdater(){

        $count = 0;
        $oldObj =  new \App\Http\Models\prepaid\OutboundLoadDetails;
        $vclObj =  new \App\Http\Models\prepaid\ViciCallLog;
        $cn = new Carbon;
        $obloads = $oldObj->where('uniqueid',null)->get(); pre($obloads);
        // foreach ($obloads as $k => $v):
        //   $v->uniqueid = $vclObj->select('uniqueid')->where('number_dialed',$v->extension)->where('start_epoch',$v->startepoch)->first()->uniqueid; //echo '<br>';
        //   if($v->uniqueid): $count++; $v->save(); endif;
        // endforeach;

        if($count):

        endif;

    }

    public function test2(){

        $olObj =  new \App\Http\Models\prepaid\OutboundLoad;
        $loads = $olObj->active()->with(['details'])->get();
        $changed = 0;
    
        foreach ($loads as $load):

            $total_sec = 0;

            if($load->details):
                foreach ($load->details as $datail):
                    if($datail['call_details']):
                        $total_sec += $datail['call_details']['length_in_sec'];
                    endif;
                endforeach;
            endif;

            if($load->used != ($total_sec/60)): $changed++; endif;

            if(($load->minutes*60) <= $total_sec): $load->status = 0; endif;
            $load->used = ($total_sec/60);
            $load->remaining = (($load->minutes*60) - $total_sec)/60;
            $load->save();

        endforeach;        

    }

}