<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\prepaid\loadCreate;
use App\Http\Models\prepaid\Load;
use App\Response;

class loadController extends Controller
{

	public function __construct(){

	}

	public function index($subscriber){

		$restrictions = new Restrictions;
		return view('prepaid.load.index',compact('subscriber','restrictions'));

	}

	public function create($subscriber){

		$restrictions = new Restrictions;
		return view('prepaid.load.create',compact('subscriber','restrictions'));

	} 

	public function store(loadCreate $request,Load $load){

		$gn = new \App\Http\Controllers\prepaid\generalController;
		$request["enddate"] = date("Y-m-t", strtotime($request->startdate));
		flash()->success('added new load!');
		//pre($request->all());
		//$gn->pre($request->all());
		$load->create($request->all());
		$subscriber_id = $request->input('subscriber_id');
		return redirect()->route('prepaid.subscriber.show',$subscriber_id);

	}

	public function edit($load){

		$loadData = Load::find($load);
		$restrictions = new Restrictions;
		return view('prepaid.load.edit',compact('loadData','restrictions') );

	}
	
	public function update(Request $load){

		$loadData = Load::find($load["id"]);
		$loadData["id"] = $load["id"];
		$loadData["startdate"] = $load["startdate"];
		$loadData["minutes"] = $load["minutes"];
		$loadData["status"] = 1;
		$loadData["enddate"] = date('Y-m-d', strtotime('+30 days', strtotime($load["startdate"])));
		$saved = $loadData->save();

		if($saved){

			flash()->success('Load successfully updated');

		}else{

			flash()->error('Check your input.');

		}

		
		return redirect()->route('prepaid.subscriber.show',$load["subscriber_id"]);
	}

	public function destroy(){

	}

	public function generate(){
 
 		$restrictions = new Restrictions;
		return view('prepaid.load.generate','restrictions');

	}

} 