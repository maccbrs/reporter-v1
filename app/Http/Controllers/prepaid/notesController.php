<?php namespace App\Http\Controllers\prepaid;

use App\Http\Controllers\prepaid\accessController as Restrictions;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\prepaid\Notes;

class notesController extends Controller
{

	public function create($subscriber){
		
        $restrictions = new Restrictions;
		return view('prepaid.notes.create',compact('subscriber','restrictions'));

	}

	public function store(Request $request,Notes $notes,Guard $auth){
        $this->validate($request, [
            'content' => 'required'
        ]);	
        //echo $auth->user()->id; die;
        $input = $request->only(['subscriber_id','content']);
        $input['users_id'] = $auth->user()->id;

        if($notes->create($input)){
        	flash()->success('note successfully saved');
        	return redirect()->route('prepaid.subscriber.show',$input['subscriber_id']);
        	
        }
	}

}