<?php namespace App\Http\Controllers\blockage;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;


class blockageController extends Controller
{

	public function index(){

		$DboCalldetailViw = new \App\Http\Models\blockage\DboCalldetailViw;

		$rawCalls = $DboCalldetailViw
					->where('CallDirection','Inbound')
					->whereBetween('ConnectedDate',$this->timerange())
					->with(['wrapup'])->get();

		$blockages = [];

		foreach($rawCalls as $rc):

			$hour = $this->Hour($rc->ConnectedDate);
			$dispo = ($rc->wrapup?'HANDLED':'DROP');
			if(!isset($blockages[$hour][$dispo])):			
				$blockages[$hour][$dispo] = 1;
			else:
				$blockages[$hour][$dispo]++;
			endif;

			if(isset($blockages[$hour]['TOTAL'])):
				$blockages[$hour]['TOTAL']++;
			else:
				$blockages[$hour]['TOTAL'] = 1;
			endif;

		endforeach;
 


		$emails = ['noc@magellan-solutions.com','jovielyn.bauyon@magellan-solutions.com','hazel.osias@magellan-solutions.com','ardee.roman@magellan-solutions.com'];


		$Blockage = new \App\Http\Models\blockage\Blockage;
		$Blockage->create(['emails' => json_encode($emails),'contents' => json_encode($blockages),'subject' => 'Omnitrix Blockage Report']);

	}

	public function send(){

		$Blockage = new \App\Http\Models\blockage\Blockage;
		$item = $Blockage->where('sent',0)->first();

		if(!empty($item))
		{
		$emails = json_decode($item->emails,true);
		$subject = $item->subject;
		$data = json_decode($item->contents,true);
		//pre($data);
		ksort($data);
        Mail::send('emails.nexus-blockage', ['blockages' => $data], function ($m) use ($emails,$subject)  {
            $m->from('webdev_report@magellan-solutions.com', 'Magellan Report');
            $m->to($emails);
            $m->subject($subject);
        });		
        $item->sent = 1;
		$item->save();
		}
	}

	public function send_test(){

		$Blockage = new \App\Http\Models\blockage\Blockage;
		$item = $Blockage->where('sent',0)->first();

		if(!empty($item))
		{
		//pre($item);
		$emails = 'marc.briones@magellan-solutions.com';
		$subject = $item->subject;
		$data = 'test';
		//ksort($data);
        Mail::send('emails.nexus-blockage', ['blockages' => $data], function ($m) use ($emails,$subject)  {

            $m->from('webdev_report@magellan-solutions.com', 'Magellan Report');
            $m->to($emails);
            $m->subject($subject);
        });		

        $item->sent = 1;
		$item->save();
		}
	}


	private function timerange(){

		$b = Carbon::parse(Carbon::now()->format('y-m-d H:00:00'));
		$a =  Carbon::parse(Carbon::now()->subDay()->format('y-m-d H:00:00'));
		return [$a,$b];

	}

	private function Hour($time){
		return Carbon::parse($time)->format('H');
	}


}
