<?php namespace App\Http\Controllers\lily;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Models\Lily\Tasklist;
use App\Http\Requests\lily\tasklistRequest;
use App\Http\Helpers\Getter;
use App\User;

class tasklistController extends Controller
{

	public function index(Tasklist $task){

		$restrictions = new Restrictions;
		$tasklist = $task->where('type',1)->with(['user'])->get();// pre($tasklist->toArray());
		return view('lily.tasklist.index',compact('tasklist','restrictions'));

	}

	public function create(){   

		$get = new Getter;
		$restrictions = new Restrictions;
		$users = $get->by_department(5); //pre($users);			
		return view('lily.tasklist.create',compact('users','restrictions'));
		
	}

	public function store(tasklistRequest $r,Tasklist $task){

		$x = $task->create($r->all());
		$logger = new Logger;
		$logger->create($r,$r->all());
		flash()->success('successfully saved!');
		return redirect()->route('lily.tasklist.show',$x->id);

	}

	public function show($tasklist){

		$restrictions = new Restrictions;
		return view('lily.tasklist.show',compact('tasklist','restrictions'));

	}

	public function edit($task){ //pre($task->toArray());

		$get = new Getter;
		$restrictions = new Restrictions;
		$users = $get->by_department(5); //pre($users);		
		return view('lily.tasklist.edit',compact('task','users','restrictions'));

	}

	public function update(tasklistRequest $r,$task){// pre($r->all());

		$log['from'] = $task->toArray();
		$log['to'] = $r->all();

		$x = $task->update($r->all());

		$logger = new Logger;
		$logger->create($r,$log);

		flash()->success('successfully saved!');
		return redirect()->route('lily.tasklist.show',$x);	

	}

} 