<?php namespace App\Http\Controllers\lily;
use Illuminate\Http\Request;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Controllers\generalController as GNC;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Getter;
use App\Http\Models\Lily\desr as Desr;
use App\Http\Models\Lily\user as user_noc;
 
class desrTaskController extends Controller
{

	public function index(desr $desr){
		
		$restrictions = new Restrictions;
		$users = $desr->paginate(15);
		return view('lily.desr.task.index',compact('users','restrictions'));


	}

	public function add($user){

		$restrictions = new Restrictions;
		$selected = [];
		$gnc = new GNC;
		$selections = $gnc->apps();
		if($user->access != ''): 
			$selected = json_decode($user->access,true); 
		endif;
		return view('lily.user.access.add',compact('user','selections','selected','restrictions'));

	}

	public function store(Request $request,$user){

		$user->access = json_encode($request->only('apps')['apps']);
		$user->save();
		return redirect()->route('lily.user.access.index',$user->id);
		
	}

}