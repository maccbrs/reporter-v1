<?php namespace App\Http\Controllers\lily;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Models\Lily\Tasklist;
use App\Http\Models\Lily\Task;
use App\Http\Helpers\Getter;
use App\Http\Requests\lily\doitRequest;
use App\Http\Requests\lily\taskRequest;
use Auth;


class taskController extends Controller
{


	public function __construct(){
		$this->id = Auth::user()->id;
	}

	public function index(){

		$t = new Task;
		$restrictions = new Restrictions;
		$daily =  $this->_sched('daily');
		$weekly =  $this->_sched('weekly');
		$monthly =  $this->_sched('monthly');
		$custom = $this->_customAssigned();
		
		return view('lily.task.index',compact('daily','weekly','monthly','custom','restrictions'));

	}

	public function doit($task){
	//	pre($task->toArray());
		return view('lily.task.doit',compact('task','restrictions'));
	}

	public function _sched($freq){

		$t = new Task; 
		return  $t->select('task.*')
					->leftJoin('tasklist','tasklist.id','=','task.tasklist_id')
					//->leftJoin('users','tasklist.assigned','=','users.id')
					->whereIn('tasklist.assigned',[$this->id,2])
					->where('task.status','=',1)
					->where('task.deadline','>',date('Y-m-d H:i:s'))
					->where('tasklist.frequency','=',$freq)
					->where('tasklist.type','=',1)
					->with(['tasklist'])
					->get();

	}

	public function doitShow($task){

		$restrictions = new Restrictions;
		return view('lily.doit.show',compact('task','restrictions'));

	}

	public function doitUpdate(doitRequest $do, $task){

		//logs
		$logger = new Logger;
		$logger->update($do,$task,$do->except(['_token','_method']));

		$task->remarks = $do->input('remarks');
		$task->status = $do->input('status');
		$task->save();

		flash()->success('Successfully saved!');
		return redirect()->route('lily.task.index');

	}	

	public function create(){

		$get = new Getter;
		$restrictions = new Restrictions;
		$users = $get->by_department(5); //pre($users);			
		return view('lily.task.create2',compact('users','restrictions'));

	}



	public function store(taskRequest $r){

		$tl = new Tasklist;
		$t = new Task;
		$res = $tl->create([
			'title' => $r->input('title'),
			'description' => $r->input('description'),
			'assigned' => $r->input('assigned'),
			'type' => 0
		]); 
		 $t->create([
		 	'tasklist_id' => $res->id,
		 	'deadline' => $r->input('deadline')
		 ]);

		 //logs
		 if($res):
			 $logger = new Logger;
			 $log = ['description' => 'create new task '.$r->input('title').' with id '.$res->id];
			 $logger->create($r,$log);
		 endif;

		 flash()->success('task successfully added!');
		 return redirect()->route('lily.task.index');
	}


	public function _customAssigned(){

		$t = new Task; 
		return  $t->select('task.*','users.name as name')
					->leftJoin('tasklist','tasklist.id','=','task.tasklist_id')
					->leftJoin('users','tasklist.assigned','=','users.id')
					->whereIn('tasklist.assigned',[$this->id,2])
					->where('task.status','=',1)
					->where('task.deadline','>',date('Y-m-d H:i:s'))
					->where('tasklist.type','=',0)
					->with(['tasklist'])
					->get();

	}
}   