<?php namespace App\Http\Controllers\lily;

use App\Http\Controllers\lily\access2Controller as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class accessController extends Controller
{

	public function show($user){

		$restrictions = new Restrictions;
		return view('lily.access.show',compact('user','restrictions'));

	}

	public function edit($user){

		$restrictions = new Restrictions;
		return view('lily.access.edit',compact('user','restrictions'));
		
	}

}