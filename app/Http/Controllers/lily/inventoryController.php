<?php

namespace App\Http\Controllers\lily;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Models\Lily\RtC;
use App\Http\Models\Lily\Answer;
use App\Http\Models\Lily\Did;
use App\Http\Models\Lily\Headset;
use App\Http\Requests\lily\didRequest;
use App\Http\Requests\lily\headsetRequest; 
use App\User;

class inventoryController extends Controller
{

	public function all(){

		$restrictions = new Restrictions;
		return view('lily.inventory.all',compact('restrictions'));

	}


	//-------------------------- dids
	public function did(){


		$dids = $this->did_get('tfn/did'); 
		$restrictions = new Restrictions;
		return view('lily.inventory.did.index',compact('dids','restrictions'));

	}

	public function did_edit($did){

		$restrictions = new Restrictions;
		return view('lily.inventory.did.edit',compact('did','restrictions'));

	}

	public function did_update($did,didRequest $r){ //pre($r->all());

		$input = $r->all();
		$input['answer_id'] = $did->id;
		$this->did_insert($input,$r);
		$did->content = $input['content'];
		$did->save();
		flash()->success('successfully updated!');
		return redirect()->route('lily.inventory.did.edit',$did->id);

	}

	public function did_get($i){

		$ans = new Answer;
		return $ans->where('tag',$i)
				 ->select('answer.*','did.vendor','did.date_subscribe','did.campaign','did.status as did_status')
				 ->leftJoin('did','answer.id','=','did.answer_id')
				 ->get();

	}

	public function did_insert($input,$r = []){

		$did = new Did;
		$logger = new Logger;

		$x = $did->where('answer_id',$input['answer_id'])->first();
		if($x){
			$x->update($input);
			$logger->update($r,$input);	
		}else{
			$did->create($input);
			$logger->create($r,$input);
		}

	}

	//------------------------- headsets

	public function headset(Headset $hs){

		$headsets = $hs->get();
		$restrictions = new Restrictions;
		return view('lily.inventory.headset.index',compact('headsets','restrictions'));

	}

	public function headset_create(){

		$restrictions = new Restrictions;
		return view('lily.inventory.headset.create',compact('restrictions'));

	}

	public function headset_store(headsetRequest $r,Headset $headset){

		$logger = new Logger;
		$x = $headset->create($r->all());
		$logger->create($r,$r->all());
		flash()->success('successfully saved!');
		return redirect()->route('lily.inventory.headset.show',$x->id);

	}

	public function headset_show($headset){

		$restrictions = new Restrictions;
		return view('lily.inventory.headset.show',compact('headset','restrictions'));

	}



} 