<?php namespace App\Http\Controllers\lily;

use App\Http\Controllers\lily\access2Controller as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class lilyController extends Controller
{

	
	public function index(){

		$restrictions = new Restrictions;
		return view('lily.index',compact('restrictions')); 
		
	}

}