<?php namespace App\Http\Controllers\lily;

use App\Http\Controllers\lily\access2Controller as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\Lily\Loggers;
use Auth;

class loggerController
{

 	public function insert($i,$o){


 	}

 	public function get($request,$type = 'view'){
 		
          $routename = $request->route()->getName();
          $routearr = explode('.',$routename);

          $i = [
            'user' => Auth::user()->id,
            'type' => 'browse',
            'date' => ceil(time()/300)*300,
            'app' => $routearr[0],
            'routename' => $routename,
            'content' => json_encode([])
          ];

          $logObj = new Loggers;
          $loggers = $logObj
            ->where('user',$i['user'])
            ->where('type',$i['type'])
            ->where('date',$i['date'])
            ->where('routename',$i['routename'])
            ->first();
          
          if(!$loggers): $logObj->create($i); endif;

 	}

 	public function report($request,$content){
 		
          $routename = $request->route()->getName();
          $routearr = explode('.',$routename);

          $i = [
            'user' => Auth::user()->id,
            'type' => 'report',
            'date' => ceil(time()/300)*300,
            'app' => $routearr[0],
            'routename' => $routename,
            'content' => json_encode([])
          ];

          $logObj = new Loggers;
          $loggers = $logObj
            ->where('user',$i['user'])
            ->where('type',$i['type'])
            ->where('date',$i['date'])
            ->where('routename',$i['routename'])
            ->first();
          
          if(!$loggers): $logObj->create($i); endif;

 	}

 	public function update($request,$fr,$to){
 		
          $routename = $request->route()->getName();
          $routearr = explode('.',$routename);
          $cont['from'] = $fr;
          $cont['to'] = $to; 

          $i = [
            'user' => Auth::user()->id,
            'type' => 'update',
            'date' => ceil(time()/300)*300,
            'app' => $routearr[0],
            'routename' => $routename,
            'content' => json_encode($cont)
          ];

          $logObj = new Loggers;
          $loggers = $logObj
            ->where('user',$i['user'])
            ->where('type',$i['type'])
            ->where('date',$i['date'])
            ->where('routename',$i['routename'])
            ->first();
          
          if(!$loggers): $logObj->create($i); endif;

 	}

 	public function create($request,$cont){ 
 		
          $routename = $request->route()->getName();
          $routearr = explode('.',$routename);

          $i = [
            'user' => Auth::user()->id,
            'type' => 'create',
            'date' => ceil(time()/300)*300,
            'app' => $routearr[0],
            'routename' => $routename,
            'content' => json_encode($cont)
          ];

          $logObj = new Loggers;
          $loggers = $logObj
            ->where('user',$i['user'])
            ->where('type',$i['type'])
            ->where('date',$i['date'])
            ->where('routename',$i['routename'])
            ->first();
          
          if(!$loggers): $logObj->create($i); endif;

 	}

}