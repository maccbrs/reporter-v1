<?php namespace App\Http\Controllers\lily;

use Illuminate\Http\Request;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Getter;
use App\User;
use Auth;
use App\Http\Models\Lily\desr as Desr;
use App\Http\Models\Lily\user as user_noc;

class desrController extends Controller
{ 

	 public function index(user_noc $user){

	 	$restrictions = new Restrictions;
	 	$users = $user->paginate(15);

	 	return view('lily.desr.index',compact('users','restrictions'));
	 	
	 }

	 public function add(){

	 	$restrictions = new Restrictions;

	 	return view('lily.desr.add',compact('restrictions'));

	 }

	 public function create(Request $r,Desr $desr){

	 	$restrictions = new Restrictions; 
	 	$data = $r->all();

	 	$desr->create($data);


	 	flash()->success('user successfully added!');
	 	return redirect()->route('lily.desr.index');

	 }

	 public function edit($user){


	 }

	 public function update(Request $r, $user){
	
	 }


}