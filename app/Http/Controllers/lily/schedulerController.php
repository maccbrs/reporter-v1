<?php namespace App\Http\Controllers\lily;

use App\Http\Controllers\lily\access2Controller as Restrictions;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Models\Lily\Tasklist;
use App\Http\Models\Lily\Task;
use App\User;
use Auth;

class schedulerController extends Controller
{

	public function daily(Tasklist $t,Task $task){

		$data = $t->where('frequency','daily')->get();
		if($data->toArray()){
			foreach ($data as $v) {
				$day = date("Y-m-d H:i:s");
				$input[] = [
				'tasklist_id' => $v->id,
				'deadline' => addDay($day,1),
				'created_at' => $day,
				'updated_at' => $day
				];
			}
			$task->insert($input);			
		}

	}

	public function doit($task){
//		pre($task->toArray());
	}

} 