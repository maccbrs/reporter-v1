<?php die;
namespace App\Http\Controllers\lily;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Requests\doitRequest;
use App\Http\Models\Lily\Doit;

class doitController extends Controller
{

	public function show($task){//pre($task->toArray());
		return view('lily.doit.show',compact('task'));
	}

	public function update(doitRequest $do, $task, Doit $doit){

		$logcont['from'] = $task->status;
		$logcont['to'] = $do->input('status');

		$task->status = $logcont['to'];
		$task->save();

		$input = [
		'tasklist_id' => $task->tasklist->id,
		'remarks' => $do->input('remarks')
		];
		$doit->create($input);
		flash()->success('Successfully saved!');
		return redirect()->route('lily.task.index');
	}

}  