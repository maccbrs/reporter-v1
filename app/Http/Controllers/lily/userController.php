<?php namespace App\Http\Controllers\lily;

use Illuminate\Http\Request;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Getter;
use App\User;
use Auth;
use Image;

class userController extends Controller
{ 

	 public function index(User $user){

	 	$restrictions = new Restrictions;
	 	$users = $user->paginate(15);
	 	return view('lily.user.index',compact('users','restrictions'));

	 }

	 public function add(Getter $getter){

	 	$restrictions = new Restrictions;
	 	return view('lily.user.add',compact('getter','restrictions'));
	 }

	 public function create(Request $r,User $user){

	 	$restrictions = new Restrictions; 
	 	$data = $r->all();
	 	$log = $r->except(['_method','_token','password']);
	 	$data['password'] = bcrypt($r->input('password')); 	
	 	$option['color'] = $r->input('color');
	 	$option['access'] = array('lily');
	 	$data['options'] = json_encode($option);
	 	$user->create($data);

		$logger = new Logger;
		$logger->create($r,$log);
		
	 	flash()->success('user successfully added!');
	 	return redirect()->route('lily.user.index');

	 }

	 public function edit($user){

	 	$restrictions = new Restrictions;
	 	return view('lily.user.edit',compact('user','restrictions'));

	 }

	 public function update(Request $r, $user){

	 	if($r->hasFile('avatar')){
    		$avatar = $r->file('avatar');
    		$filename = $user->id . /*time() . */ '.' . $avatar->getClientOriginalExtension();

    		Image::make($avatar)->resize(300,300)->save( public_path('uploads/avatars/' . $filename));
    		$useravatar = $user;
    		$useravatar->avatar = $filename;
    		$useravatar->save();
	    
    	}

	 	$log['from'] = $user->toArray();
	 	$data = $r->all();
	 	$options = ($user->options != ''?json_decode($user->options):json_decode("{}"));
	 	if($options):
		 	$options->color = $r->input('color');
		 	$data['options'] = json_encode($options);
			$logger = new Logger;
			$logger->update($r,$user,$data);  
		 	$user->update($data);	 		
	 	endif;
	 	flash()->success('record successfully updated');

	 	return redirect()->route('lily.user.index');
	 	
	 }


}