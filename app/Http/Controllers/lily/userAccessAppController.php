<?php namespace App\Http\Controllers\lily;
use Illuminate\Http\Request;
use App\Http\Controllers\lily\loggerController as Logger;
use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Getter;
use App\User;
use Route;
use DB;

class userAccessAppController extends Controller
{

	public function index($user,$app){
		
			$remove = ['logout','404'];
			$userRoutes = [];

			$routeCollection = Route::getRoutes();
			$arr = [];

			foreach ($routeCollection as $value) {

			    $x = $value->getName();
			    $arr[$x] = $x;

			} 

			$routenames = array_unique($arr);

			foreach ($routenames as $k => $v):
			    $rn = explode('.', $v);
			    if(count($rn) > 1):
			        $arrRoutes[$k]['app'] = $rn[0];
			    	if(count($rn) > 2):
			    		$arrRoutes[$k]['class'] = $rn[1];
			    		if(count($rn) > 3):
			    			$arrRoutes[$k]['method'] = $rn[2].' '.$rn[3];
			    		else:
			    		 	$arrRoutes[$k]['method'] = $rn[2];
			    		endif;
			    	else:
			    		$arrRoutes[$k]['class'] = $rn[0];
			    		$arrRoutes[$k]['method'] = $rn[1];
			        endif;               
			    endif;
			endforeach; 
			//pre($arrRoutes);
			$finalRoutes = [];
			foreach ($arrRoutes as $k => $v):
				if($v['app'] == $app):
					$finalRoutes[$v['class']]['method'][$k] = $v['method'];
				endif;
			endforeach;
			$routes = $finalRoutes;

			if($user->routes != ''):
				$aRoutes = json_decode($user->routes,true);
				$userRoutes = (isset($aRoutes[$app])?$aRoutes[$app]:[]);
			endif;

			$restrictions = new Restrictions;
			return view('lily.user.access.app.index',compact('routes','user','app','userRoutes','restrictions'));	

	}

	public function update(Request $request,$user){ 

 		if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300,300)->save( public_path('uploads/avatars/' . $filename));
    		$useravatar = Auth::user();
    		$useravatar->avatar = $filename;
    		$useravatar->save();
	    
    	}


		$routes = [];
		if($user->routes != ''): $routes = json_decode($user->routes,true); endif;
		$routes[$request->input('app')] = $request->input('routes');
		$user->routes = json_encode($routes);
		$user->save();
		flash()->success('successfully updated!');
		return redirect()->back();

	}


}