<?php namespace App\Http\Controllers\lily;

use App\Http\Controllers\lily\access2Controller as Restrictions;
use App\Http\Controllers\generalController;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Lily as Helper;
use Illuminate\Http\Request;
use App\Http\Requests;
use Route;
use DB;

class logsController extends Controller
{

	public function index(Request $r){

		$restrictions = new Restrictions;
        $remove = ['logout','404'];

        $routeCollection = Route::getRoutes();
        $arr = [];

        foreach ($routeCollection as $value) {

            $x = $value->getName();
            $arr[$x] = $x;

        }

        $routenames = array_unique($arr);
//        pre($routenames);

        foreach ($routenames as $k => $v):
            $rn = explode('.', $v);
            if(count($rn) > 1):
	            $arrRoutes[$k]['app'] = $rn[0];
	        	if(count($rn) > 2):
	        		$arrRoutes[$k]['class'] = $rn[1];
	        		if(count($rn) > 3):
	        			$arrRoutes[$k]['method'] = $rn[2].' '.$rn[3];
	        		else:
	        		 	$arrRoutes[$k]['method'] = $rn[2];
	        		endif;
	        	else:
	        		$arrRoutes[$k]['class'] = $rn[0];
	        		$arrRoutes[$k]['method'] = $rn[1];
	            endif;               
            endif;
        endforeach; 
        //pre($arrRoutes);
        $finalRoutes = [];
        foreach ($arrRoutes as $k => $v):
        	$finalRoutes[$v['app']][$v['class']]['method'][$k] = $v['method'];
        endforeach;
        //pre($finalRoutes);

		return view($r->route()->getName(),compact('arrRoutes','finalRoutes','restrictions'));		

	}

	public function viewlogs($logs){

		$gn = new generalController;
		$conn = $gn->connection($logs);
		$logs = DB::connection($conn)->table('loggers')->where('routename',$logs)->paginate(5);
		$restrictions = new Restrictions;
		return view('lily.logs.viewlogs',compact('logs','restrictions'));

	}

	public function show($log){

		$restrictions = new Restrictions;
		$content = json_decode($log['content'],true);
		$type = (isset($log->type)?$log->type:'browse');
		return view('lily.logs.'.$type,compact('log','content','restrictions'));
		
	}

}

//$arr[$x] = $x;