<?php namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Helpers\primrose\Primrose; 
use Auth;
use Route;

class generalController extends Controller
{

	public function apps(){
		return [
			'bouquet','lily','primrose','rosebud','prepaid','dahlia'
		];
	}

	public function connection($route){

		$apps = $this->apps();
		$connection = $apps[0];

		if($route != ''):
			$routearr = explode('.',$route);
			if(in_array($routearr[0], $apps)):
				$connection = $routearr[0];
			endif;
		endif;

		return $connection;

	}

	public function logs($route){
		
	}


	public function time_formatter($fr,$t){

		$help = new Primrose;
		$from = explode(' ', $fr);
		$to = explode(' ', $t);

		if(in_array($from[2], $tmz)):
			$from2 = $help->convert_tz($from[0].' '.$from[1],$from[2],Config::get('app.timezone')); 
			$to2 = $help->convert_tz($to[0].' '.$to[1],$from[2],Config::get('app.timezone')); 
			$timez = $from[2];
		else:
			$timez = Config::get('app.timezone');
			$from2 = date("Y-m-d H:i:s",strtotime($from[0].' '.$from[1]));
			$to2 = date("Y-m-d H:i:s",strtotime($to[0].' '.$to[1]));
		endif;

		return [
			'from' => $from2,
			'to' => $to2,
			'tz' => $timez
		];
	}

	public function routes(){

		$routeCollection = Route::getRoutes();
		$arr = [];

		foreach ($routeCollection as $value) {

		    $x = $value->getName();
		    $arr[$x] = $x;

		} 

		$routenames = array_unique($arr);	
		pre($routenames);	
	}	

	public function quotes(){

		$opts = array(
		  'http'=>array(
		    'method'=>"GET"
		  )
		);
		$context = stream_context_create($opts);
		$file = file_get_contents('http://quotes.rest/qod.json?category=inspire', false, $context);
		return json_decode($file);

	}
	

 
}