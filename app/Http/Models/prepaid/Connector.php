<?php namespace App\Http\Models\prepaid;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Connector extends Model
{
   protected $connection = 'prepaid';
   protected $table = 'connectors';
   protected $fillable = ['subscriber_id','did','status'];

	public function subscriber(){
		return $this->hasOne('App\Http\Models\prepaid\Subscriber','id','subscriber_id');
	}     
}