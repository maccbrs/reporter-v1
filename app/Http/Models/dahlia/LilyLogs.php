<?php namespace App\Http\Models\dahlia;

use Illuminate\Database\Eloquent\Model;
use Auth;

class LilyLogs extends Model
{
   protected $connection = 'lily';
   protected $table = 'loggers';
   protected $fillable = ['user','type','routename','date','content','created_at','updated_at','app'];

   public function users(){
        return $this->belongsTo('App\User','user','id');
   }



}