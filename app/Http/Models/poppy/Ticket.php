<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Ticket extends Model
{

   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_ticket';
   protected $fillable = ['number','user_id','user_email_id','status_id','dept_id','sla_id','topic_id','staff_id',
   'team_id','email_id','flags','ip_address','source','isoverdue','isanswered','duedate','reopened','closed','lastmessage','lastresponse','created','updated'];

   public function threads(){
   		return $this->hasMany('App\Http\Models\poppy\TicketThread','ticket_id','ticket_id')
                     ->orderBy('created','desc')
                     ->with(['user']);
   }

   public function subject(){
 		return $this->hasOne('App\Http\Models\poppy\TicketCdata','ticket_id','ticket_id');  	
   }

	public function scopeOwner($query,$user)
	{
	    return $query->where('user_id', $user);
	}  

	public function scopeOrder($query,$order)
	{
	    return $query->orderBy('lastresponse', $order);
	}  
}