<?php namespace App\Http\Models\poppy;

use Illuminate\Database\Eloquent\Model;
use Auth;

class TicketThread extends Model
{
   public $timestamps = false;
   protected $connection = 'poppy';
   protected $table = 'ost_ticket_thread';
   protected $fillable = ['pid','ticket_id','staff_id','user_id','thread_type','poster','source','title','body','format','ip_address','ip_address','created','updated'];



   public function user(){
		return $this->belongsTo('App\Http\Models\poppy\User','user_id');
   }


}