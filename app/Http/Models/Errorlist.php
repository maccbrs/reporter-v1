<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Errorlist extends Model
{
   protected $connection = 'bouquet';
   protected $table = 'errorlists';
   protected $fillable = ['desc','website'];

   public $timestamps = false;
}
