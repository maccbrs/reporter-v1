<?php namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;

class Updates extends Model
{
    protected $connection = '192.168.200.28.laravel';   
    protected $table = 'updates'; 
    protected $fillable = ['status','count', 'contents','campaign_id']; 

    public function scopeActive($query)
    {
        return $query->where('count','!=', 0);
    }

    //old
    public function board(){
        return $this->belongsTo('App\Http\Models\sunflower\Board','campaign_id','campaign_id');
    }
 
    public function mb_update($cid,$cont,$count){
    	if(!$this->where('campaign_id',$cid)->update(['contents' => $cont,'count' => $count])){
            $data = array(
                'campaign_id' => $cid,
                'count' => 1,
                'contents' => $cont
            );
            $this->create($data);
        }
    }

    public function getby_campaign($cid){
    	return $this->where('campaign_id',$cid)->first();
    }
}