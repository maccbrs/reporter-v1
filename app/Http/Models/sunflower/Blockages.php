<?php  namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Blockages extends Model
{
    protected $table = 'blockages';
    protected $fillable = ['name','campaigns', 'created_at', 'updated_at']; 

}