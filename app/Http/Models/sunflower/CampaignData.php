<?php  namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;

class CampaignData extends Model
{
    protected $table = 'campaign_data';
    protected $fillable = ['status','campaign_id', 'lob','contents','active']; 



} 