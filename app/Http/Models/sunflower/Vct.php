<?php namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;

class Vct extends Model
{

	protected $connection = '192.168.200.28.laravel';
    protected $table = 'vici_to_templar'; 
    protected $fillable = ['vici_plug','templar_socket', 'status','server','bound','name'];  


    public function exist($x,$y,$server,$bound){
       return $this->where('vici_plug',$x)
            ->where('templar_socket',$y)
            ->where('server',$server)
            ->where('bound',$bound)
            ->count();
    }


}