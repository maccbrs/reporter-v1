<?php namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $connection = '192.168.200.28.laravel';
    protected $table = 'pages';
    protected $fillable = ['status','campaign_id', 'title', 'template','contents','board_id','options']; 


    public function board(){
        return $this->belongsTo('\App\Http\Models\sunflower\Board');
    }
}