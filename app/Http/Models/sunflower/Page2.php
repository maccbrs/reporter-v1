<?php namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;

class Page2 extends Model
{
	protected $connection = '192.168.200.28.laravel';
    protected $table = 'published_pages';
    protected $fillable = ['status','id','campaign_id', 'title', 'template','contents','board_id','options']; 


    public function board(){
        return $this->belongsTo('App\Model\Board2','id');
    }
}