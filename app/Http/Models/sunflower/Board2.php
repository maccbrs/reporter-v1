<?php  namespace App\Http\Models\sunflower;

use Illuminate\Database\Eloquent\Model;

class Board2 extends Model
{
	protected $connection = '192.168.200.28.laravel';
    protected $table = 'published_boards';
    protected $fillable = ['status','id','campaign_id', 'lob', 'to','cc','from','primary_page','test_email','campaign']; 
    

    public function pages(){
        return $this->hasMany('App\Http\Models\sunflower\Page2','board_id'); 
    }      
}