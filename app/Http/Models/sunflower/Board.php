<?php  namespace App\Http\Models\sunflower;
use DB;
use Illuminate\Database\Eloquent\Model;
use Auth;
class Board extends Model
{

    protected $connection = '192.168.200.28.laravel';
    protected $table = 'boards';
    protected $fillable = ['status','campaign_id', 'lob', 'to','cc','from','primary_page','test_email','campaign']; 
    
    public function scopeActive($query){
        return $query->where('status',1);
    }

    public function pages(){
        return $this->hasMany('\App\Http\Models\sunflower\Page'); 
    } 

    public function updates(){
        return $this->hasMany('\App\Http\Models\sunflower\Updates','campaign_id','campaign_id'); 
    }  

    public function assigned(){
        return $this->hasMany('\App\Http\Models\sunflower\ConCampaigns','campaign_id','campaign_id');         
    }

    public function  mb_updates($in = []){

        if(!empty($in))
		return DB::table('boards')
        ->join('updates', 'boards.campaign_id', '=', 'updates.campaign_id')
        ->where('updates.count', '>', 0)
        ->whereIn('boards.campaign_id',$in)
        ->get();

        return DB::table('boards')
        ->join('updates', 'boards.campaign_id', '=', 'updates.campaign_id')
        ->where('updates.count', '>', 0)
        ->get();
    }  
    
    public function get_campaigns($ids){
       return $this->whereIn('campaign_id',$ids)->get(); 
    }

    public function templar_socket(){
        return $this->groupBy('campaign_id')->lists('campaign','campaign_id')->toArray();
    }

      
}