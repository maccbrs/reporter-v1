<?php namespace App\Http\Models\blockage;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Blockage extends Model
{

   protected $connection = 'rhyolite';
   protected $table = 'blockage';
   protected $fillable = ['emails','contents','subject','sent'];

} 