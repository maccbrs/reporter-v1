<?php namespace App\Http\Models\laravel;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DummyboardUser extends Model
{
   protected $connection = 'dummyboard';
   protected $table = 'users';
   protected $fillable = ['name','id'];

    
}