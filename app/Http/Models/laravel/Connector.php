<?php namespace App\Http\Models\laravel;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Connector extends Model
{
   protected $connection = 'laravel';
   protected $table = 'connectors';
   protected $fillable = ['subscriber_id','did'];

	public function subscriber(){
		return $this->hasOne('App\Http\Models\prepaid\Subscriber','id','subscriber_id');
	}     
}