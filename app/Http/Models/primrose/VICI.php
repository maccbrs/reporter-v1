<?php  

namespace App\Http\Models\primrose;

use Illuminate\Database\Eloquent\Model; 
use DB;
class VICI extends Model
{

    protected $connection = '192.168.200.132';
    protected $table = 'vicidial_closer_log';

    public function ingroups($bound){
     
        $conn = '192.168.200.132';

        switch ($bound):
            case 'out':
                $table = 'vicidial_log';
                break;
            
            default:

                $table = 'vicidial_closer_log';

                break;
        endswitch;

        return DB::connection($conn)
                        ->table($table)
                        ->select('campaign_id')
                        ->groupBy('campaign_id')
                        ->lists('campaign_id');
    }

}