<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'pages';
    protected $fillable = ['status','campaign_id', 'title', 'template','contents','board_id','options']; 


    public function board(){
        return $this->belongsTo('App\Http\Models\primrose\dummyboard\Board');
    }
}