<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Eod extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'eod_reports';
    protected $fillable = ['from','to','cc','content','bcc','name','subject','status','date']; 


}