<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;

class ConCampaigns extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'con_user_campaign';
    protected $fillable = ['users_id','campaign_id', 'reports','status','email'];  

    public function board(){
        return $this->belongsTo('App\Http\Models\primrose\dummyboard\Board','campaign_id','campaign_id');
    }

}