<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Emailer extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'emailers';
    protected $fillable = ['from','to','cc','content','bcc','name','subject']; 


}