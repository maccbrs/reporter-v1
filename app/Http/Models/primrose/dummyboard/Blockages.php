<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Blockages extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'blockages';
    protected $fillable = ['name','campaigns', 'created_at', 'updated_at']; 

}