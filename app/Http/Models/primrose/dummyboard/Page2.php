<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;

class Page2 extends Model
{

	protected $connection = 'dummyboard';
    protected $table = 'published_pages';
    protected $fillable = ['status','id','campaign_id', 'title', 'template','contents','board_id','options']; 


    public function board(){
        return $this->belongsTo('App\Http\Models\primrose\dummyboard\Board2','id');
    }
}