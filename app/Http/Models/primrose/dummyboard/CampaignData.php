<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;

class CampaignData extends Model
{
	protected $connection = 'dummyboard'; 
    protected $table = 'campaign_data';
    protected $fillable = ['status','campaign_id', 'lob','contents','active','pageid']; 



} 