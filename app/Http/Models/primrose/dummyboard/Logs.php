<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Logs extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'logs';
    protected $fillable = ['status','campaign_id','contents','users_id']; 

}