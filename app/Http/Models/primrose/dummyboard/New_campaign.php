<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;

class New_campaign extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'new_campaign';
    protected $fillable = ['campaign_id', 'lob']; 

}