<?php namespace App\Http\Models\primrose\dummyboard;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Catcher extends Model
{
	protected $connection = 'dummyboard';
    protected $table = 'catcher';
    protected $fillable = ['content']; 


}