<?php
namespace App\Http\Models\primrose;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Viciconn extends Model
{
   protected $connection = 'primrose'; 
   protected $table = 'viciconn'; 
   protected $fillable = ['server','name','dids','type','bound','timezone','created_at','updated_at'];



}