<?php namespace App\Http\Models\primrose\connectors;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PageConnectors extends Model
{
  
   protected $connection = 'dummyboard';
   protected $table = 'page_connectors'; 
   protected $fillable = ['pageids','title','created_at','updated_at'];


} 