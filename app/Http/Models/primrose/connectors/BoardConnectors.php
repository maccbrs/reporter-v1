<?php namespace App\Http\Models\primrose\connectors;

use Illuminate\Database\Eloquent\Model;
use Auth;

class BoardConnectors extends Model
{
  
   protected $connection = 'dummyboard';
   protected $table = 'vici_to_templar'; 
   protected $fillable = ['name','vici_plug','templar_socket','server','bound','options','status','created_at','updated_at'];


} 