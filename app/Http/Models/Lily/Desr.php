<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Desr extends Model
{
   protected $connection = 'lily';
   protected $table = 'desr';
   protected $fillable = ['current_act','pending_act','date_assigned','emp_assigned'];


	// public function answer(){
	// 	return $this->hasOne('App\Http\Models\Answer');
	// }   
}