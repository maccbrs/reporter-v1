<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class User extends Model
{
   protected $connection = 'lily';
   protected $table = 'users';
   protected $fillable = ['name','email','password'];


	// public function answer(){
	// 	return $this->hasOne('App\Http\Models\Answer');
	// }   
}