<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Task extends Model
{
    protected $connection = 'lily';
    protected $table = 'task';
    protected $fillable = ['tasklist_id','remarks','deadline','status','created_at','updated_at'];


	public function tasklist(){
		return $this->hasOne('App\Http\Models\Lily\Tasklist','id','tasklist_id');
	}   

} 