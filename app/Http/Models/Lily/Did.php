<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Did extends Model
{
   protected $connection = 'lily';
   protected $table = 'did';
   protected $fillable = ['answer_id','vendor','date_subscribe','campaign','status','created_at','updated_at'];


	public function answer(){
		return $this->hasOne('App\Http\Models\Answer');
	}   
}