<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Headset extends Model
{
	protected $connection = 'lily';
   protected $table = 'headset';
   protected $fillable = ['type','serial','date_issued','user','condition','history','created_at','updated_at'];


	// public function answer(){
	// 	return $this->hasOne('App\Http\Models\Answer');
	// }   
}