<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DoneTask extends Model
{
   protected $connection = 'lily';
   protected $table = 'done_tasklist';
   protected $fillable = ['remarks','status','created_at','updated_at'];


	// public function answer(){
	// 	return $this->hasOne('App\Http\Models\Answer');
	// }   
}