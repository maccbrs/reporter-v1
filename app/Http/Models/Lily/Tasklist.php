<?php
namespace App\Http\Models\Lily;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Tasklist extends Model
{
   protected $connection = 'lily';
   protected $table = 'tasklist';
   protected $fillable = ['title','description','assigned','frequency','day','type','shift','created_at','updated_at'];


	public function user(){
		return $this->hasOne('App\User','id','assigned');
	}   
} 