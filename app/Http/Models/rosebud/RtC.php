<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class RtC extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'requirement_to_campaign';
   protected $fillable = [
   		'requirement_id',
   		'campaign_id',
   		'users_id',
   		'type',
   		'to',
   		'title',
   		'instructions',
   		'remarks',
   		'status',
   		'options'
   		];


   public function requirement(){
      return $this->hasOne('App\Http\Models\rosebud\Requirement', 'id','requirement_id');
   }

   public function campaign(){
      return $this->hasOne('App\Http\Models\rosebud\Campaign', 'id','campaign_id');
   }

   public function client_status(){
      return $this->hasOne('App\Http\Models\rosebud\ClientStatus', 'campaign_id','campaign_id');
   }

   public function user(){
      return $this->hasOne('App\User', 'id','users_id');
   }

   public function answer(){
      return $this->hasMany('App\Http\Models\rosebud\Answer','rtc_id');
   }

   public function message(){
      return $this->hasOne('App\Http\Models\rosebud\Message', 'rtc_id');
   }


}

 