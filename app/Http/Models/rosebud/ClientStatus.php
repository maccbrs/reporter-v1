<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ClientStatus extends Model
{
	protected $connection = 'rosebud';
   protected $table = 'client_status'; 
   protected $fillable = ['status','campaign_id','created_at','updated_at','due_date'];


}