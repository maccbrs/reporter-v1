<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Message extends Model
{
	protected $connection = 'rosebud';
   protected $table = 'messages';
   protected $fillable = ['content','rtc_id','created_at','updated_at'];
}