<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Cluster extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'clusters';
   protected $fillable = ['name','status','created_at','updated_at'];


   	public function utc(){
		return $this->hasMany('App\Http\Models\rosebud\User2Cluster', 'clusters_id')->with(['user']);
	}   

}