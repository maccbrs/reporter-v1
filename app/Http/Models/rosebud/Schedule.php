<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Schedule extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'schedule';
   protected $fillable = [
   		'users_id',
   		'from',
   		'to',
   		'title',
   		'description',
   		'status',
   		'users_id',
   		'type',
   		'color'
   		];

}