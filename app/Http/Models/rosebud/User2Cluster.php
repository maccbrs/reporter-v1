<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class User2Cluster extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'usertocluster';
   protected $fillable = [
   		'users_id',
   		'clusters_id',
   		'status',
   		'created_at',
   		'updated_at'
   		];

	public function user(){
		return $this->hasOne('App\User','id', 'users_id')->with(['schedules']);
	}

}