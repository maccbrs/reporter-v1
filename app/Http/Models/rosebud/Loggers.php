<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model; 
use Auth;

class Loggers extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'loggers';
   protected $fillable = ['user','type','routename','date','content','created_at','updated_at','app'];

   public function users(){

        return $this->belongsTo('App\User','user','id');

   }

}