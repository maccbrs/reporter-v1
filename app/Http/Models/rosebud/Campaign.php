<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Campaign extends Model
{
	protected $connection = 'rosebud';
   protected $table = 'campaign'; 
   protected $fillable = [
		'project_name',				
		'project_alias',
		'launch_date',								
		'campaign_stages',	 			
		'industries',				
		'country_focus',				
		'commercial_arrangement',											
		'type_of_program',				
		'project_description',	
		'contract_period',				
		'company_name',				
		'contact_name',				
		'title',				
		'contact_no',				
		'email_address',
		'web_url',				
		'company_address',				
		'call_volume_history',				
		'service_coverage',				
		'estimated_call_volume',
		'estimated_aht',				
		'call_flow',				
		'agent_headcount',				
		'agent_level',				
		'screening_process',	
		'job_description',	
		'status',	
		'options',				
		'user_id',
		'created_at',
		'updated_at'	
   ];

   	public function checklist(){
		return $this->hasMany('App\Http\Models\rosebud\RtC', 'campaign_id')->with(['requirement','answer','client_status']);
	}

	public function client_status(){
		return $this->hasOne('App\Http\Models\rosebud\ClientStatus', 'campaign_id');
	}


} 