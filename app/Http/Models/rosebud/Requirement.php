<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Requirement extends Model
{
	protected $connection = 'rosebud';
   protected $table = 'requirements';
   protected $fillable = ['name','department_id','category','sla'];
}