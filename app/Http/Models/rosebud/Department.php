<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Department extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'department';
   protected $fillable = ['name','id'];
}