<?php
namespace App\Http\Models\rosebud;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Answer extends Model
{
   protected $connection = 'rosebud';
   protected $table = 'answer'; 
   protected $fillable = ['rtc_id','content','tag','created_at','updated_at','status'];
}