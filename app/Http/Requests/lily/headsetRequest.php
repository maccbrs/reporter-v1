<?php

namespace App\Http\Requests\lily;

use App\Http\Requests\Request;

class headsetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'serial' => 'required',
            'date_issued' => 'required',
            'user' => 'required',
            'condition' => 'required'
        ];
    }
}
