<?php

namespace App\Http\Requests\rosebud;

use App\Http\Requests\Request;

class storeAnswerReq extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rtc_id' => 'required',
            'content' => 'required',
            'tag' => 'required'
        ];
    }
}
