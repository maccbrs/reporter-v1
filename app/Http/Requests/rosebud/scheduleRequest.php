<?php

namespace App\Http\Requests\rosebud;

use App\Http\Requests\Request;

class scheduleRequest extends Request
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'from' => 'required',
            'to' => 'required',
            'title' => 'required'
        ];
    }
    
}
