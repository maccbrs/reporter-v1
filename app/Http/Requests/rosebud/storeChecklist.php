<?php

namespace App\Http\Requests\rosebud;

use App\Http\Requests\Request;

class storeChecklist extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'requirement_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'requirement_id.required' => 'Name is required'
        ];
    }    
}
