<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon; 
use App\Http\Models\Loggers;
use DB;

class loggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {

       $response = $next($request);

       if (!Auth::guard($guard)->guest() && $request->server()['REQUEST_METHOD'] == 'GET'):
        
            $routename = $request->route()->getName();
        //  pre($routename);
          if($routename):

            $routearr = explode('.',$routename);
            $except = ['landing','404'];
            if(!in_array($routearr[0],$except)):
              $date = date("Y-m-d H:i:s");
              $i = [
                'user' => Auth::user()->id,
                'type' => 'browse',
                'date' => ceil(time()/300)*300,
                'app' => $routearr[0],
                'routename' => $routename,
                'content' => json_encode([]), 
                'created_at' => $date,
                'updated_at' => $date
              ];

              $bouquet = ['home'];
              $conn = (in_array($routearr[0],$bouquet)?'bouquet':$routearr[0]);
              $logObj = DB::connection($conn)->table('loggers');

              $loggers = $logObj
                ->where('user',$i['user'])
                ->where('type',$i['type'])
                ->where('date',$i['date'])
                ->where('routename',$i['routename'])
                ->first();
              
              if(!$loggers): $logObj->insert($i); endif;
            endif;
            
          endif;
       endif;

       return $response;
    }

} 
