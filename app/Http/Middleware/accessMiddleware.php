<?php namespace App\Http\Middleware;
use Closure;
use Auth;
use App\Http\Controllers\generalController as GC;

class accessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if(is_object($request->route())){


        $response = $next($request);

            $routeString = $request->route()->getName();
            $routename = explode('.',$routeString);
            $except = ['landing',404,'login','live'];

            $routes = [];
            if(!in_array($routename[0],$except)):
                if(!isset(Auth::user()->id)): return redirect('/login'); endif;
            else:
                if(!isset(Auth::user()->id)): return $response; endif;
            endif;

            if(Auth::user()->user_type == 'admin'):
                $access = ['dahlia','rosebud','primrose','lily'];
            else:
                $access = (Auth::user()->access != ''?json_decode(Auth::user()->access,true):[]);
            endif;

            $routesArr = (Auth::user()->routes != ''?json_decode(Auth::user()->routes,true):[]);
            if($routesArr):
                foreach ($routesArr as $val):
                    if($val):
                        foreach ($val as $v) {
                            $routes[] = $v;
                        }
                    endif;
                endforeach;
            endif;
            
            if (isset(Auth::user()->id) && !in_array($routename[0],$except)):
                if(!in_array($routename[0], $access) && Auth::user()->user_type != 'admin'): return redirect()->route('404'); endif;
                $index = $routename[0].'.index';
                if(!in_array($routeString,$routes) && $routeString != $index && Auth::user()->user_type != 'admin'):
                    return redirect()->route($index);
                else:
                    Auth::user()->routes = $routes;
                endif;
            endif;
            
            return $response;

        }else{

            $response = $next($request);
            $routes = [];
            if(!isset(Auth::user()->id)): return $response; endif;

            if(Auth::user()->user_type == 'admin'):
                $access = ['dahlia','rosebud','primrose','lily'];
            else:
                $access = (Auth::user()->access != ''?json_decode(Auth::user()->access,true):[]);
            endif;

            $routesArr = (Auth::user()->routes != ''?json_decode(Auth::user()->routes,true):[]);
            if($routesArr):
                foreach ($routesArr as $val):
                    if($val):
                        foreach ($val as $v) {
                            $routes[] = $v;
                        }
                    endif;
                endforeach;
            endif;

            $routeString = $request->route()->getName();
            $routename = explode('.',$routeString);
            $except = ['landing',503,'login',''];
            
            if (isset(Auth::user()->id) && !in_array($routename[0],$except)):
                if(!in_array($routename[0], $access) && Auth::user()->user_type != 'admin'): return redirect()->route('404'); endif;
                $index = $routename[0].'.index';
                if(!in_array($routeString,$routes) && $routeString != $index && Auth::user()->user_type != 'admin'):
                    return redirect()->route($index);
                else:
                    Auth::user()->routes = $routes;
                endif;
            endif;
            
            return $response; 

        }

    }
}
 