<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('prepaid.admin.index'));
});

// SUBSCRIBER

Breadcrumbs::register('SubscriberView', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Subscriber', route('prepaid.subscriber.index'));
});

Breadcrumbs::register('SubscriberAdd', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Add ', route('prepaid.subscriber.create'));
});

Breadcrumbs::register('SubscriberShow', function($breadcrumbs, $id)
{	

    $breadcrumbs->parent('SubscriberView');
    $breadcrumbs->push('Show', route('prepaid.subscriber.show', $id));
});

Breadcrumbs::register('SubscriberEdit', function($breadcrumbs, $id)
{	

    $breadcrumbs->parent('SubscriberView');
    $breadcrumbs->push('Edit', route('prepaid.subscriber.edit', $id));
});


// USER

Breadcrumbs::register('UserView', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('User', route('prepaid.user.index'));
});

Breadcrumbs::register('UserAdd', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Add User', route('prepaid.user.create'));
});

Breadcrumbs::register('UserEdit', function($breadcrumbs, $id)
{	

    $breadcrumbs->parent('UserView');
    $breadcrumbs->push('Edit', route('prepaid.user.edit', $id));
});

Breadcrumbs::register('UserShow', function($breadcrumbs, $id)
{	

    $breadcrumbs->parent('UserView');
    $breadcrumbs->push('Show', route('prepaid.user.show', $id));
});

// Connector

Breadcrumbs::register('ConnectorView', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Connector', route('prepaid.connector.index'));
});

Breadcrumbs::register('ConnectorAdd', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Add ', route('prepaid.connector.create'));
});

Breadcrumbs::register('ConnectorEdit', function($breadcrumbs, $id)
{	

    $breadcrumbs->parent('ConnectorView');
    $breadcrumbs->push('View', route('prepaid.connector.edit', $id));
});

Breadcrumbs::register('ConnectorShow', function($breadcrumbs, $id)
{	

    $breadcrumbs->parent('ConnectorView');
    $breadcrumbs->push('Show', route('prepaid.connector.show', $id));
});


// Load

Breadcrumbs::register('LoadAdd', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('SubscriberShow', $id);
    $breadcrumbs->push('Load', route('prepaid.load.create', $id));
});

// Overage

Breadcrumbs::register('OverageAdd', function($breadcrumbs, $id)
{
    $breadcrumbs->parent('SubscriberShow', $id);
    $breadcrumbs->push('Overage', route('prepaid.reserved.create', $id));
});

//PDF

Breadcrumbs::register('LoadPDF', function($breadcrumbs, $load_id, $subscriber_id)
{
    $breadcrumbs->parent('SubscriberShow', $subscriber_id);
    $breadcrumbs->push('Load', route('prepaid.reserved.create',$load_id, $subscriber_id));
});


