<?php  


$router->group(['namespace' => 'sunflower\backend'],function(){

	$this->group(['middleware' => ['auth'],'prefix' => 'sunflower'],function(){	

			$this->get('/',['as' => 'sunflower.index','uses' => 'boardController@index']);

			$this->group(['prefix' => 'boards'],function(){
				$this->get('/',['as' => 'sunflower.board.index','uses' => 'boardController@index']); 
				$this->get('add',['as' => 'sunflower.board.add','uses' => 'boardController@add']);
				$this->post('save',['as' => 'sunflower.board.add.save','uses' => 'boardController@save']);
				$this->get('edit/{board}',['as' => 'sunflower.board.edit','uses' => 'boardController@edit']); 
				$this->post('update/{board}',['as' => 'sunflower.board.update','uses' => 'boardController@update']);
				$this->get('delete/{board}',['as' => 'sunflower.board.delete','uses' => 'boardController@delete']); 
				$this->get('test',['as' => 'sunflower.board.test','uses' => 'boardController@test']);
				$this->group(['prefix' => 'mail'],function(){
					$this->get('/{board}',['as' => 'sunflower.board.mail','uses' => 'boardController@mail']);
					$this->post('/save/{board}',['as' => 'sunflower.board.mail-save','uses' => 'boardController@mail_save']);
				});

			});    

			$this->group(['prefix' => 'vct'],function(){
				$this->get('/',['as' => 'sunflower.vct.index','uses' => 'vctController@index']);
				$this->get('/create',['as' => 'sunflower.vct.create','uses' => 'vctController@create']);
				$this->post('/create',['as' => 'sunflower.vct.store','uses' => 'vctController@store']);
				$this->post('/{vct_id}/delete',['as' => 'sunflower.vct.destroy','uses' => 'vctController@destroy']);
			}); 

			$this->group(['prefix' => 'page'],function(){
				$this->get('{board}/add',['as' => 'sunflower.page.add','uses' => 'pageController@add']);
				$this->post('create/{boardId}',['as' => 'sunflower.page.create','uses' => 'pageController@create']); 
				$this->get('edit/{page}',['as' => 'sunflower.page.edit','uses' => 'pageController@edit']);
				$this->post('delete/{pageId}',['as' => 'sunflower.page.destroy','uses' => 'pageController@destroy']);
				$this->post('update/{page}',['as' => 'sunflower.page.update','uses' => 'pageController@update']);
 			});

			$this->group(['prefix' => 'publish'],function(){
				$this->get('/',['as' => 'sunflower.publish.index','uses' => 'publishController@index']);
				$this->post('/{board}/update',['as' => 'sunflower.publish.update','uses' => 'publishController@update']);
			});

			$this->group(['prefix' => 'campaign'],function(){
				$this->get('/',['as' => 'sunflower.campaign.index','uses' => 'campaignController@index']);
				$this->get('/add',['as' => 'sunflower.campaign.add','uses' => 'campaignController@add']);
				$this->post('/add',['as' => 'sunflower.campaign.add-create','uses' => 'campaignController@create']);
				$this->get('/delete/{id}',['as' => 'sunflower.campaign.delete','uses' => 'campaignController@delete']);
			});

			$this->group(['prefix' => 'emailer'],function(){
				$this->get('/',['as' => 'sunflower.emailer.index','uses' => 'emailerController@index']);
				$this->get('/edit/{emailer_id}',['as' => 'sunflower.emailer.edit','uses' => 'emailerController@edit']);
				$this->post('/delete/{emailer_id}',['as' => 'sunflower.emailer.destroy','uses' => 'emailerController@destroy']);

				//old
				$this->get('/add',['as' => 'sunflower.emailer.add','uses' => 'emailerController@add']);
				$this->post('/create',['as' => 'sunflower.emailer.create','uses' => 'emailerController@create']);
				
				$this->post('/edit/{emailer_id}',['as' => 'sunflower.emailer.update','uses' => 'emailerController@update']);
				
			}); 

			$this->group(['prefix' => 'bookworm'],function(){
				$this->get('/blockages',['as' => 'sunflower.bookworm.blockages-index','uses' => 'bookwormController@index']);
			}); 

			$this->group(['prefix' => 'tool'],function(){
				$this->get('/',['as' => 'sunflower.tool.changecrc','uses' => 'toolController@changecrc']);
			}); 			

	}); 

});  

$router->group(['namespace' => 'sunflower\frontend'],function(){

	$this->group(['prefix' => 'preview'],function(){
	    $this->get('/{board}/{page}',['as' => "sunflower.preview.board-page", 'uses' => "templatesController@index"]);
	    $this->get('/{board}',['as' => "sunflower.preview.board", 'uses' => "templatesController@board"]);
	});
	$this->group(['prefix' => 'live'],function(){ 
		$this->get('/{board2}/{page2}',['as' => "sunflower.live.page", 'uses' => "templatesController@index"]);
	    $this->get('/{board2}',['as' => "sunflower.live.index", 'uses' => "templatesController@board"]);
	});	
	$this->group(['prefix' => 'train'],function(){ 
		$this->get('/{board2}/{page2}',['as' => "sunflower.train.index", 'uses' => "trainController@index"]);
	    $this->get('/{board2}',['as' => "sunflower.train.board", 'uses' => "trainController@board"]);
	    $this->get('/',['as' => "sunflower.train.train", 'uses' => "trainController@train"]);
	});
	$this->group(['prefix' => 'emailer'],function(){ 
		$this->get('/send/{emailer_id}',['as' => 'sunflower.emailer.send','uses' => 'emailerController@send']);
		$this->post('/send/{emailer_id}',['as' => 'sunflower.emailer.sent','uses' => 'emailerController@sent']);
	});

});   


