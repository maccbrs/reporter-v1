<?php 

$this->get('/',['as' => 'dahlia.index','uses' => 'dahliaController@index']);


$this->group(['prefix' => 'rosebud'],function(){
	$this->get('/',['as' => 'dahlia.rosebud.index','uses' => 'rosebudController@index']);
	$this->get('/viewlogs/{routename}',['as' => 'dahlia.rosebud.viewlogs','uses' => 'rosebudController@viewlogs']);
	$this->get('/show/{id}',['as' => 'dahlia.rosebud.show','uses' => 'rosebudController@show']);
});

$this->group(['prefix' => 'primrose'],function(){
	$this->get('/',['as' => 'dahlia.primrose.index','uses' => 'primroseController@index']);
	$this->get('/viewlogs/{routename}',['as' => 'dahlia.primrose.viewlogs','uses' => 'primroseController@viewlogs']);
	$this->get('/show/{id}',['as' => 'dahlia.primrose.show','uses' => 'primroseController@viewlogs']);
});

$this->group(['prefix' => 'lily'],function(){ 
	$this->get('/',['as' => 'dahlia.lily.index','uses' => 'lilyController@index']);
	$this->get('/viewlogs/{routename}',['as' => 'dahlia.lily.viewlogs','uses' => 'lilyController@viewlogs']);
	$this->get('/show/{id}',['as' => 'dahlia.lily.show','uses' => 'lilyController@show']);
});

$this->group(['prefix' => 'dahlia'],function(){
	$this->get('/',['as' => 'dahlia.dahlia.index','uses' => 'dahliaController@dahlia']);
	$this->get('/viewlogs/{routename}',['as' => 'dahlia.dahlia.viewlogs','uses' => 'dahliaController@viewlogs']);
	$this->get('/show/{id}',['as' => 'dahlia.dahlia.show','uses' => 'dahliaController@show']);
}); 

$this->group(['prefix' => 'prepaid'],function(){
	$this->get('/',['as' => 'dahlia.prepaid.index','uses' => 'prepaidController@index']);
	$this->get('/viewlogs/{routename}',['as' => 'dahlia.prepaid.viewlogs','uses' => 'prepaidController@viewlogs']);
	$this->get('/show/{id}',['as' => 'dahlia.prepaid.show','uses' => 'prepaidController@show']);
}); 