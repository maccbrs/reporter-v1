<?php  

$this->get('/',['as' => 'rosebud.index','uses' => 'rosebudController@index']);
//$this->get('/test',['as' => 'rosebud.index','uses' => 'rosebudController@test']);

//dates available
$this->group(['prefix' => 'datesavailable'],function(){
	$this->get('/generate',['as' => 'rosebud.datesavailable.generate','uses' => 'datesavailableController@generateDates']);
	$this->get('/',['as' => 'rosebud.datesavailable.index','uses' => 'datesavailableController@index']);
	$this->post('/update',['as' => 'rosebud.datesavailable.update','uses' => 'datesavailableController@update']);
});




$this->group(['prefix' => 'checklist'],function(){
	$this->get('/show/{campaign_id}',['as' => 'rosebud.checklist.show','uses' => 'checklistController@show']);
	$this->get('/add',['as' => 'rosebud.checklist.add','uses' => 'checklistController@add']);
	$this->post('/add',['as' => 'rosebud.checklist.create','uses' => 'checklistController@create']);

});

//------------------------------------------------------------------------------------------------------->

$this->group(['prefix' => 'user'],function(){
	$this->get('/',['as' => 'rosebud.user.index','uses' => 'userController@index']);
	$this->get('/add',['as' => 'rosebud.user.add','uses' => 'userController@add']);
	$this->post('/add',['as' => 'rosebud.user.create','uses' => 'userController@create']);
	$this->get('/edit/{user_id}',['as' => 'rosebud.user.edit','uses' => 'userController@edit']);
	$this->post('/edit/{user_id}',['as' => 'rosebud.user.update','uses' => 'userController@update']);
});

$this->group(['prefix' => 'campaign'],function(){
	$this->get('/',['as' => 'rosebud.campaign.index','uses' => 'campaignController@index']);
	$this->get('/add',['as' => 'rosebud.campaign.add','uses' => 'campaignController@add']);
	$this->post('/add',['as' => 'rosebud.campaign.create','uses' => 'campaignController@create']);
	$this->get('/show/{campaign_id}',['as' => 'rosebud.campaign.show','uses' => 'campaignController@show']);
	$this->get('/edit/{campaign_id}',['as' => 'rosebud.campaign.edit','uses' => 'campaignController@edit']);
	$this->post('/update/{campaign_id}',['as' => 'rosebud.campaign.update','uses' => 'campaignController@update']);
	$this->get('/client_status/edit/{campaign_id}',['as' => 'rosebud.campaign.client_status.edit','uses' => 'campaignController@edit_client_status']);
	$this->post('/client_status/update/{client_status_id}',['as' => 'rosebud.campaign.client_status.update','uses' => 'campaignController@update_client_status']);
});

//requirement
$this->group(['prefix' => 'requirement'],function(){
	$this->get('/',['as' => 'rosebud.requirement.index','uses' => 'requirementController@index']);
	$this->get('/add',['as' => 'rosebud.requirement.add','uses' => 'requirementController@add']);
	$this->post('/add',['as' => 'rosebud.requirement.create','uses' => 'requirementController@create']);
	$this->get('/edit/{requirement_id}',['as' => 'rosebud.requirement.edit','uses' => 'requirementController@edit']);
	$this->post('/update/{requirement_id}',['as' => 'rosebud.requirement.update','uses' => 'requirementController@update']);
	$this->post('/delete/{requirement_id}',['as' => 'rosebud.requirement.delete','uses' => 'requirementController@delete']);
});
 
//requirement_to_campaign
$this->group(['prefix' => 'rtc'],function(){
	$this->get('/create/{campaign_id}',['as' => 'rosebud.rtc.create','uses' => 'rtcController@create']);
	$this->post('/store/{campaign_id}',['as' => 'rosebud.rtc.store','uses' => 'rtcController@store']);
	$this->get('/show/{campaign_id}',['as' => 'rosebud.rtc.show','uses' => 'rtcController@show']);
	$this->get('/edit/{rtc_id}',['as' => 'rosebud.rtc.edit','uses' => 'rtcController@edit']);
	$this->post('/update/{rtc_id}',['as' => 'rosebud.rtc.update','uses' => 'rtcController@update']);
});

//request
$this->group(['prefix' => 'request'],function(){
	$this->get('/',['as' => 'rosebud.request.index','uses' => 'requestController@index']);
	$this->get('/calendar',['as' => 'rosebud.request.calendar','uses' => 'requestController@calendar']);
	$this->get('/show/{rtc_id}',['as' => 'rosebud.request.show','uses' => 'requestController@show']);
	$this->get('/status/{rtc_id}',['as' => 'rosebud.request.status','uses' => 'requestController@status']);
	$this->post('/status/update/{rtc_id}',['as' => 'rosebud.request.status.update','uses' => 'requestController@status_update']);
});

//answer
$this->group(['prefix' => 'answer'],function(){
	$this->get('/',['as' => 'rosebud.answer.index','uses' => 'answerController@index']);
	$this->get('/create/{rtc_id}',['as' => 'rosebud.answer.create','uses' => 'answerController@create']);
	$this->post('/store',['as' => 'rosebud.answer.store','uses' => 'answerController@store']);
});

//message
$this->group(['prefix' => 'message'],function(){
	$this->get('/create/{rtc_id}',['as' => 'rosebud.message.create','uses' => 'messageController@create']);
	$this->post('/store/{rtc_id}',['as' => 'rosebud.message.store','uses' => 'messageController@store']);
});

//calendar
$this->group(['prefix' => 'calendar'],function(){
	$this->get('/launch',['as' => 'rosebud.calendar.launch','uses' => 'calendarController@launch']);
	$this->get('/request',['as' => 'rosebud.calendar.request','uses' => 'calendarController@request']);
	$this->get('/my',['as' => 'rosebud.calendar.my','uses' => 'calendarController@my']);
	//$this->post('/store/{rtc_id}',['as' => 'rosebud.calendar.store','uses' => 'calendarController@store']);
});

//schedule
$this->group(['prefix' => 'schedule'],function(){
	$this->get('/add',['as' => 'rosebud.schedule.add','uses' => 'scheduleController@create']);
	$this->post('/add',['as' => 'rosebud.schedule.store','uses' => 'scheduleController@store']);
	$this->get('/show/{schedule_id}',['as' => 'rosebud.schedule.show','uses' => 'scheduleController@show']);
	$this->get('/edit/{schedule_id}',['as' => 'rosebud.schedule.edit','uses' => 'scheduleController@edit']);
	$this->post('/update/{schedule_id}',['as' => 'rosebud.schedule.update','uses' => 'scheduleController@update']);
}); 

//cluster
$this->group(['prefix' => 'cluster'],function(){
	$this->get('/',['as' => 'rosebud.cluster.index','uses' => 'clusterController@index']);
	$this->get('/create',['as' => 'rosebud.cluster.create','uses' => 'clusterController@create']);
	$this->post('/store',['as' => 'rosebud.cluster.store','uses' => 'clusterController@store']);
	$this->get('/show/{cluster_id}',['as' => 'rosebud.cluster.show','uses' => 'clusterController@show']);
	$this->get('/calendar/{cluster_id}',['as' => 'rosebud.cluster.calendar','uses' => 'clusterController@calendar']);
	$this->get('/user/create/{cluster_id}',['as' => 'rosebud.cluster.user.create','uses' => 'clusterController@user_create']);
	$this->post('/user/store/{cluster_id}',['as' => 'rosebud.cluster.user.store','uses' => 'clusterController@user_store']);
}); 

// pdf
$this->group(['prefix' => 'export'],function(){
	$this->get('/pdf/{campaign_id}/{indicator}',['as' => 'export.pdf','uses' => 'campaignController@export']);
}); 