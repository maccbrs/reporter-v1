<?php 

$this->get('/',['as' => 'prepaid.admin.index','uses' => 'adminController@index']);
$this->get('/test',['as' => 'prepaid.test.index','uses' => 'testController@index']);
$this->get('/index',['as' => 'prepaid.index','uses' => 'adminController@index']);

//users
$this->group(['prefix' => 'user'],function(){
	$this->get('/',['as' => 'prepaid.user.index','uses' => 'userController@index']);
	$this->get('/show/{id}',['as' => 'prepaid.user.show','uses' => 'userController@show']);
	$this->get('/create',['as' => 'prepaid.user.create','uses' => 'userController@create']);
	$this->post('/store',['as' => 'prepaid.user.store','uses' => 'userController@store']);
	$this->get('/edit/{id}',['as' => 'prepaid.user.edit','uses' => 'userController@edit']);	
	$this->post('/edit/{id}',['as' => 'prepaid.user.update','uses' => 'userController@update']);
	$this->get('/delete/{id}',['as' => 'prepaid.user.delete','uses' => 'userController@destroy']);	
});

//subscriber
$this->group(['prefix' => 'subscriber'],function(){ 
	
	$this->get('/show/{subscriber_id}',['as' => 'prepaid.subscriber.show','uses' => 'subscriberController@show']);
	$this->get('/create',['as' => 'prepaid.subscriber.create','uses' => 'subscriberController@create']);
	$this->post('/store',['as' => 'prepaid.subscriber.store','uses' => 'subscriberController@store']);
	$this->get('/edit/{subscriber_id}',['as' => 'prepaid.subscriber.edit','uses' => 'subscriberController@edit']);
	$this->get('/send/{subscriber_id}',['as' => 'prepaid.subscriber.send','uses' => 'subscriberController@send']);
	$this->post('/edit/{subscriber_id}',['as' => 'prepaid.subscriber.update','uses' => 'subscriberController@update']);
	$this->get('/delete/{subscriber_id}',['as' => 'prepaid.subscriber.delete','uses' => 'subscriberController@delete']);
	$this->get('/',['as' => 'prepaid.subscriber.index','uses' => 'subscriberController@index']);
	$this->get('/index_all',['as' => 'prepaid.subscriber.index_all','uses' => 'subscriberController@index_all']);
});
//connector  
$this->group(['prefix' => 'connector'],function(){
	$this->get('/',['as' => 'prepaid.connector.index','uses' => 'connectorController@index']);
	$this->get('/show/{connector_id}',['as' => 'prepaid.connector.show','uses' => 'connectorController@show']);
	$this->get('/create',['as' => 'prepaid.connector.create','uses' => 'connectorController@create']);
	$this->post('/store',['as' => 'prepaid.connector.store','uses' => 'connectorController@store']);
	$this->get('/edit/{connector_id}',['as' => 'prepaid.connector.edit','uses' => 'connectorController@edit']);
	$this->get('/delete/{connector_id}',['as' => 'prepaid.connector.delete','uses' => 'connectorController@delete']);
	$this->post('/edit/{connector_id}',['as' => 'prepaid.connector.update','uses' => 'connectorController@update']);
	
}); 

//load
$this->group(['prefix' => 'load'],function(){
	$this->get('/',['as' => 'prepaid.load.index','uses' => 'loadController@index']);
	$this->get('/{subscriber_id}/create',['as' => 'prepaid.load.create','uses' => 'loadController@create']);
	$this->post('/store',['as' => 'prepaid.load.store','uses' => 'loadController@store']);
	$this->get('/edit/{load_id}',['as' => 'prepaid.load.edit','uses' => 'loadController@edit']);
	$this->post('/edit/{load_id}',['as' => 'prepaid.load.update','uses' => 'loadController@update']);
	$this->get('/generate','loadController@generate');

});

//reserved
$this->group(['prefix' => 'reserved'],function(){
	$this->get('/{subscriber_id}/create',['as' => 'prepaid.reserved.create','uses' => 'reservedController@create']);
	$this->post('/store',['as' => 'prepaid.reserved.store','uses' => 'reservedController@store']);
});

//calls
$this->group(['prefix' => 'calls'],function(){
	$this->get('/load/{load_id}',['as' => 'prepaid.calls.load','uses' => 'callsController@load']);
	$this->get('/reserved/{subscriber_id}',['as' => 'prepaid.calls.reserved','uses' => 'callsController@reserved']);
	$this->get('/pdf/{load_id}/{subscriber_id}/{startdate}/{enddate}',['as' => 'prepaid.calls.pdf','uses' => 'callsController@pdf']);
	$this->get('/excel/{load_id}/{subscriber_id}/{startdate}/{enddate}',['as' => 'prepaid.calls.excel','uses' => 'callsController@excel']);
	$this->get('/pdfptool/{load_id}/{subscriber_id}/{start_date}/{end_date}',['as' => 'prepaid.calls.pdfptool','uses' => 'callsController@pdfptool']);
	$this->get('/excelptool/{load_id}/{subscriber_id}/{start_date}/{end_date}',['as' => 'prepaid.calls.excelptool','uses' => 'callsController@excelptool']);
	$this->post('/generate',['as' => 'prepaid.calls.generate','uses' => 'callsController@generate']);
});

//notes
$this->group(['prefix' => 'notes'],function(){
	$this->get('/',['as' => 'prepaid.notes.index','uses' => 'notesController@index']);
	$this->get('/create/{subscriber_id}',['as' => 'prepaid.notes.create','uses' => 'notesController@create']);
	$this->post('/store',['as' => 'prepaid.notes.store','uses' => 'notesController@store']);
	$this->get('/edit',['as' => 'prepaid.notes.edit','uses' => 'notesController@edit']);
	$this->post('/edit',['as' => 'prepaid.notes.update','uses' => 'notesController@update']);
	$this->get('/pdf/{load_id}/{subscriber_id}/{startdate}/{enddate}',['as' => 'prepaid.calls.pdf','uses' => 'callsController@pdf']);
	$this->get('/excel/{load_id}/{subscriber_id}/{startdate}/{enddate}',['as' => 'prepaid.calls.excel','uses' => 'callsController@excel']);
	$this->get('/pdfptool/{load_id}/{subscriber_id}/{start_date}/{end_date}',['as' => 'prepaid.calls.pdfptool','uses' => 'callsController@pdfptool']);
	$this->get('/excelptool/{load_id}/{subscriber_id}/{start_date}/{end_date}',['as' => 'prepaid.calls.excelptool','uses' => 'callsController@excelptool']);
	//$this->post('/generate',['as' => 'prepaid.calls.generate','uses' => 'callsController@generate']);
});

$this->group(['prefix' => 'emailer'],function(){
	$this->post('/{emailerId}',['as' => 'prepaid.emailer.update','uses' => 'emailerController@update']);
});

$this->group(['prefix' => 'outboundload'],function(){
	$this->post('/{subscriberId}',['as' => 'prepaid.outboundload.store','uses' => 'outboundloadController@store']);
	$this->get('/records/{loadId}',['as' => 'prepaid.outboundload.records','uses' => 'outboundloadController@records']);
});

$this->group(['prefix' => 'generate'],function(){
	$this->post('/calltransfer',['as' => 'prepaid.generate.calltransfer','uses' => 'generateController@calltransfer']);
	$this->get('/ctget/{ctid}',['as' => 'prepaid.generate.ctget','uses' => 'generateController@ctget']);
}); 