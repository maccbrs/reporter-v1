<?php 
$this->get('/',['as' => 'lily.index','uses' => 'lilyController@index']);


$this->group(['prefix' => 'access'],function(){
	$this->get('/show/{user_id}',['as' => 'lily.access.show','uses' => 'accessController@show']);
	$this->get('/edit/{user_id}',['as' => 'lily.access.edit','uses' => 'accessController@edit']);
});

$this->group(['prefix' => 'desr'],function(){

	$this->get('/',['as' => 'lily.desr.index','uses' => 'desrController@index']);
	$this->get('/add',['as' => 'lily.desr.add','uses' => 'desrController@add']);
	$this->post('/create',['as' => 'lily.desr.create','uses' => 'desrController@create']);

	$this->group(['prefix' => 'task'],function(){
		$this->get('/index/{lilyUserId}',['as' => 'lily.desr.task.index','uses' => 'desrTaskController@index']);

	});

});

$this->group(['prefix' => 'user'],function(){
	$this->get('/',['as' => 'lily.user.index','uses' => 'userController@index']);
	$this->get('/add',['as' => 'lily.user.add','uses' => 'userController@add']);
	$this->post('/add',['as' => 'lily.user.create','uses' => 'userController@create']);
	$this->get('/edit/{user_id}',['as' => 'lily.user.edit','uses' => 'userController@edit']);
	$this->post('/edit/{user_id}',['as' => 'lily.user.update','uses' => 'userController@update']);
	$this->group(['prefix' => 'access'],function(){
		$this->get('/index/{lilyUserId}',['as' => 'lily.user.access.index','uses' => 'userAccessController@index']);
		$this->get('/add/{lilyUserId}',['as' => 'lily.user.access.add','uses' => 'userAccessController@add']);
		$this->post('/store/{lilyUserId}',['as' => 'lily.user.access.store','uses' => 'userAccessController@store']);
		$this->group(['prefix' => 'app'],function(){
			$this->get('/index/{lilyUserId}/{app_name}',['as' => 'lily.user.access.app.index','uses' => 'userAccessAppController@index']);
			$this->post('/update/{lilyUserId}',['as' => 'lily.user.access.app.update','uses' => 'userAccessAppController@update']);
		});
	});
});


$this->group(['prefix' => 'inventory'],function(){

	$this->get('/all',['as' => 'lily.inventory.all','uses' => 'inventoryController@all']);

	$this->group(['prefix' => 'did'],function(){
		$this->get('/',['as' => 'lily.inventory.did','uses' => 'inventoryController@did']);
		$this->get('/edit/{did_id}',['as' => 'lily.inventory.did.edit','uses' => 'inventoryController@did_edit']);
		$this->post('/update/{did_id}',['as' => 'lily.inventory.did.update','uses' => 'inventoryController@did_update']);
	});

	$this->group(['prefix' => 'headset'],function(){
		$this->get('/',['as' => 'lily.inventory.headset.index','uses' => 'inventoryController@headset']);
		$this->get('/create',['as' => 'lily.inventory.headset.create','uses' => 'inventoryController@headset_create']);
		$this->post('/store',['as' => 'lily.inventory.headset.store','uses' => 'inventoryController@headset_store']);
		$this->get('/show/{headset_id}',['as' => 'lily.inventory.headset.show','uses' => 'inventoryController@headset_show']);
	});
}); 

$this->group(['prefix' => 'tasklist'],function(){
	$this->get('/',['as' => 'lily.tasklist.index','uses' => 'tasklistController@index']);
	$this->get('/create',['as' => 'lily.tasklist.create','uses' => 'tasklistController@create']);
	$this->post('/store',['as' => 'lily.tasklist.store','uses' => 'tasklistController@store']);
	$this->get('/show/{tasklist_id}',['as' => 'lily.tasklist.show','uses' => 'tasklistController@show']);
	$this->get('/edit/{tasklist_id}',['as' => 'lily.tasklist.edit','uses' => 'tasklistController@edit']);
	$this->post('/update/{tasklist_id}',['as' => 'lily.tasklist.update','uses' => 'tasklistController@update']);
});

//scheduler
$this->group(['prefix' => 'scheduler'],function(){
	$this->get('/daily',['as' => 'lily.scheduler.daily','uses' => 'schedulerController@daily']);
});


$this->group(['prefix' => 'task'],function(){
	$this->get('/',['as' => 'lily.task.index','uses' => 'taskController@index']); 
	$this->get('/create',['as' => 'lily.task.create','uses' => 'taskController@create']); 
	$this->post('/store',['as' => 'lily.task.store','uses' => 'taskController@store']);
	$this->group(['prefix' => 'doit'],function(){
		$this->get('/{task_id}',['as' => 'lily.task.doit.show','uses' => 'taskController@doitShow']);
		$this->post('/update/{task_id}',['as' => 'lily.task.doit.update','uses' => 'taskController@doitUpdate']);	
	});	
});

$this->group(['prefix' => 'logs'],function(){
	$this->get('/',['as' => 'lily.logs.index','uses' => 'logsController@index']); 
	$this->get('/viewlogs/{logs_routename}',['as' => 'lily.logs.viewlogs','uses' => 'logsController@viewlogs']);
	$this->get('/show/{logs_id}',['as' => 'lily.logs.show','uses' => 'logsController@show']); 
});
 

// $this->group(['prefix' => 'inventory'],function(){
	
// });
