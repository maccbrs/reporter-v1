<?php 

$this->get('/',['as' => 'primrose.index','uses' => 'primroseController@index']); 
$this->get('/test',['as' => 'primrose.test.index','uses' => 'testController@index']);


$this->group(['prefix' => 'block'],function(){
	$this->get('/',['as' => 'primrose.block','uses' => 'blockController@index']);
	$this->post('/post',['as' => 'primrose.block.post','uses' => 'blockController@post']);
	$this->get('/blockage',['as' => 'primrose.test.blockage','uses' => 'blockController@blockage']);
});

$this->group(['prefix' => 'listreporter'],function(){
	$this->get('/',['as' => 'primrose.listreporter','uses' => 'listReporterController@index']);
	$this->post('/post',['as' => 'primrose.listreporter.post','uses' => 'listReporterController@post']);
});

$this->group(['prefix' => 'agent'],function(){
	$this->get('/',['as' => 'primrose.agent','uses' => 'agentController@index']);
	$this->post('/post',['as' => 'primrose.agent.post','uses' => 'agentController@post']);
	$this->post('/post2',['as' => 'primrose.agent.post2','uses' => 'agentController@post2']);
	$this->get('/agentbreak',['as' => 'primrose.agentbreak','uses' => 'agentController@agent_break']);
	$this->get('/agentlog',['as' => 'primrose.agentlog','uses' => 'agentController@agent_log']);
	$this->post('/agent_break_post',['as' => 'primrose.agent.agent_break_post','uses' => 'agentController@agent_break_post']);
	$this->post('/agent_log_post',['as' => 'primrose.agent.agent_log_post','uses' => 'agentController@agent_log_post']); 
});

$this->group(['prefix' => 'custom_report'],function(){
	$this->get('/', ['as' => 'primrose.custom.report.index', 'uses' => 'customReportController@index']);
	$this->post('/generate', ['as' => 'primrose.custom.report.generate', 'uses' => 'customReportController@generate']);
});	

$this->group(['prefix' => 'settings'],function(){
	$this->get('/',['as' => 'primrose.settings','uses' => 'settingsController@index']);
	$this->group(['prefix' => 'campaign'],function(){

		$this->get('/add',['as' => 'primrose.settings.campaign.add','uses' => 'settingsController@campaign_add']);
		$this->post('/store',['as' => 'primrose.settings.campaign.store','uses' => 'settingsController@campaign_store']);
		$this->get('/select-server',['as' => 'primrose.settings.campaign.select-server','uses' => 'settingsController@campaign_add']);
		$this->get('/campaign_index',['as' => 'primrose.settings.campaign_index','uses' => 'settingsController@campaign_index']);
	});

	$this->group(['prefix' => 'productivity'],function(){
		$this->get('/add',['as' => 'primrose.settings.productivity.add','uses' => 'settingsController@productivity_add']);
		$this->get('/select-server',['as' => 'primrose.settings.productivity.select-server','uses' => 'settingsController@productivity_add']);
	});

	$this->get('/campaign_edit/{id}',['as' => 'primrose.settings.campaign_edit','uses' => 'settingsController@campaign_edit']);
	$this->post('/campaign_update/{id}',['as' => 'primrose.settings.campaign_update','uses' => 'settingsController@campaign_update']);
	$this->post('/campaign_variable',['as' => 'primrose.settings.campaign_variable','uses' => 'settingsController@campaign_variable']);
	$this->get('/campaign_delete/{id}',['as' => 'primrose.settings.campaign_delete','uses' => 'settingsController@campaign_delete']);
});

$this->group(['prefix' => 'accntstat'],function(){
	$this->get('/occl/{type}',['as' => 'primrose.accntstat.occl','uses' => 'accntstatController@occl']);
	$this->post('/occl/{type}',['as' => 'primrose.accntstat.occl.post','uses' => 'accntstatController@occl_post']);
	$this->post('/occl',['as' => 'primrose.accntstat.occl.postajax','uses' => 'accntstatController@occl_postajax']);
});   

$this->group(['prefix' => 'productivity'],function(){
	$this->get('/',['as' => 'primrose.productivity.index','uses' => 'productivityController@index']);
	$this->post('/',['as' => 'primrose.productivity.show','uses' => 'productivityController@show']);
	$this->post('/export',['as'=>'primrose.productivity.export','uses'=>'productivityController@downloadExcel']);
});  

$this->group(['prefix' => 'campaigns','namespace' => 'campaigns'],function(){
	$this->get('/',['as' => 'primrose.campaigns.index','uses' => 'campaignsController@index']);
	$this->group(['prefix' => 'eod'],function(){
		$this->get('/{cid}',['as' => 'primrose.campaigns-eod.index','uses' => 'campaignsEodController@index']);
	});

	$this->group(['prefix' => 'settings'],function(){
		$this->get('/{pr_campaign_id}',['as' => 'primrose.campaigns-settings.timezone','uses' => 'campaignSettingsController@timezone']);
		$this->post('/{pr_campaign_id}',['as' => 'primrose.campaigns-settings.timezone-post','uses' => 'campaignSettingsController@timezone_post']);
	});

	$this->group(['prefix' => 'test'],function(){
		$this->get('eod',['as' => 'primrose.campaigns-test.eod','uses' => 'campaignTestController@eod']);
		//$this->get('/',['as' => 'primrose.test.index','uses' => 'testController@index']);
	});
}); 

$this->group(['prefix' => 'connectors','namespace' => 'connectors'],function(){ 
	$this->get('/',['as' => 'primrose.connectors.index','uses' => 'connectorsController@index']);
	$this->post('/destroy/{id}',['as' => 'primrose.connectors.destroy','uses' => 'connectorsController@destroy']);

	$this->group(['prefix' => 'board'],function(){
		$this->get('/',['as' => 'primrose.connectors.board-index','uses' => 'connectorsController@board_index']);
		$this->post('/update/{conId}',['as' => 'primrose.connectors.board-update','uses' => 'boardConnectorController@update_timezone']);
	});
	
	$this->group(['prefix' => 'timerange'],function(){
		$this->post('/update/{conId}',['as' => 'primrose.connectors.timerange-update','uses' => 'boardConnectorController@update_timerange']);		
	});

	$this->group(['prefix' => 'sendtime'],function(){
		$this->post('/update/{conId}',['as' => 'primrose.connectors.sendtime-update','uses' => 'boardConnectorController@update_sendtime']);		
	});	

	$this->group(['prefix' => 'account-statistics'],function(){
		$this->get('/show/{type}',['as' => 'primrose.connectors.acc-stats-index','uses' => 'accountStatsController@account_index']);
	});

});

$this->group(['prefix' => 'reporter','namespace' => 'reporter'],function(){ 
	$this->group(['prefix' => 'eod','namespace' => 'eod'],function(){
		$this->get('/',['as' => 'primrose.reporter.reporter-eod-index','uses' => 'reporterEodController@index']);
		$this->get('/eodGenerator',['as' => 'primrose.reporter.reporter-eod-generator','uses' => 'reporterEodController@eodGenerator']);
		$this->get('/eod-generator-page',['as' => 'primrose.reporter.reporter-eod-generator-page','uses' => 'reporterEodController@eodGeneratorPage']);
		$this->post('/eod-generator-page',['as' => 'primrose.reporter.reporter-eod-generator-page','uses' => 'reporterEodController@eodGeneratorPage']);
		$this->post('/eodGenerator',['as' => 'primrose.reporter.eod-generator','uses' => 'reporterEodController@eodGenerator']);
		$this->get('/date/{date}',['as' => 'primrose.reporter.reporter-eod-date','uses' => 'reporterEodController@date']);
		$this->get('/show/{eodId}',['as' => 'primrose.reporter.reporter-eod-show','uses' => 'reporterEodController@show']);
		$this->get('/detail/{eodId}',['as' => 'primrose.reporter.reporter-eod-detail','uses' => 'reporterEodController@detail']);
		$this->get('/submitted-data',['as' => 'primrose.reporter.submitted-data','uses' => 'reporterEodController@submitted_data']);
		$this->post('/submitted-data-post',['as' => 'primrose.reporter.submitted-data-post','uses' => 'reporterEodController@submitted_data_post']);
		$this->get('/emailer',['as' => 'primrose.reporter.emailer','uses' => 'reporterEodController@emailer']);
		$this->post('/emailer-post',['as' => 'primrose.reporter.emailer-post','uses' => 'reporterEodController@emailer_post']);
		
	}); 
});?>