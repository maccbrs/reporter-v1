<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Carbon\Carbon;
use DB;
use Mail;
use Config;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {


        $schedule->call(function () {
            
            $eod = new \App\Http\Controllers\eod\eodController;
            $eod->eodStep4();

        })->cron('*/1 * * * *');

        // $schedule->call(function () {
            
        //     $eod = new \App\Http\Controllers\eod\eodController;
        //     $eod->backup_email();

        // })->cron('*/1 * * * *');

        //disabled 7/19/2017 if more than 3 months, please delete
        // $schedule->call(function () {

        //     $olObj =  new \App\Http\Models\prepaid\OutboundLoad;
        //     $loads = $olObj->active()->with(['details'])->get();
        //     $changed = 0;
        //     foreach ($loads as $load):
        //         $total_sec = 0;

        //         if($load->details):
        //             foreach ($load->details as $datail):
        //                 if($datail['call_details']):
        //                     $total_sec += $datail['call_details']['length_in_sec'];
        //                 endif;
        //             endforeach;
        //         endif;

        //         if($load->used != ($total_sec/60)): $changed++; endif;

        //         if(($load->minutes*60) <= $total_sec): $load->status = 0; endif;
        //         $load->used = ($total_sec/60);
        //         $load->remaining = (($load->minutes*60) - $total_sec)/60;
        //         $load->save();
        //     endforeach;  
        //     DB::connection('mytestdb')->table('outboundloadupdater')->insert([
        //         'content' => 'run outbound load updater with '.$changed.' change/changes.',
        //         'date' => date('Y-m-d H:i:s')
        //     ]);

        // })->cron('*/2 * * * *');

        $schedule->call(function () {
            $gn = new \App\Http\Controllers\generalController;

            $x = $gn->quotes();

            DB::connection('mytestdb')->table('quotes')->insert([
                'content' => json_encode($x->contents),
                'date' => date('Y-m-d H:i:s')
            ]);

        })->daily();


        //disabled 7/19/2017 if more than 3 months, please delete
        // $schedule->call(function () {

        //     $count = 0;
        //     $oldObj =  new \App\Http\Models\prepaid\OutboundLoadDetails; 
        //     $vclObj =  new \App\Http\Models\prepaid\ViciCallLog;
        //     $cn = new Carbon;
        //     $obloads = $oldObj->where('uniqueid',null)->get(); 

        //     if($obloads):
        //         foreach ($obloads as $k => $v):
        //           $v->uniqueid = $vclObj->select('uniqueid')->where('number_dialed',$v->extension)->where('start_epoch',$v->startepoch)->first()->uniqueid; //echo '<br>';
        //           if($v->uniqueid): $count++; $v->save(); endif;
        //         endforeach;
        //     endif;

        //     if($count):
        //         DB::connection('mytestdb')->table('outboundloadupdater')->insert([
        //             'content' => 'run every  2 secs with '.$count.' change/changes',
        //             'date' => date('Y-m-d H:i:s')
        //         ]);
        //     else:
        //         DB::connection('mytestdb')->table('outboundloadupdater')->insert([
        //             'content' => 'run every 2 secs with no changes',
        //             'date' => date('Y-m-d H:i:s')
        //         ]);
        //     endif;

        // })->cron('*/1 * * * *');

        //disabled 7/19/2017 if more than 3 months, please delete
        // $schedule->call(function () {

        //     $count = 0;
        //     $oldObj =  new \App\Http\Models\prepaid\OutboundLoadDetails;
        //     $vclObj =  new \App\Http\Models\prepaid\ViciCallLog;
        //     $cn = new Carbon;
        //     $obloads = $oldObj->where('created_at','>=',$cn->now()->subMinutes(5))->get();

        //     if($obloads):
        //         foreach ($obloads as $k => $v):
        //           $v->uniqueid = $vclObj->select('uniqueid')->where('number_dialed',$v->extension)->where('start_epoch',$v->startepoch)->first()->uniqueid; //echo '<br>';
        //           if($v->uniqueid): $count++; $v->save(); endif;
        //         endforeach;
        //     endif;

        //     if($count):
        //         DB::connection('mytestdb')->table('outboundloadupdater')->insert([
        //             'content' => 'run every 5 mins with '.$count.' change/changes',
        //             'date' => date('Y-m-d H:i:s')
        //         ]);
        //     else:
        //         DB::connection('mytestdb')->table('outboundloadupdater')->insert([
        //             'content' => 'run every 5 mins with no changes',
        //             'date' => date('Y-m-d H:i:s')
        //         ]);
        //     endif;

        // })->cron('*/15 * * * *');



        //disabled 7/19/2017 if more than 3 months, please delete
        // $schedule->call(function () {

        //     ini_set('max_execution_time', 180);

        //     $test = false;
        //     $done = [];

        //     $dmboard = new \App\Http\Models\primrose\dummyboard\Board;
        //     $boardsObj = new \App\Http\Models\primrose\connectors\BoardConnectors;
        //     $boards = $boardsObj->orderBy('created_at', 'desc')->get();
        //     $gc = new \App\Http\Controllers\primrose\generalController;
        //     $tbl = new \App\Http\Helpers\primrose\Tables;
        //     $sendtimes = [];
        //     $current_date = date('Y-m-d');
        //     $tym = date('H:i');
        //     $current_time = date('H:i:s'); 
        //     $current_hour = date('H:i');

        //     $precision = 30;
        //     $timestamp = strtotime($current_hour);
        //     $precision = 60 * $precision;
        //     $current_time_hour = date('H:i', round($timestamp / $precision) * $precision);

        //     $text = '';
        //     $container = [];

        //     foreach ($boards as $v): //$connector   

        //         $hourset = date("H:i", strtotime($gc->_options($v->options,'sendtime')));

        //         $precision2 = 30;
        //         $timestamp2 = strtotime($hourset);
        //         $precision2 = 60 * $precision2;
        //         $current_settime = date('H:i', round($timestamp2 / $precision2) * $precision2);
                
        //         if($current_time_hour == $current_settime):

        //                 $container[] = $gc->_options($v->options,'sendtime') .' - '. $tym;

        //                 $html_data = $html_data = '';
        //                 $logs = $data = $in = $templar_socket = [];

        //                 if(!in_array($v->templar_socket, $done)):
        //                     $dates = $tbl->timerange($v->options); 
        //                     $dates['time'] = $current_time;
        //                     $board = $dmboard->whereIn('campaign_id',json_decode($v->templar_socket,true))->where('status',1)->first()->toArray();
        //                     $vici = DB::connection('192.168.200.132')->table(($v->bound == 'out'?'vicidial_log':'vicidial_closer_log'));                                
        //                     $done[] = $v->templar_socket;
        //                     $bound = $v->bound == 'out'?'vicidial_log':'vicidial_closer_log';

        //                     if($board):

        //                         $opt = json_decode($board['options']);
        //                         $in = json_decode($v->vici_plug,true);
        //                         $dates['campaigns'] = $in;

        //                         $count = $vici
        //                             ->select(DB::raw('count(*) as count'))
        //                             ->whereBetween($bound.'.call_date', [$dates['fr'],$dates['to']])
        //                             ->whereIn($bound.'.campaign_id',$in)
        //                             ->whereNotIn($bound.'.status',['DROP','INCALL','QUEUE','TEST','test'])
        //                             ->groupBy($bound.'.status')
        //                             ->orderBy($bound.'.status', 'asc')
        //                             ->get();

        //                         $logs = $vici->leftJoin('vicidial_campaign_statuses', 'vicidial_campaign_statuses.status', '=', $bound.'.status')
        //                             ->select($bound.'.status','vicidial_campaign_statuses.status_name',DB::raw('count(*) as count'))
        //                             ->whereBetween($bound.'.call_date', [$dates['fr'],$dates['to']])
        //                             ->whereIn($bound.'.campaign_id',$in)
        //                             ->whereNotIn($bound.'.status',['DROP','INCALL','QUEUE','TEST','test'])
        //                             ->groupBy($bound.'.status')
        //                             ->orderBy($bound.'.status', 'asc')
        //                             ->get(); 

        //                         foreach ($logs as $key => $value) {
                                        
        //                             $logs[$key]->count = $count[$key]->count;
                                       
        //                         }

                                  
        //                         if(!empty($logs)):

        //                             $html_logs = $tbl->html_logs($logs);  
        //                             $data = DB::connection('dummyboard')->table('campaign_data')
        //                                     ->whereIn('campaign_id',json_decode($v->templar_socket,true))
        //                                     ->where('contents', 'like', '%"request_status":"live"%')
        //                                     ->whereBetween('created_at',[$dates['fr'],$dates['to']])
        //                                     ->get();

        //                             if(!empty($data)):
        //                                 $html_data = $tbl->convert_time_drc($data, $dates['to_tz'] );
        //                                 $html_data = $tbl->html_data($data);
        //                             endif;


        //                             if($test):
        //                                 $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
        //                                 $board['to'] = ['marlonbbernal@mailinator.com', 'howell.calabia@magellan-solutions.com'];
        //                                 $board['from'] = 'reporter@magellan-solutions.com';
        //                                 $board['cc'] = false;
        //                                 $board['bcc'] = false;
        //                             else:
        //                                 $board['subject'] = (isset($board['lob'])?$board['lob'].' EOD report':'Magellan Reporting Service');
        //                                 $board['to'] = (!empty($board['to'])?$tbl->emailer_parser2($board['to']):false);
        //                                 $board['from'] = ($board['from'] != ''?$board['from']:'reporter@magellan-solutions.com');
        //                                 $board['cc'] = (!empty($board['cc'])?$tbl->emailer_parser2($board['cc']):false);
        //                                 $board['bcc'] = (!empty($board['bcc'])?$tbl->emailer_parser2($board['bcc']):false);
        //                             endif;

        //                             if(!$board['to']):
        //                                     $board['to'] = 'NOC@magellan-solutions.com';
        //                                     Mail::send('primrose.email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
        //                                        $m->from($board['from'], 'Magellan Reporting Service');
        //                                        $m->to($board['to'], $board['lob'])->subject('No Recipient eod!'); 
        //                                     }); 
        //                             else:

        //                                 if($v['id'] == 55):

        //                                     $board['to'] = array("operations@magellan-solutions.com",   "maryan.anora@magellan-solutions.com", "julie.galinato@magellan-solutions.com");

        //                                 endif;

        //                                 // $board['to'] = array("howell.calabia@magellan-solutions.com");
        //                                 // $board['cc'] = array("webdev@magellan-solutions.com");

        //                                 Mail::send('primrose.email.report', ['content' => $html_data,'dispo' => $html_logs, 'board' => $board], function ($m) use ($board) { 
        //                                    $m->from($board['from'], 'Magellan Reporting Service');
        //                                    $m->to($board['to'], $board['lob'])->subject($board['subject']); 
        //                                    if($board['cc'] && !$board['bcc']){
        //                                      $m->to($board['to'], $board['lob'])->cc($board['cc'])->subject($board['subject']); 
        //                                    }else if(!$board['cc'] && $board['bcc']){
        //                                      $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->subject($board['subject']); 
        //                                    }else if($board['cc'] && $board['bcc']){
        //                                      $m->to($board['to'], $board['lob'])->bcc($board['bcc'])->cc($board['cc'])->subject($board['subject']); 
        //                                    }
                                           
        //                                 }); 

        //                                 $status = 1;                       
        //                             endif;
                                    
        //                             if(!$test): 
        //                                 $cc = ($board['cc']?json_encode($board['cc']):'');
        //                                 $to = ($board['to']?json_encode($board['to']):'');
        //                                 $bcc = ($board['bcc']?json_encode($board['bcc']):'');
        //                                 DB::connection('dummyboard')->table('eod_reports')->insert([
        //                                     'to' => $to,
        //                                     'from' => $board['from'],
        //                                     'cc' => $cc,
        //                                     'bcc' => $bcc,
        //                                     'subject' => $board['subject'],
        //                                     'date' => date('Y-m-d '),
        //                                     'status' => $status,
        //                                     'campaign' => $board['campaign'],
        //                                     'options' => json_encode($dates),
        //                                     'content' => $html_logs.'<br>'.$html_data
        //                                 ]);
        //                             endif;

        //                         else:
        //                             if(!$test):
        //                                 DB::connection('dummyboard')->table('eod_reports')->insert([
        //                                     'date' => date('Y-m-d '),
        //                                     'status' => 0,
        //                                     'campaign' => $board['campaign']
        //                                 ]);
        //                             endif;  
        //                         endif;  

        //                 endif; 

        //             endif;                                

        //         endif;
        //     endforeach;

        //     DB::connection('mytestdb')->table('eodcron')->insert([
        //         'title' => 'test cron every 30 minute v4 '.$tym,
        //         'content' => json_encode($container),
        //         'date' => date('Y-m-d H:i:s')
        //     ]);

        // })->everyThirtyMinutes();
        


        // endofeodproper

        //disabled 7/19/2017 if more than 3 months, please delete

        // $schedule->call(function () {

        //     $vcon = new \App\Http\Models\primrose\Viciconn;
        //     $genCtlr = new \App\Http\Controllers\primrose\generalController;

        //     $params = [];

        //     $to2 = date("Y-m-d H:i:s"); 
        //     $from2 =  date('Y-m-d H:i:s',date(strtotime("-1 day", strtotime($to2 ))));
        //     $camp = $vcon->where('id','=',52)->first();
            
        //     if($camp->toArray()):

        //         $from2 = date("Y-m-d H:i:s",strtotime($from2));
        //         $to2 = date("Y-m-d H:i:s",strtotime($to2));
        //         $data = $genCtlr->_blockquery($camp,$from2,$to2); 
        //         $blocks = $genCtlr->block($data);

        //     endif;  

        //     $user = 'Kit';
        //     $param['title'] = 'Blockage';
        //     $emails = ['gerardo.resano@magellan-solutions.com','noc@magellan-solutions.com','glenn.casaljay@magellan-solutions.com','jovielyn.bauyon@magellan-solutions.com','hazel.osias@magellan-solutions.com'];

        //     //$emails = ['webdev_report@magellan-solutions.com'];

        //     Mail::send('emails.blockage', ['msg' => $blocks,'user' => $user], function ($m) use ($blocks,$emails)  {
        //         $m->from('webdev_report@magellan-solutions.com', 'Magellan Report');
        //         $m->to($emails);
        //         $m->subject('Bookworm Blockage Report');
        //     });
            
        // })->hourly();

        //this should only be available on .28
        // $schedule->call(function () {
        //     $Blockage = new \App\Http\Controllers\blockage\blockageController;
        //     $Blockage->send();
        // })->cron('*/1 * * * *');  

        //should only be available at 171
        $schedule->call(function () {
            $Blockage = new \App\Http\Controllers\blockage\blockageController;
            $Blockage->index();
        //})->cron('*/1 * * * *'); 
        })->hourly();

        $schedule->call(function () {

            $Blockage = new \App\Http\Controllers\blockage\blockageController;
            $Blockage->send();
        //})->cron('*/1 * * * *'); 
        })->cron('* * * * *');

        $schedule->call(function () {
            
            $gn = new \App\Http\Controllers\generalController;
            $x = $gn->quotes();
            DB::connection('mytestdb')->table('quotes')->insert([
                'content' => json_encode($x->contents),
                'date' => date('Y-m-d H:i:s')
            ]);

        })->daily();

        // reserve update

        // $schedule->call(function () {
            
        //     $reserved = new \App\Http\Models\prepaid\Reserved;
        //     $reservelist = $reserved->wherein('status',[1])->get();

        //     foreach ($reservelist as $key => $value) {
               
        //         $reserve = $reserved::find($value['id']);
        //         $reserve->remaining = $value['minutes'] - $value['used'];
        //         $reserve->update();
            
        //     }

        // })->everyThirtyMinutes();

    }
}
