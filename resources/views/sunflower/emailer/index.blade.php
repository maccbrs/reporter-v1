<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12"> 
                <div class="x_content">
                    <div class="row tile_count">                     
                        
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>from</th>
                                <th>to</th>
                                <th>cc</th>
                                <th>bcc</th>
                                <th>shortcode</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($emails as $e)
                             <tr>
                                <td>{{$e->name}}</td> 
                                <td>{!!$gn->formatter($e->from)!!}</td>
                                <td>{!!$gn->formatter($e->to)!!}</td>
                                <td>{!!$gn->formatter($e->cc)!!}</td>
                                <td>{!!$gn->formatter($e->bcc)!!}</td>
                                <td></td>
                                <td>
                                    <a href="{{route('sunflower.emailer.edit',$e->id)}}" class="btn btn-primary fa fa-edit"></a>
                                    {!!view('layouts.delete',['name' => $e->name,'route' => 'sunflower.emailer.destroy','id' => $e->id,'param' => $e->id])!!}
                                </td>
                             </tr>                               
                            @endforeach
                        </tbody>  
                    </table>
                        {!!$emails->links()!!}
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection