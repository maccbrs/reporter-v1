<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">

            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">
                  <form id="form-emailer" method="post" action="{{route('sunflower.emailer.update',$emails['id'])}}"> 
                  <input type="hidden" name="_method" value="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                      <div class="x_panel">
                        <div class="x_title">
                          <h4>Emailer <small>edit</small></h4>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <label for="fullname">Name * :</label>
                            <input type="text" class="form-control" id="name" value="{{old('name',$emails['name'])}}" name="name" required />

                            <label >From * :</label>
                            <input class="form-control" id="from" name="from" value="{{old('from',emailer_parser($emails['from']))}}" required />

                            <label >To * :</label>
                            <input class="form-control" id="to" name="to" value="{{old('to',emailer_parser($emails['to']))}}" required />

                            <label >Cc:</label>
                            <textarea class="form-control" id="cc" name="cc" >{{old('cc',emailer_parser($emails['cc']))}}</textarea>

                            <label >Bcc * :</label>
                            <textarea class="form-control" id="bcc" name="bcc" >{{old('bcc',emailer_parser($emails['bcc']))}}</textarea>

                            <label >Subject * :</label>
                            <input class="form-control" type="text" value="{{old('subject',$emails['subject'])}}" name="subject">
                            <label >Content * :</label>
                            <textarea rows="5" class="form-control" id="summernote" name="content" >{{old('content',$emails['content'])}}</textarea>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                      </div>
                  </form>
              </div> 
            </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
     <link href="{{$asset}}tokenfield/dist/css/tokenfield-typeahead.css" type="text/css" rel="stylesheet">
    <link href="{{$asset}}tokenfield/dist/css/bootstrap-tokenfield.css" type="text/css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.css" rel="stylesheet">
   
    <style type="text/css">
    .tokenfield .token{
      height: 31px;
    }
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')

    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.3/summernote.js"></script>
    <script type="text/javascript" src="{{$asset}}tokenfield/dist/bootstrap-tokenfield.js" charset="UTF-8"></script>
    <script type="text/javascript">
$('#from').tokenfield({
  autocomplete: { 
    delay: 100
  },
  showAutocompleteOnFocus: true
});
$('#to').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});
$('#cc').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});
$('#bcc').tokenfield({
  autocomplete: {
    delay: 100
  },
  showAutocompleteOnFocus: true
});


    $('#summernote').summernote({
      height: 400,                 // set editor height
      minHeight: null,             // set minimum height of editor
      maxHeight: null             // set maximum height of editor
    });
    $('#form-emailer').on('submit',function(){
      var markupStr = $('#summernote').summernote('code');
      var input = $("<input>")
              .attr("type", "hidden")
                     .attr("name", "content").val(markupStr);
      $('#form-emailer').append($(input));
      console.log(this.serialize());
      return false;
    });
    </script> 

@endsection