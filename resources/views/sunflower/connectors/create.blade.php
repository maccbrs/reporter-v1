<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">

            <div class="row">
                <div class="col-md-8 col-md-offset-2 ">

                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Connector<small>donot mixed up inbounds with outbounds</small></h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                        <form method="post" action="{{route('sunflower.vct.store')}}"> 
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                            <div class="form-group">
                              <label class="control-label col-md-2">Title<span class="required">*</span>
                              </label>
                              <div class="col-md-8">
                                <input class="form-control" name="title" type="text">
                              </div>
                            </div>
                            <div class="col-md-12">
                                <div class="checkbox">
                                  <label>
                                    <input type="radio" class="flat" name="bound" checked="checked" value="in"> Inbound
                                  </label>
                                </div>
                                <div class="checkbox">
                                  <label>
                                    <input type="radio" class="flat" name="bound" value="out"> Outbound
                                  </label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h4>Outbounds</h4>
                            <?php $x = 0; ?>
                            <div class="col-md-4">
                            @foreach($outbounds as $out)
                                @if($out)
                                      <input type="checkbox" name="viciCampaigns[]" value="{{$out}}" class="flat" /> {{$out}}
                                      <br />
                                      <?php $x++; ?>
                                      @if($x == 5)
                                        <?php $x = 0; ?>
                                        </div>
                                        <div class="col-md-4">
                                      @endif
                                @endif
                            @endforeach
                            </div>
                            <div class="clearfix"></div>
                            <h4>Inbounds</h4>
                            <?php $y = 0; ?>
                            <div class="col-md-4">
                            @foreach($inbounds as $in)
                                @if($in)
                                      <input type="checkbox" name="viciCampaigns[]" value="{{$in}}" class="flat" /> {{$in}}
                                      <br />
                                      <?php $y++; ?>
                                      @if($y == 5)
                                        <?php $y = 0; ?>
                                        </div>
                                        <div class="col-md-4">
                                      @endif
                                @endif
                            @endforeach
                            </div>

                            <div class="clearfix"></div>
                            <h4>Dummyboards Campaigns</h4>
                            <?php $z = 0; ?>
                            <div class="col-md-6">
                            @foreach($dmboards as $brd)
                                @if($in)
                                      <input type="checkbox" name="dummyCampaigns[]" value="{{$brd->campaign_id}}" class="flat" />{{$brd->lob}}
                                      <br />
                                      <?php $z++; ?>
                                      @if($z == 5)
                                        <?php $z = 0; ?>
                                        </div>
                                        <div class="col-md-6">
                                      @endif
                                @endif
                            @endforeach
                            </div>
                            <div class="col-md-12" style="margin-top:10px;"><button type="submit" class="btn btn-primary">submit</button></div>
                        </form>

                        </div>
                      </div>

              </div> 
            </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection