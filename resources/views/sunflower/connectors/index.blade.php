<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12"> 
                <div class="x_content">
                    <div class="row tile_count">                     
                        
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Bound</th>
                                <th>vici</th>
                                <th>board</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>

                            @if($connectors)
                                @foreach($connectors as $con)
                                    <tr>
                                        <td>{{$con->name}}</td> 
                                        <td>{{$con->bound}}</td>
                                        <td>
                                            @if(json_decode($con->vici_plug))
                                                <select class="select2_single form-control" tabindex="-1">
                                                    @foreach(json_decode($con->vici_plug) as $vp)
                                                        <option>{{$vp}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </td>
                                        <td>
                                            @if(json_decode($con->templar_socket))
                                                <select class="select2_single form-control" tabindex="-1">
                                                    @foreach(json_decode($con->templar_socket) as $vp)
                                                        <option>{{$vp}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        </td>                                        
                                        <td>
                                            {!!view('layouts.delete',['id' => $con->id,'name' => $con->name,'route' => 'sunflower.vct.destroy','param' => $con->id])!!}                                         
                                        </td>
                                    </tr>                                    
                                @endforeach

                            @endif

                        </tbody>  
                    </table>
                            {!!$connectors->links()!!}
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection