<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">

            <div class="row">
                <div class="col-md-12 ">

                      <div class="x_panel">
                        <div class="x_title">
                          <h2>{{$page->board->lob}} - {{$page->title}}</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                              <?php $include = 'sunflower.templates.'.$page->template.'._form'; ?>
                              @include($include)
                        </div>
                      </div>

              </div> 
            </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <link rel="stylesheet" type="text/css" href="{{$asset.'ckeditor2/library/grideditor/dist/grideditor.css'}}" /> 

    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')

    <script src="{{$asset.'ckeditor2/ckeditor.js'}}"></script>
    <script src="{{$asset.'ckeditor2/sample.js'}}"></script>

    <script type="text/javascript">
        CKEDITOR.replace( 'editor' );
        CKEDITOR.add
        CKEDITOR.replace( 'editor2' );
        CKEDITOR.add  
        CKEDITOR.replace( 'editor3' );
        CKEDITOR.add  
        CKEDITOR.replace( 'editor4' );
        CKEDITOR.add  
        CKEDITOR.replace( 'editor5' );
        CKEDITOR.add  

        CKEDITOR.on('instanceReady', function(e) {
      // First time
        e.editor.document.getBody().setStyle('background-color', '#ccc');
      // in case the user switches to source and back
        e.editor.on('contentDom', function() {
        e.editor.document.getBody().setStyle('background-color', '#ccc');
    });
    });    
    </script>  

@endsection