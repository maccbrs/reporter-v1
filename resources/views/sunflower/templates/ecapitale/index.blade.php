@extends('sunflower.templates.ecapitale.master')
@section('title', 'Page Title')

@section('content')

	<div id="main-container">
		<div class="container">

			@if (Session::has('flash_notification.message'))
				@include('templates.'.$page->template.'._success')
			@else

			<div class="row first-row">
				<div class="col-md-12">
					<div class="panel panel-default">
	                    <div class="panel-heading clearfix">

	                    </div>
						<div class="panel-body">

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-body">
									<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=UTF-8">
									<!--  NOTE: Please add the following <FORM> element to your page.      https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8       -->
									<form action="#" method="POST" id="myform">
										 <input type="hidden" name="debug" value="1"> 
									<input type="hidden" name="oid" value="00Do0000000KcRH">
									<input type="hidden" name="retURL" value="http://">

									<label for="first_name">First Name</label><input  id="first_name" maxlength="40" name="first_name" size="20" type="text" /><br>

									<label for="last_name">Last Name</label><input  id="last_name" maxlength="80" name="last_name" size="20" type="text" /><br>

									<label for="title">Title</label><input  id="title" maxlength="40" name="title" size="20" type="text" /><br>

									<label for="email">Email</label><input  id="email" maxlength="80" name="email" size="20" type="text" /><br>

									<label for="company">Company</label><input  id="company" maxlength="40" name="company" size="20" type="text" /><br>

									<label for="street">Street</label><textarea name="street"></textarea><br>

									<label for="city">City</label><input  id="city" maxlength="40" name="city" size="20" type="text" /><br>

									<label for="zip">Zip</label><input  id="zip" maxlength="20" name="zip" size="20" type="text" /><br>

									<label for="state_code">State/Province</label><select  id="state_code" name="state_code">
									<option value="">--None--</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option>
									</select><br>


									<label for="00No0000008zszy">Company Start Year:</label>
									<input  id="00No0000008zszy" maxlength="4" name="00No0000008zszy" size="20" type="text" /><br>



									<label for="URL">Website</label>
									<input  id="URL" maxlength="80" name="URL" size="20" type="text" /><br>

									<label for="phone">Phone</label>
									<input  id="phone" maxlength="40" name="phone" min="2" size="20" type="text" /><br>

									<label for="00No000000BuVNz">Monthly Gross Sales</label>
									<input  id="00No000000BuVNz" name="00No000000BuVNz" size="20" type="text" /><br>

									<!-- Credit Indicator: -->
									<input  id="00No000000DNtzP" name="00No000000DNtzP" size="20" type="hidden" value="99" />

									<!-- SIC Code: -->
									<input  id="00No0000008zt1Q" maxlength="20" name="00No0000008zt1Q" size="20" type="hidden" value="594201" />

									<!-- SIC Description: -->
									<input  id="00No0000008zt1R" maxlength="80" name="00No0000008zt1R" size="20" type="hidden" value="Book Dealers-Retail" />

									<!-- SIC Description 2: -->
									<input  id="00No000000DNtwp" maxlength="80" name="00No000000DNtwp" size="20" type="hidden" value="Book Dealers-Retail" />

									<!-- SIC Description 3: -->
									<input  id="00No000000DNtzK" maxlength="80" name="00No000000DNtzK" size="20" type="hidden" value="" />

									<!-- List source specific ID #:  -->
									<input  id="00No0000008zt0c" maxlength="50" name="00No0000008zt0c" size="20" type="hidden" value="425286955" />

									<!--  List Source Name:  -->
									<input  id="00No0000008zt0b" maxlength="255" name="00No0000008zt0b" size="20" type="hidden" /><br>

									<!-- Lead Source -->
									<input type="hidden"  id="lead_source" name="lead_source" value="Telemarketing">

									<!-- Last Source: -->
									<input type="hidden"  id="00No0000008zt0W" name="00No0000008zt0W" value="Telemarketing">

									<!-- Last Source Sub-Source: -->
									<input type="hidden"  id="00No0000008zt0V" name="00No0000008zt0V"  value="CC1">

									<!-- Country Code -->
									<input id="country_code" name="country_code" type="hidden" value="US">

									<!-- Original Sub-Source:-->
									<input id="00No0000008zt11" name="00No0000008zt11" type="hidden" value="CC1" />

									<!-- Lead Record Type --> 
									<input  id="recordType" name="recordType" type="hidden" value="012o0000000prtt" />
									<input type="submit" name="submit">

									</form>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
						<!-- Default panel contents -->
						<div class="panel-body">
							
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>

@endsection