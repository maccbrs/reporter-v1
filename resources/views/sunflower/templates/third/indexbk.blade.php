@extends('templates.third.master')
@section('title', 'Page Title')

<?php 

  $actual_link = $_SERVER['HTTP_HOST'];

  $results = explode('/', trim($actual_link,'/'));

  if(count($results) > 0){

      $last = $results[count($results) - 1];
  } 

  $actual_host = $_SERVER['PHP_SELF'];

  $results2 = explode('/', trim($actual_host,'/'));
 
  $preLink = "http://".$results[0]."/". $results2[1]; 

  $campaign = $results2[2];

?>

<?php

if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}


$email_subject = '';
$option = json_decode($page->options);
if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>


@section('content')
<form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
    <input type="hidden" name="_method" value="POST">

    <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
    <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
    <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
    <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
    <input type="hidden" name="email_subject" value="{{$email_subject}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
    <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
    <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

    <div class="container">
      <div class="top">
        
          @if($link == 'preview')
          <div class="pull-right">
            <label>test email:</label>
            <input type="text" name="test_email">
           </div>
          @elseif($link == 'train' && Auth::check())
          <div class="pull-right">
            <label>test email:</label>
            <input type="text" name="test_email">
           </div>
          @endif
          
           {{ Session::get('flash_notification.message') }}

      </div>

      <div class="row">
        <div class="col-md-8">
          <div class="bx o-l main-bx">

                @if (Session::has('flash_notification.message'))
                  @include('templates.'.$page->template.'._success')
                @else
                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

          </div>
        </div>

        <div class="col-md-4 sidebar ">
           <div class="bx o-l">
            
              <div class ="linkDiv">
                  
                <?php if($campaign == "56ff303456bab"){?>

                  <p><a href= "<?php echo $preLink. '/570ee783ddf6f/435'?>"><input name="MNOS" type="button" value="MNOS" /> </a></p>

                  <p><a href="<?php echo $preLink. '/57101bd74a14d/423'?>"><input name="QHEART" type="button" value="QHEART" /> </a></p>

                  <p><a href="<?php echo $preLink. '/57101c17c3a0b/424'?>"><input name="CURCUMIN" type="button" value="CURCUMIN" /> </a></p>

                  <p><a href="<?php echo $preLink. '/57101c7d2183d/426'?>"><input name="SUPREME BRAIN" type="button" value="SUPREME BRAIN" /> </a></p>

                  <p><a href="<?php echo $preLink. '/57101d50b098e'?>"><input name="REJUVENATION" type="button" value="REJUVENATION" /> </a></p>

                  <p><a href="<?php echo $preLink. '/57101d9db2f39'?>"><input name="TRANSFORMATION" type="button" value="TRANSFORMATION" /> </a></p>

                  <p><a href="<?php echo $preLink. '/57101cfd6a2e3'?>"><input name="REJUVAFLEX" type="button" value="REJUVAFLEX" /> </a></p>

                  <p><a href="<?php echo $preLink. '/5714fdb2667d1'?>"><input name="DYFLOGEST" type="button" value="DYFLOGEST" /> </a></p>

                <?php } ?>

                <?php if($campaign == "571e4c72a2370"){?>

                  <p><a href="<?php echo $preLink. '/571e4d7153706'?>"><input name="MNOS" type="button" value="MNOS" /></a></p>

                  <p><a href="<?php echo $preLink. '/571e4e0b94b44/551'?>"><input name="REJUV" type="button" value="REJUV" /></a></p>

                <?php } ?>

              </div>
            </div>
        </div>
        @endif
      </div>

    </div>
</form>   

@endsection

<script type="text/javascript">
$( document ).ready(function() {
   $(":radio[value=yes]").on('click',function(){
     $('select').attr('disabled', 'disabled');
   });
   $(":radio[value=no]").on('click',function(){
     $('select').prop( "disabled", false );
   });       
});
</script>

<style>
  input[type=button], button {
    display: inline-block;
    margin: 5px 20px;
    width: 285px;
    background-color: #D3D3D3;
    box-shadow: inset 0 0 10px #333;
    border-radius: 3px;
    font-size: 20px;
    text-align: center;
    font-weight: bold;
    padding: 10px;
  }

  p , .linkDiv{

    text-align:center;
  }

</style>
