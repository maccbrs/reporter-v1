@extends('sunflower.templates.eCapital.master')
@section('title', 'Page Title')

<?php

if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}


$email_subject = '';
$option = json_decode($page->options);
if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : " " ;
  $phone_number = !empty($_GET['phone_number']) ? $_GET['phone_number'] : " ";
  $user = !empty($_GET['user']) ? $_GET['user'] : " ";

  

  
  // $alt_number = !empty($_GET['alt_number']) ? $_GET['alt_number'] : " "; 
  // $website = !empty($_GET['email']) ? $_GET['email'] : " ";
  

  // hidden fields

  $product = !empty($_GET['product']) ? $_GET['product'] : " "; 
  $lead_source = !empty($_GET['lead_source']) ? $_GET['lead_source'] : " "; 
  $sub_source = !empty($_GET['sub_source']) ? $_GET['sub_source'] : " "; 
  $recordType = !empty($_GET['recordType']) ? $_GET['recordType'] : " "; 
  $original_source = !empty($_GET['sub_source']) ? $_GET['sub_source'] : " ";

  //

  $vendor_lead_code = !empty($_GET['vendor_lead_code']) ? $_GET['vendor_lead_code'] : " "; 
  $source_id = !empty($_GET['source_id']) ? $_GET['source_id'] : " ";
  $phone_code = !empty($_GET['phone_code']) ? $_GET['phone_code'] : " "; 
  $phone = !empty($_GET['phone']) ? $_GET['phone'] : " "; 
  $title = !empty($_GET['alt_phone']) ? $_GET['alt_phone'] : " ";
  $first_name = !empty($_GET['first_name']) ? $_GET['first_name'] : " ";
  $last_name = !empty($_GET['last_name']) ? $_GET['last_name'] : " ";
  $company = !empty($_GET['address1']) ? $_GET['address1'] : " ";
  $street = !empty($_GET['address2']) ? $_GET['address2'] : " ";
  $industry = !empty($_GET['address3']) ? $_GET['address3'] : " "; 
  $city = !empty($_GET['city']) ? $_GET['city'] : " ";
  $state_code = !empty($_GET['state']) ? $_GET['state'] : " ";
  $province = !empty($_GET['province']) ? $_GET['province'] : " ";
  $zip = !empty($_GET['postal_code']) ? $_GET['postal_code'] : " ";
  $email = !empty($_GET['email']) ? $_GET['email'] : " ";
  $security_phrase = !empty($_GET['security_phrase']) ? $_GET['security_phrase'] : " "; 
  $comments = !empty($_GET['comments']) ? $_GET['comments'] : " "; 
  $rank = !empty($_GET['rank']) ? $_GET['rank'] : " ";
  $owner = !empty($_GET['owner']) ? $_GET['owner'] : " "; 


?>

@section('content')

 <form enctype="multipart/form-data"  action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" <?php novalidate($page->campaign_id); ?>> 

  <!-- <form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
     -->
   <!--  <input type="hidden" name="_method" value="POST"> -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}">


    <div class="container">
      <div class="top">
        
        @if($link == 'preview')
        <div class="pull-right">
          <label>test email:</label>
          <input type="text" id="test_email" name="test_email">
         </div>
        @elseif($link == 'train' && Auth::check())
        <div class="pull-right">
          <label>test email:</label>
          <input type="text" name="test_email">
         </div>
        @endif
        
         {{ Session::get('flash_notification.message') }}

      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="bx o-l main-bx">
            <div class="col-md-4 sidebar ">
           <div class="bx o-l" style = "box-shadow: inset 0 0 0px ; width:75%;">

           </div>
          
        </div>

              <input type=hidden name="oid" value="00Do0000000KcRH">
              <input type=hidden name="retURL" value="http://">

              <table style = "text-align: center;">
                <tr>
                  <td>
                     <label for="first_name">First Name</label>
                  </td>
                  <td>
                    <input  id="first_name" maxlength="40" name="first_name" size="20" type="text"  class = "capitalize" value = <?php echo $first_name?> >
                  </td>
                </tr>

                <tr>
                  <td>

                     <label for="last_name">Last Name</label>
                  </td>
                  <td>

                    <input  id="last_name" maxlength="80" name="last_name" size="20" type="text" class = "capitalize"  value = <?php echo $last_name?>><br>
                   </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="title">Title</label>
                  </td>
                  <td>
                      <input  id="title" class = "title" maxlength="40" name="title" size="20" type="text"  value = <?php echo $title?>>
                  </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="email">Email</label>
                  </td>
                  <td>


                    <input  id="email" maxlength="80" name="email" size="20" type="email" onKeyUp="javascript:this.value=this.value.toLowerCase();"  value = <?php echo $email?>><br>


                    </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="company">Company</label>
                  </td>
                  <td>

                    <input  id="company" class ="company" maxlength="40" name="company" size="20" type="text"  value = <?php echo $company?>><br>

                    </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="street">Street</label>
                  </td>
                  <td>

                    <textarea class="street capitalize" name="street"  value = <?php echo $street?>></textarea>

                    </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="city">City</label>
                  </td>
                  <td>

                    <input  id="city" class = "capitalize" maxlength="40" name="city" size="20" type="text"  value = <?php echo $city?>><br>


                    </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="zip">Zip</label>
                  </td>
                  <td>

                    <input  id="zip" class = "capitalize" maxlength="20" name="zip" size="20" type="text"  value = <?php echo $zip?>><br>


                    </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="state_code">State/Province</label>
                  </td>
                  <td>


                    <select  id="state_code" name="state_code"  >

                      <option value="">--None--</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option>
                    </select><br>

                  </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="00No0000008zszy">Company Start Year:</label>
                  </td>
                  <td>


                    <input  id="00No0000008zszy " class ="com_start" maxlength="4" name="00No0000008zszy" size="20" type="text" value ="2010"  /><br>


                  </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="URL">Website</label>
                  </td>
                  <td>


                    <input  id="URL" maxlength="80" name="URL" size="20" onKeyUp="javascript:this.value=this.value.toLowerCase();"   value = <?php echo $website?>><br>

                  </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="phone">Phone</label>
                  </td>
                  <td>

                    <input  id="phone" maxlength="10" name="phone"  type="number"  value = <?php echo $phone?>>&nbsp;<span id="errmsg"></span><br>
                  </td>
                </tr>

                <tr>
                  <td>
                    
                     <label for="00No000000BuVNz">Monthly Gross Sales</label>
                  </td>
                  <td>


                    <input  id="00No000000BuVNz" type = "number" class ="gross" name="00No000000BuVNz" size="20" type="text"  ><br>

                  </td>
                </tr>
                    
        
              <!-- input type="hidden" name="debug" value="1">  -->

              <!-- Credit Indicator: -->
           

              <!-- <input  id="00No000000DNtzP" name="00No000000DNtzP" size="20" type="hidden" value= = <?php echo $phone_code?> >
 -->
              <!-- SIC Code: -->
              <input  id="00No0000008zt1Q" maxlength="20" name="00No0000008zt1Q" size="20" type="hidden" value = <?php echo $comments?> >

              <!-- SIC Description: -->
              <input  id="00No0000008zt1R" maxlength="80" name="00No0000008zt1R" size="20" type="hidden" value = <?php echo $province?> >

              <!-- SIC Description 2: -->
              <input  id="00No000000DNtwp" maxlength="80" name="00No000000DNtwp" size="20" type="hidden" value = <?php echo $province?> >

              <!-- SIC Description 3: -->
              <input  id="00No000000DNtzK" maxlength="80" name="00No000000DNtzK" size="20" type="hidden" value = <?php echo $rank?> >

              <!-- List source specific ID #:  -->
              <input  id="00No0000008zt0c" maxlength="50" name="00No0000008zt0c" size="20" type="hidden" value = <?php echo $vendor_lead_code?> >

                 <!-- product -->
              <input type="hidden"  id="00No000000BuVVk" name="00No000000BuVVk"  value = <?php echo $product?> > 

              <input  type ="hidden" id = "phone_number" name="phone_number" value="<?php echo $phone?>">
              
              <!--  List Source Name:  -->
              <input  id="00No0000008zt0b" maxlength="255" name="00No0000008zt0b" size="20" type="hidden"  value = <?php echo $source_id?>><br>

              <!-- Lead Source -->
              <input type="hidden"  id="lead_source" name="lead_source" value = <?php echo $lead_source?>>

              <!-- Last Source: -->
              <input type="hidden"  id="00No0000008zt0W" name="00No0000008zt0W" value = <?php echo $industry?>>

              <!-- Last Source Sub-Source: -->
              <input type="hidden"  id="00No0000008zt0V" name="00No0000008zt0V"  value = <?php echo $industry?>>

              <!-- Country Code -->
              <input id="country_code" name="country_code" type="hidden" value="US">

              <!-- Original Sub-Source:-->
              <input id="00No0000008zt11" name="00No0000008zt11" type="hidden"  value = <?php echo $original_source?>>

              <!-- Lead Record Type 012o0000000prtt--> 
              <input id="recordType" name="recordType" type="hidden"  value=<?php echo $recordType?> > 

              <input type ="hidden" id = "campaign_id" name="campaign_id" value="<?php echo $page->campaign_id?>">

              <input type ="hidden" id = "lead_id" name="lead_id" value="<?php echo $lead_id?>">

              <input type ="hidden" id = "user" name="user" value="<?php echo $user?>">

              <input type ="hidden" id = "lob" name="lob" value="<?php echo $board->lob?>">

              <input type ="hidden" id = "from" name="from" value=<?php echo $board->from?>>

              <input type ="hidden" id = "to" name="to" value=<?php echo $board->to?>>

              <input type ="hidden" id = "cc" name="cc" value=<?php echo $board->cc?>>

              <input type ="hidden" id = "bcc" name="bcc" value=<?php echo $board->bcc?>>

              <input type ="hidden" id = "options" name="options" value=<?php echo $board->options ?>>

              <tr>
                <td colspan = 2>

                  <input type="submit" id = "submit" name="submit">

                </td>

              </tr>
            </table>

              <br><br>

                @if (Session::has('flash_notification.message'))
                  @include('templates.'.$page->template.'._success')
                @else
                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

          </div>
        </div>

        <div class="col-md-4 sidebar ">
           <div class="bx o-l">

                <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'') ?>
                
                @if(!empty($bookwormBoards))
            
                  @if(in_array($page['campaign_id'], $bookwormBoards ))

                    @if(!empty($arr_contents['content-r']))

                      <div style = "text-align:center; margin-top: -63px;">

                        <input type = "hidden" name = "Agent Name" value ="<?php echo $user ?>">

                        <input type="submit" id ="form-submit" value="Submit">

                      </div>

                    @endif

                  @endif

                @endif

           </div>
           <div class="bx o-l">
                <div class="bottom-r">
                     @foreach($pages as $p)

                        @if($link == 'live')

                          @if($p->id != $page->id && in_array($p->id,$btns))
                            <a href="{{url('live/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                          @endif

                        @else

                          @if($p->id != $page->id && in_array($p->id,$btns))
                            <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                          @endif

                        @endif
                     @endforeach
                </div>
           </div>
        </div>
        @endif
      </div>
    </div>
</form>   

@endsection

<style>

 input[type=submit] {
    display: inline-block;
    margin: 5px 20px;
    width: 285px;
    background-color: #D3D3D3;
    box-shadow: inset 0 0 10px #333;
    border-radius: 3px;
    font-size: 20px;
    text-align: center;
    font-weight: bold;
    padding: 10px;
  }

</style>
