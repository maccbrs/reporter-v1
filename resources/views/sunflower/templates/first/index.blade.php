@extends('sunflower.templates.first.master')
@section('title', 'Page Title')


<?php


if(!empty($page->contents)){
    $contents = json_decode($page->contents); 
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
}


$email_subject = '';
$option = json_decode($page->options);
if(!empty($option)){
  $email_subject = (isset($option->email_subject)?$option->email_subject:'');
}

?>

<?php
  $lead_id = !empty($_GET['lead_id']) ? $_GET['lead_id'] : " " ;
  $phone_number = !empty($_GET['phone_number']) ? $_GET['phone_number'] : " ";
  $user = !empty($_GET['user']) ? $_GET['user'] : "test";
?>

@section('content')

<form enctype="multipart/form-data" action="{{url('data/save/'.$page->campaign_id)}}" method="post" <?php novalidate($page->campaign_id); ?>>
    <input type="hidden" name="_method" value="POST">

    <input type="hidden" name="hidden_lead_id" value="{{(isset($_POST['lead_id'])?$_POST['lead_id']:'')}}">
    <input type="hidden" name="hidden_phone_number" value="{{(isset($_POST['phone_number'])?$_POST['phone_number']:'')}}">
    <input type="hidden" name="hidden_user" value="{{(isset($_POST['user'])?$_POST['user']:'')}}">
    <input type="hidden" name="request_status" value="{{(Request::is('preview/*')||Request::is('train/*')?'test':'live')}}">
    <input type="hidden" name="email_subject" value="{{$email_subject}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
    <input type="hidden" name="all[]" value="{{json_encode($_POST)}}">
    <input type="hidden" name="all[]" value="{{json_encode($_GET)}}">

    <div class="container">
      <div class="top">
        
        @if($link == 'preview')
        <div class="pull-right">
          <label>test email:</label>
          <input type="text" name="test_email">
         </div>
        @elseif($link == 'train' && Auth::check())
        <div class="pull-right">
          <label>test email:</label>
          <input type="text" name="test_email">
         </div>
        @endif
        
         {{ Session::get('flash_notification.message') }}

      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="bx o-l main-bx">

                @if (Session::has('flash_notification.message'))
                  @include('templates.'.$page->template.'._success')
                @else
                <?= (isset($arr_contents['content-l'])?parse_input($arr_contents['content-l'],$url):'') ?>

          </div>
        </div>
        <div class="col-md-4 sidebar ">
           <div class="bx o-l">

                <?= (isset($arr_contents['content-r'])?parse_input($arr_contents['content-r'],$url):'') ?>
                
                @if(!empty($bookwormBoards))
            
                  @if(in_array($page['campaign_id'], $bookwormBoards ))

                    @if(!empty($arr_contents['content-r']))

                      <div style = "text-align:center; margin-top: -63px;">

                        <input type = "hidden" name = "Agent Name" value ="<?php echo $user ?>">

                        <input type="submit" value="Submit">

                      </div>

                    @endif

                  @endif

                @endif

           </div>
           <div class="bx o-l">
                <div class="bottom-r">
                         @foreach($pages as $p)

                            @if($link == 'live')

                              @if($p->id != $page->id && in_array($p->id,$btns))
                                <a href="{{url('live/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                              @endif

                            @else

                              @if($p->id != $page->id && in_array($p->id,$btns))
                                <a href="{{url('preview/'.$p->campaign_id.'/'.$p->id)}}" class="btn">{{$p->title}}</a> 
                              @endif

                            @endif
                         @endforeach
                </div>
           </div>
        </div>
        @endif
      </div>
    </div>
</form>   

@endsection

<script type="text/javascript">
$( document ).ready(function() {
   $(":radio[value=yes]").on('click',function(){
     $('select').attr('disabled', 'disabled');
   });
   $(":radio[value=no]").on('click',function(){
     $('select').prop( "disabled", false );
   });       
});
</script>

<style>

 input[type=submit] {
    display: inline-block;
    margin: 5px 20px;
    width: 285px;
    background-color: #D3D3D3;
    box-shadow: inset 0 0 10px #333;
    border-radius: 3px;
    font-size: 20px;
    text-align: center;
    font-weight: bold;
    padding: 10px;
  }

</style>
