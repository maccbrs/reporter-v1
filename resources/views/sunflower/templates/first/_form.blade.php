<?php

  if(!empty($page->contents)){
    $contents = json_decode($page->contents);
    foreach ($contents as $k => $v) {
      $arr_contents[$k] = $v;
    }
  }

  $email_subject = '';
  $option = json_decode($page->options);
  if(!empty($option)){
    $email_subject = (isset($option->email_subject)?$option->email_subject:'');
  }

?>
<form enctype="multipart/form-data" action="{{route('sunflower.page.update',$page->id)}}" method="post">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="id" value="{{$page->id}}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

    <div class="col-md-4">
        <div class="form-group input-group">
           <span class="input-group-addon btn-primary">Title</span>
           <input type="text" class="form-control" name="title" required value="{{(isset($page->title)?$page->title:'')}}" >
        </div>                        
    </div>
    <div class="col-md-4">
        <div class="form-group input-group">
           <span class="input-group-addon">Email Subject</span>
           <input type="text" class="form-control" name="email_subject" required value="{{$email_subject}}" >
        </div>                        
    </div>                      
    <div class="col-md-4">
          <button type="submit" class="btn btn-default pull-right">update</button> <a href="{{url('preview/'.$page->campaign_id.'/'.$page->id)}}" target="_tab" class=" preview btn btn-default pull-right">Preview</a>
          <a href="{{route('sunflower.board.edit',$page->campaign_id)}}" class="preview btn btn-default pull-right">Board Main</a>                        
    </div>  
    <div class="clearfix"></div>

    <div class="accordion" id="accordion1" role="tablist" aria-multiselectable="true">

      <div class="panel">
        <a class="panel-heading" role="tab" id="headingOne1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
          <h4 class="panel-title">Left Block</h4>
        </a>
        <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            <textarea id="editor" class="form-control" rows="20" height="100px" name="content-l"><?= (isset($arr_contents['content-l'])?$arr_contents['content-l']:'') ?></textarea>
          </div>
        </div>
      </div>

      <div class="panel">
        <a class="panel-heading collapsed" role="tab" id="headingTwo1" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo">
          <h4 class="panel-title">Right Block</h4>
        </a>
        <div id="collapseTwo1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
            <textarea id="editor2" class="form-control" rows="20" height="100px" name="content-r"><?= (isset($arr_contents['content-r'])?$arr_contents['content-r']:'') ?></textarea>
          </div>
        </div>
      </div>


    </div>

</form>