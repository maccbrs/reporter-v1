<?php $asset = URL::asset('/gentella/'); ?> 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

      <link href="{{$asset}}/css/bootstrap.min.css" rel="stylesheet">
      <link href="{{$asset}}/fonts/css/font-awesome.min.css" rel="stylesheet">
      <link href="{{$asset}}/css/animate.min.css" rel="stylesheet">
      <link href="{{$asset}}/css/custom.css" rel="stylesheet">
      <link href="{{$asset}}/css/icheck/flat/green.css" rel="stylesheet">
      <link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">
      <link href="{{$asset}}/css/editor/external/google-code-prettify/prettify.css" rel="stylesheet">
      <link href="{{$asset}}/css/editor/index.css" rel="stylesheet">
      <link href="{{$asset}}/css/select/select2.min.css" rel="stylesheet">
      <link rel="stylesheet" href="{{$asset}}/css/switchery/switchery.min.css" />
      <script src="{{$asset}}/js/jquery.min.js"></script> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <style type="text/css">
            .nav.side-menu li{
                cursor: pointer; 
            }
            .fa-rosebud{
                background: url('{{$asset}}/images/rosebud.png') no-repeat center;
                background-size:     cover; 
                width: 48px;
                height: 42px;
            }
            .nav_menu{
                display: none;
            }
        </style>
    @yield('header-scripts')
</head>


<body class="nav-sm">

    <div class="container body">

        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="{{route('sunflower.index')}}" class="site_title"><i class="fa fa-rosebud"></i> <span>Magellan Prepaid</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2>admin</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li>
                                    <a><i class="fa fa-newspaper-o"></i> Boads</a>
                                    <ul class="nav child_menu" >
                                        <li>
                                            <a href="{{route('sunflower.board.index')}}">View All</a>
                                        </li>
                                        <li>
                                            <a href="{{route('sunflower.board.add')}}">Add</a>
                                        </li>
                                    </ul>
                                </li> 
                                <li>
                                    <a><i class="fa fa-envelope-o"></i> Email Function </a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="{{route('sunflower.emailer.index')}}">View All</a>
                                        </li>
                                        <li>
                                            <a href="{{route('sunflower.emailer.add')}}">Add</a>
                                        </li>
                                    </ul>
                                </li>                                 
                                <li>
                                    <a><i class="fa fa-floppy-o"></i>Publish<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="{{route('sunflower.publish.index')}}">View All</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a><i class="fa fa-wrench"></i>Tools<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="{{route('sunflower.publish.index')}}">change crc</a>
                                        </li>
                                    </ul>
                                </li>                                                                     
                                <li>
                                    <a><i class="fa fa-sitemap"></i>connectors</a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li>
                                            <a href="{{route('sunflower.vct.index')}}">View All</a>
                                        </li>
                                        <li>
                                            <a href="{{route('sunflower.vct.create')}}">Add</a>
                                        </li>
                                    </ul>
                                </li>  

                            </ul>
                        </div>


                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href=""><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="height:105%;">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

        <script src="{{$asset}}/js/bootstrap.min.js"></script>
        <script src="{{$asset}}/js/progressbar/bootstrap-progressbar.min.js"></script>
        <script src="{{$asset}}/js/nicescroll/jquery.nicescroll.min.js"></script>
        <script src="{{$asset}}/js/icheck/icheck.min.js"></script>
        <script src="{{$asset}}/js/tags/jquery.tagsinput.min.js"></script>
        <script src="{{$asset}}/js/switchery/switchery.min.js"></script>
        <script type="text/javascript" src="{{$asset}}/js/moment/moment.min.js"></script>
        <script type="text/javascript" src="{{$asset}}/js/datepicker/daterangepicker.js"></script>
        <script src="{{$asset}}/js/editor/bootstrap-wysiwyg.js"></script>
        <script src="{{$asset}}/js/editor/external/jquery.hotkeys.js"></script>
        <script src="{{$asset}}/js/editor/external/google-code-prettify/prettify.js"></script>
        <script src="{{$asset}}/js/select/select2.full.js"></script>
        <script type="text/javascript" src="{{$asset}}/js/parsley/parsley.min.js"></script>
        <script src="{{$asset}}/js/textarea/autosize.min.js"></script>
        <script>
        autosize($('.resizable_textarea'));
        </script>
        <script type="text/javascript" src="{{$asset}}/js/autocomplete/countries.js"></script>
        <script src="{{$asset}}/js/autocomplete/jquery.autocomplete.js"></script>
        <script src="{{$asset}}/js/pace/pace.min.js"></script>
        <script src="{{$asset}}/js/custom.js"></script>

    @yield('footer-scripts')

</body>

</html>