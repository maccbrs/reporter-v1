<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-md-3 form-group pull-right top_search">
             <div class="input-group" style="margin-bottom: -20px;">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
                </span>
             </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"> 
                <div class="x_content">
                    <div class="row tile_count">
                        
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr>
                                <th>Campaign</th>
                                <th>LOB</th>
                                <th>Email to</th>
                                <th>Email cc</th>
                                <th>Email bcc</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($boards as $board)
                                    <tr class="odd gradeX" style="{{(in_array($board->campaign_id,$connected)?'background-color: rgba(0,240,0,.2)':'')}}">
                                        <td>{{$board->campaign_id}}</td>
                                        <td>{{$board->lob}}</td>
                                        <td>{!!toccbcc_layout($board->to)!!}</td>
                                        <td>{!!toccbcc_layout($board->cc)!!}</td>
                                        <td>{!!toccbcc_layout($board->bcc)!!}</td>
                                        <td>
                                            <a href="{{route('sunflower.board.edit',$board->campaign_id)}}" class="btn btn-primary btn-xs"> <i class="fa fa-pencil"></i>Edit</a>
                                            <a href="{{route('sunflower.preview.board',$board->campaign_id)}}" target="_tab" class="btn btn-primary btn-xs" ><i class="fa fa-folder"></i> Preview</a>
                                            @if( $usertype == 'admin')
                                            <a href="{{route('sunflower.board.delete',$board->campaign_id)}}" target="_tab" class="btn btn-primary btn-xs" ><i class="fa fa-trash-o"></i> Delete</a>
                                            @endif
                                        </td> 
                                    </tr>
                            @endforeach

                        </tbody>  
                    </table>
                    {{$boards->links()}}
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection