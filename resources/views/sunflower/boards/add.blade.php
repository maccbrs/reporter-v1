<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">

            <div class="row">
                <div class="col-md-6 col-md-offset-3 ">

                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Board<small>add new</small></h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                        <form method="post" action="{{route('sunflower.board.add.save')}}"> 
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                            <label >Campaign Id * :</label>
                            <input type="text" class="form-control" value="{{old('campaign')}}" name="campaign" />

                            <label >LOB * :</label>
                            <input type="text" class="form-control" value="{{old('lob')}}" name="lob"/>

                            <label >Email to * :</label>
                            <input type="email" class="form-control" value="{{old('to')}}" name="to"/>

                            <br/>
                            <button type="submit" class="btn btn-primary">submit</button>
                        </form>

                        </div>
                      </div>

              </div> 
            </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection