<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
<?php $options = ['onsubmit' => 'Every Submit','daily' => 'Daily Reports']; ?>
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">
        <form method="post" action="{{route('sunflower.board.update',$board->campaign_id)}}">
          <input type="hidden" name="_method" value="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">    
            <div class="row">
                <div class="col-md-12">

                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Board<small>edit</small></h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="btn btn-primary btn-sm collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li> 
                            <li>
                                <a class="btn btn-primary btn-sm"><i class="fa fa-envelope"></i></a>
                            </li>
                            <li>
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                            </li>                                                                                          
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                              <h4>Campaign: <small>{{$board->lob}}</small></h4>

                              <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel">
                                  <a class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Primary Page:</h4>
                                  </a>
                                  <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <?php $pcount = 0; ?>
                                        <div class="col-md-3">
                                        @if($board->pages)
                                            @foreach($board->pages as $page)

                                                <div class="checkbox">
                                                  <label>
                                                    <input type="radio" class="flat" name="primary_page" {{($page->id == $board->primary_page)?'checked="checked"':''}} value="{{$page->id}}">{{$page->title}}
                                                  </label>
                                                </div>

                                                <?php $pcount++; ?>
                                                @if($pcount == 5)
                                                    </div>
                                                    <div class="col-md-3">
                                                    <?php $pcount = 0; ?>
                                                @endif

                                            @endforeach
                                        @endif
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel">
                                  <a class="panel-heading collapsed" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title">On Submit Option:</h4>
                                  </a>
                                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        @foreach($options as $optk => $optv)
                                        <div class="checkbox">
                                           <label>
                                           <input type="checkbox" class="flat" name="opt[]" <?=(in_array($optk, $reports)?'checked="checked"':'')?> value="{{$optk}}" >{{$optv}}
                                           </label>
                                        </div>
                                        @endforeach
                                        <div class="clearfix"></div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel">
                                  <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <h4 class="panel-title">Page Buttons</h4>
                                  </a>
                                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <?php $pcount = 0; ?>
                                        <div class="col-md-3">
                                        @if($board->pages)
                                            @foreach($board->pages as $page)

                                                <div class="checkbox">
                                                  <label>
                                                    <input type="checkbox" class="flat" name="btn[]" <?=(in_array($page->id, $btns)?'checked="checked"':'')?> value="{{$page->id}}">{{$page->title}}
                                                  </label>
                                                </div>

                                                <?php $pcount++; ?>
                                                @if($pcount == 5)
                                                    </div>
                                                    <div class="col-md-3">
                                                    <?php $pcount = 0; ?>
                                                @endif

                                            @endforeach
                                        @endif
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel">
                                  <a class="panel-heading collapsed" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <h4 class="panel-title">Pages</h4>
                                  </a>
                                  <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                          <a class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".addModalPage"></a>
                                          <table class="table table-striped responsive-utilities jambo_table bulk_action">

                                            <thead>
                                                <tr >
                                                    <th>Title</th>
                                                    <th>Template</th>
                                                    <th>Link</th>
                                                    <th>Date created</th>
                                                    <th>#</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($board->pages as $page)
                                                    @if($page->status == 1)
                                                    <tr class="my-row">
                                                        <td >{{$page->title}}</td>
                                                        <td >{{$page->template}}</td>
                                                        <td>[link:board={{$page->campaign_id}}:page={{$page->id}}:name=?]</td>
                                                        <td >{{$page->created_at}}</td>
                                                        <td >
                                                            <a href="{{route('sunflower.page.edit',$page->id)}}" class="btn btn-primary fa fa-edit"></a>
                                                            {!!view('layouts.delete',['id' => $page->id,'route' => 'sunflower.page.destroy', 'param' => $page->id, 'name' => $page->title])!!}
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>

                                          </table>

                                    </div>
                                  </div>
                                </div>

                              </div>

                              </div>
                        </div>
                      </div>

              </div> 
            </div>
    </form>
    </div>

   <div class="modal fade addModalPage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

          <form method="post" action="{{route('sunflower.page.create',$board->id)}}" class="form-horizontal form-label-left"> 
              <input type="hidden" name="_method" value="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">add new page?</h4> 

                <div style="height:20px;"></div>

                <div class="form-group">
                  <label class="control-label col-md-2" for="first-name">Title<span class="required">*</span>
                  </label>
                  <div class="col-md-9">
                    <input type="text" name="title" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2" >Select Template<span>*</span>
                  </label>
                  <div class="col-md-9">
                        <select class="form-control" name="template">
                            <option value="first">first</option>
                            <option value="second">second</option>
                            <option value="third">third</option>
                            <option value="eCapital">eCapital</option> 
                            <option value="5star">5star</option>
                        </select>
                  </div>
                </div>     

                <div style="height:20px;"></div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>                                                    
              </div>
          </form>

      </div>
    </div>
  </div>     
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection