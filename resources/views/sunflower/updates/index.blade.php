<?php $usertype = Auth::user()->user_type; ?>
<?php $asset = URL::asset('/'); ?> 
@extends('sunflower.master')

@section('title', 'Board list')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12"> 
                <div class="x_content">
                    <div class="row tile_count">
                        
                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                        <thead>
                            <tr>
                                <th>Campaign</th>
                                <th>Last Update</th>
                                <th>Last Action</th>
                                <th>Updates made</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($updates as $v)
                                    <tr class="odd gradeX">
                                        <td>{{$v->board->lob}}</td>
                                        <td>{{$v->updated_at}}</td>
                                        <td>{{$gn->last_action($v->contents)}}</td>
                                        <td>{{$v->count}}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary fa fa-floppy-o" data-toggle="modal" data-target=".publishModal-{{$v->id}}"></button>
                                            <div class="modal fade publishModal-{{$v->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                              <div class="modal-dialog modal-sm">
                                                <div class="modal-content">

                                                    <form method="post" action="{{route('sunflower.publish.update',$v->campaign_id)}}"> 
                                                        <input type="hidden" name="_method" value="POST">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                                        <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                                          </button>
                                                          <h4 class="modal-title">Are you sure you want to publish changes to {{$v->board->lob}}?</h4>
                                                          <div style="height:20px;"></div>
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                          <button type="submit" class="btn btn-primary">Proceed</button>                                                    
                                                        </div>
                                                    </form>

                                                </div>
                                              </div>
                                            </div>                                             
                                        </td> 
                                    </tr>
                            @endforeach

                        </tbody>  
                    </table>
                    {{$updates->links()}}
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection 


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')



@endsection