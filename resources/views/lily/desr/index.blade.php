<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div align = "center">
                    @include('flash::message')
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th>Namess</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if($users)
                                @foreach($users as $user)
                                    <tr class="even pointer">
                                        <td class=" ">{{$user->name}}</td>
                                        <td class=" ">{{$user->email}}</td>

                                        <td class=" ">
                                            <a href="{{$restrictions->btnRoute('lily.desr.task.index',$user->id)}}" class="btn btn-sm btn-default {{$restrictions->btnDisabler('lily.user.access.index')}}">View</a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>No Records</tr> 
                                @endif
                            </tbody>

                        </table>
                        {{$users->render()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection 