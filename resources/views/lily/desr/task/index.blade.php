.<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div align = "center">
                    @include('flash::message')
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th>Date</th>
                                    <th>Current Activities</th>
                                    <th>Pending Activities</th>
                                </tr>
                            </thead>

                            <tbody>
                                @if($users)
                                @foreach($users as $user)
                                    <tr class="even pointer">
                                        <td class=" ">{{$user->date_assigned}}</td>
                                        <td class=" ">{{$user->current_act}}</td>
                                        <td class=" ">{{$user->pending_act}}</td>
                      
                                    </tr>
                                @endforeach
                                @else
                                    <tr>No Records</tr> 
                                @endif
                            </tbody>

                        </table>
                        {{$users->render()}}
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection 


@section('header-scripts')
	 <link href="{{$asset}}gentella/css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@endsection

@section('footer-scripts')
	<script src="{{$asset}}gentella/js/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script src="{{$asset}}gentella/js/colorpicker/docs.js"></script>
@endsection