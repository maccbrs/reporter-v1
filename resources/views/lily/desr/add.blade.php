<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                        @include('flash::message')
                        <br />

                        <form  class="form-horizontal " method="post" action="{{route('lily.desr.create')}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Activities for Today: <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea rows="5"   required="required" name="current_act" class="form-control col-md-7 col-xs-12"> </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Pending Activities: <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea rows="5"   required="required" name="pending_act" class="form-control col-md-7 col-xs-12"> </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="from">Date<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="from" required="required" name="date_assigned" class="form-control col-md-7 col-xs-12 input-date">
                                </div>
                            </div>

                            <input type="hidden" value = "1"   name="emp_assigned" >
                                     
                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style ="text-align: center;">
                                    <div class="col-md-6">
                                    <a href="{{URL::previous()}}" class="btn btn-primary btn-form">Cancel</a>
                                    </div>
                                    <div class="col-md-6">
                                    <button type="submit" class="btn btn-success btn-form">Submit</button>
                                    </div>
                                </div>
                            </div>

                    </form> 

                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
@endsection

@section('footer-scripts')
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script type="text/javascript">
        $('.input-date').datetimepicker({
                format: 'Y-m-d'

        });
    </script>
@endsection