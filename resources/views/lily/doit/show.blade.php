<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
<?php 
$status = [1 => 'pending',2 => 'done'];
?>

    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>{{$task->tasklist->title}}</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @include('alert.errorlist')
                    <form class="form-horizontal form-label-left" method="post" action="{{route('lily.task.doit.update',$task->id)}}">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >remarks
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="form-control" name="remarks" rows="3" ></textarea>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Status
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="status">
                            <option value="">select</option>
                            @foreach($status as $k => $v)
                            <option value="{{$k}}" {{($task->status == $k?'selected':'')}} >{{$v}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <button id="send" type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>

                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection
