<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">

                <div class="x_content">
                        @include('flash::message')
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Routename</th>
                                    <th class=" no-link last"><span class="nobr">#</span>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                              @if(!empty($logs))
                                    @foreach($logs as $log)
                                      <tr class="even pointer">
                                          <td class=" ">{{$log->users->name}}</td>
                                          <td class=" ">{{$log->created_at}}</td>
                                          <td>{{routename($log->routename)}}</td>
                                          <td class=" ">
                                            @if($log->type != 'browse')
                                                <a href="{{$restrictions->btnRoute('lily.logs.show',$log->id)}}" class="btn btn-sm btn-default fa fa-folder-open {{$restrictions->btnDisabler('lily.logs.show')}}"></a>
                                            @endif
                                          </td>
                                      </tr>
                                    @endforeach
                              @else
                                  <tr>No Records</tr> 
                              @endif
                            </tbody>

                        </table>
                        {!! $logs->render() !!}
                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection