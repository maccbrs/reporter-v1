<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">

                <div class="x_content">

                  <div class="col-md-2">
                    <ul class="nav nav-tabs tabs-left">
                      <?php $count = 0; ?>
                      @foreach($finalRoutes as $k => $v)
                        <li class="{{($count == 0?'active':'')}}">
                          <a href="#{{$k}}" data-toggle="tab">{{$k}}</a>
                        </li>
                        <?php $count++; ?>
                      @endforeach
                    </ul>
                  </div>

                  <div class="col-md-10">
                    <div class="tab-content">
                      <?php $count2 = 0; ?>
                      @foreach($finalRoutes as $k => $v)
                        <div class="tab-pane {{($count2 == 0?'active':'')}}" id="{{$k}}">

                            @foreach($v as $kclass => $vclass)
                            <table id="example" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                    <tr class="headings">
                                        <th>{{$kclass}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @foreach($vclass as $kmethod => $vmethod) 
                                        @foreach($vmethod as $key => $value) 
                                          <tr class="even pointer">
                                              <td class=" ">{{$value}}
                                                  <a href="{{route('lily.logs.viewlogs',$key)}}" class="btn btn-sm btn-default fa fa-folder-open pull-right"></a>
                                              </td>
                                          </tr>
                                        @endforeach
                                      @endforeach
                                </tbody>
                            </table>
                            @endforeach
                        </div>                        
                        <?php $count2++; ?>
                      @endforeach                      
                    </div>
                  </div>

                  <div class="clearfix"></div>




                  @include('flash::message')

                </div>


              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection