 <?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                      <h2>Task</h2>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned</th>
                            <th>Frequency</th>
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($tasks as $task)
                          <tr>
                            <td>{{$task->title}}</td>
                            <td>{{$task->description}}</td>
                            <td>{{$task->user->name}}</td>
                            <td>{{$task->frequency}}</td>
                            <td><a href="{{$restrictions->btnRoute('lily.scheduler.doit',$task->id)}}" class="btn btn-success fa fa-gavel {{$restrictions->btnDisabler('lily.scheduler.doit')}}"></a></td>
                          </tr>
                         @endforeach
                        </tbody>
                      </table>

                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection