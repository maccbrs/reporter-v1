<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('flash::message')
						        <div class="form-group">
						            <div class="col-md-12">
						                <a href="{{$restrictions->btnRoute('lily.access.edit',$user->id)}}" class="btn btn-success pull-right">Edit Access</a>
						            </div>
						        </div>                	
                </div>
            </div>
        </div>
    </div>
@endsection 


@section('header-scripts')
	 <link href="{{$asset}}gentella/css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@endsection

@section('footer-scripts')
	<script src="{{$asset}}gentella/js/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script src="{{$asset}}gentella/js/colorpicker/docs.js"></script>
@endsection