<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class = "pull-left" >
                        <a href="{{$restrictions->btnRoute('lily.user.access.add',$user->id)}}"  class="btn btn-success fa fa-plus {{$restrictions-> btnDisabler('lily.user.access.add')}}" > <h4> Add Web App </h4>
                        </a>  
                    </div> 
                	@include('flash::message')
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th>App name</th>
                                    <th class=" no-link last"><span class="nobr">#</span>
                                    </th>
                                </tr>
                            </thead>

                            <tbody>
                            	@if($apps)
                            	@foreach($apps as $app)
                                    <tr class="even pointer">
                                        <td class=" ">{{$app}}</td>
                                        <td class=" ">
                                            <a href="{{$restrictions->btnRoute('lily.user.access.app.index',['lilyUserId' =>$user->id,'app_name'=> $app])}}" class="btn btn-sm btn-default {{$restrictions->btnDisabler('lily.user.access.app.index')}}">Access</a>
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                	<tr>No Records</tr> 
                                @endif
                            </tbody>

                        </table>
                </div>
            </div>
        </div>

    </div>
@endsection 