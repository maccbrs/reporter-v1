<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
    <form class="form-horizontal form-label-left" method="post" action="{{route('lily.user.access.app.update',$user->id)}}">
    <input type="hidden" name="_method" value="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="app" value="{{$app}}"> 
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	  @include('alert.errorlist')          
                    @foreach($routes as $kclass => $vclass)
                        <div class="col-md-4">
                            <table id="example" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                    <tr class="headings">
                                        <th>{{$kclass}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @foreach($vclass as $kmethod => $vmethod) 
                                        @foreach($vmethod as $key => $value) 
                                          <tr class="even pointer">
                                              <td class=" ">
                                                   <div class="checkbox">
                                                       <label>
                                                       <input type="checkbox" {{(in_array($key,$userRoutes)?'checked':'')}} name="routes[]" value="{{$key}}">
                                                       </label>
                                                       {{$value}} ({{$key}})
                                                    </div>
                                              </td>
                                          </tr>
                                        @endforeach
                                      @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endforeach 
                </div>
            </div>
        </div>

        <br><br><br>

        <div class="row" >
          <div class="col-md-4" ></div>
          <div class="col-md-3" >
              <button type="submit" class="btn btn-success pull-right">Update</button>
          </div>
          <div class="col-md-4" ></div>
        </div>
  </form>
  </div>
@endsection 