<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="x_content">

                    @include('alert.errorlist')
                    <form class="form-horizontal form-label-left" method="post" action="{{route('lily.user.access.store',$user->id)}}">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">                        
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th>App name</th>
                                </tr>
                            </thead>

                            <tbody>
                                                  
                            	@foreach($selections as $selection)
                                    <tr class="even pointer">
                                        <td class=" ">
                                           <div class="checkbox">
                                               <label>
                                               <input type="checkbox" name="apps[]" {{(in_array($selection,$selected)?'checked':'')}} value="{{$selection}}">
                                               </label>
                                               {{$selection}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                          
                            </tbody>

                        </table>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <button id="send" type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>
                  </form> 

                </div>
            </div>
        </div>

    </div>
@endsection 