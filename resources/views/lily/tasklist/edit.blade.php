<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
<?php 
$frequents = ['daily','weekly','monthly'];
$shifts = ['first','second','middle'];
$days = ['mon','tue','wed','thu','fri','sat','sun'];
?>
    <div id="page-wrapper">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">

                    <div class = "pull-left" >
                      <a href="{{route('lily.tasklist.index')}}" class="btn btn-success fa fa-list pull-right"> View Task</a> 
                    </div>

                  <h2>Update New Task</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @include('alert.errorlist')
                    <form class="form-horizontal form-label-left" method="post" action="{{route('lily.tasklist.update',$task->id)}}">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Title
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="title" value="{{$task->title}}" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div> 

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Description
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="description" class="form-control col-md-7 col-xs-12" value="{{$task->description}}">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Assigned
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="assigned">
                            <option value="">Assigned</option>
                            @foreach($users as $u)
                            <option value="{{$u->id}}" {{($task->assigned == $u->id?'selected':'')}} >{{$u->name}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Frequency
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="frequency">
                            <option value="">Select</option>
                            @foreach($frequents as $freq)
                            <option value="{{$freq}}" >{{$freq}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Days
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="day">
                            <option value="">Select</option>
                            @foreach($days as $dy)
                            <option value="{{$dy}}" >{{$dy}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Shift
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="shift">
                            <option value="">Select</option>
                            @foreach($shifts as $shf)
                            <option value="{{$shf}}" >{{$shf}}</option>
                            @endforeach
                          </select>
                      </div>
                    </div>

                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style ="text-align: center;">
                            <div class="col-md-6">
                              <a href="{{$restrictions->btnRoute('lily.tasklist.index')}}"  class="btn btn-primary {{$restrictions->btnDisabler('lily.tasklist.index')}}">View All</a>
                            </div>
                            <div class="col-md-6">
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>

 

                  </form>

                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection