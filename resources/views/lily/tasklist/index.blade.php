 <?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class = "pull-left" >
                          <a href="{{$restrictions->btnRoute('lily.tasklist.create')}}" class="btn btn-success fa fa-plus-square pull-right {{$restrictions->btnDisabler('lily.tasklist.create')}}"><h4> Add Task </h4>
                          </a>
                        </div>
                    <div class="row tile_count">
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned</th>
                            <th>Frequency</th>
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>
                        @if($tasklist->toArray())
                          @foreach($tasklist as $task)
                            <tr>
                              <td>{{$task->title}}</td>
                              <td>{{$task->description}}</td>
                              <td>{{($task->user?$task->user->name:'')}}</td>
                              <td>{{$task->frequency}}</td>
                              <td><a href="{{$restrictions->btnRoute('lily.tasklist.edit',$task->id)}}" class="btn btn-success fa fa-edit {{$restrictions->btnDisabler('lily.tasklist.edit')}}"></a></td>
                            </tr>
                           @endforeach
                         @endif
                        </tbody>
                      </table>

                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection