<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <a href="{{$restrictions->btnRoute('lily.tasklist.index')}}" class="btn btn-success fa fa-list pull-right {{$restrictions->btnDisabler('lily.tasklist.index')}}"></a> 
                  <h2>Task</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <p>Title: {{$tasklist->title}}</p>
                  <p>Description: {{$tasklist->description}}</p>
                  <p>Assigned: {{$tasklist->user->name}}</p>
                  <p>Frequency: {{$tasklist->frequency}}</p>

                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection