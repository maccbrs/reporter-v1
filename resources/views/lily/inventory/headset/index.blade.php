<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                      <div class="col-md-3 col-md-offset-9">
                         <a href="{{$restrictions->btnRoute('lily.inventory.headset.create')}}" type="submit" class="btn btn-success pull-right {{$restrictions->btnDisabler('lily.inventory.headset.create')}}">Add</a>
                      </div>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Type</th>
                            <th>Serial</th>
                            <th>Date Issued</th>
                            <th>User</th>
                            <th>Condition</th>
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>
                              @foreach($headsets as $hs)
                              <tr>
                                <th scope="row">{{$hs->type}}</th>
                                <td>{{$hs->serial}}</td>
                                <td>{{$hs->date_issued}}</td>
                                <td>{{$hs->user}}</td>
                                <td>{{$hs->condition}}</td>
                                <td><a href="x" class="btn btn-success fa fa-edit"></a></td>
                              </tr>
                              @endforeach
                        </tbody>
                      </table>

                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection