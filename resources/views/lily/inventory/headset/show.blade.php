<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <a href="{{$restrictions->btnRoute('lily.inventory.headset.index')}}" class="btn btn-success fa fa-list pull-right {{$restrictions->btnDisabler('lily.inventory.headset.index')}}"></a>
                  <h2>DID/TFN </h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @include('alert.errorlist')
                    <p>Type: {{$headset->type}}</p>
                    <p>Serial: {{$headset->serial}}</p>
                    <p>Date Issued: {{$headset->date_issued}}</p>
                    <p>User: {{$headset->user}}</p>
                    <p>Condition: {{$headset->condition}}</p>

                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection