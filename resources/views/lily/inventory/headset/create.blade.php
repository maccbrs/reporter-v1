<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit DID/TFN</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @include('alert.errorlist')
                    <form class="form-horizontal form-label-left" method="post" action="{{route('lily.inventory.headset.store')}}">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Type
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="type">
                             <option  value="" > Select Type</option>
                             <option value="claritone">Claritone</option>
                             <option value="Plantronics">Plantronics</option>
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Serial
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="serial" value="" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Date
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="date_issued" class="form-control col-md-7 col-xs-12 datepicker" value="">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >User
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="user" class="form-control col-md-7 col-xs-12 " value="">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Condition
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="condition" class="form-control col-md-7 col-xs-12 " value="">
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <a href="{{$restrictions->btnRoute('lily.inventory.headset.index')}}"  class="btn btn-primary {{$restrictions->btnDisabler('lily.inventory.headset.index')}}">View All</a>
                        <button id="send" type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>

                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection