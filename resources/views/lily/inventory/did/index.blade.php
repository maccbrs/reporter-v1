<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Number</th>
                            <th>Vendor</th>
                            <th>Date Subscribe</th>
                            <th>Status</th>
                            <th>Campaign</th>
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>

                          @foreach($dids as $did)
                              <tr>
                                <th scope="row">{{$did->content}}</th>
                                <td>{{$did->vendor}}</td>
                                <td>{{$did->date_subscribe}}</td>
                                <td>{{$did->did_status}}</td>
                                <td>{{$did->campaign}}</td>
                                <td><a href="{{route('lily.inventory.did.edit',$did->id)}}" class="btn btn-success fa fa-edit"></a></td>
                              </tr>
                          @endforeach

                        </tbody>
                      </table>

                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection