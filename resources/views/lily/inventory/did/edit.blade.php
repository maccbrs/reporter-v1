<?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Edit DID/TFN</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    @include('alert.errorlist')
                    <form class="form-horizontal form-label-left" method="post" action="{{route('lily.inventory.did.update',$did->id)}}">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  


                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >DID/TFN 
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="content" value="{{$did->content}}" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Vendor
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="vendor">
                             <option  value="" > Select Vendor</option>
                             <option value="APN" {{($did->vendor == 'APN'?'selected':'')}} >APN</option>
                             <option value="BDD" {{($did->vendor == 'BDD'?'selected':'')}} >BDD</option>
                             <option value="ITD" {{($did->vendor == 'ITD'?'selected':'')}} >ITD</option>
                             <option value="DIDX" {{($did->vendor == 'DIDX'?'selected':'')}} >DIDX</option>
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Subscription Date
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="date_subscribe" class="form-control col-md-7 col-xs-12 datepicker" value="{{$did->date_subscribe}}">
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Status
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="status" >
                             <option value="" >Select Status</option>
                             <option {{($did->did_status == 'deactivated'?'selected':'')}} value="deactivated" >deactivated</option>
                             <option {{($did->did_status == 'active'?'selected':'')}} value="active" >active</option>
                             <option {{($did->did_status == 'ordered'?'selected':'')}} value="ordered" >ordered</option>
                             <option {{($did->did_status == 'pending'?'selected':'')}} value="pending" >pending</option>
                          </select>
                      </div>
                    </div>

                    <div class="item form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" >Campaign
                      </label>
                      <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="campaign" value="{{$did->campaign}}" class="form-control col-md-7 col-xs-12">
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group">
                      <div class="col-md-6 col-md-offset-3">
                        <a href="{{$restrictions->btnRoute('lily.inventory.did')}}"  class="btn btn-primary">View All</a>
                        <button id="send" type="submit" class="btn btn-success">Submit</button>
                      </div>
                    </div>

                  </form>

                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection