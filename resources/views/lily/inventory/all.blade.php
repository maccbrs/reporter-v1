<?php $asset = URL::asset('/'); ?> 

<style>
    
    .flip-container{

        display: none;

    }

    .btn-ghost-container{

      position: relative;
      text-align: right;
      bottom: 200px;
      right: 100px;

    }

    .btn-app{

        
        color: white;
        width: 140px;
    }

</style>

@extends('lily.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                      <div class = "background" >
                        <img src="{{$asset}}gentella/images/fullpageimage.jpg" rel="stylesheet" style = "width: 100%; filter: grayscale(50%);">
                      </div>
                    </div>

                    
                    <div class="btn-ghost-container">

                        <a class="btn btn-app {{$restrictions->btnDisabler('lily.inventory.did')}} btn-ghost" style ="background: rgba(116, 140, 141, 0.75); color:white;" href="{{$restrictions->btnRoute('lily.inventory.did')}}">
                            <i class="glyphicon glyphicon-earphone "></i> TFN/DID
                        </a>
                            
                        <a href="{{$restrictions->btnRoute('lily.inventory.headset.index')}}" style ="background: rgba(116, 140, 141, 0.75); color:white;" class="btn btn-app {{$restrictions->btnDisabler('lily.inventory.headset.index')}} btn-ghost">
                            <i class="fa fa-headphones"></i> Headset
                        </a>
                 
                        <a href="{{$restrictions->btnRoute('lily.inventory.headset.index')}}" style ="background: rgba(116, 140, 141, 0.75); color:white;" class="btn btn-app {{$restrictions->btnDisabler('lily.inventory.headset.index')}} btn-ghost">
                            <i class="fa fa-gavel"></i> Done Task (My)
                        </a>
                            
                        <a href="{{$restrictions->btnRoute('lily.inventory.headset.index')}}" style ="background: rgba(116, 140, 141, 0.75); color:white;"  class="btn btn-app {{$restrictions->btnDisabler('lily.inventory.headset.index')}} btn-ghost">
                            <i class="fa fa-gavel"></i> Done Task (All)
                        </a>
                           
                        <br><br>

                    </div>
                </div>                    
            </div>
        </div>
    </div>

@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection