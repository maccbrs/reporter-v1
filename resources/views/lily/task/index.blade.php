 <?php $asset = URL::asset('/'); ?> 
@extends('lily.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                   <div class = "pull-left" >
                      <a href="{{$restrictions->btnRoute('lily.task.create')}}" class="btn btn-success fa fa-plus pull-right {{$restrictions->btnDisabler('lily.task.create')}}"><h4>Add New Task</h4></a> 
                    </div> 


                    <div class="row tile_count">
                      @include('alert.errorlist')

                      @if($custom->toArray())
                      <h2>Assigned</h2>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned</th>
                            <th>Frequency</th>
                            <th>Deadline</th>
                            <th>Day</th>
                            <th>Shift</th>
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>

                        @foreach($custom as $d)
                          <tr>
                            <td>{{$d->tasklist->title}}</td>
                            <td>{{$d->tasklist->description}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->tasklist->frequency}}</td>
                            <td>{{$d->deadline}}</td>
                            <td>{{$d->tasklist->day}}</td>
                            <td>{{$d->tasklist->shift}}</td>
                            <td><a href="{{$restrictions->btnRoute('lily.task.doit.show',$d->id)}}" class="btn btn-success fa fa-edit {{$restrictions->btnDisabler('lily.task.doit.show')}}"></a></td>
                          </tr>
                         @endforeach
                        </tbody>
                      </table>

                      @endif

                      @if($daily->toArray())
                      <h2>Daily</h2>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned</th>
                            <th>Frequency</th>
                            <th>Deadline</th>
                            <th>Day</th>
                            <th>Shift</th>
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>

                        @foreach($daily as $d)
                          <tr>
                            <td>{{$d->tasklist->title}}</td>
                            <td>{{$d->tasklist->description}}</td>
                            <td>{{$d->name}}</td>
                            <td>{{$d->tasklist->frequency}}</td>
                            <td>{{$d->deadline}}</td>
                            <td>{{$d->tasklist->day}}</td>
                            <td>{{$d->tasklist->shift}}</td>
                            <td><a href="{{$restrictions->btnRoute('lily.task.doit.show',$d->id)}}" class="btn btn-success fa fa-edit {{$restrictions->btnDisabler('lily.task.doit.show')}}"></a></td>
                          </tr>
                         @endforeach
                        </tbody>
                      </table>

                      @endif

                      @if($weekly->toArray())
                      <h2>Weekly</h2>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned</th>
                            <th>Frequency</th>
                            <th>Deadline</th>
                            <th>Day</th>
                            <th>Shift</th>                            
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($weekly as $w)
                          <tr>
                            <td>{{$w->tasklist->title}}</td>
                            <td>{{$w->tasklist->description}}</td>
                            <td>{{$w->name}}</td>
                            <td>{{$w->tasklist->frequency}}</td>
                            <td>{{$w->deadline}}</td>
                            <td>{{$w->tasklist->day}}</td>
                            <td>{{$w->tasklist->shift}}</td>                            
                            <td><a href="{{$restrictions->btnRoute('lily.task.doit.show',$w->id)}}" class="btn btn-success fa fa-edit {{$restrictions->btnDisabler('lily.task.doit.show')}}"></a></td>
                          </tr>
                         @endforeach
                        </tbody>
                      </table>
                      @endif

                      @if($monthly->toArray())
                      <h2>Monthly</h2>
                      <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Assigned</th>
                            <th>Frequency</th>
                            <th>Deadline</th>
                            <th>Day</th>
                            <th>Shift</th>                            
                            <th>#</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($monthly as $m)
                          <tr>
                            <td>{{$m->tasklist->title}}</td>
                            <td>{{$m->tasklist->description}}</td>
                            <td>{{$m->name}}</td>
                            <td>{{$m->tasklist->frequency}}</td>
                            <td>{{$m->deadline}}</td>
                            <td>{{$m->tasklist->day}}</td>
                            <td>{{$m->tasklist->shift}}</td>                            
                            <td><a href="{{route('lily.task.doit.show',$m->id)}}" class="btn btn-success fa fa-edit"></a></td>
                          </tr>
                         @endforeach
                        </tbody>
                      </table>
                      @endif

                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection