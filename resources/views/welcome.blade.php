<!-- <?php $asset = URL::asset('/'); ?> 

<head>
    <meta charset="utf-8">
    <title>Templar Alpha</title>

    <!-- scripts -->

    <script src="{{$asset}}gentella/js/jquery.min.js"></script>
    <script type="text/javascript" src="jquery.onepage-scroll.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/welcome.js"></script>

    <!-- styles -->

<!--     <link href='onepage-scroll.css' rel='stylesheet' type='text/css'> -->
    <link rel="image_src" href="{{$asset}}gentella/images/onepage/notify_better_image.png" />
    <link href="{{$asset}}gentella/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" id="favicon" href="{{$asset}}gentella/images/onepage/favicon.png"> 
    <link rel="stylesheet" href="{{$asset}}gentella/css/onepage.css" />

<script>

  jQuery(window).load(function() {

    jQuery("#status").fadeOut();
    jQuery("#loading-text").fadeOut();
    jQuery("#preloader").delay(1000).fadeOut("slow");
    jQuery(".framewelcome").delay(3000).fadeIn("slow");
    jQuery(".page1bg").delay(2000).show(0);

      $(".main").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: 600,
        loop: true
      });

  });

</script>

<style>

    #preloader  {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #fefefe;
      z-index: 99;
      height: 100%;
   }

  #status  {
       width: 200px;
       height: 200px;
       padding:5%;
       position: absolute;
       left: 50%;
       top: 40%;
       background-image:url({{$asset}}gentella//images/preloader.gif);
       background-repeat: no-repeat;
       background-size: cover;
       background-position: center;
       margin: -100px 0 0 -100px;
       padding: 10%
   }

    #loading-text  {
      padding:5%;
      position: absolute;
      left: 50%;
      top: 30%;
      margin: -100px 0 0 -100px;
      padding: 10%
      left: 44%;
      top: 65%;
   }


  .btncolor1{

    background-color: #3498DB;
    color:white;
    width: 10%

  }

  .btncolor2{

    background-color: #0f9d58;
    color:white;

  }

  .btncolor3{

    background-color: #d56540;
    color:white;

  }

  .btncolor4{

    background-color: #748c8D;
    color:white;
  }

  .btncolor5{

    background-color: #2ECC71;
    color:white;
  }



  .darkbackground{

    background: rgba(0, 0, 0, 0.6) none repeat scroll 0% 0%;
    border: medium solid rgb(59, 59, 59);
    border-radius: 9px;

  }

  .navbar-default{

    background-color: transparent;
    border-color: transparent;    

  }

  /*nav-hover*/

  .navbar-header:hover{

    color:blue;

  }

  /*onepagenav*/




</style>

</head>

  <div id="preloader">
      <div id="status">&nbsp;  </div>
      <h3 id="loading-text">Loading ...</h3>
  </div>

<body>



  <div class="wrapper">
      <div class="main">


      <section  style="text-align : right;">

       <div class = "page1bg" style = ""> 
        <div class = "page1"> 

          <div class="main">
          <div class="header">
          
              <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
 
            <a class="navbar-brand" href="#" style="color:white;">Home</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a class="navbar-brand" href="{{ url('/logout') }}" style="color:white;"><i class="fa fa-btn fa-sign-out">Logout</a>
              </li>
            </ul>
          </div>
      </nav>
          <div class="page_container wrapper " style="top: 2%;" >   
            <div class="" style = "text-align:center;">
                  <div class="col-md-12 darkbackground">
                    
                      @foreach($restrictions->access() as $val)

                            <?php 
                              switch ($val) {
                                case "dahlia":
                                    $valmage = "website_logo5";
                                    $btncolor = "btncolor1";
                                    $btncard = "btncard1";
                                    $label =  "Logger";
                                    break;
                                case "rosebud":
                                    $valmage = "website_logo3";
                                    $btncolor = "btncolor2";
                                    $btncard = "btncard2";
                                    $label =  "Project Management";
                                    break;
                                case "primrose":
                                    $valmage = "website_logo1";
                                    $btncolor = "btncolor3";
                                    $btncard = "btncard3";
                                    $label =  "Reporter";
                                    break;
                                case "lily":
                                    $valmage = "website_logo6";
                                    $btncolor = "btncolor4";
                                    $btncard = "btncard4";
                                    $label =  "NOC Central";
                                    break;
                                case "prepaid":
                                    $valmage = "website_logo4";
                                    $btncolor = "btncolor5";
                                    $btncard = "btncard5";
                                    $label =  "Prepaid";
                                    break;

                                case "poppy":
                                    $valmage = "website_logo2";
                                    $btncolor = "btncolor5";
                                    $btncard = "btncard5";
                                    $label =  "Tickets";
                                    break;                                    
                                
                              } 
                              
                            ?>
                            <div class="card <?php echo $btncard ?>">
                              <a href="{{route($val.'.index')}}" style ="position: relative; top: 15px;"> <img src="{{$asset}}gentella/images/<?php echo $valmage ?>.png"class = "img-size">
                              </a> <br> 

                              <span style ="font-size: 180%; color: white;"> <b> {{$label}} </b></span> <br>

                            </div> 

                          @endforeach

                        <br><br>

                          <h1 id ="templarLNO"><b>{ <span id = "templar">Templar</span> like no other }</b></h1> 
                  
                  </div>
              </div>
            </div>
          </div>
          </div>
        </div> 
         </div>
      </section>
<!--
      <section class="page2" id ="reporter" name = "primrose">
        <div class="page_container" id= "primrose">
          <div class="row">
              <div class="col-md-12">
                <a href="{{route('primrose.index')}}"  style ="position: relative; top: 15px;">
                  <img src="{{$asset}}gentella/images/reportGIF.gif" style = "width:20%"><br>
                </a>
                <h1>Reporter</h1>
                <h2>Be updated in a click! Using this website, users can now generate a report from our database. This consists of reports especially customized for the ease of the client and users. Report made instant!</h2>
              </div>
              
          </div>
        </div> 
      </section>

      <section class="page2" id ="Ticket" >
        <div class="page_container" id ="ticket">
          <div class="row">
              <div class="col-md-12">
                <a href="192.168.200.29"  style ="position: relative; top: 15px;">
                  <img src="{{$asset}}gentella/images/ticketsGIF.gif" style = "width:20%"><br>
                  <h1>Sentry - Support Center</h1>
                <a href="192.168.200.29" style ="position: relative; top: 15px;">
                 <h2> Having a technical trouble? Worry no more. Support Center or Sentry is for you. Create a ticket to Inform NOC staff about it. Remember that ticket requests will be on prioritized based on status and time the ticket was created. </h2>
              </div>
          </div>
        </div>
      </section>

      <section class="page2" id ="projmanagement" >
        <div class="page_container" id ="rosebud">
          <div class="row">
              <div class="col-md-12">
                <a href="{{route('rosebud.index')}}"  style ="position: relative; top: 15px;">
                  <img src="{{$asset}}gentella/images/proj-managementGIF.gif" style = "width:20%">
                  <h1>Project Management</h1>
                <a href="{{route('rosebud.index')}}"  style ="position: relative; top: 15px;">
                 <h2> "Time is Gold". Be more productive using this website. Manage and See the schedules connected to your projects in a click. Timeline will be viewed by the users.  </h2>
              </div>
              
          </div>
        </div>
      </section>

      <section class="page2" id ="prepaid"  name = "prepaid">
        <div class="page_container" id ="prepaid">
          <div class="row">
            <a href="{{route('prepaid.index')}}"  style ="position: relative; top: 15px;">
              <div class="col-md-6">
                <img src="{{$asset}}gentella/images/prepaidGIF.gif" class = "img-size">
                <h1>Prepaid</h1>
              </div>
            </a>
            <div class="col-md-6">
                  
              <h2>Be amazed how the users in this website can monitor and regulate prepaid accounts in the company. Reports can be generated in a click. This is a Prepaid made easy versus the previous version.</h2>
              
            </div>
          </div>
        </div>
      </section>

      <section class="page2" id ="noccentral" name = "dahlia">
         <div class="page_container " id ="dahlia">
          <div class="row">
              <div class="col-md-6">
                <a href="{{route('dahlia.index')}}"  style ="position: relative; top: 15px;">
                  <img src="{{$asset}}gentella/images/nocGIF.gif" class = "img-size">
                  <h1>NOC Central</h1>
                </a>
              </div>
              <div class="col-md-6">
                <h2>Be centralized using this website. NOC projects will be viewed in a click. Staffs will be updated about the upcoming and current project status. How cool to have this! </h2>
              </div>
          </div>
        </div>
      </section>

      <section class="page2" id ="logger" name = "lily">
         <div class="page_container"  id ="lily">
          <div class="row">
              <div class="col-md-6">
                <h2>Have a quick and cool access! Use this to open up Magellan's websites.Using this, NOC staff will be in ease in giving access to every staff who needs an access. Explore and Learn about the company's technolgy</h2>
              </div>
              <a href="{{route('lily.index')}}"  style ="position: relative; top: 15px;">
                <div class="col-md-6">
                    <img src="{{$asset}}gentella/images/loggerGIF.gif" class = "img-size">
                    <h1>Logger</h1>
                </div>
              </a>
          </div>
        </div>
      </section>


       <section class="page2" id ="dummyboard">
        <div class="page_container">
          <div class="row">
              <div class="col-md-6">
                <h2>Help agents get the correct Data. Use this tool to provide them script for the campaign. </h2>
              </div>
              <div class="col-md-6">
                <a href="http://192.168.200.28"  style ="position: relative; top: 15px;">
                 <img src="{{$asset}}gentella/images/dummyboardGIF.gif" class = "img-size">
                  <h1>Dummy Board</h1>
                </a>
              </div>
          </div>
        </div>
      </section>

-->
  
  </div>
</body>

<script>

  $( "#test" ).click(function() {
    alert( "Handler for .click() called." );
  });

  $( ".card" ).hover(function() {

    $(this).find(".btn-card").show();

  });

  $( ".card" ).mouseleave(function() {

    $( ".btn-card" ).hide();

  });

</script>


 -->