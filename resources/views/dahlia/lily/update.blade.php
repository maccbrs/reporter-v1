 <?php $asset = URL::asset('/'); ?> 
@extends('dahlia.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-10 col-md-offset-1">
              <div class="x_panel">

                <div class="x_content">
                    <p>Name: {{$log->users->name}}</p>
                    <p>Route Name: {{$log->routename}}</p>
                    <p>Date: {{$log->created_at}}</p>
                        @include('flash::message')
                        <?php $except = ['created_at','updated_at','_method','_token']; ?>
                        <div class="row">
                          <div class="col-md-6">
                            <p>From</p>
                            <table class="table">
                              <tbody>
                                @foreach($content['from'] as $k => $v)
                                  @if(!in_array($k,$except) && !is_array($v))
                                  <tr>
                                    <th style="width:50%">{{$k}}</th>
                                    <td>{{$v}}</td>
                                  </tr>
                                  @endif
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <div class="col-md-6">
                            <p>To</p>
                            <table class="table">
                              <tbody>
                                @foreach($content['to'] as $k => $v)
                                  @if(!in_array($k,$except) && !is_array($v))
                                  <tr>
                                    <th style="width:50%">{{$k}}</th>
                                    <td>{{$v}}</td>
                                  </tr>
                                  @endif
                                @endforeach
                              </tbody>
                            </table>
                          </div>                          
                        </div>
                          <a class="btn btn-success" href="{{URL::previous()}}">back</a>
                </div>
              </div>
            </div>
          </div>

    </div>
@endsection  


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection