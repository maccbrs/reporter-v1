 <?php $asset = URL::asset('/'); ?> 
@extends('dahlia.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="x_panel">

                <div class="x_content">
                		<p>Name: {{$log->users->name}}</p>
                		<p>Route Name: {{$log->routename}}</p>
                		<p>Date: {{$log->created_at}}</p>
                    @include('flash::message')
                    <table class="table">
                      <tbody>
                        <?php $cont = json_decode($log->content); ?>
                        @foreach($cont as $k => $v)
                        <tr>
                          <th style="width:50%">{{ $k}}</th>
                          <td>{{$v}}</td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    <a class="btn btn-success" href="{{URL::previous()}}">back</a>
                </div>
              </div>
            </div>
          </div>

    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection