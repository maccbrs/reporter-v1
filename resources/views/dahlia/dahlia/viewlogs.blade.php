<?php $asset = URL::asset('/'); ?> 
@extends('dahlia.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">

                <div class="x_content">
                        <table id="example" class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Routename</th>
                                    <th class=" no-link last"><span class="nobr">#</span>
                                    </th>
                                </tr> 
                            </thead>

                            <tbody>
                              @if($logs)
                              @foreach($logs as $log)
                                  <tr class="even pointer">
                                      <td class=" ">{{$log->users->name}}</td>
                                      <td class=" ">{{$log->created_at}}</td>
                                      <td>{{$log->routename}}</td>
                                      <td class=" ">
                                        @if($log->type != 'browse')
                                            <a href="{{route('dahlia.dahlia.show',$log->id)}}" class="btn btn-sm btn-default fa fa-folder-open"></a>
                                        @endif
                                      </td>
                                  </tr>
                                @endforeach
                                @else
                                  <tr>No Records</tr> 
                                @endif
                            </tbody>

                        </table>
                        {!! $logs->render() !!}
                </div>
              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection