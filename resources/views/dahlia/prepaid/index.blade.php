<?php $asset = URL::asset('/'); ?> 
@extends('dahlia.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">

                <div class="x_content">

                  <div class="col-md-12">
                    <div class="tab-content">
                      <?php $count2 = 0; ?>
                      @foreach($finalRoutes as $k => $v)
                        <div class=" {{($count2 == 0?'active':'')}}" id="{{$k}}">

                            @foreach($v as $kclass => $vclass)
                            <table id="example" class="table table-striped responsive-utilities jambo_table">
                                <thead>
                                    <tr class="headings">
                                        <th>{{$kclass}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                      @foreach($vclass as $kmethod => $vmethod) 
                                        @foreach($vmethod as $key => $value) 
                                          <tr class="even pointer">
                                              <td class=" ">{{$value}}
                                                  <a href="{{route('dahlia.prepaid.viewlogs',$key)}}" class="btn btn-sm btn-default fa fa-folder-open pull-right"></a>
                                              </td>
                                          </tr>
                                        @endforeach
                                      @endforeach
                                </tbody>
                            </table>
                            @endforeach
                        </div>                        
                        <?php $count2++; ?>
                      @endforeach                      
                    </div>
                  </div>

                  <div class="clearfix"></div>





                </div>


              </div>
            </div>
          </div>



    </div>
@endsection 


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection