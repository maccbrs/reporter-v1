<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

						@include('flash::message')
						<table id="example" class="table table-striped responsive-utilities jambo_table">
						    <thead>
						        <tr class="headings">
						            <th>Name</th>
						            <th>Project Name</th>
						            <th>Deadline</th>
						            <th>Day/s Left</th>
						            <th>Status</th>
						            <th>Remarks</th>
						            <th>#</th>
						        </tr>
						    </thead> 

						    <tbody>
						       <?php  //pre($requests); ?>
						    	@if($requests)
							    	@foreach($requests as $request)
							    		<?php 
							    			$dl = addDayswithdate($request->due_date,$request->sla);
							    			$left = (days_left($dl) > 0?days_left($dl):0);
							    		 ?>
							            <tr class="even pointer">
							                <td class=" ">{{$request->name}}</td>
							                <td class=" ">{{$request->project_name}}</td>
							                <td>{{$dl}}</td>
							                <td>{{$left}}</td>
							                <td class=" ">{{(isset($statuses[$request->status])?$statuses[$request->status]:0)}}</td>
							                <td>{{$request->remarks}}</td> 
							                <td><a href="{{route('rosebud.request.show',$request->id)}}" class="btn btn-success btn-small"> <i class=" fa fa-folder-open"></i></a></td>
							            </tr>
							        @endforeach
						        @else
						        	<tr>No Records</tr>
						        @endif
						    </tbody>

						</table>

                </div>
            </div>
        </div>

    </div>
@endsection 