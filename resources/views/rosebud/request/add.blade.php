<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

		<div class="row">
		    <div class="col-md-12">
		        <div class="x_content">
                    <div class="form-group">
                      <div class="col-md-12">
                        <a href="{{route('rosebud.campaign.edit',$campaign->id)}}" type="submit" class="btn btn-success">Edit Info</a>
                        <a href="{{route('rosebud.campaign.client_status.edit',$campaign->id)}}" type="submit" class="btn btn-success">Edit Client Status</a>
                        <a href="{{route('rosebud.rtc.show',$campaign->id)}}" type="submit" class="btn btn-success">Edit Checklist</a>
                      </div>
                    </div>	
		        </div>
		    </div>
		</div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
					@include('flash::message')
	                <div class="" role="tabpanel" data-example-id="togglable-tabs">
	                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
	                        <li role="presentation" class="active"><a href="#checklist" id="checklist-tab" role="tab" data-toggle="tab" aria-expanded="true">Checklist &amp; Status </a>
	                        </li>	                    	
	                        <li role="presentation" ><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General Account Profile</a>
	                        </li>
	                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Contact Information</a>
	                        </li>
	                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Campaign Files / Documents</a>
	                        </li>
	                        <li role="presentation" class=""><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab"  aria-expanded="false">Operational Information</a>
	                        </li>
	                        <li role="presentation" class=""><a href="#tab_content5" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Technical Information</a>
	                        </li> 
	                        <li role="presentation" class=""><a href="#tab_content6" role="tab" id="profile-tab4" data-toggle="tab" aria-expanded="false">Recruitment Information</a>
	                        </li>  	                                               
	                    </ul>
	                    <div id="myTabContent" class="tab-content ">
	                    	<div role="tabpanel" class="tab-pane fade active in " id="checklist" aria-labelledby="checklist-tab">
			                        <div class="col-md-12 col-sm-12 col-xs-12">
			                            <table class="table">
			                                <thead>
			                                    <tr>
										            <th>Name</th>
										            <th>Due date</th>
										            <th>Status</th>
										            <th>Remarks</th>
			                                    </tr>
			                                </thead>
			                                <tbody>
			                                	@if($campaign->checklist->toArray())

			                                		@foreach($campaign->checklist as $list)
			                                		 <tr>
										                <td>{{(isset($requirements[$list['requirement_id']])?$requirements[$list['requirement_id']]:'unnamed')}}</td>
										                <td>{{($campaign->client_status->status != 'closed' && $list['type'] == 'regular'?'inactive': addDayswithdate($campaign->client_status->due_date,(isset($list->requirement->sla)?$list->requirement->sla:0)))}}</td>
										                <td>{{(isset($statuses[$list['status']])?$statuses[$list['status']]:'')}}</td>
										                <td>{{$list['remarks']}}</td>
										             </tr>
				                                    @endforeach

			                                    @endif 			                                    			                                                                                                                                                                                                                                                                                                                                                                  
			                                </tbody> 
			                            </table> 

					                    <div class="form-group">
					                      <div class="col-md-12">
					                        <a href="{{route('rosebud.rtc.create',$campaign->id)}}" type="submit" class="btn btn-success">Add</a>
					                      </div>
					                    </div>	
					                    			                            
			                        </div>
	                    	</div>
	                        <div role="tabpanel" class="tab-pane " id="tab_content1" aria-labelledby="home-tab">
			                        <div class="col-md-12 col-sm-12 col-xs-12">
			                            <table class="table">
			                                <tbody>
			                                    <tr>
			                                        <td class="mb-tbl">Project Name</td>
			                                        <td>{{$campaign->project_name}}</td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mb-tbl">Project Alias</td>
			                                        <td>{{$campaign->project_alias}}</td>
			                                    </tr>
			                                    <tr>
			                                        <td class="mb-tbl">Client Status</td>
			                                        <td>{{($campaign->client_status?$campaign->client_status->status:'')}}</td>
			                                    </tr> 
			                                    <tr>
			                                        <td class="mb-tbl">Campaign Stage</td>
			                                        <td>{{$campaign->campaign_stages}}</td>
			                                    </tr>  
			                                     <tr>
			                                        <td class="mb-tbl">Industry</td>
			                                        <td>{{$campaign->industries}}</td>
			                                    </tr>

			                                     <tr>
			                                        <td class="mb-tbl">Country Focus</td>
			                                        <td>{{$campaign->country_focus}}</td>
			                                    </tr>  


			                                     <tr>
			                                        <td class="mb-tbl">Commercial Arrangement</td>
			                                        <td>{{$campaign->commercial_arrangement}}</td>
			                                    </tr>
   
   			                                     <tr>
			                                        <td class="mb-tbl">Type of program</td>
			                                        <td>{{$campaign->type_of_program}}</td>
			                                    </tr>

			                                     <tr>
			                                        <td class="mb-tbl">Project Description</td>
			                                        <td>{{$campaign->project_description}}</td>
			                                    </tr>

			                                     <tr>
			                                        <td class="mb-tbl">Contract Period</td>
			                                        <td>{{$campaign->contract_period}}</td>
			                                    </tr>  			                                      

			                                </tbody>
			                            </table>
			                        </div>
	                        </div>
	                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
		                        <div class="col-md-12 col-sm-12 col-xs-12">
		                            <table class="table">
		                                <tbody>

		                                    <tr>
		                                        <td class="mb-tbl">Company Name</td>
		                                        <td>{{$campaign->company_name}}</td>
		                                    </tr>

		                                    <tr>
		                                        <td class="mb-tbl">Contact Name</td>
		                                        <td>{{$campaign->contact_name}}</td>
		                                    </tr>

		                                    <tr>
		                                        <td class="mb-tbl">Title</td>
		                                        <td>{{$campaign->title}}</td>
		                                    </tr> 

		                                    <tr>
		                                        <td class="mb-tbl">Contact Numbers</td>
		                                        <td>{{$campaign->contact_no}}</td>
		                                    </tr> 
		                                    <tr>
		                                        <td class="mb-tbl">Contact Email</td>
		                                        <td>{{$campaign->email_address}}</td>
		                                    </tr> 
		                                    <tr>
		                                        <td class="mb-tbl">Website Url</td>
		                                        <td>{{$campaign->web_url}}</td>
		                                    </tr>   
		                                     <tr>
		                                        <td class="mb-tbl">Company Address</td>
		                                        <td>{{$campaign->company_address}}</td>
		                                    </tr>
		                                </tbody>
		                            </table>
	                            
		                        </div>
	                        </div>

	                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
		                        <div class="col-md-12 col-sm-12 col-xs-12">
		                            <table class="table">                          	
		                                <tbody>  

		                                    <tr>
		                                        <td class="mb-tbl">Call Volume History </td>
		                                        <td>{{$campaign->call_volume_history}}</td> 
		                                    </tr>
                                 
		                                </tbody>
		                            </table>
		                        </div> 
	                        </div>

	                        <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">
		                        <div class="col-md-12 col-sm-12 col-xs-12">
		                            <table class="table">                         	
		                                <tbody>                            	
                                                                      
		                                </tbody>
		                            </table>
		                        </div>                        
	                        </div>

	                        <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">
		                        <div class="col-md-12 col-sm-12 col-xs-12">
		                            <table class="table">                         	
		                                <tbody>                            	

		                                                                                                        
		                                </tbody>
		                            </table>
		                        </div>                        
	                        </div>

	                        <div role="tabpanel" class="tab-pane fade" id="tab_content6" aria-labelledby="profile-tab">
		                        <div class="col-md-12 col-sm-12 col-xs-12">
		                            <table class="table">                         	
		                                <tbody>                            	

		                                                                                                        
		                                </tbody>
		                            </table>
		                        </div>                        
	                        </div>

	                    </div>
	                </div>
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<style type="text/css">
.mb-tbl{
	background: rgba(38, 185, 154, 0.16);
}
</style>
@endsection