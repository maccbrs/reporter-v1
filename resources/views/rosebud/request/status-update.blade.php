<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('alert.errorlist')

                    <br />
                    
                    <form novalidate class="form-horizontal form-label-left" method="post" action="{{route('rosebud.request.status.update',$rtc->id)}}">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                        <input type="hidden" name="rtc_id" value="{{$rtc->id}}">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_single form-control" tabindex="-1" name="status">
                                    <option {{($rtc->status == '1'?'selected':'')}} value="1">Pending</option>
                                    <option {{($rtc->status == '2'?'selected':'')}} value="2">Recieved</option>
                                    <option {{($rtc->status == '3'?'selected':'')}} value="3">Delivered</option>
                                </select> 
                            </div>
                        </div>
                       
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Update</button>
                            </div>
                        </div>
                    </form> 

                </div>
            </div>
        </div>

    </div> 
@endsection 