<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
					@include('flash::message')

					<div class="">
					   <div class="page-title">
					      <div class="title_left">
					         <h3>{{$rtc->campaign->project_alias}}</h3>
					         <h4>{{$rtc->requirement->name}}</h4>
					      </div>
					   </div>
					   <div class="clearfix"></div>

					   <div class="row">
					      <div class="col-md-12 col-sm-12 col-xs-12">


					         <div class="x_panel">
					            <div class="x_content">

					               <div class="col-md-4 col-sm-4 col-xs-12 profile_left i-pad-right">
					                  <ul class="list-unstyled user_data">
					                     <li><i class="fa fa-flag-o user-profile-icon"></i> {{$statuses[$rtc->status]}}
					                     </li>
					                     <li>
					                        <i class="fa fa-clock-o user-profile-icon"></i>
					                        <span class="label label-success">Due On:</span>
					                         {{addDayswithdate($rtc->client_status->due_date,$rtc->requirement->sla)}}
					                     </li>
					                     <li>
					                        <i class="fa fa-user user-profile-icon"></i>
					                        <span class="label label-default">Post by:</span>
					                        {{$rtc->user->name}}
					                     </li>
					                     <li>
					                        <i class="fa fa-pencil user-profile-icon"></i>
					                        <span class="label label-default">Remarks:</span>
					                        {{$rtc->remark}}
					                     </li>
					                     <li>
							                  <table class="table table-bordered">
							                    <tbody>
							                      @if(!empty($rtc->answer))
								                      @foreach($rtc->answer as $ans)
									                      <tr>
									                        <td>{{$ans->content}}</td>
									                        <td style="width:135px;">{{$ans->created_at}}</td>
									                      </tr>
								                      @endforeach
							                      @endif
							                    </tbody>
							                  </table>
					                     </li>					                     					                     
					                  </ul>
					                  <a href="{{route('rosebud.answer.create',$rtc->id)}}" class="btn btn-success"><i class="fa fa-plus-square m-right-xs"></i>Post Answer</a>
					                  <a href="{{route('rosebud.message.create',$rtc->id)}}" class="btn btn-success"><i class="fa fa-plus-square m-right-xs"></i>Post Message</a>
					                  <a href="{{route('rosebud.request.status',$rtc->id)}}" class="btn btn-success"><i class="fa fa-plus-square m-right-xs"></i>Update Status</a>
					                  <br />
					               </div>

									<div class="col-md-8 col-sm-8 col-xs-12 mail_list_column">
									   	  @if(!empty($message))
									   	      <?php $count = 1; ?>
									       	  @foreach($message as $msg)
									       	  	  @if($count <= 2)

													  <div class="mail_list">
													    <div class="left">
													      <i class="fa fa-circle"></i>
													    </div>
													    <div class="right">
													      <h3>{{$msg['user']}} <small>{{$msg['year']}} {{$msg['month']}} {{$msg['day']}} {{$msg['time']}}</small></h3>
													      <p>{{$msg['content']}}</p>
													    </div>
													  </div>
									              <?php $count++; ?>
									              @endif
									          @endforeach
									      @endif 
<!-- 										  <div class="mail_list btn btn-success">
										      <a href="" style="text-align:center;display:block; color:#fff; ">view all messages</a>
										  </div>	 -->								       
									</div>	

					            </div>
					         </div>
					      </div>
					   </div>
					</div>
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<style type="text/css">
.mb-tbl{
	background: rgba(38, 185, 154, 0.16);
}
.messages{
	list-style: none;
}
.i-pad-right i{
	padding-right:7px;
}
.x-sm{
	font-size: 10px;
}
</style>
@endsection