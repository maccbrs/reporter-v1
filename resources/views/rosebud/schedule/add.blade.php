<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                        @include('flash::message')
						<br />

                        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="{{route('rosebud.schedule.store')}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">  


                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="from">from<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="from" required="required" name="from" class="form-control col-md-7 col-xs-12 input-date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="to">to <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" id="to" name="to" required="required" class="form-control col-md-7 col-xs-12 input-date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="color">color<span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="color" id="color" name="color" required="required" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>                                        
                            <div class="form-group">
                                <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">title</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="message" required="required" class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Type</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control" tabindex="-1" name="type">
                                                    <option value="regular">regular</option>
                                                    <option value="fst-training">Fst Training</option>
                                                    <option value="product-training">Product Training</option>
                                                </select>
                                            </div>
                                        </div>                                          
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" style ="text-align: center;">
                                                <div class="col-md-6">
                                                <a href="{{URL::previous()}}" class="btn btn-primary btn-form">Cancel</a>
                                                </div>
                                                <div class="col-md-6">
                                                <button type="submit" class="btn btn-success btn-form">Submit</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form> 

                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
@endsection

@section('footer-scripts')
    <script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script>  
    <script type="text/javascript">
    	$('.input-date').datetimepicker({
                format: 'Y-m-d h:i:s'

        });
    </script>
@endsection