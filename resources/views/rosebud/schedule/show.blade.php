<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
					@include('flash::message')

					<div class="">
					   <div class="row">
					      <div class="col-md-12 col-sm-12 col-xs-12">
					         <div class="x_panel">
					            <div class="x_content">

					               <div class="col-md-4 col-sm-4 col-xs-12 profile_left">
					                  <ul class="list-unstyled user_data">
					                     <li><i class="fa fa-flash user-profile-icon"></i> 
					                     	<span class="label label-default">Title:</span>
					                     	{{$sched->title}}
					                     </li>
					                     <li><i class="fa fa-flash user-profile-icon"></i> 
					                     	<span class="label label-default">Description:</span>
					                     	{{$sched->description}}
					                     </li>					                     
					                     <li>
					                        <i class="fa fa-clock-o user-profile-icon"></i>
					                        <span class="label label-default">From - To:</span>
					                        <?php 
					                        	$from = date_format(date_create($sched->from),"Y-m-d");
					                        	$to = date_format(date_create($sched->to),"Y-m-d");
					                        ?>
					                        {{$from}} {{($from != $to? ' - '.$to:'')}}
					                     
					                     </li>
					                     <li>
					                        <i class="fa fa-flash user-profile-icon"></i>
					                        <span class="label label-default">Color:</span>
					              			<span style="background-color: {{$sched->color}}; color: #fff;">{{$sched->color}}</span>
					                     </li>
					                     <li>
					                        <i class="fa fa-flash user-profile-icon"></i>
					                        <span class="label label-default">Type</span>
					                        {{$sched->type}}
					      
					                     </li>				                     					                     
					                  </ul>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <a href="{{route('rosebud.schedule.edit',$sched->id)}}" type="submit" class="btn btn-success">edit</a>
                                            </div>
                                        </div>
					                  <br />
					               </div>


					            </div>
					         </div>
					      </div>
					   </div>
					</div>
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<style type="text/css">
.mb-tbl{
	background: rgba(38, 185, 154, 0.16);
}
.messages{
	list-style: none;
}
.i-pad-right i{
	padding-right:7px;
}
.x-sm{
	font-size: 10px;
}
</style>
@endsection