<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

						@include('flash::message')
						<table id="example" class="table table-striped responsive-utilities jambo_table">
						    <thead>
						        <tr class="headings">
						            <th>Name</th>
						            <th>Due date</th>
						            <th>Days left</th>
						            <th>Status</th>
						            <th>Remarks</th>
						            <th class=" no-link last"><span class="nobr">#</span>
						            </th>
						        </tr>
						    </thead>

						    <tbody> 

						    	@if($checklist) 
						    	@foreach($checklist as $list) 
						    		@if($list['status'] != 3)
						    		<?php 
						    			$deadline = addDayswithdate($list['client_status']['due_date'],$list['requirement']['sla']);
						    			$daysleft = (days_left($deadline) > 0?days_left($deadline):'0');
						    		 ?>
						            <tr class="even pointer {{($daysleft?'':'mb-alert')}}">
						                <td>{{(isset($requirements[$list['requirement_id']])?$requirements[$list['requirement_id']]:'unnamed')}}</td>
						                <td>{{$deadline}}</td>
						                <td>{{$daysleft}}</td>
						                <td>{{(isset($statuses[$list['status']])?$statuses[$list['status']]:'')}}</td>
						                <td>{{$list['remarks']}}</td>
						                <td>
						                	<a href="{{route('rosebud.rtc.edit',$campaign->id)}}" class="btn btn-sm btn-default">Edit</a>
						                </td>
						            </tr>
						            <?php $deadline = $daysleft = 0; ?>
						            @endif
						        @endforeach
						        @else
						        	<tr>No Records</tr> 
						        @endif
						    </tbody>

						</table>
	                    <div class="form-group">
	                      <div class="col-md-12">
	                        <a href="{{route('rosebud.campaign.show',$campaign->id)}}" type="submit" class="btn btn-primary">Cancel</a>
	                        <a href="{{route('rosebud.rtc.create',$campaign->id)}}" type="submit" class="btn btn-success">Add</a>
	                      </div>
	                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('footer-scripts')
<style type="text/css">
.table-striped>tbody>tr.mb-alert{
	background-color: rgba(255, 0, 0, .3);	
	color:#000000;
}

</style>
@endsection