<?php 
//show all available task as select 
//should have special instructions, comment, remarks
$asset = URL::asset('/'); ?> 

@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Daily active users <small>Sessions</small></h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="#"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
                                                <th>Name</th>
                                                <th>Department</th>
                                            </tr>
                                        </thead>

                                        <tbody>

											@if($requirements->toArray()['data'])
												@foreach($requirements as $r)
	                                            <tr class="even pointer">
	                                                <td class="a-center ">
	                                                    <input type="checkbox" class="tableflat">
	                                                </td>
	                                                <td class=" ">{{$r->name}}</td>
	                                                <td class=" ">{{$r->department_id}}</td>
	                                            </tr>
	                                            @endforeach
	                                        @endif

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        </div>

    </div>
@endsection 





@section('header-scripts')

@endsection

@section('footer-scripts')
        <!-- Datatables -->
        <script src="{{$asset}}gentella/js/datatables/js/jquery.dataTables.js"></script>
        <script src="{{$asset}}gentella/js/datatables/tools/js/dataTables.tableTools.js"></script>


        <script>

            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });
            });

        </script>
@endsection