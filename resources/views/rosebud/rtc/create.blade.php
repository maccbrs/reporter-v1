<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                                    @include('alert.errorlist')
									<br />

                                    <form novalidate class="form-horizontal form-label-left" method="post" action="{{route('rosebud.rtc.store',$id)}}">
                                        <input type="hidden" name="_method" value="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  


                                       <!--  requirements -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name:</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="form-control" required name="requirement_id">
                                                  <option value="">Choose..</option>
                                                  @if($requirements->toArray())
                                                    @foreach($requirements as $r)
                                                        <option {{(old('requirement_id') == $r->id?'selected':'')}} value="{{$r->id}}">{{$r->name}}</option>
                                                    @endforeach
                                                  @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Due Date</label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input  class="form-control datepicker col-md-7 col-xs-12" type="text" name="to" placeholder="add date" value="{{old('to')}}">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Instructions/Notes<span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea id="message" required="required" class="form-control" name="instructions">{{old('instructions')}}</textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                <a href="{{URL::previous()}}" class="btn btn-primary">Cancel</a>
                                                <button type="submit" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>

                                    </form> 

                </div>
            </div>
        </div>

    </div>
@endsection 


@section('footer-scripts')
    <script>
      $(function() {
        var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      });
    </script>
@endsection