<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                	@include('flash::message')
			        <div class="form-group">
			            <div class="col-md-12">
			                <a href="{{route('rosebud.requirement.add')}}" type="submit" class="btn btn-success pull-right">Add</a>
			            </div>
			        </div>  
            	
                	@if($requirements->toArray()['data'])
						<table id="example" class="table table-striped responsive-utilities jambo_table">
						    <thead>
						        <tr class="headings">
						            <th>Name</th>
						            <th>Assigned to</th>
						            <th>Sla</th>
						            <th>#</th>
						        </tr>
						    </thead>
						    <tbody>
						    	@foreach($requirements as $r)
						            <tr class="even pointer">
						                <td class=" ">{{$r->name}}</td>
						                <td class=" ">{{(isset($departments[$r->department_id])?$departments[$r->department_id]:'')}}</td>
						                <td class=" ">{{$r->sla}}</td>
						                <td>
						                	<a href="{{route('rosebud.requirement.edit',$r->id)}}" class="btn btn-success fa fa-edit"></a>
						                	<a href="#" class="btn btn-success fa fa-trash"></a>
						                </td>
						            </tr>
						        @endforeach
						    </tbody>

						</table>
                	@else
                		<div>No Record</div>
                	@endif


                </div> 
            </div>
        </div>

    </div>
@endsection 