<?php $asset = URL::asset('/'); ?> 
@extends('master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                		@include('flash::message')
						<form id="demo-form2" data-parsley-validate method="post" class="form-horizontal form-label-left" action="{{route('rosebud.cluster.user.store',$cluster->id)}}">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">  

						        <div class="form-group">
						            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">User</label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_single form-control" tabindex="-1" name="users_id">
                                        	@foreach($users as $user)
	                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
						            </div>
						        </div>  

						        <div class="ln_solid"></div>
						        <div class="form-group">
						            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						                <a href="{{URL::previous()}}" class="btn btn-primary">Cancel</a>
						                <button type="submit" class="btn btn-success">Submit</button>
						            </div>
						        </div>

						    </form>

                </div>
            </div>
        </div>

    </div>
@endsection 