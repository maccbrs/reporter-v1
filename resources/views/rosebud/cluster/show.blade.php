<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('flash::message')
			        <div class="form-group">
			            <div class="col-md-6">
			                <p>{{$cluster->name}}</p>
			            </div>
			        </div>
                  <table class="table table-bordered">
                    <tbody>
                    	
                    	@if($cluster->utc)
                    		@foreach($cluster->utc as $utc)
                    			@if($utc->user)
			                      <tr>
			                        <td>{{$utc->user->name}}</td>
			                      </tr>
		                      	@endif
		                    @endforeach
		                @endif
                    </tbody>
                  </table>	
		          <div class="form-group">
		            <div class="col-md-6">
		                <a href="{{route('rosebud.cluster.user.create',$cluster->id)}}" class="btn btn-success">Add User</a>
		            </div>
		          </div>                  		        
                </div>
            </div>
        </div>

    </div>
@endsection 