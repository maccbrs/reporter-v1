<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('flash::message')

	                  <table class="table table-bordered">
	                    <tbody>
	                    	@if($clusters->toArray())
	                    		@foreach($clusters as $clus)
				                      <tr>
				                        <td>{{$clus->name}}</td>
				                        <td>
				                        	<a href="{{route('rosebud.cluster.show',$clus->id)}}" class="btn btn-success fa fa-folder-open"></a>
				                        	<a href="{{route('rosebud.cluster.calendar',$clus->id)}}" class="btn btn-success fa fa-calendar"></a>

				                        </td>
				                      </tr>
			                    @endforeach
			                @endif
	                    </tbody>
	                  </table>	                		        
                </div>
            </div>
        </div>
    </div>
@endsection 