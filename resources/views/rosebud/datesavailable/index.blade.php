 <?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('flash::message')
                    @if($dates)
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <tbody>

					   	<form method="post" action="{{route('rosebud.datesavailable.update')}}">
						    <input type="hidden" name="_method" value="POST">
						    <input type="hidden" name="_token" value="{{ csrf_token() }}">	
                        	@for($i = 1; $i < 32; $i++)
                            <tr class="even pointer">
                            	@for($n = 1; $n < 13; $n++)
                           			@if(isset($dates[$year][$n][$i]))
		                                <td class="a-center ">
		                                    <input type="checkbox"  name="status[]" value="{{$dates[$year][$n][$i]['id']}}" {{($dates[$year][$n][$i]['status'] == 1?'checked':'')}}> {{$dates[$year][$n][$i]['date']}}
		                                </td>
		                            @else
		                                <td class="a-center ">

		                                </td>
		                            @endif		                            
                                @endfor
                            </tr>
                            @endfor
                            <button type="submit" class="btn btn-success">save</button>
                        </form>
                        </tbody>
                    </table>
                    @else
                    <p>No Dates available</p>
                    @endif
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<link href="{{$asset}}gentella/css/icheck/flat/green.css" rel="stylesheet">
<link href="{{$asset}}gentella/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
@endsection

@section('footer-scripts')
	<script src="{{$asset}}gentella/js/icheck/icheck.min.js"></script>
	<script type="text/javascript">
            $(document).ready(function () {
                $('input.tableflat').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    indeterminateClass: 'indeterminate',
                    radioClass: 'iradio_flat-green'
                });
            });

        
	</script>
@endsection