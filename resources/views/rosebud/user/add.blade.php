<?php $asset = URL::asset('/'); ?> 
@extends('master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                		@include('flash::message')
						<form id="demo-form2" data-parsley-validate method="post" class="form-horizontal form-label-left" action="{{route('rosebud.user.create')}}">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">  

						        <div class="form-group">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
						            </label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
						                <input type="text" id="name" required="required" name="name" class="form-control col-md-7 col-xs-12">
						            </div>
						        </div>

						        <div class="form-group">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
						            </label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
						                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
						            </div>
						        </div>

						        <div class="form-group">
						            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Department</label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="select2_single form-control" tabindex="-1" name="department_id">
                                            <option value="1">NOC</option>
                                            <option value="2">Training</option>
                                            <option value="3">OPS</option>
                                            <option value="4">QA</option>
                                            <option value="5">BD</option>
                                        </select>
						            </div>
						        </div>  

						        <div class="form-group">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password<span class="required">*</span>
						            </label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
						                <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
						            </div>
						        </div>

						        <div class="form-group">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="color">color<span class="required">*</span>
						            </label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
						                <input type="color" id="color" name="color" required="required" class="form-control col-md-7 col-xs-12">
						            </div>
						        </div>

						        <div class="ln_solid"></div>
						        <div class="form-group">
						            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						                <a href="{{URL::previous()}}" class="btn btn-primary">Cancel</a>
						                <button type="submit" class="btn btn-success">Submit</button>
						            </div>
						        </div>

						    </form>

                </div>
            </div>
        </div>

    </div>
@endsection 