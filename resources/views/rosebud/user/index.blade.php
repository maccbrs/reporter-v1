<?php $asset = URL::asset('/'); ?> 
@extends('master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('flash::message')
                                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                                        <thead>
                                            <tr class="headings">
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Department</th>
                                                <th>Color</th>
                                                <th class=" no-link last"><span class="nobr">#</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        	@if($users)
                                        	@foreach($users as $user)
	                                            <tr class="even pointer">
	                                                <td class=" ">{{$user->name}}</td>
	                                                <td class=" ">{{$user->email}}</td>
	                                                <td class=" ">{{$user->department_id}}</td>
	                                                <td style="background-color:{{user_color($user->options)}};">{{user_color($user->options)}}</td>
	                                                <td class=" "><a href="{{route('rosebud.user.edit',$user->id)}}" class="btn btn-sm btn-default">Edit</a></td>
	                                            </tr>
                                            @endforeach
                                            @else
                                            	<tr>No Records</tr>
                                            @endif
                                        </tbody>

                                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection 