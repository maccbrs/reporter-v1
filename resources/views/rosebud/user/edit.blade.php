<?php $asset = URL::asset('/'); ?> 
@extends('master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                		@include('flash::message')
						<form id="demo-form2" data-parsley-validate method="post" class="form-horizontal form-label-left" action="{{route('rosebud.user.update',$user->id)}}">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">  

						        <div class="form-group">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span>
						            </label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
						                <input type="text" id="name" required="required" name="name" class="form-control col-md-7 col-xs-12" value="{{old('name',$user->name)}}">
						            </div>
						        </div>

						        <div class="form-group">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
						            </label>
						            <div class="col-md-6 col-sm-6 col-xs-12">
						                <input type="email" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" value="{{old('email',$user->email)}}">
						            </div>
						        </div>


						        <div class="form-group colorpicker-element">
						            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="color">color<span class="required">*</span>
						            </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    	<input type="text" class="demo1 form-control" name="color" value="{{old('color',user_color($user->options))}}" />
                                    </div>
						        </div>

						        <div class="ln_solid"></div>
						        <div class="form-group">
						            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
						                <a href="{{URL::previous()}}" class="btn btn-primary">Cancel</a>
						                <button type="submit" class="btn btn-success">Submit</button>
						            </div>
						        </div>

						    </form>

                </div>
            </div>
        </div>

    </div>
@endsection 


@section('header-scripts')
	 <link href="{{$asset}}gentella/css/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
@endsection

@section('footer-scripts')
	<script src="{{$asset}}gentella/js/colorpicker/bootstrap-colorpicker.min.js"></script>
	<script src="{{$asset}}gentella/js/colorpicker/docs.js"></script>
@endsection