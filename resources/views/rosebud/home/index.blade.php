<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">
      <div class="row">
        <div class="col-md-12">

            <div class="x_content">
              <div class="row tile_count">
                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                  <div class="left"></div>
                  <div class="right">
                    <span class="count_top">For Launching projects</span>
                    <div class="count green">3</div>
                  </div>
                </div>

                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                  <div class="left"></div>
                  <div class="right">
                    <span class="count_top">Soft closed</span>
                    <div class="count green">2</div>
                  </div>
                </div>

                <div class="animated flipInY col-md-2 col-sm-4 col-xs-4 tile_stats_count">
                  <div class="left"></div>
                  <div class="right">
                    <span class="count_top">Pending Request</span>
                    <div class="count green">12</div>
                  </div>
                </div>
              </div>                    
          </div>
        </div>
      </div>
    </div>
@endsection 

@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">

    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
 
    .flip-container {
        perspective: 1000px;
    }
        /* flip the pane when hovered */
        .flip-container:hover .flipper, .flip-container.hover .flipper {
            transform: rotateY(180deg);
        }

    .flip-container, .front, .back {
        width: 100%;
        height: 170px;
    }

    /* flip speed goes here */
    .flipper {
        transition: 0.6s;
        transform-style: preserve-3d;

        position: relative;
    }

    /* hide back of pane during swap */
    .front, .back {
        backface-visibility: hidden;

        position: absolute;
        top: 0;
        left: 0;
    }

    /* front pane, placed above back */
    .front {
        z-index: 2;
        /* for firefox 31 */
        transform: rotateY(0deg);
    }

    /* back, initially hidden pane */
    .back {
        transform: rotateY(180deg);
    }

    </style>

@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
<script>

    $(document).ready(function() {
        $('.child_menu').css("display","none");
        $('.nav.side-menu > li').removeClass("active");

        $( ".back" ).append( $( "<h1 id ='templarLNO'><b>{ <span id = 'templar'>Templar</span> like no other }</b></h1>" ) );
    });

</script>

@endsection