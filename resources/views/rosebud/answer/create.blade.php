<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                	@include('alert.errorlist')

                    <br />
                    
                    <form novalidate class="form-horizontal form-label-left" method="post" action="{{route('rosebud.answer.store')}}">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                        <input type="hidden" name="rtc_id" value="{{$rtc->id}}">

                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Content</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  class="form-control col-md-7 col-xs-12" type="text" name="content" placeholder="" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tag
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_single form-control" tabindex="-1" name="tag">
                                    <option value="tfn/did">DID/TFN</option>
                                    <option value="scripts">Scripts</option>
                                    <option  value="dummyboard">Dummyboard</option>
                                </select> 
                            </div>
                        </div>
                       
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="" class="btn btn-primary">Cancel</a>
                                <button type="submit" class="btn btn-success">Post</button>
                            </div>
                        </div>
                    </form> 

                </div>
            </div>
        </div>

    </div>
@endsection 