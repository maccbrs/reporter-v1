<br>

<div>
	<table class="table">                          
        <tbody>                             
            @if($categories['Technical Information'])
                @foreach($categories['Technical Information'] as $cat)
                <tr>
                    <td class="mb-tbl">p{{$cat['name']}}</td>
                    <td>{{$cat['content']}}</td> 
                </tr>
                @endforeach
            @endif  
                                                                                
        </tbody>
    </table>
</div>

<style>
	table {
	    border-collapse: collapse;
	    width: 100%;
	}

	th, td {
	    text-align: left;
	    padding: 8px;
	}

	tr:nth-child(even){background-color: #f2f2f2}

	th {
	    background-color: #1569C7;
	    color: white;
	}

</style>

