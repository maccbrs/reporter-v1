<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                    @include('alert.errorlist')
					<br />

                    <?php $statusId = ($campaign->client_status?$campaign->client_status->id:0); ?>
                    <form novalidate class="form-horizontal form-label-left" method="post" action="{{route('rosebud.campaign.client_status.update',$statusId)}}">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                        <input type="hidden" name="campaign_id" value="{{$campaign->id}}">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Status
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_single form-control" tabindex="-1" name="status">
                                    <?php $status = ($campaign->client_status?$campaign->client_status->status:''); ?>
                                    <option {{($status == 'potential'?'selected':'')}} value="potential">Potential</option>
                                    <option {{($status == 'soft closed'?'selected':'')}}  value="soft closed">Soft Closed</option>
                                    <option {{($status == 'closed'?'selected':'')}}  value="closed">Closed</option>
                                </select> 
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Official Closed Date</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input  class="form-control datepicker col-md-7 col-xs-12" type="text" name="due_date" placeholder="enter official date if necessary" >
                            </div>
                        </div>
                        
                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="" class="btn btn-primary">Cancel</a>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </form> 

                </div>
            </div>
        </div>

    </div>
@endsection 


@section('footer-scripts')
    <script>
      $(function() {
        var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      });
    </script>
@endsection