<br>

<div>
	<table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Due date</th>
                <th>Status</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            @if($campaign->checklist->toArray())

                @foreach($campaign->checklist as $list)
                 <tr>
                    <td>{{(isset($requirements[$list['requirement_id']])?$requirements[$list['requirement_id']]:'unnamed')}}</td>
                    <td>{{($campaign->client_status->status != 'closed' && $list['type'] == 'regular'?'inactive': addDayswithdate($campaign->client_status->due_date,(isset($list->requirement->sla)?$list->requirement->sla:0)))}}</td>
                    <td>{{(isset($statuses[$list['status']])?$statuses[$list['status']]:'')}}</td>
                    <td>{{$list['remarks']}}</td>
                 </tr>
                @endforeach

            @endif                                                                                                                                                                                                                                                                                                                                                                                                                            
        </tbody> 
    </table> 
</div>

<style>
	table {
	    border-collapse: collapse;
	    width: 100%;
	}

	th, td {
	    text-align: left;
	    padding: 8px;
	}

	tr:nth-child(even){background-color: #f2f2f2}

	th {
	    background-color: #1569C7;
	    color: white;
	}

</style>

