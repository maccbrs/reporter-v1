<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">


        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
					@include('flash::message')
	                <div class="" role="tabpanel" data-example-id="togglable-tabs">

	                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">                    	
	                        <li role="presentation" class="active" ><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">General Account Profile</a>
	                        </li>
	                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab"  aria-expanded="false">Contact Information</a>
	                        </li>
	                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Campaign Files / Documents</a>
	                        </li>	                                               
	                    </ul>

                      <form class="form-horizontal form-label-left" method="post" action="{{route('rosebud.campaign.update',$campaign->id)}}">
                      <input type="hidden" name="_method" value="POST">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">  

	                    <div id="myTabContent" class="tab-content ">

	                        <div role="tabpanel" class="tab-pane fade active in " id="tab_content1" aria-labelledby="home-tab">

                              <div class="col-md-6 col-xs-12">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>General Account Profile</h2>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <br />
                                    
                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Project Name</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="project_name" value="{{$campaign->project_name}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Project Alias</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="project_alias" value="{{$campaign->project_alias}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Launch Date</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control datepicker" name="launch_date" value="{{$campaign->launch_date}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaign Stage</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="campaign_stages" value="{{$campaign->campaign_stages}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Industry</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="industries" value="{{$campaign->industries}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Country Focus</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="country_focus" value="{{$campaign->country_focus}}">
                                        </div>
                                      </div>                                                                                

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Commercial Arrangement</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="commercial_arrangement" value="{{$campaign->commercial_arrangement}}">
                                        </div>
                                      </div>  

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Type of program</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="type_of_program" value="{{$campaign->type_of_program}}">
                                        </div>
                                      </div>  

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Project Description</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="project_description" value="{{$campaign->project_description}}">
                                        </div>
                                      </div>  

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contract Period</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="contract_period" value="{{$campaign->contract_period}}">
                                        </div>
                                      </div>  
                                                                                                                                          
                                      <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                          <button type="submit" class="btn btn-primary">Cancel</button>
                                          <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                      </div>

                                  </div>
                                </div>
                              </div>

	                        </div>


	                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                              <div class="col-md-6 col-xs-12">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Contact Information</h2>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <br />


                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="company_name" value="{{$campaign->company_name}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Name</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="contact_name" value="{{$campaign->contact_name}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="title" value="{{$campaign->title}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Numbers</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="contact_no" value="{{$campaign->contact_no}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Contact Email</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="email_address" value="{{$campaign->email_address}}">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Website Url</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="web_url" value="{{$campaign->web_url}}">
                                        </div>
                                      </div>                                                                                

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Address</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="company_address" value="{{$campaign->company_address}}">
                                        </div>
                                      </div>  

                                                                                                                                          
                                      <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                          <button type="submit" class="btn btn-primary">Cancel</button>
                                          <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                      </div>

                                  </div>
                                </div>
                              </div>

	                        </div>

	                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">

                              <div class="col-md-6 col-xs-12">
                                <div class="x_panel">
                                  <div class="x_title">
                                    <h2>Contact Information</h2>
                                    <div class="clearfix"></div>
                                  </div>
                                  <div class="x_content">
                                    <br />

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Call Volume History</label>
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                          <input type="text" class="form-control" name="call_volume_history" value="{{$campaign->call_volume_history}}">
                                        </div>
                                      </div>
                                                                                                                                          
                                      <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                          <button type="submit" class="btn btn-primary">Cancel</button>
                                          <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                      </div>

                                  </div>
                                </div>
                              </div>

	                        </div>

	                    </div>
                      </form>

	                </div>
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<style type="text/css">
.mb-tbl{
	background: rgba(38, 185, 154, 0.16);
}
</style>
@endsection