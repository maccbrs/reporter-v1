<br>

<div>
	<table class="table">
        <tbody>

            <tr>
                <td class="mb-tbl"><b>Company Name</b></td>
                <td>{{$campaign->company_name}}</td>
            </tr>

            <tr>
                <td class="mb-tbl"><b>Contact Name</b></td>
                <td>{{$campaign->contact_name}}</td>
            </tr>

            <tr>
                <td class="mb-tbl"><b>Title</b></td>
                <td>{{$campaign->title}}</td>
            </tr> 

            <tr>
                <td class="mb-tbl"><b>Contact Numbers</b></td>
                <td>{{$campaign->contact_no}}</td>
            </tr> 
            <tr>
                <td class="mb-tbl"><b>Contact Email</b></td>
                <td>{{$campaign->email_address}}</td>
            </tr> 
            <tr>
                <td class="mb-tbl"><b>Website Url</b></td>
                <td>{{$campaign->web_url}}</td>
            </tr>   
             <tr>
                <td class="mb-tbl"><b>Company Address</b></td>
                <td>{{$campaign->company_address}}</td>
            </tr>
        </tbody>
    </table>
</div>

<style>
	table {
	    border-collapse: collapse;
	    width: 100%;
	}

	th, td {
	    text-align: left;
	    padding: 8px;
	}

	tr:nth-child(even){background-color: #f2f2f2}

	th {
	    background-color: #1569C7;
	    color: white;
	}

</style>

