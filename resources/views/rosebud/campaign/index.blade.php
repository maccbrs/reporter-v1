<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

						@include('flash::message')
						<table id="example" class="table table-striped responsive-utilities jambo_table">
						    <thead>
						        <tr class="headings">
						            <th>Project Name</th>
						            <th>Launch Date</th>
						            <th>Contact person</th>
						            <th>Client Status</th>
						            <th class=" no-link last"><span class="nobr">#</span>
						            </th>
						        </tr>
						    </thead> 

						    <tbody>
						       <?php  //pre($campaigns); ?>
						    	@if($campaigns)
						    	@foreach($campaigns as $campaign)
						            <tr class="even pointer">
						                <td class=" ">{{$campaign->project_name}}</td>
						                <td class=" ">{{$campaign->launch_date}}</td>
						                <td class=" ">{{$campaign->contact_name}}</td>
						                <td class=" ">{{($campaign->client_status?$campaign->client_status->status:'')}}</td>
						                <td class=" ">
						                	<a href="{{route('rosebud.campaign.client_status.edit',$campaign->id)}}" class="btn btn-sm btn-default">Edit Client Status</a>
						                	<a href="{{route('rosebud.campaign.edit',$campaign->id)}}" class="btn btn-sm btn-default">Edit</a>
						                	<a href="{{route('rosebud.campaign.show',$campaign->id)}}" class="btn btn-sm btn-default">view</a>
						                	<a href="{{route('rosebud.rtc.show',$campaign->id)}}" class="btn btn-sm btn-default">checklist</a>
						                </td>
						            </tr>
						        @endforeach
						        @else
						        	<tr>No Records</tr>
						        @endif
						    </tbody>

						</table>

                </div>
            </div>
        </div>

    </div>
@endsection 