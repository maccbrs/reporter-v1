<br>

<div>
	<table class="table">
        <tbody> 
            <tr>
                <td class="mb-tbl"><b>Project Name</td>
                <td>{{$campaign->project_name}}</td>
            </tr>
            <tr>
                <td class="mb-tbl"><b>Project Alias</td>
                <td>{{$campaign->project_alias}}</td>
            </tr>
            <tr>
                <td class="mb-tbl"><b>Client Status</td>
                <td>{{($campaign->client_status?$campaign->client_status->status:'')}}</td>
            </tr> 
            <tr>
                <td class="mb-tbl"><b>Campaign Stage</td>
                <td>{{$campaign->campaign_stages}}</td>
            </tr>  
             <tr>
                <td class="mb-tbl"><b>Industry</td>
                <td>{{$campaign->industries}}</td>
            </tr>

             <tr>
                <td class="mb-tbl"><b>Country Focus</td>
                <td>{{$campaign->country_focus}}</td>
            </tr>  

             <tr>
                <td class="mb-tbl"><b>Commercial Arrangement</td>
                <td>{{$campaign->commercial_arrangement}}</td>
            </tr>

            <tr>
                <td class="mb-tbl"><b>Type of program</td>
                <td>{{$campaign->type_of_program}}</td>
            </tr>

             <tr>
                <td class="mb-tbl"><b>Project Description</td>
                <td>{{$campaign->project_description}}</td>
            </tr>

             <tr>
                <td class="mb-tbl"><b>Contract Period</td>
                <td>{{$campaign->contract_period}}</td>
            </tr>  			                                      

        </tbody>
    </table>
</div>

<style>
	table {
	    border-collapse: collapse;
	    width: 100%;
	}

	th, td {
	    text-align: left;
	    padding: 8px;
	}

	tr:nth-child(even){background-color: #f2f2f2}

	th {
	    background-color: #1569C7;
	    color: white;
	}

</style>

