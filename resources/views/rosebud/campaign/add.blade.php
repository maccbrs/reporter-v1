<?php $asset = URL::asset('/'); ?> 
@extends('rosebud.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">

                                    @include('flash::message')
									<br />

                                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="{{route('rosebud.campaign.create')}}">
                                    <input type="hidden" name="_method" value="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  


                                          <!-- Smart Wizard -->
                                    <p>This is a basic form wizard example that inherits the colors from the selected scheme.</p>

                                    
                                    <div id="wizard" class="form_wizard wizard_horizontal">
                                        <ul class="wizard_steps">
                                            <li>
                                                <a href="#step-1">
                                                    <span class="step_no">1</span>
                                                    <span class="step_descr">
                                            General Account Profile
                                        </span>
                                                </a> 
                                            </li>
                                            <li>
                                                <a href="#step-2">
                                                    <span class="step_no">2</span>
                                                    <span class="step_descr">
                                            Contact Information<br />
                                        </span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#step-3">
                                                    <span class="step_no">3</span>
                                                    <span class="step_descr">Campaign Files / Documents<br /></span>
                                                </a>
                                            </li>


                                            <li>
                                                <a href="#step-4">
                                                    <span class="step_no">4</span>
                                                    <span class="step_descr">
                                                        Operational Information
                                                    </span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#step-5">
                                                    <span class="step_no">5</span>
                                                    <span class="step_descr">
                                                    Technical Information<br />
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-6">
                                                    <span class="step_no">6</span>
                                                    <span class="step_descr">
                                                    Recruitment Information<br />
                                                    </span>
                                                </a>
                                            </li>

                                        </ul>
                                        <div id="step-1">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Project Name
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="project_name" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3  col-sm-3 col-xs-12" for="last-name">Launch Date
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="launch_date" required="required" class="form-control col-md-7 col-xs-12 datepicker">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3  col-sm-3 col-xs-12" for="last-name">Fst Training Date
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="fst_training_date" required="required" class="form-control col-md-7 col-xs-12 datepicker">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Training Date 
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="product_training_date" required="required" class="form-control col-md-7 col-xs-12 datepicker">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Project Alias 
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="project_alias" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div> 
                                                                                                                                                
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaign Stages
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="select2_single form-control" name="campaign_stages">
                                                            <option value="proposal sent">Proposal Sent</option>
                                                            <option value="positive feedback">Positive Feedback</option>
                                                            <option value="request form sent">Request Form Sent</option>
                                                            <option value="draft contract sent">Draft Contract Sent</option>
                                                            <option value="payment made">payment made</option>
                                                            <option value="pre implementations">pre implementations</option>
                                                            <option value="implementations">Implementations</option>
                                                            <option value="launch completed">Launch Completed</option>
                                                        </select> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Client Status
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="select2_single form-control" tabindex="-1" name="client_status">
                                                            <option value="potential">Potential</option>
                                                            <option value="softclose">Soft Close</option>
                                                            <option value="closed">Closed</option>
                                                        </select> 
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Industry
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="industries"  required="required" class="form-control col-md-7 col-xs-12 ">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Country Focus
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="country_focus" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Commercial Arrangement
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="select2_single form-control" tabindex="-1" name="commercial_arrangement">
                                                            <option value="omni shared">Omni Shared</option>
                                                            <option value="omni standalone">Omni Standalone</option>
                                                            <option value="dedicated">Dedicated</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Type of Program
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <select class="select2_single form-control" tabindex="-1" name="type_of_program">
                                                            <option value="inbound">Inbound</option>
                                                            <option value="outbound">Outbound</option>
                                                            <option value="backoffice">Backoffice</option>
                                                        </select>
                                                    </div>
                                                </div>   

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Project Description
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="project_description" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Contract Period
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="contract_period" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  
                                  
                                        </div>
                                        <div id="step-2">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Company Name
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="company_name" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>   

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Contact Name
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="contact_name" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Title
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="title" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Contact Numbers
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="contact_no" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email Address
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="email_address" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Website Url
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="web_url" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Company Address
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="company_address" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>                                               
                                                                                                                                                                                                
                                        </div>
                                        <div id="step-3">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Train and Trainer Materials
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="train_and_trainer_materials" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="train_and_trainer_materials" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scripts and Rebuttals
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="scripts_and_rebuttals" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="scripts_and_rebuttals" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Knowledge Base Info
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="knowledge_base_info" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="knowledge_base_info" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Estimated AHT
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="estimated_aht" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Call Volume History
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="call_volume_history" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Sample Call Recordings
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="sample_call_recordings" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="sample_call_recordings" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  


                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">IVR Recordings
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="ivr_recording" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="ivr_recording" value="1" />
                                                        </p>
                                                    </div>
                                                </div> 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
                                        </div>
                                        <div id="step-4">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Service Coverage
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="service_coverage" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Estimated Call Volume
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="estimated_call_volume" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Estimated AHT
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="average_aht" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Call Flow
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="call_flow" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Supervisor Assigned
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="assigned_supervisor" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="assigned_supervisor" value="1" />
                                                        </p>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Seat Plan Details
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="seat_plan_details" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="seat_plan_details" value="1" />
                                                        </p>
                                                    </div>
                                                </div> 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                        </div>

                                        <div id="step-5">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">TFN/DID
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="tfn_did" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="tfn_did" value="1" />
                                                        </p>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Client Ftp Access
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="client_ftp_access" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="client_ftp_access" value="1" />
                                                        </p>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">IVR Setup
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="ivr_setup" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="ivr_setup" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">VM Setup
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="vm_setup" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="vm_setup" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Magellan FTP Access
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="magellan_ftp_access" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="magellan_ftp_access" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Calling List Details
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="calling_list_details" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="calling_list_details" value="1" />
                                                        </p>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dispostion / CRC
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="dispo_crc" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="dispo_crc" value="1" />
                                                        </p>
                                                    </div>
                                                </div>    

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">URL Webform
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="url_webform" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="url_webform" value="1" />
                                                        </p>
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Website Access
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p>
                                                          No:
                                                          <input type="radio" class="flat" name="web_access" value="0" checked="" required /> Yes:
                                                          <input type="radio" class="flat" name="web_access" value="1" />
                                                        </p>
                                                    </div>
                                                </div>                         
                                        </div>
                                        <div id="step-6">

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Agent Headcount
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="agent_headcount" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Agent Level
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="agent_level" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Screening Process
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="screening_process" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>  

                                                <div class="form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Job Description
                                                    </label>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <input type="text" name="job_description" required="required" class="form-control col-md-7 col-xs-12">
                                                    </div>
                                                </div>                          
                                        </div>

                                    </div>
                                    <!-- End SmartWizard Content -->
                        </form>
                </div>
            </div>
        </div>

    </div>
@endsection 

@section('header-scripts')
<link rel="stylesheet" type="text/css" href="{{URL::asset('js/datetime/jquery.datetimepicker.css')}}">
<link href="{{$asset}}gentella/css/icheck/flat/green.css" rel="stylesheet">
@endsection


@section('footer-scripts')
<!-- daterangepicker -->
<script type='text/javascript' src="{{URL::asset('js/datetime/jquery.datetimepicker.js')}}"></script> 
  <!-- switchery -->
   <link rel="stylesheet" href="" src="{{$asset}}gentella/css/switchery/switchery.min.css" /> 
  <!-- icheck -->
  <script src="{{$asset}}gentella/js/icheck/icheck.min.js"></script>
 
<script type="text/javascript" src="{{$asset}}gentella/js/wizard/jquery.smartWizard.js"></script>
<script type="text/javascript">
        $(document).ready(function () {
            // Smart Wizard     
            $('#wizard').smartWizard({onFinish:onFinishCallback});

            function onFinishCallback() {
               // $('#wizard').smartWizard('showMessage', 'Finish Clicked');
               $('#demo-form2').submit();
                //alert('Finish Clicked');
            }

        });
</script>

    <script>
      $(function() {
        var date = $('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
      });
    </script>

@endsection
