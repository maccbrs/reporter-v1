
    <table style=" width: 100%; border-collapse: collapse;">
      <tr >
        @foreach($keys as $key)
        <th style="white-space: pre-line;  background: #2B547E; color: white; font-weight: bold; padding: 6px; border: 1px solid #ccc; text-align: center; ">{{ucfirst(strtolower(str_replace('_', ' ', $key)))}}</th>
        @endforeach
      </tr>
      @foreach($value as $val)
        <tr>
            @foreach($keys as $key)
              <td style=" border: 1px solid #ddd; text-align: center; padding: 6px; border: 1px solid #ccc; text-align: center;">{{$val[$key]}}</td>
            @endforeach
        </tr> 
      @endforeach                    
      </table>
      <br><br>


