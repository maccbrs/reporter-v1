<?php $asset = URL::asset('/'); ?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <!-- The character set should be utf-8 -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width"/>
  <!-- Link to the email's CSS, which will be inlined into the email -->

  <link href="{{$asset}}assets/css/zurb/css/foundation-emails.css" rel="stylesheet">


</head>

<body>
  <!-- Wrapper for the body of the email -->
  <table class="body" data-made-with-foundation style="max-width:1500px;">
    <tr>
      	<td class="float-center" align="left" valign="top">
          	<table >

	         	@if(isset($board['lob']))
					<b>EOD report</b>
				@endif

				@if(isset($dispo))

					<table class="body" data-made-with-foundation>
						<td class="float-center" align="left" valign="top">
							{!!$dispo!!}
						</td>
					</table>
					
					<br>
					<b>Summary of Customer Information.</b>
					<br>

				@endif

				@if(isset($content)) 

					<table class="body" data-made-with-foundation>
						<td class="float-center" align="left" valign="top">
							{!!$content!!}
						</td>
					</table>
		 
				@endif

                	
       		</table>
      	</td>
    </tr>
  </table>
</body>
</html>
