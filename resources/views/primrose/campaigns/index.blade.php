<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Campaign Settings"; ?> 
@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">

                            <div class="x_content">
                              <br>
                              <div class="x_title" style = "text-align:center;">

                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Id</th>
                                          <th>Name</th>
                                          <th style="width:65px;">Onsubmit</th>
                                          <th style="width:65px;">EOD</th>
                                          <th style="width:65px;">Timezone</th>
                                          <th>#</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($campaigns as $camp)
                                        <tr>
                                          <td>{{$camp->campaign_id}}</td>
                                          <td>{{$camp->campaign}}</td>
                                          <td>
                                            {{(in_array('onsubmit',pr_options($camp->options))?'yes':'no')}}
                                          </td>
                                          <td>
                                            {{(in_array('daily',pr_options($camp->options))?'yes':'no')}}
                                          </td>
                                          <td>
                                            {{pr_timezone($camp->options)}}
                                          </td>
                                          <td style="width: 235px;">
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Reports</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <a href="{{route('primrose.campaigns-eod.index',$camp->campaign_id)}}">EOD Reports</a>
                                                </li>
                                                <li>
                                                  <a href="#">On Submit</a>
                                                </li>
                                              </ul>
                                            </div>

                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Settings</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <a href="{{route('primrose.campaigns-settings.timezone',$camp->campaign_id)}}">Timezone</a>
                                                </li>
                                              </ul>
                                            </div> 

                                          </td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                    {!! $campaigns->render() !!}
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .mb-color p{
      color: #1F88A7;
    }

    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

@endsection