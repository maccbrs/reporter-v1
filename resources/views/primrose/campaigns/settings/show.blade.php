<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Timezone Settings"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('header-script')
<link href="{{$asset}}gentella/css/select/select2.min.css" rel="stylesheet">
@endsection

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-6 col-xs-12 col-md-offset-3">
              <div class="x_panel">
                <div class="x_title">
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                  <h2>Timezone</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <br />
                  <form class="form-horizontal form-label-left" method="post" action="{{route('primrose.campaigns-settings.timezone-post',$campaign_id)}}">
                  <input type="hidden" name="_method" value="POST">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
             
                    <div class="form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Timezone</label>
                      <div class="col-md-9 col-sm-9 col-xs-12">
                        <select class="select2_single form-control" tabindex="-1" name="timezone">
                          @foreach($timezones as $tz)
                          <option {{($timezone == $tz?'selected':'')}} value="{{$tz}}">{{$tz}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group" >
                      <div class="col-md-9 col-sm-9 col-xs-12 pull-right" style ="text-align:center; width:50%;">
                        <button type="submit" class="btn btn-success">Update</button>
                      </div>
                    </div>

                  </form>

                </div>
              </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .mb-color p{
      color: #1F88A7;
    }

    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
<script src="{{$asset}}gentella/js/select/select2.full.js"></script>

@endsection