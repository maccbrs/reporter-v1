<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Settings"; ?> 
@extends('primrose.master')

@section('title', 'dashboard')


@section('content')

  <div id="page-wrapper" class = "full-page-bg">
    <div class="row">
      <div class="col-md-12">
        <div class="x_content">
          <div class="row tile_count">
            <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
              <div class = "background" >
                <img src="{{$asset}}gentella/images/fullpageimage.jpg" rel="stylesheet" style = "width: 100%; filter: grayscale(50%);">
                  
              </div>
            </div>


          </div>
            <div class = "btn-ghost">
              <a class="btn btn-large btn-blue" href="{{route('primrose.settings.campaign.select-server')}}">
                <i class="fa fa-flag"></i> Campaign
              </a>

              <a class="btn btn-large btn-blue" href="{{route('primrose.settings.productivity.select-server')}}">
                    <i class="fa fa-flag"></i> Productivity
              </a>
            </div>
        </div>
      </div>
    </div>
  </div>

   <!--  <div class="container">
        <div class="responsive-scale ">
            <a class="btn btn-large btn-blue" href="{{route('primrose.settings.campaign.select-server')}}">
                <i class="fa fa-flag"></i> Campaign
            </a>
            <a class="btn btn-large btn-blue" href="{{route('primrose.settings.productivity.select-server')}}">
                <i class="fa fa-flag"></i> Productivity
            </a>
        </div>
    </div> -->



   
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">

    .responsive-scale{
        width:100%;
        padding-bottom: 66.666666666667%; /* 960px/1440px */
        background-image:url({{$asset}}img/vintage-bg.jpg);
        background-size:cover;
        background-position:center; /* IE fix */
    }

    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }

    .fst{
        background-color: #ee8823;
    }

    .ptd{
        background-color: #589796;
    }

    #calendar{
        margin-top: 11px;
    }

    .btn-ghost{

      position: relative;
      text-align: right;
      bottom: 200px;

    }

    @import url(http://fonts.googleapis.com/css?family=Raleway);

    *, *:before, *:after {
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      margin: 0;
      padding: 0;
    }

    .wrap {
      width: 100%;
      max-width: 900px;
      margin: 4em auto;
      font-family: Raleway, Arial, sans-serif;
      padding: 1em 2em;
    }

    hr {
        display: block;
        height: 1px;
        border: 0;
        border-top: 2px solid #eee;
        margin: 2em 0;
        padding: 0;
    }

    h1, h2 {
      margin-bottom: 1em;
      border-bottom: 2px solid #eee;
      line-height: 1.5;
    }

    h1 > small{
      color: #666;
    }

    h1 > small > a,
    p > a{
      color: #3CB371;
      text-decoration: none;
    }
    h1 > small > a:hover,

    p > a:hover{
      text-decoration: underline;
    }

    /* Buttons styles */
    input::-moz-focus-inner,
    button::-moz-focus-inner {
        border: 0;
        padding: 0;
    }

    input[type="submit"].btn,
    button.btn {
        cursor: pointer;
    }

    a.btn,.btn {
      margin-right: 1em; /* remove this while use*/
      margin-bottom: 1em; /* remove this while use*/
      display: inline-block;
      outline: none;
      *zoom: 1;
      text-align: center;
      text-decoration: none;
      font-family: inherit;
      font-weight: 300;
      letter-spacing: 1px;
      vertical-align: middle;
      border: 1px solid;
      transition: all 0.2s ease;
      box-sizing: border-box;
      text-shadow: 0 1px 0 rgba(0,0,0,0.01);
    }
    /* Radius */
    .btn-radius {
      border-radius: 3px;
    }
    /* Sizes */

    .btn-large {
        font-size: 25px;
        padding: 0.5625em 1.5em;
    }

    /* Colors */

    .btn-blue {
      background: rgba(213, 101, 64, 0.7) none repeat scroll 0% 0%;
      border-color: none;
      color: white;
    }
    
    .btn-blue:hover {
       border-color: rgb(213, 101, 64);
      background: #d56540;
             
    }

    .flip-container{

      display:none;

    }


    </style>

  @endsection

  @section('footer-scripts')
  <script src="{{$asset}}gentella/js/moment/moment.min.js"></script>


  @endsection