<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Settings"; ?> 
@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
  <div class="margin-top"></div>
    <div class="col-md-6 col-md-offset-3">
      <div class="x_panel">
        <div class="x_title">
          <h2>Select Server</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <br />
            @include('alert.errorlist')
            <form class="form-horizontal form-label-left " method="get" action="{{route('primrose.settings.campaign.add')}}">
            <input type="hidden" name="_method" value="get">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">  

            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Select Server</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                <select class="form-control" name="server">
                  <option value="">Choose option</option>
                  <option value="Server 1" >Server 1</option>
                  <option value="Server 2" >Server 2</option>
                </select>
              </div>
            </div>
                             
            <div class="ln_solid"></div>
            <div class="form-group" style ="text-align:right;">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="submit" class="btn btn-success">Submit</button>
              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
@endsection  

@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .margin-top{
        height: 50px;
    }
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>


@endsection