<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Campaign Settings"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">
      <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" /> 
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">

                            <div class="x_content">
                            <td>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-7"></div>
                                  <div class="col-md-5">
                                    <label for="sel1">Select Type:</label>
                                    <select class="form-control" id="type_variable">
                                      <option value="omnitrix">Omnitrix</option>
                                      <option value="nomnitrix">Non Omnitrix</option>
                                      <option value="blockage">Blockage</option>
                                      <option value="productivity">Productivity</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                                                
                              <br>
                              
                              <div class="x_title" style = "text-align:center;">
                                <div class = "section_hide">
                                    <table class="table table-bordered">
                                      <thead >
                                        <tr>
                                          <th style ="width:10%" >Name</th>
                                          <th style ="width:50%">Accounts</th>
                                          <th style ="width:20%">Type</th>
                                          <th style ="width:20%">Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($campaigns as $cmp)
                                          <tr >
                                            <td >{{$cmp->name}}</td>
                                            <td class = "scrolltd">{{$cmp->dids}}</td>
                                            <td >{{$cmp->type}}</td>
                                            <td >
                                              
                                                <a href="{{$restrictions->btnRoute('primrose.settings.campaign_edit',$cmp->id)}}">
                                                  <button class="btn btn-success" type="button">Update </button> 
                                                </a>
                                              
                                                <a href="{{$restrictions->btnRoute('primrose.settings.campaign_delete',$cmp->id)}}">
                                                  <button class="btn btn-success" type="button">Delete</button> 
                                                </a>
                                                
                                            </td>
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                   
                              </div>
                                <div class = "section_show"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>


@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .mb-color p{
      color: #1F88A7;
    }

        input[type=checkbox]{
        display: inline;
    }

    .scrolltd{
        
        display: inline-block;
        width: 500px;
        overflow: auto;
    }

    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script>



    $( "#type_variable" ).change(function() {
       $('.section_hide').hide();
      type = $( "#type_variable" ).val();

        $.ajaxSetup({          
      headers: {  
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
      }); 

       // $.ajax({
          var data = "key="+type;
          $.ajax({

              type: "POST",
              url: "/primrose/settings/campaign_variable",
              data: data,
              success:function(data){
                 $('.section_show').html("");
                $('.section_show').append(data);
              },


            })



        });


</script>

@endsection