
                                    <table class="table table-bordered">
                                      <thead style ="width:100%">
                                        <tr>
                                          <th style ="width:10%" >Name</th>
                                          <th style ="width:50%">Accounts</th>
                                          <th style ="width:20%">Type</th>
                                          <th style ="width:20%">Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($campaigns as $cmp)
                                          <tr>
                                            <td>{{$cmp->name}}</td>
                                            <td>{{substr($cmp->dids,0,50)}}..</td>
                                            <td>{{$cmp->type}}</td>
                                            <td>
                                            
                                              <a href="{{$restrictions->btnRoute('primrose.settings.campaign_edit',$cmp->id)}}">
                                                <button class="btn btn-success" type="button">Update </button> 
                                              </a>
                                              
                                              <a href="{{$restrictions->btnRoute('primrose.settings.campaign_delete',$cmp->id)}}">
                                                <button class="btn btn-success" type="button">Delete</button> 
                                              </a>
                                                
                                            </td>
                                          </tr>

                                        @endforeach
                                      </tbody>
                                    </table>
                                   
