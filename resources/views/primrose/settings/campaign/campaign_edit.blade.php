<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Campaign Settings"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">
      <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" /> 
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">

                            <div class="x_content">
                            <td>
                              @foreach($campaigns as $cmp)
                                <form class="form-horizontal form-label-left" method="post" action="{{route('primrose.settings.campaign_update',$cmp->id)}}">
                                    <input type="hidden" name="_method" value="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                        <h3>Edit Campaign Variable</h3>
                                        <div class="clear-fix"></div>
                                      </div>

                                      <div class="form-group row">
                                       <br><br>
                                        <div class="col-sm-4">
                                          <label for="formGroupExampleInput">Campaign Name</label>
                                        </div>
                                        <div class="col-sm-6">
                                          <input value="{{strtoupper($cmp->name)}}" name="name" class="form-control" id="formGroupExampleInput" placeholder="Example input">
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                     
                                        <div class="col-sm-4">
                                          <label for="formGroupExampleInput">Type</label>
                                        </div>
                                        <div class="col-sm-6">
                                              <select class="form-control" name="type" value="{{$cmp->type}}" text="{{$cmp->type}}" >
                                                <option value="omnitrix">Omnitrix</option>
                                                <option value="nomnitrix">Non Omnitrix</option>
                                                <option value="blockage">Blockage</option>
                                                <option value="productivity">Productivity</option>
                                              </select>
                                        </div>
                                      </div> 

                                      <div class="form-group row">
                                        
                                        <div class="col-sm-12">
                                        
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                        <div class="col-sm-12"></div> 
                                        <div>
                                            <div><b>INBOUND</b></div>
                                            @foreach($vicidialin as $v)
                                              @if($v != '')

                                              <?php $dids = json_decode($cmp->dids); ?>

                                                @if(in_array($v,$dids))

                                                    <div class="col-md-3">
                                                        <div class="radio">
                                                            <label style ="color:black">
                                                                <input checked type="checkbox" name="vici[]" value="{{$v}}" >{{$v}}
                                                            </label>
                                                        </div>                                                   
                                                    </div>

                                                  @else
                                                   
                                                    <div class="col-md-3">
                                                        <div class="radio">
                                                            <label style ="color:#73879C">
                                                                <input type="checkbox" name="vici[]" value="{{$v}}" >{{$v}}
                                                            </label>
                                                        </div>                                                   
                                                    </div>
                                                    
                                                @endif
                                               @endif
                                            @endforeach   
                                        </div>
                                    </div>  

                                    <br>

                                    <div class="form-group row">
                                        <div class="col-sm-12"></div> 
                                        <div>
                                            <div><b>OUTBOUND</b></div>
                                        @foreach($vicidialout as $v2)
                                          @if($v2 != '')
                                              <?php $dids = json_decode($cmp->dids); ?>

                                                @if(in_array($v2,$dids))

                                                    <div class="col-md-3">
                                                        <div class="radio">
                                                            <label style ="color:black">
                                                                <input checked type="checkbox" name="vici[]" value="{{$v2}}" >{{$v2}}
                                                            </label>
                                                        </div>                                                   
                                                    </div>

                                                  @else
                                                   
                                                    <div class="col-md-3">
                                                        <div class="radio">
                                                            <label style ="color:#73879C">
                                                                <input type="checkbox" name="vici[]" value="{{$v2}}" >{{$v2}}
                                                            </label>
                                                        </div>                                                   
                                                    </div>
                                                    
                                              @endif
                                            @endif
                                          @endforeach   
                                        </div>
                                    </div>  

                                      <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" style="width:100px;">update</button>
                                      </div>
                                  </form>             
                                @endforeach
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>


@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .mb-color p{
      color: #1F88A7;
    }

        input[type=checkbox]{
        display: inline;
    }

    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>



@endsection