<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Settings"; ?> 
@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" style="height:105%;">
                <div style="margin-top: 20px;"></div>
                    <div class="col-md-10 col-md-offset-1">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Create a productivity from did groupings</h2>
                          <div class="clearfix"></div>
                        </div>                                            
                        <div class="x_content">

                            <br />
                            @include('alert.errorlist')
                            <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.settings.campaign.store')}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        
                            <input type="hidden" name="type" value="productivity">


                        <div class="form-group">
                          <label class="control-label col-md-1">Name</label>
                          
                          <div class="col-md-4">
                            <input type="text" class="form-control" name="name">
                          </div>

                         <label class="control-label col-md-1">Bound</label>
                          <div class="col-md-2">
                                <select class="form-control" name="bound">
                                  <option value="">Choose option</option>
                                  <option value="in">Inbound</option>
                                  <option value="out">Outbound</option>
                                </select>
                          </div>

                          <label class="control-label col-md-1">Timezone</label>
                            <div class="col-md-3"> 
                                <select class="select2_single form-control" tabindex="-1" name="timezone">
                                    @foreach($timezones as $tz) 
                                    <option value="{{$tz}}">{{$tz}}</option>
                                    @endforeach
                                </select>
                            </div>                                                  
                        </div>  
    
                       <div class="ln_solid"></div>
                                                
                        <div class="form-group">
                            <div class="ln_solid"></div> 
                            <div>
                                <div>INBOUND</div>
                                @foreach($vicidialin as $v)
                                    @if($v != '')
                                        <div class="col-md-3">
                                            <div class="radio">
                                                <label style ="color:#73879C">
                                                    <input type="checkbox" name="vici[]" value="{{$v}}" >...{{substr($v,-14)}}
                                                </label>
                                            </div>                                                   
                                        </div>
                                    @endif
                                @endforeach   
                            </div>
                        </div>  

                        <br>

                        <div class="form-group">
                            <div class="ln_solid"></div> 
                            <div>
                                <div>OUTBOUND</div>
                            @foreach($vicidialout as $v)
                                @if($v != '')
                                    <div class="col-md-3">
                                        <div class="radio">
                                            <label style ="color:#73879C">
                                                <input type="checkbox" name="vici[]" value="{{$v}}">...{{substr($v,-14)}}
                                            </label>
                                        </div>                                                   
                                    </div>
                                @endif
                            @endforeach    
                            </div>
                        </div>  

                    <div class="form-group">
                        <div class="col-md-4"></div>
                          <div class="col-md-4">
                            <div class="ln_solid"></div>
                            <button type="submit" class="btn btn-success">Submit</button> 
                          </div>
                        <div class="col-md-4"></div>
                    </div>

                    </form> 

                </div>
              </div>
            </div>


                            </div>
                        </div>
                    </div> 
                </div>                                        
@endsection   


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    input[type=checkbox]{
        display: inline;
    }
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>


@endsection