<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Productivity Report"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12">
                            
                          <div class = "pull-right">
                            <div class="col-md-12">
                              <form class="form-horizontal form-label-left" method="post" action="{{route('primrose.productivity.export')}}" novalidate>
                                {{ csrf_field() }}
                                <input type="hidden" name="dispo" value="{{(isset($param['dispo'])?json_encode($param['dispo']):'')}}">
                                <input type="hidden" name="campaigns" value="{{$param['campaigns']}}">
                                <input type="hidden" name="from" value="{{$param['from']}}">
                                <input type="hidden" name="to" value="{{$param['to']}}">
                                <button type="submit" class="btn btn-primary" style ="background-color:#40b0d5;">Export</button>
                              </form>
                            </div>  
                          </div>
                          <div class="pull-right">
                            <div class="col-md-12">
                              {!!view('primrose.productivity.productivity-modal',['param' => $param,'dispo'=>$dispo])!!}
                            </div>
                          </div>

                          <br><br> 

                            @include('alert.errorlist') 
                            
                            <div class="x_content"> 
                              <br>
                              <div class="x_title" style = "text-align:center;">
                                <main>
                                  <input id="tab1" type="radio" name="tabs" checked>
                                  <label for="tab1">Call Details</label>
                                    
                                  <input id="tab2" type="radio" name="tabs">
                                  <label for="tab2">Daily Connectime - Min</label>
                                    
                                  <input id="tab3" type="radio" name="tabs">
                                  <label for="tab3">Daily Connectime - Sec</label>
                                    
                                  <input id="tab4" type="radio" name="tabs">
                                  <label for="tab4">Daily Crc</label>

                                  <input id="tab5" type="radio" name="tabs">
                                  <label for="tab5">Daily Crc %</label>

                                  <section id="content1">
                                   <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>AgentName</th>
                                          <th>CallDate</th>
                                          <th>PhoneNum</th>
                                          <th>Campaign</th>
                                          <th>CRC</th>
                                          <th>ConnectTime</th>
                                          <th>TalkTime</th>
                                          <th>WrapTime</th> 
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @if($return->toArray())

                                          @foreach($return as $data)
                                          <tr>
                                            <td>{{$data->user}}</td>
                                            <td>{{$data->call_date}}</td>
                                            <td>{{$data->phone_number}}</td>
                                            <td>{{$data->campaign_id}}</td>
                                            <td>{{$data->status}}</td>
                                            <td>{{$gn->roundsix((isset($data->agent->talk_sec)?$data->agent->talk_sec:0)+(isset($data->agent->dispo_sec)?$data->agent->dispo_sec:0))}}</td>
                                            <td>{{(isset($data->agent->talk_sec)?$data->agent->talk_sec:0)}}</td>
                                            <td>{{(isset($data->agent->dispo_sec)?$data->agent->dispo_sec:0)}}</td>
                                          </tr>
                                          @endforeach

                                        @endif
                                      
                                      </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content2">
                                     <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th>Day</th>
                                            <th>Date</th>
                                            <th>ConnectTime</th>
                                            <th>TalkTime</th>
                                            <th>WrapTime</th>
                                          </tr>
                                        </thead>
                                        <tbody> 
                                          @if($summary)
                                            @foreach($summary as $k => $sum)
                                            <tr>
                                              <td>{{$sum['day']}}</td>
                                              <td>{{$k}}</td>
                                              <td>{{$sum['totalm']}}</td>
                                              <td>{{$sum['talkm']}}</td>
                                              <td>{{$sum['wrapm']}}</td>
                                            </tr>
                                            @endforeach
                                          @endif                                        
                                        </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content3">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Day</th>
                                          <th>Date</th>
                                          <th>ConnectTime</th>
                                          <th>TalkTime</th>
                                          <th>WrapTime</th>
                                        </tr>
                                      </thead>
                                      <tbody> 
                                        @if($summary)
                                          @foreach($summary as $k => $sum)
                                          <tr>
                                            <td>{{$sum['day']}}</td>
                                            <td>{{$k}}</td>
                                            <td>{{$sum['total']}}</td>
                                            <td>{{$sum['talk']}}</td>
                                            <td>{{$sum['wrap']}}</td>
                                          </tr>
                                          @endforeach
                                        @endif                                        
                                      </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content4">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                            <th>CRC</th>
                                          @foreach($dates as $k => $d)
                                            <th>{{$k}}</th>
                                          @endforeach
                                        </tr>
                                        <tr>
                                            <th></th>
                                          @foreach($dates as $k => $d)
                                            <th>{{$d}}</th>
                                          @endforeach
                                        </tr>                                        
                                      </thead>
                                      <tbody>

                                        @if($crc)
                                          @foreach($crc as $k => $d)
                                          <tr>
                                            <td>{{$k}}</td>
                                            @foreach($d as $v)
                                            <td>{{$v['data']['count']}}</td>
                                            @endforeach
                                          </tr>
                                          @endforeach

                                          <?php $cnt = 0; ?>
                                          @foreach($crc as $crc2)
                                            @if($cnt > 0) <?php break; ?> @endif
                                              <tr>
                                                <td>total</td>
                                                @foreach($crc2 as $k => $d)
                                                <td>{{$d['data']['totalcounts']}}</td>
                                                @endforeach
                                              </tr>
                                            <?php $cnt++; ?>
                                          @endforeach
                                        @endif

                                      </tbody>
                                    </table>
                                  </section>

                                  <section id="content5">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                            <th>CRC</th>
                                          @foreach($dates as $k => $d)
                                            <th>{{$k}}</th>
                                          @endforeach
                                        </tr>
                                        <tr>
                                            <th></th>
                                          @foreach($dates as $k => $d)
                                            <th>{{$d}}</th>
                                          @endforeach
                                        </tr>    
                                      </thead>
                                      <tbody>
                                        @if($crc)
                                          @foreach($crc as $k => $d)
                                          <tr>
                                            <td>{{$k}}</td>
                                            @foreach($d as $v)
                                            <td>{{$v['data']['percent']}}%</td>
                                            @endforeach
                                          </tr>
                                          @endforeach
                                        @endif  
                                      </tbody>
                                    </table>
                                  </section>                                  
                                </main>
                                <br>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>

                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    #tab5:checked ~ #content5 {
      display: block;
    }
    #content4,#content5{
      overflow: scroll;
    }
    </style>
@endsection

@section('footer-scripts')
 <script src="{{$asset}}gentella/js/icheck/icheck.min.js"></script>
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
@endsection