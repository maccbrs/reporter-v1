<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".modalDataShow{{$param['campaigns']}}">filter dispo</button>


  <div class="modal fade modalDataShow{{$param['campaigns']}} " tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

              <div class="x_panel">
                <div class="x_title">
                  <h2>Select Disposition to exclude</h2>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form class="form-horizontal form-label-left" method="post" action="{{route('primrose.productivity.show')}}" novalidate>
                  {{ csrf_field() }}
                  {!!view('primrose.forms.hidden',['name' => 'from','value' => $param['from']])!!}
                  {!!view('primrose.forms.hidden',['name' => 'to','value' => $param['to']])!!}
                  {!!view('primrose.forms.hidden',['name' => 'campaigns','value' => $param['campaigns']])!!}
                  
                  <div class="row">
                    <div class="col-md-12">
                        {!!view('primrose.forms.checkboxes',['name' => 'dispo','value' => '','choices' => $dispo,'default' => ''])!!}
                        <div class="form-group">
                          <div class="col-md-12">
                            <button  type="submit" class="btn btn-success pull-right">proceed</button>
                          </div>
                        </div>                        
                    </div>                 
                  </div>

                  </form>
                </div>
          </div>

      </div>
    </div>
  </div>

