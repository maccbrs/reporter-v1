<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Agent Break Report"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12">
                              <div class="x_panel">
                            <div class="row">

                                <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.agent.agent_break_post')}}"> 
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                    <div class="col-md-3">
                                      <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                    </div> 

                                    <div class="col-md-3">
                                        <select class="select2_group form-control" name="campaign">
                                            <option value="">Select</option>

                                            @foreach($campaign_list as $campaign)
                                            <option value="{{$campaign}}">{{$campaign}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                      <button type="submit" href="#" class="btn btn-primary">Generate</button>
                                    </div>                            
                                </form>

                            </div>

                            <div class="x_content"> 
                                <div class="x_title" style = "text-align:center;">
                                  <div>
                                    <ul class="nav nav-tabs" role="tablist">
                                      <li role="presentation" class="active tab-call"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab">Logs</a></li>
                                    </ul>

                                      <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane fade in active" id="statistics">
                                            <br>
                                            <table class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    <th>Call Date</th>
                                                    <th>User</th>
                                                    <th>Campaign</th>
                                                    <th>Sub Status</th>
                                                    <th>Total Time</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                               
                                                  @foreach($results['call'] as $campaign)
                                                 
                                                  <tr>
                                                    <td>{{$campaign['call_date']}}</td>
                                                    <td>{{$campaign['user']}}</td>
                                                    <td>{{$campaign['campaign']}}</td>
                                                    <td>{{$campaign['substatus']}}</td>
                                                    <td>{{$campaign['total']}}</td>
                                                  </tr>
                                                  @endforeach
                                                </tbody>
                                            </table>
                                          </div>
                                      </div>    
                                    </div>
                                  <br>
                                  <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }

    
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',
  timezoneList: {!!$timezone!!} 
});
</script>
@endsection