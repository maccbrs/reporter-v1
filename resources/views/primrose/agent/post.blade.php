<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Agent Account Report"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                          <div class="col-md-12">
                          <div class="x_panel">
                
                           <br>

                            <div class = "row border-report"><br><br>
                              
                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.agent.post')}}"> 
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                <div class="col-md-3">
                                    <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                </div>

                                <div class="col-md-3">
                                  <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                </div> 

                                <div class="col-md-3">
                                    <select class="select2_group form-control" name="user">
                                        <option value="">Select</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->user}}">{{$user->full_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                  <button type="submit" href="#" class="btn btn-primary">Generate</button>

                                </div>   

                               </form>

                              </div>
                              
                             <br><br>
                              
                            </div>

                            @if($results)
                            <div class="row">
                                <div class="row mb-color">
                                    <div class="col-md-4">
                                      <p><label>From:</label> {{$results['from']}}</p>
                                    </div>
                                    <div class="col-md-4">
                                      <p><label>To:</label> {{$results['to']}}</p>
                                    </div>
                            
                                    <div class="col-md-4">
                                      <p><label>User:</label> {{$results['user']}}</p>
                                    </div>
                                </div>                    
                            </div>
                            @endif

                            <div class="x_content"> 

                              <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#stats">Statistics</a></li>
                                <li><a data-toggle="tab" href="#pause">Pause Codes</a></li>
                                <li><a data-toggle="tab" href="#dispo">Disposition Breakdown</a></li>
                                <li><a data-toggle="tab" href="#inbound">Inbound Calls</a></li>
                                <li><a data-toggle="tab" href="#outbound">Outbound Calls</a></li>
                                <li><a data-toggle="tab" href="#vici">Vici Log Data</a></li>
                              </ul>

                              <div class="tab-content">
                                <div id="stats" class="tab-pane fade in active">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Call Direction</th>
                                          <th>Total Calls</th>
                                          <th>Handled Calls</th>
                                          <th>Total Talk Time</th>
                                          <th>Total Wrap Time</th>
                                          <th>Total Wait Time</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @if($results['stats'])
                                        <tr>
                                          <td>Inbound</td>
                                          <td>{{$results['stats']['offered']}}</td>
                                          <td>{{$results['stats']['handled']}}</td>
                                          <td>{{$results['stats']['talktime']}}</td>
                                          <td>{{$results['stats']['wraptime']}}</td>
                                          <td>{{$results['stats']['waittime']}}</td>
                                        </tr>
                                        @endif
                                        @if($results['stats2'])
                                        <tr>
                                          <td>Outbound</td>
                                          <td>{{$results['stats2']['offered']}}</td>
                                          <td>{{$results['stats2']['handled']}}</td>
                                          <td>{{$results['stats2']['talktime']}}</td>
                                          <td>{{$results['stats2']['wraptime']}}</td>
                                          <td>{{$results['stats2']['waittime']}}</td>
                                        </tr>
                                        @endif                                        
                                      </tbody>
                                    </table>
                                </div>
                                <div id="pause" class="tab-pane fade">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Log Code</th>
                                          <th>Count</th>
                                          <th>Total Pause Time</th>
                                        </tr>
                                      </thead>
                                      <tbody> 
                                        @foreach($results['pausecodes'] as $pc)
                                        <tr>
                                          <td>{{$pc['name']}}</td>
                                          <td>{{$pc['count']}}</td>
                                          <td>{{$pc['pause_sec']}}</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                </div>
                                <div id="dispo" class="tab-pane fade">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Disposition</th>
                                          <th>Count</th>
                                          <th>Percentage</th>
                                          <th>AHT</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($results['dispo']['dispo'] as $dispo)
                                        <tr>
                                          <td>{{$dispo['name']}}</td>
                                          <td>{{$dispo['count']}}</td>
                                          <td>{{$dispo['percent']}}</td>
                                          <td>{{(isset($dispo['aht'])?$dispo['aht']:'')}}</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                </div>
                                <div id="inbound" class="tab-pane fade">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Agent Id</th>
                                          <th>Call Date - Time</th>
                                          <th>Phone Number</th>
                                          <th>Campaign</th>
                                          <th>Disposition</th>
                                          <th>Talk Time</th>
                                          <th>Wrap Time</th>
                                          <th>Wait Time</th>
                                          <th>Queue</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach($results['calls'] as $calls)
                                        <tr>
                                          <td>{{$calls['user']}}</td>
                                          <td>{{$calls['call_date']}}</td>
                                          <td>{{$calls['phone_number']}}</td>
                                          <td>{{$calls['campaign']}}</td>
                                          <td>{{$calls['status']}}</td>
                                          <td>{{$calls['talktime']}}</td>
                                          <td>{{$calls['wraptime']}}</td>
                                          <td>{{$calls['waittime']}}</td>
                                          <td>{{$calls['queuetime']}}</td>
                                        </tr>
                                        @endforeach

                                      </tbody>
                                    </table>
                                </div>
                                <div id="outbound" class="tab-pane fade">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Agent Id</th>
                                          <th>Call Date - Time</th>
                                          <th>Phone Number</th>
                                          <th>Campaign</th>
                                          <th>Disposition</th>
                                          <th>Talk Time</th>
                                          <th>Wrap Time</th>
                                          <th>Wait Time</th>
                                          <th>Queue</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach($results['calls2'] as $calls)
                                        <tr>
                                          <td>{{$calls['user']}}</td>
                                          <td>{{$calls['call_date']}}</td>
                                          <td>{{$calls['phone_number']}}</td>
                                          <td>{{$calls['campaign']}}</td>
                                          <td>{{$calls['status']}}</td>
                                          <td>{{$calls['talktime']}}</td>
                                          <td>{{$calls['wraptime']}}</td>
                                          <td>{{$calls['waittime']}}</td>
                                          <td>{{$calls['queuetime']}}</td>
                                        </tr>
                                        @endforeach

                                      </tbody>
                                    </table>
                                </div>
                                <div id="vici" class="tab-pane fade">
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>User</th>
                                          <th>Event Time</th>
                                          <th>Pause Seconds</th>
                                          <th>Wait Seconds</th>
                                          <th>Talk Seconds</th>
                                          <th>Dispo Seconds</th>
                                          <th>Dead Seconds</th>
                                          <th>Campaign</th>
                                          <th>Log ID</th>
                                          <th>Session</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach($results['log_data'] as $logs)
                                        <tr>
                                          <td>{{$logs->user}}</td>
                                          <td>{{$logs->event_time}}</td>
                                          <td>{{$logs->pause_sec}}</td>
                                          <td>{{$logs->wait_sec}}</td>
                                          <td>{{$logs->talk_sec}}</td>
                                          <td>{{$logs->dispo_sec}}</td>
                                          <td>{{$logs->dead_sec}}</td>
                                          <td>{{$logs->campaign_id}}</td>
                                          <td>{{$logs->agent_log_id}}</td>
                                          <td>{{$logs->sub_status}}</td>
                                        </tr>
                                        @endforeach

                                      </tbody>
                                    </table>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>


                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
        #tab5:checked ~ #content5 {
          display: block;
        }
    
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',

});
</script>
@endsection