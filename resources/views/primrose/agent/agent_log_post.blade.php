<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Agent Account Report"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                          <div class="col-md-12">
                            <div class="x_panel">
                 
                           <br>

                            <div class = "row border-report"><br><br>
                              
                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.agent.agent_log_post')}}"> 
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                <div class="col-md-3">
                                    <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                </div>

                                <div class="col-md-3">
                                  <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                </div> 

                                <div class="col-md-3">
                                  <select class="select2_group form-control" name="campaign">
                                      <option value="">Select</option>
                                      @foreach($campaign_list as $camp)
                                      <option value="{{$camp}}">{{$camp}}</option>
                                      @endforeach
                                  </select>
                                </div>

                                <div class="col-md-3">
                                  <button type="submit" href="#" class="btn btn-primary">Generate</button>

                                </div>   

                               </form>

                              </div>
                              
                             <br><br>
                              
                            </div>

                            <div class="x_content"> 
                              <br>
                              <div class="x_title" style = "text-align:center;">
                                <main>
                                  <input id="tab1" type="radio" name="tabs" checked>
                                  <label for="tab1">Agent Logs</label>
                                  <section id="content1">
                                     <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th>User</th>
                                            <th>Event Time</th>
                                            <th>Pause Seconds</th>
                                            <th>Wait Seconds</th>
                                            <th>Talk Seconds</th>
                                            <th>Dispo Seconds</th>
                                            <th>Dead Seconds</th>
                                            <th>Campaign</th>
                                            <th>Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($campaign_log as $log)
                                          <tr>
                                            <td>{{!empty($log->user) ? $log->user : ""}}</td>
                                            <td>{{!empty($log->event_time) ? $log->event_time : ""}}</td>
                                            <td>{{!empty($log->pause_sec) ? $log->pause_sec : ""}}</td>
                                            <td>{{!empty($log->wait_sec) ? $log->wait_sec : ""}}</td>
                                            <td>{{!empty($log->talk_sec) ? $log->talk_sec : ""}}</td>
                                            <td>{{!empty($log->dispo_sec) ? $log->dispo_sec : ""}}</td>
                                            <td>{{!empty($log->dead_sec) ? $log->dead_sec : ""}}</td>
                                            <td>{{!empty($log->campaign_id) ? $log->campaign_id : ""}}</td>
                                            <td>{{!empty($log->sub_status) ? $log->sub_status : ""}}</td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                  </section>

                                </main>
                                <br>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                            </div>


                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
        #tab5:checked ~ #content5 {
          display: block;
        }
    
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',

});
</script>
@endsection