<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Agent Account Report"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                      <div class="col-md-12">
                          <div class="x_panel">
                          
                           <br>

                            <div class = "row border-report"><br><br>
                              
                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.agent.post2')}}"> 
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                <div class="col-md-3">
                                    <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                </div>

                                <div class="col-md-3">
                                  <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                </div> 

                                <div class="col-md-3">
                                    <select class="select2_group form-control" name="user">
                                        <option value="">Select</option>
                                        @foreach($users as $user)
                                        <option value="{{$user->user}}">{{$user->full_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-3">
                                  <button type="submit" href="#" class="btn btn-primary">Generate</button>

                                </div>   

                               </form>

                              </div>
                              
                             <br><br>
                              
                            </div>

                            @if($results)
                            <div class="row" style="margin-top: 15px;" >
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-10">
                                  <div class="row mb-color">
                                      <div class="col-md-3">
                                        <p><label>From:</label> {{$results['from']}}</p>
                                      </div>
                                      <div class="col-md-3">
                                        <p><label>To:</label> {{$results['to']}}</p>
                                      </div>
                                      <div class="col-md-3">
                                        <p><label>Timezone:</label> {{$results['timezone']}}</p>
                                      </div>
                                      <div class="col-md-3">
                                        <p><label>User:</label> {{$results['user']}}</p>
                                      </div>
                                  </div>
                                </div>                            
                            </div>
                            @endif

                            <div class="x_content"> 
                              <br>
                              <div class="x_title" style = "text-align:center;">
                                <main>
                                  <input id="tab1" type="radio" name="tabs" checked>
                                  <label for="tab1">Statistics</label>
                                    
                                  <input id="tab2" type="radio" name="tabs">
                                  <label for="tab2">Pause Codes</label>
                                    
                                  <input id="tab3" type="radio" name="tabs">
                                  <label for="tab3">Disposition Breakdown</label>
                                    
                                  <input id="tab4" type="radio" name="tabs">
                                  <label for="tab4">Inbound Calls</label>

                                  <input id="tab5" type="radio" name="tabs">
                                  <label for="tab5">Outbound Calls</label>

                                  <section id="content1">
                                   <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Call Direction</th>
                                          <th>Handled Calls</th>
                                          <th>Total Calls</th>
                                          <th>Total Talk Time</th>
                                          <th>Total Wait Time</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @if($results['stats'])
                                        <tr>
                                          <td>Inbound</td>
                                          <td>{{$results['stats']['handled']}}</td>
                                          <td>{{$results['stats']['offered']}}</td>
                                          <td>{{$results['stats']['talktime']}}</td>
                                          <td>{{$results['stats']['wraptime']}}</td>
                                        </tr>
                                        @endif
                                        @if($results['stats2'])
                                        <tr>
                                          <td>Outbound</td>
                                          <td>{{$results['stats2']['handled']}}</td>
                                          <td>{{$results['stats2']['offered']}}</td>
                                          <td>{{$results['stats2']['talktime']}}</td>
                                          <td>{{$results['stats2']['wraptime']}}</td>
                                        </tr>
                                        @endif                                        
                                      </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content2">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Log Code</th>
                                          <th>Count</th>
                                          <th>Total Pause Time</th>
                                        </tr>
                                      </thead>
                                      <tbody> 
                                        @foreach($results['pausecodes'] as $pc)
                                        <tr>
                                          <td>{{$pc['name']}}</td>
                                          <td>{{$pc['count']}}</td>
                                          <td>{{$pc['pause_sec']}}</td>
                                        </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content3">
                                     <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th>Disposition</th>
                                            <th>Count</th>
                                            <th>Percentage</th>
                                            <th>AHT</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @if($results['dispo']['dispo']) 
                                            @foreach($results['dispo']['dispo'] as $dispo)
                                            <tr>
                                              <td>{{$dispo['name']}}</td>
                                              <td>{{$dispo['count']}}</td>
                                              <td>{{$dispo['percent']}}</td>
                                              <td>{{(isset($dispo['aht'])?$dispo['aht']:'')}}</td>
                                            </tr>
                                            @endforeach
                                          @endif
                                        </tbody>
                                      </table>
                                  </section>
                                    
                                  <section id="content4">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Agent Id</th>
                                          <th>Call Date - Time</th>
                                          <th>Phone Number</th>
                                          <th>Campaign</th>
                                          <th>Disposition</th>
                                          <th>Talk Time</th>
                                          <th>Wrap Time</th>
                                          <th>Wait Time</th>
                                          <th>Queue</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach($results['calls'] as $calls)
                                        <tr>
                                          <td>{{$calls['user']}}</td>
                                          <td>{{$calls['call_date']}}</td>
                                          <td>{{$calls['phone_number']}}</td>
                                          <td>{{$calls['campaign']}}</td>
                                          <td>{{$calls['status']}}</td>
                                          <td>{{$calls['talktime']}}</td>
                                          <td>{{$calls['wraptime']}}</td>
                                          <td>{{$calls['waittime']}}</td>
                                          <td>{{$calls['queuetime']}}</td>
                                        </tr>
                                        @endforeach

                                      </tbody>
                                    </table>
                                  </section>

                                  <section id="content5">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Agent Id</th>
                                          <th>Call Date - Time</th>
                                          <th>Phone Number</th>
                                          <th>Campaign</th>
                                          <th>Disposition</th>
                                          <th>Talk Time</th>
                                          <th>Wrap Time</th>
                                          <th>Wait Time</th>
                                          <th>Queue</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        @foreach($results['calls2'] as $calls)
                                        <tr>
                                          <td>{{$calls['user']}}</td>
                                          <td>{{$calls['call_date']}}</td>
                                          <td>{{$calls['phone_number']}}</td>
                                          <td>{{$calls['campaign']}}</td>
                                          <td>{{$calls['status']}}</td>
                                          <td>{{$calls['talktime']}}</td>
                                          <td>{{$calls['wraptime']}}</td>
                                          <td>{{$calls['waittime']}}</td>
                                          <td>{{$calls['queuetime']}}</td>
                                        </tr>
                                        @endforeach

                                      </tbody>
                                    </table>
                                  </section>                                  
                                </main>
                                <br>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                            </div>


                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
        #tab5:checked ~ #content5 {
          display: block;
        }
    
    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',
  timezoneList: {!!$timezone!!} 
});
</script>
@endsection