<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Agent Account Report"; ?> 
@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" >
                          <div class="x_panel">
                 
                           <br>

                            <div class = "row border-report"><br><br>
                              
                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.agent.post')}}"> 
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                <div class="col-md-3">
                                    <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                </div>

                                <div class="col-md-3">
                                  <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                </div> 

                                <div class="col-md-3">
                                  <select class="select2_group form-control" name="user">
                                      <option value="">Select</option>
                                      @foreach($users as $user)
                                      <option value="{{$user->user}}">{{$user->full_name.' ('.$user->user.')'}}</option>
                                      @endforeach
                                  </select>
                                </div>

                                <div class="col-md-3">
                                  <button type="submit" href="#" class="btn btn-primary">Generate</button>

                                </div>   

                               </form>

                              </div>
                              
                             <br><br>
                              
                            </div>
                            @include('alert.errorlist')
                            <div class="x_content">
                              <br>
                              <div class="x_title" style = "text-align:center;">
                                <main>
                                  <input id="tab1" type="radio" name="tabs" checked>
                                  <label for="tab1">Statistics</label>
                                    
                                  <input id="tab2" type="radio" name="tabs">
                                  <label for="tab2">Disposition Breakdown</label>
                                    
                                  <input id="tab3" type="radio" name="tabs">
                                  <label for="tab3">Drop Calls</label>
                                    
                                  <input id="tab4" type="radio" name="tabs">
                                  <label for="tab4">Calls</label>
                                    
                                  <section id="content1">
                                   <table class="table table-bordered">
                                    </table>
                                  </section>
                                    
                                  <section id="content2">
                                     <table class="table table-bordered">
                                    </table>
                                  </section>
                                    
                                  <section id="content3">
                                     <table class="table table-bordered">
                                      </table>
                                  </section>
                                    
                                  <section id="content4">
                                     <table class="table table-bordered">
                                    </table>
                                  </section>
                                </main>
                                <br>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }


    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',
  timezoneList: {!!$timezone!!} 
});
</script>
@endsection