<div class="item form-group">
<?php $value = (in_array($value, $choices)?$value:$default); ?>
@foreach($choices as $k => $v)
  <label>
    <input type="checkbox" class="flat" {{($value == $v?'checked':'')}} name="{{$name}}[]" value="{{$v}}" > {{$v}}
  </label>
@endforeach
</div>