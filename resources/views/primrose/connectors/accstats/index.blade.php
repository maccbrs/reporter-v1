<?php $asset = URL::asset('/'); ?> 
<?php $page_name = 'Connector'; ?> 
@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">

                                
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <table class="table table-striped table-bordered">

                                    <thead>
                                      <tr>
                                        <th>Name</th>
                                        <th>DIDS</th>
                                        <th>Status</th>
                                        <th>Date Created</th>
                                        <th>#</th>
                                      </tr>
                                    </thead> 

                                    <tbody>
                                      @foreach($connectors as $camp)
                                      <tr>
                                        <td>{{$camp->name}}</td>
                                            <td>                     
                                              <select class="select2_group form-control" style="width: 100%;">
                                              <?php $dids = json_decode($camp->dids); ?>
                                                    @foreach($dids as $did)
                                                        <option >{{$did}}</option>
                                                    @endforeach
                                              </select>
                                           </td>
                                        <td>{{$camp->type}}</td>
                                        <td>{{$camp->created_at}}</td>
                                        <td>
            							                <button type="button" class="btn btn-primary fa fa-trash" data-toggle="modal" data-target=".mb-modal-{{$count}}"></button>

            							                <div class="modal fade mb-modal-{{$count}}" tabindex="-1" role="dialog" aria-hidden="true">
            							                  <div class="modal-dialog modal-lg">
            							                    <div class="modal-content">

            							                      <div class="modal-header">
            							                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            							                        </button>
            							                        <h4 class="modal-title">Delete</h4>
            							                      </div>
            							                      <div class="modal-body">
            							                      	<p>Are you sure you want to delete {{$camp->name}}?</p>
            							                      </div>
            							                      <div class="modal-footer">
            							                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              									                  <form method="post" action="{{route('primrose.connectors.destroy',$camp->id)}}"> 
              									                     <input type="hidden" name="_method" value="POST">
              									                     <input type="hidden" name="_token" value="{{ csrf_token() }}"> 							                        
                        													   <button type="submit" class="btn btn-primary">Delete</button>
                        												  </form>
            							                      </div>

            							                    </div>
            							                  </div>
            							                </div> 

                                        </td>
                                      </tr>
                                      <?php $count++; ?>
                                      @endforeach
                                    </tbody>
                                  </table>
                                  {!!$connectors->links()!!}
                                </div>
                              </div>
                            </div>

                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')


@endsection