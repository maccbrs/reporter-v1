<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Connector Settings"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">

                            <div class="x_content">
                              <br>
                              <div class="x_title" style = "text-align:center;">

                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Name</th>
                                          <th>Vicidial Link</th>
                                          <th>Dummyboard Link</th>
                                          <th>Timezone</th>
                                          <th>Sendtime</th>
                                          <th>Eod Time Range</th>
                                          <th>#</th>
                                        </tr>
                                      </thead> 
                                      <tbody> <?php $count = 0; ?>
                                        @foreach($boards as $board)
                                        <tr>
                                          <td>{{$board->name}}</td>
                                          <td>
                                            <div class="form-group">
                                              <div class="col-md-12">
                                                <select class="form-control">
                                                  @foreach(json_decode($board->vici_plug) as $v)
                                                  <option>{{$v}}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>
                                          </td> 

                                          <td>
                                            <div class="form-group">
                                              <div class="col-md-12">
                                                <select class="form-control">
                                                  @foreach(json_decode($board->templar_socket) as $v)
                                                  <option>{{$v}}</option>
                                                  @endforeach
                                                </select>
                                              </div>
                                            </div>
                                          </td>

                                          <td>
                                            {{($gc->_options($board->options))}}
                                          </td>

                                          <td>
                                            {{($gc->_options($board->options,'sendtime'))}}
                                          </td>

                                          <td>
                                            <div class="form-group">
                                              <div class="col-md-12">
                                                <select class="form-control">
                                                  <option>from: {{($gc->_options($board->options,'from'))}}</option>
                                                  <option>to: {{($gc->_options($board->options,'to'))}}</option>
                                                </select>
                                              </div>
                                            </div>
                                          </td>

                                          <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Actions</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <button type="button" data-toggle="modal" data-target=".timezone-{{$count}}">edit timezone</button> 
                                                </li>
                                                <li>
                                                  <button type="button" data-toggle="modal" data-target=".timerange-{{$count}}">edit time range</button> 
                                                </li>
                                                <li>
                                                  <button type="button" data-toggle="modal" data-target=".sendtime-{{$count}}">edit send time</button>                                                  
                                                </li>                                                                                                 
                                              </ul>
                                            </div> 
                                          </td>
                                        </tr>

                                        <div class="modal fade timezone-{{$count}}" tabindex="-1" role="dialog" aria-hidden="true">
                                          <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <form class="form-horizontal form-label-left" method="post" action="{{route('primrose.connectors.board-update',$board->id)}}">
                                                <input type="hidden" name="_method" value="POST">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                                    <h3>Edit Timezone</h3>
                                                    <div class="clear-fix"></div>
                                                  </div>
                                                  <div class="modal-body">
                                                        <div class="form-group" style="height:27px;">
                                                          <label class="control-label col-md-1">Time</label>
                                                          <div class="col-md-11">
                                                              <select class="select2_single form-control" tabindex="-1" name="timezone">
                                                                @foreach($timezones as $tz) 
                                                                <option {{($gc->_options($board->options) == $tz?'selected':'')}} value="{{$tz}}">{{$tz}}</option>
                                                                @endforeach
                                                              </select>
                                                          </div>
                                                        </div>
                                                        <div class="clear-fix"></div>
                                                  </div>
                                                  <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary" style="width:100px;">update</button>
                                                  </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div> 

                                        <div class="modal fade sendtime-{{$count}}" tabindex="-1" role="dialog" aria-hidden="true">
                                          <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                              <form method="post" action="{{route('primrose.connectors.sendtime-update',$board->id)}}"> 
                                                <input type="hidden" name="_method" value="POST">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                                  <h3>Set sending time of eod</h3>
                                                  <div class="clear-fix"></div>
                                                </div>
                                                <div class="modal-body">
                                                      <div class="form-group" style="height:27px;">
                                                        <label class="control-label col-md-1">Time</label>
                                                        <div class="col-md-11">
                                                          <input type="text" class="form-control datetimepicker" name="sendtime" value="{{($gc->_options($board->options,'sendtime'))}}">
                                                        </div>
                                                      </div>
                                                      <div class="clear-fix"></div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="submit" class="btn btn-primary" style="width:100px;">update</button>
                                                </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="modal fade timerange-{{$count}}" tabindex="-1" role="dialog" aria-hidden="true">
                                          <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.connectors.timerange-update',$board->id)}}"> 
                                                <input type="hidden" name="_method" value="POST">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                                    <h3>Timerange</h3>
                                                    <div class="clear-fix"></div>
                                                  </div>
                                                  <div class="modal-body">
                                                        <div class="form-group" style="height:27px;">
                                                          <label class="control-label col-md-1">Time</label>
                                                          <div class="col-md-5">
                                                             from <input type="text" class="form-control datetimepicker" placeholder="From" name="from" value="{{$gc->_options($board->options,'from')}}">
                                                          </div>

                                                          <div class="col-md-5">
                                                            to <input type="text" class="form-control datetimepicker" placeholder="To" name="to" value="{{$gc->_options($board->options,'to')}}">
                                                          </div> 
                                                        </div>
                                                        <div class="clear-fix"></div>
                                                  </div>
                                                <div class="modal-footer">
                                                  <button type="submit" class="btn btn-primary" style="width:100px;">update</button>
                                                </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>                                                                                 

                                        <?php $count++; ?>
                                        @endforeach
                                      </tbody>
                                    </table>

                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .mb-color p{
      color: #1F88A7;
    }

    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
<script>
  $('.datetimepicker').datetimepicker({datepicker:false,format: 'H:i:s'});
</script>
@endsection