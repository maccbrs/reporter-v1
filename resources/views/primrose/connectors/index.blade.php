<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Connector Settings"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">

                            <div class="x_content">
                              <br>
                              <div class="x_title" style = "text-align:center;">

                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>Name</th>
                                          <th>#</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        <tr>
                                          <td>Board</td>
                                          <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Actions</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <a href="{{route('primrose.connectors.board-index')}}">Index</a>
                                                </li>
                                              </ul>
                                            </div> 
                                          </td>
                                        </tr>

                                        <tr>
                                          <td>Account Statistics</td>
                                          <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Actions</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <a href="{{route('primrose.connectors.acc-stats-index','omnitrix')}}">Omnitrix Index</a>
                                                </li>
                                                <li>
                                                  <a href="{{route('primrose.connectors.acc-stats-index','nomnitrix')}}">Nonomnitrix Index</a>
                                                </li>                                                
                                              </ul>
                                            </div> 
                                          </td>
                                        </tr>

                                        <tr>
                                          <td>Blockage</td>
                                          <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Actions</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <a href="{{route('primrose.connectors.acc-stats-index','blockage')}}">Index</a>
                                                </li>
                                              </ul>
                                            </div> 
                                          </td>
                                        </tr>

                                        <tr>
                                          <td>Productivity</td>
                                          <td>
                                            <div class="btn-group">
                                              <button type="button" class="btn btn-success">Actions</button>
                                              <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                              </button>
                                              <ul class="dropdown-menu" role="menu">
                                                <li>
                                                  <a href="{{route('primrose.connectors.acc-stats-index','productivity')}}">Index</a>
                                                </li>
                                              </ul>
                                            </div> 
                                          </td>
                                        </tr>

                                      </tbody>
                                    </table>

                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    .mb-color p{
      color: #1F88A7;
    }

    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

@endsection