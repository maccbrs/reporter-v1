<?php $asset = URL::asset('/'); ?> 
<?php $page_name = ucfirst (($type == "nomnitrix")? "Non-Omnitrix" : "Omnitrix" ) . " Statistics Report"; ?>  
 
@extends('primrose.master')

@section('title', 'dashboard')

@section('content')

<style>

  div > .long-table{

    display: block;
    overflow-x: auto;

  }
</style>

 <div id="page-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="x_content">
                <div class="row tile_count">
                    <div class="col-md-12" style="margin-top:-20px; padding:15px;">
                        <div class="x_panel">

                            <br>

                            <div class="row border-report"><br><br>

                                <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.accntstat.occl.post',$type)}}">
                                    <input type="hidden" name="_method" value="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="col-md-3">
                                        <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                    </div>

                                    <div class="col-md-3">
                                        <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                    </div>

                                    <div class="col-md-3">
                                        <select class="select2_group form-control" name="campaigns">
                                          <option value="">Select</option>
                                          @foreach($lists as $list)
                                          <option value="{{$list->id}}">{{$list->name}}</option>
                                          @endforeach
                                      </select>
                                    </div>

                                    <div class="col-md-3">
                                        <button type="submit" href="#" class="btn btn-primary">Generate</button>

                                    </div>

                                </form>

                            </div>
                           
                            <br><br>

                        </div>

                        @include('alert.errorlist')

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#stat">Statistics</a></li>
                            <li><a data-toggle="tab" href="#dispo">Disposition Breakdown</a></li>
                            <li><a data-toggle="tab" href="#drop">Drop Calls</a></li>
                            <li><a data-toggle="tab" href="#call">Calls</a></li>
                            <li><a data-toggle="tab" href="#vendor">Vendor</a></li>
                            <li><a data-toggle="tab" href="#average">Average</a></li>

                            @if(!empty($listname))
                            <li><a data-toggle="tab" href="#listsname">Lists</a></li>
                             @endif

                            @if($camp['dids'] == '["COURTRM"]')
                            <li><a data-toggle="tab" href="#asbestos">Asbestos</a></li>
                            <li><a data-toggle="tab" href="#invokana">Invokana</a></li>
                            <li><a data-toggle="tab" href="#talcum">Talcum</a></li>
                            @endif
                           <!--  <li><a data-toggle="tab" href="#agent">Per Agent</a></li> -->
                        </ul>

                        <div class="tab-content">
                        <div id="stat" class="tab-pane fade in active">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Offered</th>
                                            <th>Handled</th>
                                            <th>Abandoned</th>
                                            <th>Answer Rate</th>
                                            <th>(< 30 secs)</th>
                                            <th>(< 60 secs)</th>
                                            <th>( > 60 secs)</th>
                                            <th>Service Level</th>
                                            <th>Total Talk Time</th>
                                            <th>Total Wrap Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$total['offered']}}</td>
                                            <td>{{$total['handled']}}</td>
                                            <td>{{$total['abandoned']}}</td>
                                            <td>{{$total['answerRate']}}%</td>
                                            <td>{{$total['l30']}}</td>
                                            <td>{{$total['l60']}}</td>
                                            <td>{{$total['g60']}}</td>
                                            <td>{{$total['sl']}}%</td>
                                            <td>{{$total['talktime']}}</td>
                                            <td>{{$total['wraptime']}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="dispo" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Disposition</th>
                                            <th>Count</th>
                                            <th>Percentage</th>
                                            <th>AHT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dispo['dispo'] as $disp)
                                        <tr>
                                            <td>{{(isset($disp['name'])?$disp['name']:'')}}</td>
                                            <td>{{(isset($disp['count'])?$disp['count']:'')}}</td>
                                            <td>{{(isset($disp['percent'])?$disp['percent']:'0')}}</td>
                                            <td>{{(isset($disp['aht'])?$disp['aht']:'0:0:0')}}</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <th>Total</th>
                                            <th>{{$dispo['total_call']}}</th>
                                            <th>100%</th>
                                            <th>{{$dispo['total_aht']}}</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="drop" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Call Date</th>
                                            <th>LOB</th>
                                            <th>Phone Number</th>
                                            <th>Status</th>
                                            <th>Queue Time</th>
                                            <th>Unique ID</th>
                                            <th>Lead ID</th>
                                            <th>List ID</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($dropped['result'] as $drop)
                                        <tr>
                                            <td>{{$drop['call_date']}}</td>
                                            <td>{{$drop['campaign']}}</td>
                                            <td>{{$drop['phone_number']}}</td>
                                            <td>{{$drop['status']}}</td>
                                            <td>{{$drop['queuetime']}}</td>
                                            <td>{{$drop['uniqueid']}}</td>
                                            <td>{{$drop['lead_id']}}</td>
                                            <td>{{$drop['list_id']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="call" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($calls as $call)
                                        <tr>
                                            <td>{{$call['user']}}</td>
                                            <td>{{$call['call_date']}}</td>
                                            <td>{{$call['phone_number']}}</td>
                                            <td>{{$call['campaign']}}</td>
                                            <td>{{$call['status']}}</td>
                                            <td>{{$call['talktime']}}</td>
                                            <td>{{$call['wraptime']}}</td>
                                            <td>{{$call['waittime']}}</td>
                                            <td>{{$call['queuetime']}}</td>
                                            <td>{{$call['comment']}}</td>
                                            <td>'{{$call['uniqueid']}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="vendor" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered long-table ">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                            <th>TFN</th>
                                            <th>Lead ID</th>
                                            <th>List ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($vendors as $call)

                                        <tr>
                                            <td>{{$call['user']}}</td>
                                            <td>{{$call['call_date']}}</td>
                                            <td>{{$call['phone_number']}}</td>
                                            <td>{{$call['campaign']}}</td>
                                            <td>{{$call['status']}}</td>
                                            <td>{{$call['talktime']}}</td>
                                            <td>{{$call['wraptime']}}</td>
                                            <td>{{$call['waittime']}}</td>
                                            <td>{{$call['queuetime']}}</td>
                                            <td>{{$call['comment']}}</td>
                                            <td>'{{$call['uniqueid']}}</td>
                                            <td>{{$call['vendor_lead_code']}}</td>
                                            <td>{{$call['lead_id']}}</td>
                                            <td>{{$call['list_id']}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div id="average" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Total Agent Calls</th>
                                            <th>Total Dropped</th>
                                            <th>Dropped %</th>
                                            <th>Ave Agent Talk Time</th>
                                            <th>Ave Agent Wrap Time</th>
                                            <th>Ave Handled Time</th>
                                            <th>Ave Agent Wait Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$total['handled2']}}</td>
                                            <td>{{$total['abandoned']}}</td>
                                            <td>{{$total['drop_ave']}}</td>
                                            <td>{{$total['talktime_ave']}}</td>
                                            <td>{{$total['wraptime_ave']}}</td>
                                            <td>{{$total['handled_ave']}}</td>
                                            <td>{{$total['waittime_ave']}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                           @if(!empty($listname))
                            <div id="listsname" class="tab-pane fade listable">
                                <div class="row">
                                <div class="col-md-3">
                                 <button type="button" id="export" class="btn btn-warning">Export</button>
                                </div>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Comments</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($listname as $list)
                                        <tr>
                                            <td>{{$list['user']}}</td>
                                            <td>{{$list['call_date']}}</td>
                                            <td>{{$list['phone_number']}}</td>
                                            <td>{{$list['campaign']}}</td>
                                            <td>{{$list['status']}}</td>
                                            <td>{{$list['talktime']}}</td>
                                            <td>{{$list['wraptime']}}</td>
                                            <td>{{$list['waittime']}}</td>
                                            <td>{{$list['queuetime']}}</td>
                                            <td>{{ $list['first_name'] }} {{ $list['last_name'] }}</td>
                                            <td>{{ $list['email'] }}</td>
                                            <td>{{ $list['comments'] }}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif
                            

                            @if($camp['dids'] == '["COURTRM"]')
                            <div id="asbestos" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($calls as $call)
                                        @if(strpos($call['status'], '1') || $call['status'] == 'TURDOW' || $call['status'] == 'MESTRA' || $call['status'] == 'CBASBE' || $call['status'] == 'LCAHRS' || $call['status'] == 'LCFTRA' || $call['status'] == 'LUCTRA' || $call['status'] == 'MEAHRS' || $call['status'] == 'MEFTRA' || $call['status'] == 'MESTRA')
                                        <tr>
                                            <td>{{$call['user']}}</td>
                                            <td>{{$call['call_date']}}</td>
                                            <td>{{$call['phone_number']}}</td>
                                            <td>{{$call['campaign']}}</td>
                                            <td>{{$call['status']}}</td>
                                            <td>{{$call['talktime']}}</td>
                                            <td>{{$call['wraptime']}}</td>
                                            <td>{{$call['waittime']}}</td>
                                            <td>{{$call['queuetime']}}</td>
                                            <td>{{$call['comment']}}</td>
                                            <td>'{{$call['uniqueid']}}</td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div id="invokana" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($calls as $call)
                                        @if(strpos($call['status'], '2') || $call['status'] == 'CBINVO' || $call['status'] == 'QINVOK' || $call['status'] == 'TINVOK')
                                        <tr>
                                            <td>{{$call['user']}}</td>
                                            <td>{{$call['call_date']}}</td>
                                            <td>{{$call['phone_number']}}</td>
                                            <td>{{$call['campaign']}}</td>
                                            <td>{{$call['status']}}</td>
                                            <td>{{$call['talktime']}}</td>
                                            <td>{{$call['wraptime']}}</td>
                                            <td>{{$call['waittime']}}</td>
                                            <td>{{$call['queuetime']}}</td>
                                            <td>{{$call['comment']}}</td>
                                            <td>'{{$call['uniqueid']}}</td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div id="talcum" class="tab-pane fade">
                                <br>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($calls as $call)
                                        @if(strpos($call['status'], '3') || $call['status'] == 'TDTALC' || $call['status'] == 'QTALC' || $call['status'] == 'CBTALC')
                                        <tr>
                                            <td>{{$call['user']}}</td>
                                            <td>{{$call['call_date']}}</td>
                                            <td>{{$call['phone_number']}}</td>
                                            <td>{{$call['campaign']}}</td>
                                            <td>{{$call['status']}}</td>
                                            <td>{{$call['talktime']}}</td>
                                            <td>{{$call['wraptime']}}</td>
                                            <td>{{$call['waittime']}}</td>
                                            <td>{{$call['queuetime']}}</td>
                                            <td>{{$call['comment']}}</td>
                                            <td>'{{$call['uniqueid']}}</td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection  


@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">

$(document).ready(function (){
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'Y-m-d HH:mm:ss',
  timezoneList: {!!$timezone!!} 
});

 $('#export').click(function(){
        var url='data:application/vnd.ms-excel,' + encodeURIComponent($('.listable').html()) 
        location.href=url
        return false
    })

})

</script>
@endsection