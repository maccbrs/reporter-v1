<?php $asset = URL::asset('/'); ?>
<?php $page_name = ucfirst (($type == "nomnitrix")? "Non-Omnitrix" : "Omnitrix" ) . " Statistics Report"; ?>  
@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="x_content">
          <div class="row tile_count">
            <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
              <div class="x_panel">
      
                <br>

                <div class = "row border-report"><br><br>
                            
             <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.accntstat.occl.post',$type)}}"> 
                {{--         <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.accntstat.occl.postajax')}}">  --}}
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                    <div class="col-md-3">
                        <input type="text" class="form-control datetimepicker" id="from" placeholder="From" name="from" autocomplete="off">
                    </div>

                    <div class="col-md-3">
                      <input type="text" class="form-control datetimepicker" id="to" placeholder="To" name="to" autocomplete="off">
                    </div> 

                    <div class="col-md-3">
                        <select class="select2_group form-control" id="campaigns" name="campaigns">
                            <option value="">Select</option>
                            @foreach($lists as $list)
                            <option value="{{$list->id}}">{{$list->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                      <button type="submit" id="submit" class="btn btn-primary">Generate</button>

                    </div>   

                  </form>

                </div> 

                <br><br>
                            
            </div>
                    @include('alert.errorlist')
                    
              
                    <div class="x_content"><br>
                          <div class="x_title" style = "text-align:center;">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active tab-call"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab">Statistics</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#dispo" aria-controls="dispo" role="tab" data-toggle="tab">Disposition Breakdown</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#drop" aria-controls="drop" role="tab" data-toggle="tab">Drop Calls</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#calls" aria-controls="calls" role="tab" data-toggle="tab">Calls</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#vendor" aria-controls="vendor" role="tab" data-toggle="tab">Vendor</a></li>
                                </ul>
                                <div class="tab-content">

                                 
                                </div>
                            </div>

                                  <br>
                                  <div class="clearfix"></div>
                              </div>
                            </div>

                      {{--          <div class="col-md-12">

                              <table id="statistics" class="table table-bordered">
                                  <thead>
                                    <tr>
                                    <th>Offered</th>
                                    <th>Handled</th>
                                    <th>Abandoned</th>
                                    <th>Answer Rate</th>
                                    <th>(< 30 secs)</th>
                                    <th>(< 60 secs)</th>
                                    <th>( > 60 secs)</th>
                                    <th>Service Level</th>
                                    <th>Total Talk Time</th>
                                    <th>Total Wrap Time</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  <td></td>
                                  </tbody>
                              </table>

                            </div>  --}}
          </div>
        </div>
      </div>                    
    </div>
  </div>
</div>
@endsection  


@section('header-scripts')
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }


    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
<script>
  $('.datetimepicker').datetimepicker({format: 'Y-m-d H:i:s'});
</script>

<script type="text/javascript">
  
  $(document).ready(function (){
   
   $("#submit").click(function (){

    // var statistics = $("#statistics").DataTable({
    //   "dom": "rti",
    //   "serverSide": true,
    //   "searching": false,
    //   "processing": true,
    //   "orderable": false,
    //   "ajax": {
    //     "url":  "{{route('primrose.accntstat.occl.postajax')}}",
    //     "type": "POST",
    //     "cache": false,
    //     "data": function(d){

    //       d._token = "{{csrf_token()}}";
    //       d.from = $("#from").val();
    //       d.to = $("#to").val();
    //       d.campaigns = $("#campaigns").val();
          
    //     }

    //   },
    //   "columns":[
    //   {"data": "offered"},
    //   {"data": "handled"},
    //   {"data": "abandoned"},
    //   {"data": "answerrate"},
    //   {"data": "l30"},
    //   {"data": "l60"},
    //   {"data": "g60"},
    //   {"data": "servicelevel"},
    //   {"data": "totaltalktime"},
    //   {"data": "totalwraptime"}

    //   ],

    // })

    $.ajax({
      url: "{{route('primrose.accntstat.occl.postajax')}}",
      type: "POST",

    })

  })


  })
</script>
@endsection