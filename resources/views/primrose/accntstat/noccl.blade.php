<?php $asset = URL::asset('/'); ?> 
<?php $page_name = ucfirst (($type == "nomnitrix")? "Non-Omnitrix" : "Omnitrix" ) . " Statistics Report"; ?>  

@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">
                            <div class="row">
                              <!-- <div class="col-xs-2"><img src="{{$asset}}img/report-icon.png"  style = "width:100%;" ></div> -->
                              <div class="col-xs-12">

                                <br><h1>Omnitrix Campaign Call Logs</h1><br><br><br>
                                <form class="form-horizontal form-label-left " method="get" action="{{route('primrose.index')}}">
                              <input type="hidden" name="_method" value="GET">
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                    <div class="col-md-3">
                                      <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                    </div>
                                    <div class="col-md-3">
                                      <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                    </div> 
                                    <div class="col-md-3">
                                          <select class="select2_group form-control">
                                              <option value="CT">Connecticut</option>
                                              <option value="DE">Delaware</option>
                                              <option value="FL">Florida</option>
                                              <option value="GA">Georgia</option>
                                              <option value="IN">Indiana</option>
                                              <option value="ME">Maine</option>
                                              <option value="MD">Maryland</option>
                                              <option value="MA">Massachusetts</option>
                                              <option value="MI">Michigan</option>
                                              <option value="NH">New Hampshire</option>
                                              <option value="NJ">New Jersey</option>
                                              <option value="NY">New York</option>
                                              <option value="NC">North Carolina</option>
                                              <option value="OH">Ohio</option>
                                              <option value="PA">Pennsylvania</option>
                                              <option value="RI">Rhode Island</option>
                                              <option value="SC">South Carolina</option>
                                              <option value="VT">Vermont</option>
                                              <option value="VA">Virginia</option>
                                              <option value="WV">West Virginia</option>
                                          </select>
                                    </div>
                                    <button type="submit" href="#" class="btn btn-primary">Generate</button>                               
                               </form>

                              </div>
                            </div>

                            <div class="x_content">
                              <br>
                              <div class="x_title" style = "text-align:center;">
                                <main>
                                  <input id="tab1" type="radio" name="tabs" checked>
                                  <label for="tab1">Statistics</label>
                                    
                                  <input id="tab2" type="radio" name="tabs">
                                  <label for="tab2">Disposition Breakdown</label>
                                    
                                  <input id="tab3" type="radio" name="tabs">
                                  <label for="tab3">Drop Calls</label>
                                    
                                  <input id="tab4" type="radio" name="tabs">
                                  <label for="tab4">Calls</label>
                                    
                                  <section id="content1">
                                   <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>First Name</th>
                                          <th>Last Name</th>
                                          <th>Username</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>Mark</td>
                                          <td>Otto</td>
                                          <td>@mdo</td>
                                        </tr>
                                        <tr>
                                          <td>Jacob</td>
                                          <td>Thornton</td>
                                          <td>@fat</td>
                                        </tr>
                                        <tr>
                                          <td>Larry</td>
                                          <td>the Bird</td>
                                          <td>@twitter</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content2">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>First Name</th>
                                          <th>Last Name</th>
                                          <th>Username</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>Mark</td>
                                          <td>Otto</td>
                                          <td>@mdo</td>
                                        </tr>
                                        <tr>
                                          <td>Jacob</td>
                                          <td>Thornton</td>
                                          <td>@fat</td>
                                        </tr>
                                        <tr>
                                          <td>Larry</td>
                                          <td>the Bird</td>
                                          <td>@twitter</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </section>
                                    
                                  <section id="content3">
                                     <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Username</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                          </tr>
                                          <tr>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                          </tr>
                                          <tr>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                  </section>
                                    
                                  <section id="content4">
                                     <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>First Name</th>
                                          <th>Last Name</th>
                                          <th>Username</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr>
                                          <td>Mark</td>
                                          <td>Otto</td>
                                          <td>@mdo</td>
                                        </tr>
                                        <tr>
                                          <td>Jacob</td>
                                          <td>Thornton</td>
                                          <td>@fat</td>
                                        </tr>
                                        <tr>
                                          <td>Larry</td>
                                          <td>the Bird</td>
                                          <td>@twitter</td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </section>
                                </main>
                                <br>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>



    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }


    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'Y-m-d HH:mm:ss',
  timezoneList: {!!$timezone!!} 
});
</script>
@endsection