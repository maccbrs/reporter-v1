<?php $asset = URL::asset('/'); ?> 
<?php $page_name = 'List Report'; ?>  
 
@extends('primrose.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">
   
                           <br>

                            <div class = "row border-report"><br>
                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.listreporter.post')}}"> 

                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                <input type="hidden" name="campaigns" value = "empty">

                                  <div class="col-md-3">
                                    Search by
                                    <select class="selecttype form-control " name="searchby">
                                      <option value="" disabled>Search By</option>
                                      <option value="List">List ID</option>
                                      <option value="Campaign">Campaign</option>
                                    </select>  
                                  </div> 

                                  <div class="col-md-3 inputtype">
                                    <span class = "inputtype1">
                                      List ID
                                      <input type="text" class="form-control "  placeholder="List ID" name="list_id">
                                    </span>
                                    <span class = "inputtype2">
                                      Campaign
                                        <select class="selecttype form-control " name="campaign">
                                        <option value="" disabled>Search By</option>
                                          @foreach($lists as $k => $v)
                                            <option value="{{$k}}">{{$v}}</option>
                                          @endforeach
                                      </select> 
                                    </span>
                                  </div>

                                  <div class="col-md-2">
                                    <br>
                                    <input type="text" class="form-control datetimepicker frompick" placeholder="From" name="from">
                                  </div>

                                  <div class="col-md-2">
                                    <br>
                                  <input type="text" class="form-control datetimepicker topick" placeholder="To" name="to">
                                 </div> 

                                  <div class="col-md-2">
                                    <br>
                                    <button type="submit" href="#" class="btn btn-primary">
                                    Generate</button>  
                                  </div>     

                               </form>

                              </div>
                              
                             <br><br>
                              
                            </div>
                            
                          @include('alert.errorlist')

                        <div class="x_content"><br>
                          <div class="x_title" style = "text-align:center;">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                  <li role="presentation" class="active tab-call"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab">Statistics</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#dispo" aria-controls="dispo" role="tab" data-toggle="tab">Disposition Breakdown</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#drop" aria-controls="drop" role="tab" data-toggle="tab">Drop Calls</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#calls" aria-controls="calls" role="tab" data-toggle="tab">Calls</a></li>
                                  <li role="presentation" class = "tab-call"><a href="#vendor" aria-controls="vendor" role="tab" data-toggle="tab">Vendor</a></li>
                                </ul>
                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane fade in active" id="statistics">
                                      <br>
                                      <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Offered</th>
                                              <th>Handled</th>
                                              <th>Abandoned</th>
                                              <th>Answer Rate</th>
                                              <th>( < 30 secs)</th>
                                              <th>( < 60 secs)</th>
                                              <th>( > 60 secs)</th>
                                              <th>Service Level</th>
                                              <th>Total Talk Time</th>
                                              <th>Total Wrap Time</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr>
                                              <td>{{$total['offered']}}</td>
                                              <td>{{$total['handled']}}</td>
                                              <td>{{$total['abandoned']}}</td>
                                              <td>{{$total['answerRate']}}%</td>
                                              <td>{{$total['l30']}}</td>
                                              <td>{{$total['l60']}}</td>
                                              <td>{{$total['g60']}}</td>
                                              <td>{{$total['sl']}}%</td>
                                              <td>{{$total['talktime']}}</td>
                                              <td>{{$total['wraptime']}}</td>
                                            </tr>
                                          </tbody>
                                      </table>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="dispo">
                                      <br>
                                      <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Disposition</th>
                                              <th>Count</th>
                                              <th>Percentage</th>
                                              <th>AHT</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            @foreach($dispo['dispo'] as $disp)
                                            <tr>
                                              <td>{{(isset($disp['name'])?$disp['name']:'')}}</td>
                                              <td>{{(isset($disp['count'])?$disp['count']:'')}}</td>
                                              <td>{{(isset($disp['percent'])?$disp['percent']:'0')}}</td>
                                              <td>{{(isset($disp['aht'])?$disp['aht']:'0:0:0')}}</td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                              <th>Total</th>
                                              <th>{{$dispo['total_call']}}</th>
                                              <th>100%</th>
                                              <th>{{$dispo['total_aht']}}</th>
                                            </tr>
                                          </tbody>
                                      </table>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="drop">
                                      <br>
                                      <table class="table table-bordered">
                                          <thead>
                                            <tr>
                                              <th>Call Date</th>
                                              <th>LOB</th>
                                              <th>Phone Number</th>
                                              <th>Status</th>
                                              <th>Queue Time</th>
                                              <th>Unique ID</th>
                                            </tr>
                                          </thead>

                                          <tbody>
                                            @foreach($dropped['result'] as $drop)
                                            <tr>
                                              <td>{{$drop['call_date']}}</td>
                                              <td>{{$drop['campaign']}}</td>
                                              <td>{{$drop['phone_number']}}</td>
                                              <td>{{$drop['status']}}</td>
                                              <td>{{$drop['queuetime']}}</td>
                                              <td>'{{$drop['uniqueid']}}</td>
                                            </tr>
                                            @endforeach
                                          </tbody>
                                      </table>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="calls">
                                      <br>
                                      <table class="table table-bordered">
                                            <thead>
                                              <tr>
                                                <th>Agent Id</th>
                                                <th>Call Date - Time</th>
                                                <th>Phone Number</th>
                                                <th>Campaign</th>
                                                <th>Disposition</th>
                                                <th>Talk Time</th>
                                                <th>Wrap Time</th>
                                                <th>Wait time</th>
                                                <th>Queue Time</th>
                                                <th>Comment</th>
                                                <th>Unique ID</th>
                                              </tr>
                                            </thead>
                                            <tbody>

                                              @foreach($calls as $call)
                                                <tr>
                                                  <td>{{$call['user']}}</td>
                                                  <td>{{$call['call_date']}}</td>
                                                  <td>{{$call['phone_number']}}</td>
                                                  <td>{{$call['campaign']}}</td>
                                                  <td>{{$call['status']}}</td>
                                                  <td>{{$call['talktime']}}</td>
                                                  <td>{{$call['wraptime']}}</td>
                                                  <td>{{$call['waittime']}}</td>
                                                  <td>{{$call['queuetime']}}</td>
                                                  <td>{{$call['comment']}}</td>
                                                  <td>'{{$call['uniqueid']}}</td>

                                                </tr>
                                              @endforeach
                                            </tbody>
                                      </table>

                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="vendor">
                                      <br>

                                      <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                            <th>Lead ID</th>
                                            <th>List ID</th>
                                          </tr>
                                        </thead>
                                        <tbody>

                                          @foreach($vendors as $call)

                                            <tr>
                                              <td>{{$call['user']}}</td>
                                              <td>{{$call['call_date']}}</td>
                                              <td>{{$call['phone_number']}}</td>
                                              <td>{{$call['campaign']}}</td>
                                              <td>{{$call['status']}}</td>
                                              <td>{{$call['talktime']}}</td>
                                              <td>{{$call['wraptime']}}</td>
                                              <td>{{$call['waittime']}}</td>
                                              <td>{{$call['queuetime']}}</td>
                                              <td>{{$call['comment']}}</td>
                                              <td>'{{$call['uniqueid']}}</td>
                                              <td>{{$call['lead_id']}}</td>
                                              <td>{{$call['list_id']}}</td>
                                              
                                            </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>
                                </div>
                            </div>

                                  <br>
                                  <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">

    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }

    .fst{
        background-color: #ee8823;
    }

    .ptd{
        background-color: #589796;
    }

    #calendar{
        margin-top: 11px;
    }

    .inputtype2{
        display: none;
    }


    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',
  timezoneList: {!!$timezone!!} 
});
</script>


<script type="text/javascript">

  var date = $('.datetimepicker').datetimepicker({
    timeFormat: 'HH:mm:ss z',
    timezoneList: {!!$timezone!!} 
  });

  $( document ).ready(function() {

      $(".frompick").prop('disabled', true);
      $(".topick").prop('disabled', true);
  });

  $( ".selecttype" ).change(function() {
    
    typeofsearch = $( ".selecttype" ).val();

    if(typeofsearch == 'List'){
      
      $( ".inputtype2" ).hide();
      $( ".inputtype1" ).show();

      $(".frompick").prop('disabled', true);
      $(".topick").prop('disabled', true);
      
    }

    if(typeofsearch == 'Campaign'){
      
      $( ".inputtype2" ).show();
      $( ".inputtype1" ).hide();

      $(".frompick").prop('disabled', false);
      $(".topick").prop('disabled', false);
     
    }
    
  });

</script>


@endsection