<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "List Reporter"; ?> 

@extends('primrose.master')

@section('title', 'dashboard')


@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">
                        <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
                          <div class="x_panel">
                      
                            <div class = "row border-report"><br>
                              <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.listreporter.post')}}"> 

                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                <input type="hidden" name="campaigns" value = "empty">

                                  <div class="col-md-3">
                                    Search by
                                    <select class="selecttype form-control " name="searchby">
                                      <option value="" disabled>Search By</option>
                                      <option value="List">List ID</option>
                                      <option value="Campaign">Campaign</option>
                                    </select>  
                                  </div> 

                                  <div class="col-md-3 inputtype">
                                    <span class = "inputtype1">
                                      List ID
                                      <input type="text" class="form-control "  placeholder="List ID" name="list_id">
                                    </span>
                                    <span class = "inputtype2">
                                      Campaign
                                        <select class="selecttype form-control " name="campaign">
                                        <option value="" disabled>Search By</option>
                                          @foreach($lists as $k => $v)
                                            <option value="{{$k}}">{{$v}}</option>
                                          @endforeach
                                      </select> 
                                    </span>
                                  </div>

                                  <div class="col-md-2">
                                    <br>
                                    <input type="text" class="form-control datetimepicker frompick" placeholder="From" name="from">
                                  </div>

                                  <div class="col-md-2">
                                    <br>
                                  <input type="text" class="form-control datetimepicker topick" placeholder="To" name="to">
                                 </div> 

                                  <div class="col-md-2">
                                    <br>
                                    <button type="submit" href="#" class="btn btn-primary">
                                    Generate</button>  
                                  </div>     

                               </form>

                              </div>
                              
                              
                            </div>
                            @include('alert.errorlist')
                            <div class="x_content">
                              <br>
                              <div class="x_title" style = "text-align:center;">
                                <main>
                                  <input id="tab1" type="radio" name="tabs" checked>
                                  <label for="tab1">No Data</label>
                                    
                                    
                                  <section id="content1">
                                   <table class="table table-bordered">
                                    </table>
                                  </section>
                                    
                                </main>
                                <br>
                                <div class="clearfix"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

@endsection  


@section('header-scripts')

    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">

      .cchart{
          padding: 5px;
          background-color: #0F9D58; 
          border-radius: 1px;
          color: #fff;     
      }

      .fst{
          background-color: #ee8823;
      }

      .ptd{
          background-color: #589796;
      }

      #calendar{
          margin-top: 11px;
      }

      .inputtype2{
        display: none;
      }

    </style>
@endsection

@section('footer-scripts')

<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">

  var date = $('.datetimepicker').datetimepicker({
    timeFormat: 'HH:mm:ss z',
    timezoneList: {!!$timezone!!} 
  });

  $( document ).ready(function() {

      $(".frompick").prop('disabled', true);
      $(".topick").prop('disabled', true);
  });

  $( ".selecttype" ).change(function() {
    
    typeofsearch = $( ".selecttype" ).val();

    if(typeofsearch == 'List'){
      
      $( ".inputtype2" ).hide();
      $( ".inputtype1" ).show();

      $(".frompick").prop('disabled', true);
      $(".topick").prop('disabled', true);
      
    }

    if(typeofsearch == 'Campaign'){
      
      $( ".inputtype2" ).show();
      $( ".inputtype1" ).hide();

      $(".frompick").prop('disabled', false);
      $(".topick").prop('disabled', false);
     
    }
    
  });

</script>

@endsection