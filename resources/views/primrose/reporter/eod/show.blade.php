<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "EOD Report"; ?> 
@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
  <style>
      
      .mj-body > div{

        width:100%;

      }

  </style>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                    @include('flash::message')
                                    <p>To: {{$data['to']}} <a href="{{route('primrose.reporter.reporter-eod-detail',$data['id'])}}" class="btn btn-success fa fa-folder-open-o pull-right"></a></p>
                                    <p>From: {{$data['from']}}</p>
                                    <p>Subject: {{$data['subject']}}</p>
                                    <p>Cc: {{$data['cc']}}</p>
                                    <p>Bcc: {{$data['bcc']}}</p>

                                    @if($data['options'])
                                      <p>Time Range -> From: {{$helper->_options($data['options'],'ofr')}} - To: {{$helper->_options($data['options'],'oto')}} {{$helper->_options($data['options'],'fr_tz')}}</p>
                                      @if($helper->_options($data['options'],'fr_tz') != $helper->_options($data['options'],'to_tz'))
                                      <p style="color:green; text-decoration:underline;">Converted Time Range -> From: {{$helper->_options($data['options'],'fr')}} - To: {{$helper->_options($data['options'],'to')}} {{$helper->_options($data['options'],'to_tz')}}</p>
                                      @endif
                                      <p>Time generated: {{$helper->_options($data['options'],'time')}}</p>

                                    @endif
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <div>
                                    {!!$data['content']!!}
                                  </div>
                                </div>
                              </div>
                            </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>
@endsection  