<?php $asset = URL::asset('/'); ?>
<?php $page_name = "Submitted Data"; ?> 

@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
    <style>

      div > .table-bordered{

        display: block;
        overflow-x: auto;

      }
    </style>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Actual entries from database</h2>
                                 
                                  <div class="clearfix"></div>
                                </div>

                                  <div class="x_panel">
                                    <div class = "row border-report"><br>
                                      <form class="form-horizontal form-label-left " method="post" action=""> 

                                        <input type="hidden" name="_method" value="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                        <div class="col-md-3">
                                            <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                        </div>

                                        <div class="col-md-3">
                                          <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                        </div> 

                                        <div class="col-md-3">
                                            <select class="select2_group form-control" name="campaigns">
                                                <option value="">Select</option>
                                                @foreach($lists as $list)
                                                  <option value="{{$list->id}}">{{$list->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                          <button type="submit" href="#" class="btn btn-primary">Generate</button>
                                        </div>  
                                      </form>
                                    </div> 
                                                
                                </div>

                                <div class="x_content">
                                  <ul class="nav nav-tabs">
                                    <li class="active" ><a data-toggle="tab" href="#menu2">Total</a></li>
                                    <li><a data-toggle="tab" href="#all">Submitted Data</a></li>
                                    <li><a data-toggle="tab" href="#home">New Contacts</a></li>
                                    <li><a data-toggle="tab" href="#menu1">Transfer</a></li>
                                    <li><a data-toggle="tab" href="#menu3">Call Data</a></li>
                                  </ul>

                                  <div class="tab-content">
                                    <div id="home" class="tab-pane fade  ">
                                      <h2>Meeting Schedule</h2>
                                      <table class="table table-striped table-bordered">
        
                                        <tbody> 
                                          @foreach($new as $key => $value2)

                                            <tr>
                                              <th>Account Name</th>
                                              <td>Logi Analytics</td>
                                            </tr>

                                           <tr>
                                              <th>Contact ID</th>
                                              <td>{{!empty($value2->Who_ID ) ? $value2->Who_ID : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Account ID</th>
                                              <td>{{!empty($value2->What_ID ) ? $value2->What_ID : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Description</th>
                                              <td>{{!empty($value2->NOTE ) ? $value2->NOTE : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Meeting Date</th>
                                              <td>{{!empty($value2->Meeting_Date ) ? $value2->Meeting_Date : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Transferred To</th>
                                              <td>{{!empty($value2->Transferred_To ) ? $value2->Transferred_To : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Transferred By</th>
                                              <td>{{!empty($value2->Transferred_By ) ? $value2->Transferred_By : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Preferred Phone</th>
                                              <td>{{!empty($value2->Preferred_Phone ) ? $value2->Preferred_Phone : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Software Company </th>
                                              <td>{{!empty($value2->TYPE_OF_SOFTWARE ) ? $value2->TYPE_OF_SOFTWARE : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Company Size</th>
                                              <td>{{!empty($value2->HOW_MANY_CUSTOMER_DO_YOU_HAVE ) ? $value2->HOW_MANY_CUSTOMER_DO_YOU_HAVE : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Transferred Date</th>
                                              <td>{{!empty($value2->Transfer_Date ) ? $value2->Transfer_Date : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Subject</th>
                                              <td>Meeting Schedule</td>
                                            </tr>

                            <!--                 <tr>
                                              <th>Call Disposition</th>
                                              <td>{{!empty($value2->disposition ) ? $value2->disposition : ""}}</td>
                                            </tr>
 -->
                                           <!--  <tr>
                                              <th>OWNERID</th>
                                              <td>{{!empty($value2->owner_id ) ? $value2->owner_id : ""}}</td>
                                            </tr> -->

                                          </tr>
                                          @endforeach
                                        </tbody> 
                            
                                        </table>
                                    </div>
                                    <div id="all" class="tab-pane fade  ">

                                      @if(!empty($sorted_all))

                                      <table class="table table-striped table-bordered">
                                        <thead>
                                          <tr>
                                            @foreach($sorted_all['head'] as $key2 => $value2) 
                                                <th>{{!empty($key2 ) ? $key2 : ""}}</th>
                                            @endforeach
                                          </tr>
                                        </thead>
                                        <tbody>
                                          
                                            @foreach($sorted_all['content'] as $key2 => $value2) 
                                              <tr>
                                                @foreach($value2 as $key3 => $value3) 
                                                <td>{{!empty($value3 ) ? $value3 : ""}}</td>
                                                 @endforeach
                                              </tr>
                                            @endforeach
                                          
                                         
                                         
                                        </tbody> 
                                      </table>
                                      @endif
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                      <h2>Magellan New Transfers</h2>
                                      <table class="table table-striped table-bordered">
        
                                        <tbody> 
                                          @foreach($transfer as $key => $value2)

                                            <tr>
                                              <th>Account Name</th>
                                              <td>Logi Analytics</td>
                                            </tr>

                                           <tr>
                                              <th>Contact ID</th>
                                              <td>{{!empty($value2->Who_ID ) ? $value2->Who_ID : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Account ID</th>
                                              <td>{{!empty($value2->What_ID ) ? $value2->What_ID : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Description</th>
                                              <td>{{!empty($value2->NOTE ) ? $value2->NOTE : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Meeting Date</th>
                                              <td>{{!empty($value2->Meeting_Date ) ? $value2->Meeting_Date : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Transferred To</th>
                                              <td>{{!empty($value2->Transferred_To ) ? $value2->Transferred_To : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Transferred By</th>
                                              <td>{{!empty($value2->Transferred_By ) ? $value2->Transferred_By : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Preferred Phone</th>
                                              <td>{{!empty($value2->Preferred_Phone ) ? $value2->Preferred_Phone : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Software Company </th>
                                              <td>{{!empty($value2->TYPE_OF_SOFTWARE ) ? $value2->TYPE_OF_SOFTWARE : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Company Size</th>
                                              <td>{{!empty($value2->HOW_MANY_CUSTOMER_DO_YOU_HAVE ) ? $value2->HOW_MANY_CUSTOMER_DO_YOU_HAVE : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Transferred Date</th>
                                              <td>{{!empty($value2->Transfer_Date ) ? $value2->Transfer_Date : ""}}</td>
                                            </tr>

                                            <tr>
                                              <th>Subject</th>
                                              <td>Transfer</td>
                                            </tr>

                            <!--                 <tr>
                                              <th>Call Disposition</th>
                                              <td>{{!empty($value2->disposition ) ? $value2->disposition : ""}}</td>
                                            </tr>
 -->
                                           <!--  <tr>
                                              <th>OWNERID</th>
                                              <td>{{!empty($value2->owner_id ) ? $value2->owner_id : ""}}</td>
                                            </tr> -->

                                          </tr>
                                          @endforeach
                                        </tbody> 
                            
                                        </table>
                                    </div>
                                    <div id="menu2" class="tab-pane fade in active">
                                      <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Offered</th>
                                                <th>Handled</th>
                                                <th>Abandoned</th>
                                                <th>Answer Rate</th>
                                                <th>(< 30 secs)</th>
                                                <th>(< 60 secs)</th>
                                                <th>( > 60 secs)</th>
                                                <th>Service Level</th>
                                                <th>Total Talk Time</th>
                                                <th>Total Wrap Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$total['offered']}}</td>
                                                <td>{{$total['handled']}}</td>
                                                <td>{{$total['abandoned']}}</td>
                                                <td>{{$total['answerRate']}}%</td>
                                                <td>{{$total['l30']}}</td>
                                                <td>{{$total['l60']}}</td>
                                                <td>{{$total['g60']}}</td>
                                                <td>{{$total['sl']}}%</td>
                                                <td>{{$total['talktime']}}</td>
                                                <td>{{$total['wraptime']}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </div>
                                    <div id="menu3" class="tab-pane fade">
                                      <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Agent Id</th>
                                            <th>Call Date - Time</th>
                                            <th>Phone Number</th>
                                            <th>Campaign</th>
                                            <th>Disposition</th>
                                            <th>Talk Time</th>
                                            <th>Wrap Time</th>
                                            <th>Wait time</th>
                                            <th>Queue Time</th>
                                            <th>Comment</th>
                                            <th>Unique ID</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($calls as $call)
                                        <tr>
                                            <td>{{$call['user']}}</td>
                                            <td>{{$call['call_date']}}</td>
                                            <td>{{$call['phone_number']}}</td>
                                            <td>{{$call['campaign']}}</td>
                                            <td>{{$call['status']}}</td>
                                            <td>{{$call['talktime']}}</td>
                                            <td>{{$call['wraptime']}}</td>
                                            <td>{{$call['waittime']}}</td>
                                            <td>{{$call['queuetime']}}</td>
                                            <td>{{$call['comment']}}</td>
                                            <td>'{{$call['uniqueid']}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>


@endsection  


@section('header-scripts')

@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',
  timezoneList: {!!$timezone!!} 
});
</script>
@endsection