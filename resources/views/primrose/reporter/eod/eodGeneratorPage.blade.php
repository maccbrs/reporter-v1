<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "EOD Report (Page)"; ?> 
@extends('primrose.master')
@section('title', 'dashboard')

@section('content')

  <div id="page-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="x_content">
          <div class="row tile_count">
            <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
              <div class="x_panel">
      
                <br>

                <div class = "row border-report"><br><br>
                            
                  <form method="post" action="{{route('primrose.reporter.reporter-eod-generator-page')}}"> 

                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                    <div class="col-md-3">
                        <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                    </div>

                    <div class="col-md-3">
                      <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                    </div> 

                    <div class="col-md-3">
                        <select class="select2_group form-control" name="pageconid">
                            <option value="">Select</option>
                            @foreach($lists as $list)
                            <option value="{{$list->id}}">{{$list->title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                      <button type="submit" href="#" class="btn btn-primary">Generate</button>

                    </div>   

                  </form>

                </div> 

                <br><br>
                            
            </div>

              @include('alert.errorlist')
              
              <div class="row">
                <div class="col-md-12"> 
                    <div class="panel panel-default">
                      <div class="panel-heading">
                         EOD Report 
                      </div>

                      <div class="panel-body" style="overflow:auto;">
                        <?php $data = dynamicParse2($items);?>

                        @if($data)
                        <table>
                          <tr>
                            @foreach($data['keys'] as $keys)
                              <th style="padding:5px; margin-right: 5px; border: 1px solid #fff;">{{$keys}}</th>
                            @endforeach
                          </tr>

                          @foreach($data['data'] as $item)
                          <tr>
                              @foreach($data['keys'] as $k)
                                <td style="border:1px solid #ebebeb; font-size:12px;">{{(isset($item[$k])?$item[$k]:'')}}</td>
                              @endforeach
                          </tr>
                          @endforeach
                        </table>
                        @endif
                      </div>
                  </div>
                </div>
              </div>

          </div>
        </div>
      </div>                    
    </div>
  </div>
</div>


@endsection  

@section('footer-scripts')

<script>
  $('.datetimepicker').datetimepicker({format: 'Y-m-d H:i:s'});
</script>
@endsection