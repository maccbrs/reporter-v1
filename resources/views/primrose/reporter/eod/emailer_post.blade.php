<?php $asset = URL::asset('/'); ?>
<?php $page_name = "Submitted Data"; ?> 

@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
    <style>

      div > .table-bordered{

        display: block;
        overflow-x: auto;

      }
    </style>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Actual entries from database</h2>
                                 
                                  <div class="clearfix"></div>
                                </div>

                                  <div class="x_panel">
                                    <div class = "row border-report"><br>
                                      <form class="form-horizontal form-label-left " method="post" action=""> 

                                        <input type="hidden" name="_method" value="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                        <div class="col-md-3">
                                            <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                        </div>

                                        <div class="col-md-3">
                                          <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                        </div> 

                                        <div class="col-md-3">
                                            <select class="select2_group form-control" name="campaigns">
                                                <option value="">Select Email Subject</option>
                                                  @foreach($emailer_list as $list)
                                              <option value="{{$list}}">{{$list}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                          <button type="submit" href="#" class="btn btn-primary">Generate</button>
                                        </div>  
                                      </form>
                                    </div> 
                                                
                                </div>

                                <div class="x_content">
                                  <ul class="nav nav-tabs">
                                    <li class="active" ><a data-toggle="tab" href="#menu2">Email Data</a></li>
                                  </ul>

                                  <div class="tab-content">
                                    <div id="menu2" class="tab-pane fade in active">
                                      <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>To</th>
                                                <th>CC</th>
                                                <th>From</th>
                                                <th>BCC</th>
                                                <th>Subject</th>
                                                <th>User</th>
                                                <th>Notes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                              @foreach($content_decoded as $key => $value) 
                                                @if(!empty($value->subject))
                                                  <tr>
                                                    <td>{{!empty($value->to ) ? $value->to : ""}}</td>
                                                    <td>{{!empty($value->cc ) ? json_encode($value->cc) : ""}}</td>
                                                    <td>{{!empty($value->from ) ? json_encode($value->from) : ""}}</td>
                                                    <td>{{!empty($value->bcc ) ? json_encode($value->bcc) : ""}}</td>
                                                    <td>{{!empty($value->subject ) ? $value->subject : ""}}</td>
                                                    <td>{{!empty($value->user ) ? $value->user : " "}}</td>
                                                    <td>{{!empty($value->notes ) ? $value->notes : " "}}</td>
                                                </tr>
                                              @endif
                                            @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                  </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>


@endsection  


@section('header-scripts')

@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">
var date = $('.datetimepicker').datetimepicker({
  timeFormat: 'HH:mm:ss z',
  timezoneList: {!!$timezone!!} 
});
</script>
@endsection