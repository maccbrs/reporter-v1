<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "EOD Report"; ?> 
@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>EOD Reports</h2>
                                 
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <table class="table table-striped table-bordered">

                                    <thead>
                                      <tr>
                                        <th>Date</th>
                                        <th>Count</th>
                                        <th>#</th>
                                      </tr>
                                    </thead> 

                                    <tbody>
                                      @foreach($lists as $list)
                                      <tr>
                                        <td>{{$list['date']}}</td>
                                        <td>{{$list['count']}}</td>
                                        <td><a href="{{route('primrose.reporter.reporter-eod-date',$list['date'])}}" class="btn btn-success fa fa-folder-open-o"></a></td>
                                      </tr>
                                      @endforeach
                                    </tbody>
                                  </table>
                                  {!!$lists->links()!!}
                                </div>
                              </div>
                            </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>
@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    </style>
@endsection

@section('footer-scripts')

    <!-- Datatables-->
    <script src="{{$asset}}gentella/js/datatables/jquery.dataTables.min.js"></script>
    <script src="{{$asset}}gentella/js/datatables/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('#datatable').dataTable();
      });
    </script>

@endsection