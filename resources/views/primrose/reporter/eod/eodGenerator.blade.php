<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "EOD Report"; ?> 
@extends('primrose.master')
@section('title', 'dashboard')

@section('content')

  <div id="page-wrapper">
    <div class="row">
      <div class="col-md-12">
        <div class="x_content">
          <div class="row tile_count">
            <div class="col-md-12" style ="margin-top:-20px; padding:15px;">
              <div class="x_panel">
      
                <br>

                <div class = "row border-report"><br><br>
                            
                  <form method="post" action="{{route('primrose.reporter.eod-generator')}}"> 

                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                    <div class="col-md-3">
                        <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                    </div>

                    <div class="col-md-3">
                      <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                    </div> 

                    <div class="col-md-3">
                        <select class="select2_group form-control" name="campaigns">
                            <option value="">Select</option>
                            @foreach($lists as $list)
                            <option value="{{$list->id}}">{{$list->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                      <button type="submit" href="#" class="btn btn-primary">Generate</button>

                    </div>   

                  </form>

                </div> 

                <br><br>
                            
            </div>

              @include('alert.errorlist')
              
              <div class="row">
                <div class="col-md-12"> 
                    <div class="panel panel-default">
                      <div class="panel-heading">
                         EOD Report 
                      </div>

                      <div class="panel-body">
                          @if(!empty($html_logs) && !empty($html_data))
                            {!!$html_logs!!}
                          @else
                            <p>No Call logs</p>
                          @endif
                          <br>
                          @if(!empty($html_data))
                           
                            <div class="container-outer">
                             <div class="container-inner">
                                {!!$html_data!!}
                             </div>
                            </div>
                          
                          @else
                            <p>No Board data</p>
                          @endif
                       </div>
                  </div>
                </div>
              </div>

          </div>
        </div>
      </div>                    
    </div>
  </div>
</div>


@endsection  

@section('footer-scripts')

<script>
  $('.datetimepicker').datetimepicker({format: 'Y-m-d H:i:s'});
</script>
@endsection