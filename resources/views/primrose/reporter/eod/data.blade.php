<?php $asset = URL::asset('/'); ?>
<?php $page_name = "Submitted Data"; ?> 

@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Actual entries from database</h2>
                                 
                                  <div class="clearfix"></div>
                                </div>

                                  <div class="x_panel">
                                    <div class = "row border-report"><br>
                                      <form class="form-horizontal form-label-left " method="post" action="{{route('primrose.reporter.submitted-data-post')}}"> 

                                        <input type="hidden" name="_method" value="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 

                                        <div class="col-md-3">
                                            <input type="text" class="form-control datetimepicker" placeholder="From" name="from">
                                        </div>

                                        <div class="col-md-3">
                                          <input type="text" class="form-control datetimepicker" placeholder="To" name="to">
                                        </div> 

                                        <div class="col-md-3">
                                            <select class="select2_group form-control" name="campaigns">
                                                <option value="">Select</option>
                                                @foreach($lists as $list)
                                                  <option value="{{$list->id}}">{{$list->name}}</option>
                                                @endforeach
                                            </select> 
                                        </div>

                                        <div class="col-md-3">
                                          <button type="submit" href="#" class="btn btn-primary">Generate</button>
                                        </div>  
                                      </form>
                                    </div> 
                                                
                                </div>

                                <div class="x_content">
                                  <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#home">New Contacts</a></li>
                                    <li><a data-toggle="tab" href="#menu1">Transfer</a></li>
                                    <li><a data-toggle="tab" href="#menu2">Call Data</a></li>
                                  </ul>

                                  <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                     
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                      
                                    </div>
                                    <div id="menu2" class="tab-pane fade">
                                      
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>
@endsection  


@section('header-scripts')

@endsection

@section('footer-scripts')
  <script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
  <script>
    $('.datetimepicker').datetimepicker({format: 'Y-m-d H:i:s'});
  </script>
@endsection