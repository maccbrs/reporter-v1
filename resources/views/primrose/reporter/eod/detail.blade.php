<?php $asset = URL::asset('/'); ?>
<?php $page_name = "Datail"; ?> 

@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                    <div class="row tile_count">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Actual entries from database</h2>
                                 
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <table class="table table-striped table-bordered">

                                  <thead>
                                      <tr>
                                        <th>Unique Id</th>
                                          <th>Call Date</th>
                                          <th>Status</th>
                                          <th>User</th>
                                          <th>Campaign Id</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($result as $list) 
                                      <tr>
                                        <td>{{$list->uniqueid}}</td>
                                        <td>{{$list->call_date}}</td>
                                        <td>{{$list->status}}</td>
                                        <td>{{$list->user}}</td>
                                        <td>{{$list->campaign_id}}</td>
                                      </tr>
                                    @endforeach
                                  </tbody> 
                      
                                  </table>
                                </div>
                              </div>
                            </div>
                    </div>                    
                </div>
            </div>
        </div>

    </div>
@endsection  


@section('header-scripts')

@endsection

@section('footer-scripts')

@endsection