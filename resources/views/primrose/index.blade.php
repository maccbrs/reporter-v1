<?php $asset = URL::asset('/'); ?> 
<?php $page_name = "Welcome to Reporter"; ?> 
@extends('primrose.master')

@section('title', 'dashboard')


@section('content')

    <div id="page-wrapper">

        <div class="row">
            <div class="col-md-12">
                <div class="x_content">
                   
                </div>
            </div>
        </div>
    </div>

@endsection  


@section('header-scripts')
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.css" rel="stylesheet">
    <link href="{{$asset}}gentella/css/calendar/fullcalendar.print.css" rel="stylesheet" media="print">
    <style type="text/css">

    .cchart{
        padding: 5px;
        background-color: #0F9D58; 
        border-radius: 1px;
        color: #fff;     
    }

    .fst{

        background-color: #ee8823;
    }

    .ptd{
        background-color: #589796;
    }

    #calendar{
        margin-top: 11px;
    }

     .flip-container {
        perspective: 1000px;
    }
        /* flip the pane when hovered */
        .flip-container:hover .flipper, .flip-container.hover .flipper {
            transform: rotateY(180deg);
        }

    .flip-container, .front, .back {
        width: 100%;
        height: 170px;
    }

    /* flip speed goes here */
    .flipper {
        transition: 0.6s;
        transform-style: preserve-3d;
        height: 100%;
        position: relative;
    }

    /* hide back of pane during swap */
    .front, .back {
        backface-visibility: hidden;

        position: absolute;
        top: 0;
        left: 0;
    }

    /* front pane, placed above back */
    .front {
        z-index: 2;
        /* for firefox 31 */
        transform: rotateY(0deg);
    }

    /* back, initially hidden pane */
    .back {
        transform: rotateY(180deg);
    }


    </style>
@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

<script type="text/javascript">

    var date = $('.datetimepicker').datetimepicker({
      timeFormat: 'HH:mm:ss z',
      timezoneList: {!!$timezone!!} 
    });

    $(document).ready(function() {
        $( ".back" ).append( $( "<h1 id ='templarLNO'><b>{ <span id = 'templar'>Templar</span> like no other }</b></h1>" ) );
    });

</script>

@endsection