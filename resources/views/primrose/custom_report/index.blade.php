<?php $asset = URL::asset('/'); ?>
<?php $page_name = "Agent Activity"; ?> 
@extends('primrose.master')
@section('title', 'dashboard')

@section('content')
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<div class = "row border-report"><br><br>

				<form class="form-horizontal form-label-left " method="post" action="{{route('primrose.custom.report.generate')}}"> 

					<input type="hidden" name="_method" value="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}"> 

					<div class="col-md-3">
						<input type="text" class="form-control datetimepicker" id="from" placeholder="From" name="from" autocomplete="off">
					</div>

					<div class="col-md-3">
						<input type="text" class="form-control datetimepicker" id="to" placeholder="To" name="to" autocomplete="off">
					</div> 

					<div class="col-md-3">
						<select class="select2_group form-control" id="campaigns" name="campaigns">
							<option value="">Select</option>
							@foreach($campaigns as $camp)
							<option value="{{ $camp->id }}">{{ $camp->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="col-md-3">
						<button type="submit" id="submit" class="btn btn-primary">Generate</button>

					</div>   

				</form>

			</div> 

		</div>
	</div>
	@if(isset($data))
	<div class="row">
		<div class="col-md-3">
		<button type="button" id="export_to_excel" class="btn btn-info btn-sm">Export</button>
		</div>
	</div>
	@endif
	<div class="row">

		<div class="results">
		@if(isset($data))
		<table class="table">
				
			<thead>
				<th>User</th>
				<th>Date/Time</th>
				<th>Pause</th>
				<th>Wait</th>
				<th>Talk</th>
				<th>Dispo</th>
				<th>Dead</th>
				<th>Customer</th>
				<th>Status</th>
				<th>Lead</th>
				<th>Type</th>
				<th>Campaign</th>
				<th>Pause Code</th>

			</thead>
			<tbody>
				@foreach($data as $key => $val)
				<tr>
					<td>{{ $val['user'] }}</td>
					<td>{{ $val['date']}}</td>
					<td>{{ $val['pause'] }}</td>
					<td>{{ $val['wait'] }}</td>
					<td>{{ $val['talk'] }}</td>
					<td>{{ $val['dispo'] }}</td>
					<td>{{ $val['dead'] }}</td>
					<td>{{ $val['customer'] }}</td>
					<td>{{ $val['status'] }}</td>
					<td>{{ $val['lead'] }}</td>
					<td>{{ $val['type'] }}</td>
					<td>{{ $val['campaign'] }}</td>
					<td>{{ $val['pause_code'] }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@endif
		</div>
	</div>

</div>

@endsection

@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
<script type="text/javascript">

	$('.datetimepicker').datetimepicker({format: 'Y-m-d H:i:s'});

	$('#export_to_excel').click(function(){
        var url='data:application/vnd.ms-excel,' + encodeURIComponent($('.results').html()) 
        location.href=url
        return false
    })
</script>

@endsection