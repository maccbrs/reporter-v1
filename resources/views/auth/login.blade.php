@extends('layouts.app')

@section('content')

<?php $asset = URL::asset('/'); ?> 

<style>
    
.navbar {

    display:none;

}

</style>

    <link rel="icon" type="image/png" href="images/favicon.png">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="{{$asset}}gentella/js/jquery-2.1.3.min.js"></script>
    <script src="{{$asset}}gentella/js/functions.js"></script>

    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

    <link href="{{$asset}}gentella/css/style.css" rel="stylesheet">

    <link href="{{$asset}}gentella/fonts/css/font-awesome.min.css" rel="stylesheet">
    

<script>

  jQuery(window).load(function() {

      jQuery("#status").fadeOut();
      jQuery("#loading-text").fadeOut();
      jQuery("#preloader").delay(1000).fadeOut("slow");
      jQuery(".frame").delay(3000).fadeIn("slow");
     jQuery(".fore-bird").delay(5000).show(0);

  })

</script>


<body>
  <div id="preloader">

      <div id="status">&nbsp;  </div>
      <h3 id="loading-text">Loading ...</h3>

  </div>

  <section class="bird-box">
    <article>
   <!--  <div class="fore-bird animate-bounce"  ></div> -->
      <div class=" secondary-bg " >
      <div class="frame" style ="margin-top: 150px">

          <div  id = "form-holder">
            <img src="{{$asset}}gentella/images/logo_on_black.png">
          </div>

          <br>
         
          <div  id = "form-holder">
            <form class="form-horizontal" id ="login-form" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}

              <div class=" form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <label class="col-md-4  login-text">E-Mail Address</label>

                  <div class="col-md-8">
                      <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label class="col-md-4  login-text">Password</label>

                  <div class="col-md-8">
                      <input type="password" class="form-control" name="password">

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>

              <div class="form-group login-group">
                  
                     <button type="submit" class="btn btn-primary">
                          <i class="fa fa-btn fa-sign-in"></i>Login
                      </button> <br>
                
                      <label style ="color: white;">
                          <input type="checkbox" class = "login-text" name="remember" > Remember Me
                      </label>
                      
                  <div class="checkbox">
                      <a class="btn btn-link login-text" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                  </div>
                  </div>
            </form>
          </div>


         
      </div>
      </div>
    </article>
  </section>

  <section class="content website-container">
    <article>
      <h1>Magellan Websites</h1> 

      <hr>
      <h5><br><br>>This site is a compilation of all websites used by Magellan Solutions staffs to generate report, create a dummy boards, monitor incoming and outgoing calls, check call costs, manage projects, giving access and many more. Be inspred with modern design and code structure used. If you have concerns or questions, please dont hesitate to contact us. <br><br></h5>

      <hr>
      <div class="clothes-pics">
        <div class="row img-row">
          <figure class="columns four"><img src="{{$asset}}gentella/images/dummyboardGIF.gif">
            <figcaption>Templar </figcaption>
          </figure>
          <figure class="columns four"><img src="{{$asset}}gentella/images/reportGIF.gif">
            <figcaption>Reporter </figcaption>
          </figure>
          <figure class="columns four"><img src="{{$asset}}gentella/images/prepaidGIF.gif">
            <figcaption>Prepaid</figcaption>
          </figure>
        </div>
        <div class="row img-row margin-div">
          <figure class="columns four"><img src="{{$asset}}gentella/images/proj-managementGIF.gif">
            <figcaption>Project Mangement </figcaption>
          </figure>
          <figure class="columns four"><img src="{{$asset}}gentella/images/nocGIF.gif">
            <figcaption>NOC Central </figcaption>
          </figure>
          <figure class="columns four"><img src="{{$asset}}gentella/images/loggerGIF.gif">
            <figcaption>Logger</figcaption>
          </figure>
        </div>
      </div>
    </article>
  </section>

  <footer>
    <div class="row footer-stuff">
      <div class="columns three"><strong>FIND US ON</strong>
        <ul>
          <li><a href="https://twitter.com/search?q=Magellan%20Solutions&src=typd" target= "_blank">Twitter</a></li>
          <li><a href="https://www.facebook.com/magellansolutions/?fref=ts" target= "_blank">Facebook</a></li>
          <li><a href="http://www.jobstreet.com.ph/en/companies/492598-magellan-solutions" target= "_blank">Jobstreet</a></li>
          <li><a href="https://www.linkedin.com/company/455507?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A455507%2Cidx%3A2-1-2%2CtarId%3A1464996435311%2Ctas%3AMagellan%20Solution" target= "_blank">Linked In</a></li>
        </ul>
      </div>
     
      <div class="columns six">
        <p><strong>Sign Up for the newsletter</strong> be informed about the news with our IT Department</p>
        <form class="row">
          <div class="columns eight">
            <input type="email" placeholder="Your Email" class="u-full-width">
          </div>
          <div class="columns four">
            <input type="submit" class="button-primary">
          </div>
        </form>
      </div>
    </div>
  </footer>

  <script src="js/jquery-2.1.3.min.js"></script>
  <script src="js/functions.js"></script>

</body>

@endsection
