@extends('prepaid.master')

@section('title', 'dashboard')


@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                 <div class="x_panel">
                    <div class="x_title col-md-12 ">
                        <h2>Edit Load</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
           				<form class="form-horizontal form-label-left input_mask" method="post" action="{{$restrictions->btnRoute('prepaid.load.update',$loadData->id)}}">
	                        <input type="hidden" name="_method" value="POST">
	                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
	                        <input type="hidden" name="subscriber_id" value="{{$loadData->subscriber_id}}">
	                        <input type="hidden" name="id" value="{{$loadData->id}}">

	                        <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Date</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input name="startdate" type="text" class="form-control has-feedback-left datepicker" value="{{$loadData->startdate}}">
	                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Minute</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input name="minutes" type="text" class="form-control has-feedback-left" placeholder="Client" value="{{$loadData->minutes}}">
	                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>
                                             
                            <div class="ln_solid"></div>

                            <div class="form-group" >
                              <div class="col-md-9 col-sm-9 col-xs-12 pull-right" style ="text-align:center; width:50%;">
                                <button type="submit" class="btn btn-success {{$restrictions->btnDisabler('prepaid.load.update')}}">Submit</button>
                              </div>
                            </div>


	                    </form>
                    </div>
                </div>               
            </div>
        </div>
    </div>
@endsection 