@extends('prepaid.master')

@section('title', 'Add Load')


@section('content')
{!! Breadcrumbs::render('LoadAdd', $subscriber->id) !!} 
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                 <div class="x_panel">
                    <div class="x_title col-md-12 ">
                        <h2>Add Load</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        @include('alert.error_list')
                        <br />
                        <form class="form-horizontal form-label-left input_mask" method="post" action="{{$restrictions->btnRoute('prepaid.load.store')}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input type="hidden" name="subscriber_id" value="{{$subscriber->id}}">

                            <div class="col-md-12 form-group has-feedback">
                                <input name="startdate" type="text" class="datepicker form-control has-feedback-left" placeholder="Start Date">
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <!-- <div class="col-md-12 form-group has-feedback">
                                <input name="enddate" type="text" class="datepicker form-control has-feedback-left" placeholder="End Date">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div> -->

                            <div class="col-md-12 form-group has-feedback">
                                <input name="minutes" type="text" class="form-control has-feedback-left" placeholder="Minutes">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class=" col-xs-6 ">
                                    <br><br>
                                    <button type="submit" class="btn btn-primary">Cancel</button>
                                </div>
                                <div class=" col-xs-6 ">
                                    <br><br>
                                    <button type="submit" class="btn btn-success {{$restrictions->btnDisabler('prepaid.load.store')}}">Submit</button>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>               
            </div>
        </div>

    </div>
@endsection 


