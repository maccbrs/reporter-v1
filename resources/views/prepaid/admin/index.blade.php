@extends('prepaid.master')

@section('title', 'dashboard')

@section('content')
    <div id="page-wrapper">
        {!! Breadcrumbs::render('home') !!}
        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">

                    <div class="x_title">
                        <h2>Load and Overage Monitoring</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                      <div class="x_panel">
                        <div class="x_title">
                          <h2>Table Representation</h2>
                          <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                          </ul>
                          <div class="clearfix"></div>
                        </div>
                        <div class="x_content" style="display: block;">
                          <table class="table table-hover">
                            <thead>
                              <tr>
                                <th>Subsscriber</th>
                                <th>Remaning Subscription</th>
                                <th>Remaning Overage</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($subscription as $l => $list)
                                @if($list['percentageload'] < 10)
                                  @if($list['reserve'] < 50 )
                                    <?php $critical = 'critical'; ?>
                                  @else
                                    <?php $critical = 'overage'; ?>
                                  @endif
                                @else
                                  <?php $critical = 'normalb'; ?>
                                @endif

                              <tr class="<?php echo $critical ?>">
                                <td>{{$list['subscriber']}}</td>
                                <td>{{$list['load']}}</td>
                                <td>{{$list['reserve']}}</td>
                                <td>{{$list['startdate']}}</td>
                                <td>{{$list['enddate']}}</td>
                              </tr>

                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                      <div class="x_content">
                        <div class="x_panel">
                          <div class="x_title">
                            <h2>Graphical Representation<small>Click Remaining Minutes and Overage Minutes</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                              </li>
                              <li><a class="close-link"><i class="fa fa-close"></i></a>
                              </li>
                            </ul>
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content" style="display: block;">
                              <canvas id="mybarChart"></canvas>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection 
<?php $asset = URL::asset('/'); ?> 
@section('footer-scripts')
    <script src="{{$asset}}gentella/js/moris/raphael-min.js"></script>
    <script src="{{$asset}}gentella/js/chartjs/chart.min.js"></script>

    <script>

    $(document).ready(function() {
            $('.child_menu').css("display","none");
            $('.nav.side-menu > li').removeClass("active");

            $( ".back" ).append( $( "<h1 id ='templarLNO'><b>{ <span id = 'templar'>Templar</span> like no other }</b></h1>" ) );
        });

    </script>

    <style type="text/css">
      
      .cchart{
          padding: 5px;
          background-color: #0F9D58; 
          border-radius: 1px;
          color: #fff;     
      }
      .fst{
          background-color: #ee8823;
      }
      .ptd{
          background-color: #589796;
      }
      #calendar{
          margin-top: 11px;
      }
   
      .flip-container {
          perspective: 1000px;
      }
          /* flip the pane when hovered */
          .flip-container:hover .flipper, .flip-container.hover .flipper {
              transform: rotateY(180deg);
          }

      .flip-container, .front, .back {
          width: 100%;
          height: 170px;
      }

      /* flip speed goes here */
      .flipper {
          transition: 0.6s;
          transform-style: preserve-3d;

          position: relative;
      }

      /* hide back of pane during swap */
      .front, .back {
          backface-visibility: hidden;

          position: absolute;
          top: 0;
          left: 0;
      }

      /* front pane, placed above back */
      .front {
          z-index: 2;
          /* for firefox 31 */
          transform: rotateY(0deg);
      }

      /* back, initially hidden pane */
      .back {
          transform: rotateY(180deg);
      }

      .critical {
        
        background-color: #f2dede;
      }

      .overage{

         background-color: #fcf8e3;
      }

    </style>


    <script type="text/javascript">

        // Bar chart
        var ctx = document.getElementById("mybarChart"); 
        var mybarChart = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: {!!$label!!},
            datasets: [{
              label: 'Remaining Minutes',
              backgroundColor: "#26B99A",
              data: {!!$remaining!!}
            }, {
              label: 'Overage Minutes',
              backgroundColor: "#03586A",
              data: {!!$or!!}
            }]
          },

          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

    </script>


@endsection