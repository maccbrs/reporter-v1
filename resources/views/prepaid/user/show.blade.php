@extends('prepaid.master')

@section('content')
{!! Breadcrumbs::render('UserShow' , $user->id) !!}
<div id="page-wrapper div-margin">
    <div class="m-t"></div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body ">
                    
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type ="hidden" value= <?php echo $user->id ?>>
                    
                    <div>
                        <label class="col-md-4 control-label div-margin">Name</label>

                        <div class="col-md-6">
                            <input class="form-control div-margin" name="name" value= <?php echo $user->name ?>>
                        </div>
                    </div>

                    <div>
                        <label class="col-md-4 control-label div-margin">E-Mail</label>

                        <div class="col-md-6">
                            <input type="email" class="form-control div-margin" name="email" value= <?php echo $user->email ?>>
                        </div>
                    </div>

                    <div>
                        <label class="col-md-4 control-label div-margin">Created At</label>
                        <div class="col-md-6">
                            <input  class="form-control div-margin" value= <?php echo $user->created_at ?>>
                        </div>
                    </div>

                </div>
            </div>
        </div>
       
    </div>
</div>

@endsection

<style>
    
    .div-margin {

        margin: 5px;

    }

</style>



