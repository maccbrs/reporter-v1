 @extends('prepaid.master')

@section('title', 'Create User')


@section('content')
{!! Breadcrumbs::render('UserAdd') !!}
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                 <div class="x_panel">
                    <div class="x_title col-md-12 ">
                        <h2>Create User</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form class="form-horizontal form-label-left input_mask" method="post" action="{{$restrictions->btnRoute('prepaid.user.store')}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            
                            <div class="col-md-12 form-group has-feedback">
                                <input name="name" type="text" class="form-control has-feedback-left" placeholder="Name">
                                <span class="fa fa-bars form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-12 form-group has-feedback">
                                <input name="email" type="text" class="form-control has-feedback-left" placeholder="Email">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <div class="col-md-12 form-group has-feedback">
                                <input name="password" type="text" class="form-control has-feedback-left" placeholder="Password">
                                <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                            </div>

                            <input type="hidden" name="created_at" value="date("Y-m-d H:i:s")">
                            <input type="hidden" name="updated_at" value="date("Y-m-d H:i:s")">

                            <center>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success {{$restrictions->btnDisabler('prepaid.user.store')}}">Submit</button>
                                    </div>
                                </div>
                            </center>
                        </form>
                    </div>
                </div>               
            </div>
        </div>

    </div>
@endsection 