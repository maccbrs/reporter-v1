@extends('prepaid.master')

@section('content')
{!! Breadcrumbs::render('UserEdit' , $user->id) !!}
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
             <div class="x_panel">
                <div class="x_title col-md-12 ">
                    <h2>Edit User</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask" method="post" action="{{$restrictions->btnRoute('prepaid.user.update',$user->id)}}">
                        <input type="hidden" name="_method" value="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token()}}">

                        <div class="col-md-12 form-group has-feedback">
                            <input name="client" type="text" class="form-control has-feedback-left" placeholder="Client" value="{{$user->name}}">
                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-12 form-group has-feedback">
                            <input name="email" type="text" class="form-control has-feedback-left" placeholder="Email" value="{{$user->email}}">
                            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        <div class="col-md-12 form-group has-feedback">
                            <input name="email" type="text" class="form-control has-feedback-left" placeholder="New Password" value="{{$user->password}}">
                            <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                        </div>

                        <div class="ln_solid"></div>
                        <center>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success {{$restrictions->btnDisabler('prepaid.user.update')}}">Submit</button>
                                </div>
                            </div>
                        </center>
                    </form>
                </div>
            </div>               
        </div>
    </div>
</div>

@endsection

<style>
    
    .form-control {

        margin: 5px;

    }

</style>




