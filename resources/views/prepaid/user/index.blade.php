@extends('prepaid.master')

@section('content')

<style>

    .btn-info{

        padding: 2px 4px;

    }

    span{

        font-size: 11px;

    }

    .container-action{

        width:70%;
        height:30px;
        margin-left:0px;
        margin-right: 0px;
    }

</style> 
{!! Breadcrumbs::render('UserView') !!}
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Users List</div>
                    <div class="panel-body">
                        <div class="bs-example" data-example-id="bordered-table">
                            <table class="table table-bordered">

                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th> 
                                        <th>Action</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ctr = '0'?>
                                    @foreach ($user as $users)
                                    <?php $ctr ++;?>
                                    <tr>

                                        <th scope="row">{{$ctr}}</th>
                                        <td>{{ $users->name }}</td>
                                        <td>{{ $users->email }}</td>
                                        <td style = "text-align:center;">
                                            <div class="container container-action" >
                                              <div class="row">

                                                <a href="{{$restrictions->btnRoute('prepaid.user.edit',$users->id)}}" class="{{$restrictions->btnDisabler('prepaid.user.edit')}}">
                                                    <div class="col-sm-4">
                                                      <button type="button" class="btn btn-info glyphicon glyphicon-pencil" aria-hidden="true"></button>
                                                      <br><span>Edit</span>
                                                    </div>
                                                </a>

                                                <a href="{{$restrictions->btnRoute('prepaid.user.show',$users->id)}}" class="{{$restrictions->btnDisabler('prepaid.user.show')}}">
                                                    <div class="col-sm-4">
                                                      <button type="button" class="btn btn-info glyphicon glyphicon-zoom-in" aria-hidden="true"></button>
                                                      <br><span>View</span>
                                                    </div>
                                                </a>

                                                <a href="{{$restrictions->btnRoute('prepaid.user.delete',$users->id)}}" class="{{$restrictions->btnDisabler('prepaid.user.delete')}}">
                                                    <div class="col-sm-4">
                                                      <button type="button" class="btn btn-info glyphicon glyphicon-trash" aria-hidden="true"></button>
                                                      <br><span>Delete</span>
                                                    </div>
                                                </a>

                                              </div>
                                            </div> 
                                            <br>
                                            <div class="grid3col">
                                        </td>
                                    </tr>
                                    @endforeach 
                                </tbody>
                                 
                            </table>
                           <?php echo $user->render(); ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


