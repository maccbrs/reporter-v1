 @extends('prepaid.master')

@section('title', 'Create connector')

@section('content')
{!! Breadcrumbs::render('ConnectorEdit' , $connector->id) !!}
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                 <div class="x_panel">
                    <div class="x_title col-md-12 ">
                        <h2>Edit connector</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                       
                        <form class="form-horizontal form-label-left input_mask" method="post" action="{{route('prepaid.connector.update',$connector->id)}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Campaign/Client</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                
                                    <input name = "subscriber_id" type ="hidden" class="form-control" value = "{{$connector->subscriber_id}}"  >
                                    <input type ="text" class="form-control" disabled  value = "{{$subscribers[$connector->subscriber_id]}}"  >
                
                                </select>
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">DID</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input name="did" type="text" class="form-control has-feedback-left" placeholder="did" value="{{$connector->did}}">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>
                                             

                                             
                            <div class="ln_solid"></div>

                            <div class="form-group" >
                              <div class="col-md-9 col-sm-9 col-xs-12 pull-right" style ="text-align:center; width:50%;">
                                 <button type="submit" class="btn btn-success">Submit</button>
                              </div>
                            </div>

                        </form>
                    </div>
                </div>               
            </div>
        </div>

    </div>
@endsection 