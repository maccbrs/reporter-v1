@extends('prepaid.master')


@section('content')

<style>

    .btn-info,.btn-info:hover{

        padding: 2px 4px;
        margin:0px;

    }

    span{

        font-size: 11px;

    }



</style> 

{!! Breadcrumbs::render('ConnectorView') !!}

<div class="container">
    <div class="row">
     @include('alert.success')
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">connector List</div>
                    <div class="panel-body">
                        <div class="bs-example" data-example-id="bordered-table">
                            <table class="table table-bordered">

                                <thead>
                                    <tr>
                                        <th>subscriber</th>
                                        <th>did</th> 
                                        <th>Action</th> 
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($connectors as $connector)
                                    <tr>

                                        <td>{{ !empty($connector->subscriber->client) ? $connector->subscriber->client : " " }}</td>

                                        <td>{{ $connector->did }}</td>
                                        <td >


                                           
                                                <a href="{{$restrictions->btnRoute('prepaid.connector.edit',$connector->id)}}" class="{{$restrictions->btnDisabler('prepaid.connector.edit')}}">
                                                
                                                      <button type="button" class="btn btn-info glyphicon glyphicon-pencil" ></button>
                                               
                                                </a>

                                                <a href="{{$restrictions->btnRoute('prepaid.connector.show',$connector->id)}}" class="{{$restrictions->btnDisabler('prepaid.connector.edit')}}">
                                                    
                                                      <button type="button" class="btn btn-info glyphicon glyphicon-zoom-in" ></button>
                                                    
                                                </a>

                                                 <a href="{{$restrictions->btnRoute('prepaid.connector.delete',$connector->id)}}" class="{{$restrictions->btnDisabler('prepaid.connector.edit')}}">
                                                        
                                                      <button type="button" class="btn btn-info glyphicon glyphicon-trash" ></button>
                                                  
                                                </a>

                                       
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                 
                            </table>
                            <center>
                            <?php echo $connectors->render(); ?>
                            </center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
