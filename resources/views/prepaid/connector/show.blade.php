<?php $asset = URL::asset('/'); ?> 
@extends('prepaid.master')

@section('title', 'dashboard')

@section('content')
{!! Breadcrumbs::render('ConnectorShow' , $connector->id) !!}
    <div id="page-wrapper" >

    <div class="row" style = "background: url('{{$asset}}gentella/images/laptop.jpg') no-repeat center; height: 500px;">
    

    <center>
        <h3 >Show Connector</h3>
    </center>

    <br>
            
    </div>

    @include('alert.success')

    <div class = "row text-in-monitor">
        <div class="col-xs-4 col-md-2"></div>
            <div class="col-xs-10 col-md-8">
                <div class = "screen">
                    <h1>Subscriber ID: <span class = "data-screen">{{$connector->subscriber_id}}</span></h1> <br>
                    <h1>DID: <span class = "data-screen">{{$connector->did}}</span></h1>
                </div>
            <div class="col-xs-4 col-md-2"></div>
        </div>
    </div>

    <div class = "prepaid-div">
        <a href="{{$restrictions->btnRoute('prepaid.connector.edit',$connector->id)}}" class="{{$restrictions->btnDisabler('prepaid.connector.edit')}} "><input type="button" value ="Edit" class="prepaid-button" ></a>
    </div>



@endsection 

<style>

    .text-in-monitor{

        text-align: center;
        position: relative;
        bottom: 315px;

    }

    .screen{

        color:white;
    }

    .data-screen{

        color:yellow;
    }

    .prepaid-div{

        text-align: center;
        position: relative;
        bottom: 140px;  
    }

    .prepaid-button{

        padding:15px; 
        width:120px; 
        color:white;
        background-color: #00B269;
    }

    .prepaid-button:hover{

        background-color: #b20049;
    }

</style>