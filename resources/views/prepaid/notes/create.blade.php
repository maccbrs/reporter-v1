@extends('prepaid.master')

@section('title', 'dashboard')
<style type="text/css">
	.form-horizontal .control-label.t-a-l{
		text-align: left;
	}
</style>
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                 <div class="x_panel">
                    <div class="x_title col-md-12 ">
                        <h2>Add Notes</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    	@include('alert.error_list')
                        <br />
                        <form class="form-horizontal form-label-left input_mask" method="post" action="{{route('prepaid.notes.store')}}">

                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input type="hidden" name="subscriber_id" value="{{$subscriber->id}}">

		                    <div class="form-group">
		                      <label class="control-label col-md-3 col-sm-3 col-xs-12  t-a-l">Content <span class="required">*</span>
		                      </label>
		                      <div class="col-md-12 col-sm-12 col-xs-12">
		                        <textarea class="form-control t-a-l" rows="3" name="content"></textarea>
		                      </div>
		                    </div>

                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success {{$restrictions->btnDisabler('prepaid.notes.store')}}">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>               
            </div>
        </div>

    </div> 
@endsection 