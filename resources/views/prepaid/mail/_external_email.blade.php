<html >
  <body>
    <div>
      <div>
      <table style ="">
        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px;">Account</th>
          <td style ="border-style:solid; border-width:1px;">{{$data['account_name']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>

        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Contract Minutes  </th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['minutes']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>

        <tr>
          <td></td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td></td>
        </tr>

        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px;">Contract Minutes Left %  </th>
          <td  style ="border-style:solid; border-width:1px;">{{floor($data['load_ave'])}}%&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Contract Minutes Used  </th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['used_minutes']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Remaining Contract Minutes  </th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['remaining']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>

        <tr>
          <td></td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td></td>
        </tr>


        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Overage Minutes Used  </th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['overage_used']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Remaining Overage Minutes  </th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['overage_remaining']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>

        <tr>
          <td></td>
        </tr>
        <tr>
          <td></td>
        </tr>
        <tr>
          <td></td>
        </tr>

        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Subscription</th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['startdate']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>

        <tr >
          <th style ="text-align:left; background-color:#00CED1;border-style:solid; border-width:1px; ">Expiration Date</th>
          <td  style ="border-style:solid; border-width:1px;">{{$data['enddate']}}&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
       
      </table>
    </div>
  </div>
</body>
</html>
