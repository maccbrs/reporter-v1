@extends('prepaid.master')

@section('content')

<style>

    .btn-info, .btn-info:hover{

        padding: 2px 4px;
        margin: 0px;

    }
    span{

        font-size: 11px;

    }

</style> 

{!! Breadcrumbs::render('SubscriberView') !!}

<div class="container">
    <div class="row">
        <div class="col-sm-12 ">
             @include('flash::message')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{$restrictions->btnRoute('prepaid.subscriber.index_all')}}">

                    <button type="button" class="btn btn-info  glyphicon glyphicon-eye-open pull-right {aria-hidden='true'"> View All</button></a>
                    
                    <h3>Subscriber List </h3>  

                    <table class="table table-bordered" style = "margin-bottom:0px;">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Program Code</th>
                                <th>Email</th> 
                                <th>Action</th> 
                            </tr>
                        </thead>

                        <tbody>
                            <?php $ctr = '0'?>
                            @foreach($subscribers as $subscriber)
                            <?php $ctr ++;?>
                            <tr>
                                <td>{{ $subscriber->client }}</td>
                                <td>{{ $subscriber->alias }}</td>
                                <td>{{ substr($subscriber->email,0, 30) . "..." }}</td>
                                <td style = "text-align:center;">
                                    <div class="container container-action" >
                                        <a href="{{$restrictions->btnRoute('prepaid.subscriber.edit',$subscriber->id)}}">
                                            <button type="button" class="btn btn-info glyphicon glyphicon-pencil {{$restrictions->btnDisabler('prepaid.subscriber.edit')}}" aria-hidden="true"></button>
                                        </a>   
                                        <a href="{{$restrictions->btnRoute('prepaid.subscriber.show',$subscriber->id)}}">
                                            <button type="button" class="btn btn-info glyphicon glyphicon-zoom-in {{$restrictions->btnDisabler('prepaid.subscriber.show')}}" aria-hidden="true"></button>
                                        </a> 
                                        <a href="{{$restrictions->btnRoute('prepaid.subscriber.delete',$subscriber->id)}}"><button type="button" class="btn btn-info glyphicon glyphicon-eye-close " aria-hidden="true"></button>
                                
                                        </a>
                                    </div> 
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <center>
                    <?php echo $subscribers->render(); ?>
                    </center>
                
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
