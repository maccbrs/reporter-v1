<?php $asset = URL::asset('/'); ?> 
@extends('prepaid.master')

@section('title', 'dashboard')

@section('header-scripts')
    <link href="{{$asset}}gentella/css/floatexamples.css" rel="stylesheet" type="text/css" />
    
@endsection

@section('content')
    
@include('flash::message')

 {!! Breadcrumbs::render('SubscriberShow' , $subscriber->id) !!}    

<div id="page-wrapper">
    <div class="m-t"></div> 
    <div class="row">
         
            @if($subscriber->active)
                <div class="col-md-9 mb-custom">

                    <div class="row tile_count "> 
                      <div class="animated flipInY col-md-3 tile_stats_count {{($subscriber->connected?'bgGreen':'bgRed')}}">
                        <div class="left"></div>
                        <div class="right">
                        
                          <span class="count_top " style = "color:white;"><i class="fa fa-rss"></i>  Connection</span>
                          <div class="  count " style = "font-size:20px; color:white;">{{($subscriber->connected?'connected':'inactive')}}</div>
                        </div>
                      </div>

                      <div class="animated flipInY col-md-2 tile_stats_count {{($subscriber->active['remaining'] > 99?'bgGreen': ($subscriber->active['remaining'] > 50 ?'bgYellow' :'bgRed'))}} ">
                        <div class="left"></div>
                        <div class="right">
                          <span class="count_top" style = "font-size:9px; color:white;"><i class="fa fa-clock-o"></i>  Remaining Minutes</span>
                          <div class="count white" style = "font-size:25px; color:white;">{{$subscriber->active['remaining']}}</div>
                        </div>                                           
                      </div> 

                      <div class="animated flipInY col-md-2 tile_stats_count  {{(0 > 49?'bgGreen': (0 > 20 ?'bgYellow' :'bgRed'))}}  ">
                        <div class="left"></div>
                        <div class="right">
                          <span class="count_top" style = "font-size:9px; "><i class="fa fa-info-circle"></i>  Remaining Overage</span>
                          <div class="count white" style = "font-size:25px;">{{!empty($reserved_prepaid['remaining']) ? $reserved_prepaid['remaining'] : "" }}</div>
                         
                        </div> 
                      </div>
                      
                      <div class="animated flipInY col-md-2 tile_stats_count {{$gn->daysleft($subscriber->active['enddate']) > 15 ?'bgGreen' :'bgRed'}}">
                        <div class="left"></div>
                        <div class="right">
                          <span class="count_top" style = "font-size:9px;"><i class="fa fa-exclamation-circle"></i>  Expires in:</span>
                          <div class="count white" style = "font-size:25px;">{{$gn->daysleft($subscriber->active['enddate'])}} days</div>
                        </div>
                      </div>

                      
                      <div class="animated flipInY col-md-2 tile_stats_count  {{$average > 50 ?'bgGreen' :'bgRed'}}">
                        <div class="left"></div>
                        <div class="right">
                          <span class="count_top" style = "font-size:9px;"><i class="fa fa-exclamation-circle"></i>  Total Average Call:</span>
                          <div class="count white" style = "font-size:25px;">{{!empty($average) ? $average : " "}} %</div>
                        </div>
                      </div>

                    </div>
                </div>
            

            @endif

    </div> <br>

    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
            <section class="panel">
                <div class="panel-body">
                  <div>
                    <a href="{{$restrictions->btnRoute('prepaid.load.create',$subscriber->id)}}" class="btn btn-app pull-left {{$restrictions->btnDisabler('prepaid.load.create')}}">
                      <i class="fa fa-plus-square"></i> Add Load
                    </a>
                    <a href="{{$restrictions->btnRoute('prepaid.reserved.create',$subscriber->id)}}" class="btn btn-app pull-left {{$restrictions->btnDisabler('prepaid.reserved.create')}}">
                        <i class="fa fa-plus-square"></i> Add Overage
                    </a> 
                  </div> <br><br><br><br><br>
                    <div class="project_detail">
                        <p class="title">Client Company</p>
                        <p>{{$subscriber->client}}</p>
                        <p class="title">Email Address</p>
                        <p>{{$subscriber->email}}</p>
                    </div>
                    <div class=" mtop20">
                      <a href="{{$restrictions->btnRoute('prepaid.generate.ctget',$subscriber->id)}}" class="btn btn-sm btn-primary {{$restrictions->btnDisabler('prepaid.generate.ctget')}}">call transfer</a>
                        <a href="{{$restrictions->btnRoute('prepaid.subscriber.edit',$subscriber->id)}}" class="btn btn-sm btn-primary {{$restrictions->btnDisabler('prepaid.subscriber.edit')}}">Edit</a>
                        <a href="{{$restrictions->btnRoute('prepaid.subscriber.send',$subscriber->id)}}" class="btn btn-sm btn-success {{$restrictions->btnDisabler('prepaid.subscriber.send')}}">Send an email</a>
                    </div>
                </div>
<!--                 <div class="sidebar-widget">
                    @if($subscriber->active)
                    <p>Active load remaining</p>
                    <canvas width="150" height="80" id="foo" class="" style="width: 250px; height: 100px;"></canvas>
                    <div class="goal-wrapper">
                        <span id="gauge-text" class="gauge-value pull-left">{{$subscriber->active['remaining']}}</span>
                        <span id="goal-text" class="goal-value pull-right">{{$subscriber->active['minutes']}}</span>
                    </div>
                    @else
                    <p>No Active load</p>
                    @endif
                </div> -->                
            </section>
        </div>

        @if($subscriber->active)
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="dashboard_graph x_panel">
                <div class="x_content">
                    @include('flash::message')
                    <div class="demo-container" style="height:280px">
                        <div id="placeholder33x" class="demo-placeholder"></div>
                    </div> 
                </div>
            </div>
        </div>  
        @elseif($subscriber->reserves && $subscriber->reserves['status'])
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="dashboard_graph x_panel">
                <div class="x_content">
                    <div class="demo-container" style="height:280px">

                            <div class="clearfix"></div>
                                <div id="page-wrapper">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                      <div class="x_title">
                                        <h2>Reserved Load Activated</h2>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="x_content">
                                        <div class="dashboard-widget-content">

                                          <div class="col-md-4 hidden-small">
                                            <table class="countries_list">
                                              <tbody>
                                                <tr>
                                                  <td>Total Reserved Load</td>
                                                  <td class="fs15 fw700 text-right">{{$subscriber->reserves['minutes']}}</td>
                                                </tr>
                                                <tr>
                                                  <td>Used</td>
                                                  <td class="fs15 fw700 text-right">{{$subscriber->reserves['used']}}</td>
                                                </tr>
                                                <tr>
                                                  <td>Used%</td>
                                                  <td class="fs15 fw700 text-right">{{round(($subscriber->reserves['used']/$subscriber->reserves['minutes'])*100)}}%</td>
                                                </tr>                        
                                                <tr>
                                                  <td>Remaining</td>
                                                  <td class="fs15 fw700 text-right">{{$subscriber->reserves['remaining']}}</td>
                                                </tr>
                                                <tr>
                                                  <td>Remaining %</td>
                                                  <td class="fs15 fw700 text-right">{{round(($subscriber->reserves['remaining']/$subscriber->reserves['minutes'])*100)}}%</td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </div>
                                         <div class="col-md-8">
                                            <div class="sidebar-widget">
                                                <canvas width="150" height="80" id="foo2" class="" style="width: 250px; height: 100px;"></canvas>
                                                <div class="goal-wrapper">
                                                    <span id="gauge-text" class="gauge-value pull-left">{{$subscriber->reserves['used']}}</span>
                                                    <span id="goal-text" class="goal-value pull-right">{{$subscriber->reserves['minutes']}}</span>
                                                </div>
                                            </div> 
                                         </div>

                                        </div>
                                      </div>
                                    </div>
                                </div>      
                            </div>
                    </div> 
                </div>
            </div>
        </div>          
        @endif      
    </div>
 
    <div class="row">
        <div class="col-md-12">
            
            <div class="" role="tabpanel" data-example-id="togglable-tabs">

                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Active Load</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content2" role="tab" data-toggle="tab"  aria-expanded="false">Overages</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content3" role="tab" data-toggle="tab" aria-expanded="false">Load Archives</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content4" role="tab" data-toggle="tab" aria-expanded="false">Notes</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content5" role="tab" data-toggle="tab" aria-expanded="false">Email</a>
                    </li> 
                    <li role="presentation" class=""><a href="#tab_content6" role="tab" data-toggle="tab" aria-expanded="false">Outbound</a>
                    </li>                                         
                </ul>

                <div id="myTabContent" class="tab-content">

                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Total Minutes</th>
                                    <th>Used</th>
                                    <th>Remaining</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if($subscriber->active)
                                <tr>
                                    <td>{{$subscriber->active['startdate']}}</td>
                                    <td>{{$subscriber->active['enddate']}}</td>
                                    <td>{{$subscriber->active['minutes']}}</td>
                                    <td>{{round($subscriber->active['used'], 2)}}</td>
                                    <td>{{($subscriber->active['remaining'] = 0) ? round($subscriber->active['remaining'], 2 ) : 0}}</td>
                                    <td style = "text-align:center;">
                                        <div class="container container-action" >
                                          <div class="row">

                                            <a href="{{$restrictions->btnRoute('prepaid.load.edit',$subscriber->active['id'])}}">
                                                <div class="col-sm-4">
                                                  <button type="button" class="btn btn-info glyphicon glyphicon-pencil {{$restrictions->btnDisabler('prepaid.load.edit')}}" aria-hidden="true"></button>
                                                  <br><span>Edit</span>
                                                </div>
                                            </a>

                                            <a href="{{$restrictions->btnRoute('prepaid.calls.load',$subscriber->active['id'])}}">
                                                <div class="col-sm-4">
                                                  <button type="button" class="btn btn-info glyphicon glyphicon-zoom-in {{$restrictions->btnDisabler('prepaid.calls.load')}}" aria-hidden="true"></button>
                                                  <br><span>View</span>
                                                </div>
                                            </a>

                                          </div>
                                        </div> 
                                        <br>
                                    </td>
                                </tr>
                                @else
                                <tr>no records found</tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Registered Minutes</th>
                                    <th>Remaining</th>
                                    <th>Cumulative</th>
                                    <th>Status</th>
                                    <th>Date Added</th>
                                </tr>
                            </thead>
                            <tbody>
                             @if(!empty($subscriber->reserves))
                              <?php $sum = 0;?>   

                                @if(!empty($overage))
                                  @foreach($overage as $list)
                                    <tr>
                                        <td>{{$list['minutes']}}</td>
                                        <td>{{$list['remaining']}}</td>
                                        <td>{{$list['sum']}}</td>
                                        <td>{{$list['status']}}</td>
                                        <td>{{$list['date']}}</td>
                                    </tr>              
                                  @endforeach
                                @endif

                             @else
                                    <tr>No records found</tr>
                             @endif
                            </tbody>
                        </table>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Minutes</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(!empty($subscriber->loads->toArray()))
                                    @foreach($subscriber->loads as $ctr => $loadv)
                                    <tr>
                                        <td>{{$loadv->id}}
                                        <td>{{$loadv->minutes}}</td>
                                        <td>{{$loadv->startdate}}</td>
                                        <td>{{$loadv->enddate}}</td>
                                        <td>
                                            <a href="{{$restrictions->btnRoute('prepaid.load.edit',$loadv->id)}}" class="{{$restrictions->btnDisabler('prepaid.load.edit')}} btn btn-success glyphicon-pencil glyphicon">
                                            </a>
                                            <a href="{{$restrictions->btnRoute('prepaid.calls.load',$loadv->id)}}" class="{{$restrictions->btnDisabler('prepaid.calls.load')}} btn btn-success glyphicon glyphicon-zoom-in">
                                            </a>                                           
                                        </td>
                                    </tr>
                                    @endforeach

                                @else
                                    <tr>No Records found</tr>
                                @endif

                            </tbody>
                        </table>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">

                       <a href="{{$restrictions->btnRoute('prepaid.notes.create',$subscriber->id)}}" class="btn btn-sm btn-success {{$restrictions->btnDisabler('prepaid.notes.create')}}">Add Notes</a>
                 
                      <ul class="list-unstyled timeline widget">
                         @if(!empty($notes)) 
                            @foreach($notes as $note)
                                <li>
                                  <div class="block">
                                    <div class="block_content">
                                      <h2 class="title">
                                        {{$note->content}}
                                      </h2>
                                      <div class="byline">
                                        <span>{{$note->created_at}} </span> by <a>{{(isset($users[$note->users_id])?$users[$note->users_id]:'')}}</a>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                            @endforeach
                        @endif

                      </ul>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_content5">
                        <button type="button" class="btn btn-primary fa fa-edit pull-right" data-toggle="modal" data-target=".emailerFunction"></button>
                        <div class="modal fade emailerFunction" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <form method="post" action="{{route('prepaid.emailer.update',$subscriber->id)}}"> 
                                    <input type="hidden" name="_method" value="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                      </button>
                                      <h4 class="modal-title">Edit</h4>

                                        <label >To * :</label>
                                        <textarea class="form-control" name="to" >{{old('to',($subscriber->emailers?$gn->formatter($subscriber->emailers['to']):''))}}</textarea>

                                        <label >Cc:</label>
                                        <textarea class="form-control" name="cc" >{{old('cc',($subscriber->emailers?$gn->formatter($subscriber->emailers['cc']):''))}}</textarea>

                                      <div style="height:20px;"></div>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      <button type="submit" class="btn btn-primary" style="width:auto;">Proceed</button>                                                    
                                    </div>
                                </form>                         
                            </div>
                          </div>
                        </div> 

                      <h4>to:</h4>
                      <ul class="list-unstyled timeline widget">
                        @if($subscriber->emailers)
                            @foreach(json_decode($subscriber->emailers['to']) as $to)<li>{{$to}}</li>@endforeach
                        @endif
                      </ul>

                      <h4>cc:</h4>
                      <ul class="list-unstyled timeline widget">
                        @if($subscriber->emailers)
                            @foreach(json_decode($subscriber->emailers['cc']) as $cc)<li>{{$cc}}</li>@endforeach
                        @endif                        
                      </ul> 

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="tab_content6">
                        <button type="button" class="btn btn-primary fa fa-plus pull-right" data-toggle="modal" data-target=".addOutboundLoad"></button>
                        <div class="modal fade addOutboundLoad" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <form method="post" action="{{route('prepaid.outboundload.store',$subscriber->id)}}"> 
                                    <input type="hidden" name="_method" value="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>

                                        <h4 class="modal-title">Add Load</h4>
                                        <div class="col-md-12 form-group has-feedback">
                                            <input name="startdate" type="text" class="datepicker form-control has-feedback-left" placeholder="Start Date">
                                            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-12 form-group has-feedback">
                                            <input name="minutes" type="text" class="form-control has-feedback-left" placeholder="Minutes">
                                            <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                        </div>

                                      <div style="height:20px;"></div>
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                      <button type="submit" class="btn btn-primary" style="width:auto;">Proceed</button>                                                    
                                    </div>
                                </form>                         
                            </div>
                          </div>
                        </div> 

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Status</th>
                                    <th>Minutes</th>
                                    <th>Remaining</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subscriber->outboundloads as $obload)
                                <tr>
                                    <td>{{($obload['status']?'active':'inactive')}}</td>
                                    <td>{{$obload['minutes']}}</td>
                                    <td>{{($obload['remaining']?$obload['remaining']:0)}}</td>
                                    <td>{{$obload['startdate']}}</td>
                                    <td>{{$obload['enddate']}}</td>
                                    <td>
                                        <a class="btn btn-primary fa fa-trash"></a>
                                        <a href="{{route('prepaid.outboundload.records',$obload->id)}}" class="btn btn-primary fa fa-file-text-o"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>

            </div>

        </div>
    </div>

</div>



@endsection 

<style>
    
    .div-margin {

        margin: 5px;

    }

    td, th {

        font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif;
        font-size: 13px;
        font-weight: 400;

    }



</style>

@section('footer-scripts')

    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/date.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="{{$asset}}gentella/js/flot/jquery.flot.resize.js"></script>

    <!-- gauge js -->
    <script type="text/javascript" src="{{$asset}}gentella/js/gauge/gauge.min.js"></script>
 

    <script type="text/javascript">
            var opts2 = {
              lines: 12, // The number of lines to draw
              angle: 0.15, // The length of each line
              lineWidth: 0.44, // The line thickness
              pointer: {
                length: 0.9, // The radius of the inner circle
                strokeWidth: 0.015, // The rotation offset
                color: '#000000' // Fill color
              },
              limitMax: true,   // If true, the pointer will not go past the end of the gauge
              colorStart: '#6FADCF',   // Colors
              colorStop: '#8FC0DA',    // just experiment with them
              strokeColor: '#E0E0E0',   // to see which ones work best for you
              generateGradient: true
            };
            var target2 = document.getElementById('foo2'); // your canvas element
            var gauge2 = new Gauge(target2).setOptions(opts2); // create sexy gauge!
            gauge2.maxValue = {{$subscriber->reserves['minutes']}}; // set max gauge value
            gauge2.animationSpeed = 32; // set animation speed (32 is default value)
            gauge2.set({{$subscriber->reserves['used']}}); // set actual value

    </script>





        <script type="text/javascript">
            //define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
            var chartColours = ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

            //generate random number for charts
            randNum = function () {
                return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
            }

            $(function () {

                var d1 = {!!$graphdata!!}; 
                console.log(d1);
                for (index = 0; index < d1.length; ++index) {
                    d1[index][0] = new Date(d1[index][0]).getTime();
                }

                var d1size = d1.length - 1;
                var chartMinDate = d1[0][0]; //first day
                var chartMaxDate = d1[d1size][0]; //last day

                var tickSize = [1, "day"];
                var tformat = "%d/%m/%y";
                 
                //graph options
                var options = {
                    grid: {
                        show: true,
                        aboveData: true,
                        color: "#3f3f3f",
                        labelMargin: 10,
                        axisMargin: 0,
                        borderWidth: 0,
                        borderColor: null,
                        minBorderMargin: 5,
                        clickable: true,
                        hoverable: true,
                        autoHighlight: true,
                        mouseActiveRadius: 100
                    },
                    series: {
                        lines: {
                            show: true,
                            fill: true,
                            lineWidth: 2,
                            steps: false
                        },
                        points: {
                            show: true,
                            radius: 4.5,
                            symbol: "circle",
                            lineWidth: 3.0
                        }
                    },
                    legend: {
                        position: "ne",
                        margin: [0, -25],
                        noColumns: 0,
                        labelBoxBorderColor: null,
                        labelFormatter: function (label, series) {
                            // just add some space to labes
                            return label + '&nbsp;&nbsp;';
                        },
                        width: 40,
                        height: 1
                    },
                    colors: chartColours,
                    shadowSize: 0,
                    tooltip: true, //activate tooltip
                    tooltipOpts: {
                        content: "%s: %y.0",
                        xDateFormat: "%m/%d",
                        shifts: {
                            x: -30,
                            y: -50
                        },
                        defaultTheme: false
                    },
                    yaxis: {
                        min: 0,
                    },
                    xaxis: {
                        mode: "time",
                        minTickSize: tickSize,
                        timeformat: tformat,
                        min: chartMinDate,
                        max: chartMaxDate
                    }
                };

                var plot = $.plot($("#placeholder33x"), [{
                    label: "Average Call",
                    data: d1,
                    lines: {
                        fillColor: "rgba(150, 202, 89, 0.12)"
                    }, //#96CA59 rgba(150, 202, 89, 0.42)
                    points: {
                        fillColor: "#fff"
                    }
                }], options);

            });
        </script>
        <!-- /flot --> 

@if(false)
     <script>
            var opts = {
              lines: 12, // The number of lines to draw
              angle: 0.15, // The length of each line
              lineWidth: 0.44, // The line thickness
              pointer: {
                length: 0.9, // The radius of the inner circle
                strokeWidth: 0.015, // The rotation offset
                color: '#000000' // Fill color
              },
              limitMax: true,   // If true, the pointer will not go past the end of the gauge
              colorStart: '#6FADCF',   // Colors
              colorStop: '#8FC0DA',    // just experiment with them
              strokeColor: '#E0E0E0',   // to see which ones work best for you
              generateGradient: true
            };
            var target = document.getElementById('foo'); // your canvas element
            var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
            gauge.maxValue = {{ $load['load'][0]->minutes }}; // set max gauge value
            gauge.animationSpeed = 32; // set animation speed (32 is default value)
            gauge.set({{$load['totalmin']}}); // set actual value


    </script>

    <style>

      .fontsize {

              font-size: 20px;
      }

      .container-action{

          width:50%;
          height:20px;
          margin-left:0px;
          margin-right: 0px;
      }

    .btn-info{

        padding: 2px 4px;

    }

    span{

        font-size: 11px;

    }

    .tile_count .tile_stats_count {
        border-left: 0px solid #333;
        padding: 0;
        border-radius: 8px;
        margin: 1px;
    }

    .mb-custom .tile_count{
        height: 50px;
        margin-top:0px;
        margin-bottom: 0px;
    }
    .mb-custom .tile_stats_count .right{
        height: auto;
    }

    .cd-breadcrumb.triangle li.current > * {
        color: #ffffff;
        background-color: #008A52;
        border-color:#008A52;
    }

    .cd-breadcrumb.triangle li.current > * {
        color: #ffffff;
        background-color: #008A52;
        border-color: #337ab7;
    }



    

</style>


@endif

@endsection