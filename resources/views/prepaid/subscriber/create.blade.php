 @extends('prepaid.master')

@section('title', 'Create Subscriber')

@section('content')
{!! Breadcrumbs::render('SubscriberAdd') !!}
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                 <div class="x_panel">
                    @include('alert.error_list')
                    <div class="x_title col-md-12 ">
                        <h2>Create Subscriber</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <br />
                        <form class="form-horizontal form-label-left input_mask" method="post" action="{{$restrictions->btnRoute('prepaid.subscriber.store')}}">
                            <input type="hidden" name="_method" value="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                            <input type="hidden" name="status" value = "1"   >

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Client</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input name="client" type="text" class="form-control has-feedback-left" >
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Program Code</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input name="alias" type="text" class="form-control has-feedback-left" >
                                <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input name="email" type="text" class="form-control has-feedback-left" >
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                              </div>
                            </div>
                                             
                            <div class="ln_solid"></div>
                            <div class="form-group" >
                              <div class="col-md-9 col-sm-9 col-xs-12 pull-right" style ="text-align:center; width:50%;">
                                <button type="submit" class="btn btn-success">Submit</button>
                              </div>
                            </div>

<!--                             <div class="col-md-12 form-group has-feedback">
                                <input name="email" type="text" class="form-control has-feedback-left" placeholder="Email">
                                <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                                <br><br>
                            </div>

                            
                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 "></div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                                        <button type="submit" class="btn btn-success {{$restrictions->btnDisabler('prepaid.subscriber.store')}}">Submit</button>
                                    </div>
                                </div>
                             -->
                        </form>
                    </div>
                </div>               
            </div>
        </div>

    </div>
@endsection  