<?php $asset = $_SERVER["DOCUMENT_ROOT"] ."/";?> 

<style>

    .logoimg{

        width: 5000%;
        margin-top: 50px;

    }

    span {

        font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif; 

    }

    .tableData {

        margin: auto;
        padding: 5px;
        width: 90%;
        text-align: center;

    }

    .tableDataTD {

        border: 1px solid;
        border-collapse: collapse;

    }

    .tableDataTotal {

        border: 0px solid;

    }


</style>

<head>

    <title>Magellan Prepaid | </title>
     <link href="localhost:8000/gentella/css/floatexamples.css" rel="stylesheet" type="text/css" />
    
</head>

<div class="container">
    <div class="panel panel-default" style = "padding:15px;">
        <div class="row">
            <div class = "container">

                <table>
                    <tr>
                        <td style = "width: 75%;">
                            <div style = " width: 180%; background-color:#0EBFE9 ; padding:9px;  margin-top: 0px; text-align: right;" >
                                <I><span style = "color:white; margin: 20px; ">See The Future Your Way</span></I>
                            </div>
                        </td>
                        <td style = "width: 25%;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                            <img src="{{$asset}}gentella/images/logo2.png" alt="..." class=" logoimg " style ="width: 5000%; margin-top:0px;" >
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <br>
                
        <div class="row">
            <div class="col-xs-12 col-md-6">
            </div>
            <div class="col-xs-6 col-md-6 animated bounceInUp" style = "text-align: right;">
                <span><b>Magellan Solutions</b> <br>
                33rd Floor, Summit One Tower,<br>
                Shaw Blvd, Mandaluyong, 1552 Metro Manila
                </span>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div style = "background-color:black ;  " class = "dd animated bounceInLeft">
                        <I><span style = "color:white; margin: 20px; ">Call Log Information</span></I>
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-xs-12 col-md-8 animated bounceInUp">
                <div style = "  margin-left:20px;">
                    <b>Client Name : </b> {{ $subscribers['client'] }} <br>
                    <b>Caller Id : </b>  {{ $loads[0]->phone_number }} <br>
                    <b>Email : </b>  {{ $subscribers['email'] }} <br>
                </div>
            </div>
        </div>

        <br>

        <div style = "background-color:black ;  " class ="dd animated bounceInRight">
            <I><span style = "color:white; margin: 20px; ">Call List</span></I>
        </div>

        <br>

        @if(!empty($loads))

        <div >
           <table class = "tableData tableDataTD" style = "padding: 5px;" >
                <thead >
                    <tr>
                        <th class = "tableDataTD">#</th>
                        <th class = "tableDataTD">user</th>
                        <th class = "tableDataTD">call date</th>
                        <th class = "tableDataTD">phone number</th>
                        <th class = "tableDataTD">talk time</th> 
                        <th class = "tableDataTD">wrap time</th>
                        <th class = "tableDataTD">total handled</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php ?>
                    <?php $ctr = 0; ?>
                    <?php $sumTalk = 0; ?>
                    <?php $sumWrap = 0; ?>
                    <?php $sumhandle = 0; ?>
                    @foreach($loads as $load)

                    <?php $ctr++; ?>
                    <?php $sumTalk = ($load->talk_Sec/60) + $sumTalk; ?>
                    <?php $sumWrap = ($load->dispo_Sec/60) + $sumWrap; ?>
                    <?php $sumhandle = ($load->seconds/60) + $sumhandle; ?>

                    <tr>
                        <th scope="row" class = "tableDataTD">{{$ctr}}</th>
                        <td class = "tableDataTD">{{ $load->user }} </td>
                        <td class = "tableDataTD">{{ $load->call_date }} </td>
                        <td class = "tableDataTD">{{ $load->phone_number }} </td>
                        <td class = "tableDataTD">{{ number_format((float)($load->talk_Sec/60), 2, '.', '') }} </td>
                        <td class = "tableDataTD">{{ number_format((float)($load->dispo_Sec/60), 2, '.', '') }}</td>
                        <td class = "tableDataTD">{{ number_format((float)($load->seconds/60), 2, '.', '') }}</td>
                    </tr>

                    @endforeach

                    
                </tbody>
            </table>
        </div>

        @else

        <a class="btn btn-app">
            <i class="fa fa-times"></i> No Record Found!
        </a> 

        @endif 

        @if(!empty($loads))

        <br>

        <table class = "tableData tableDataTD tableDataTotal">
            <tr>
                <td class = "tableDataTD tableDataTotal" style = "width:57%; text-align:left;">
                    <b>Total : </b>
                </td>
                <td class = "tableDataTD tableDataTotal" style = "width:13%;">
                    <b> {{ number_format((float)($sumTalk), 2, '.','')}} </b>
                </td>
                <td class = "tableDataTD tableDataTotal" style = "width:14%;">
                     <b> {{ number_format((float)($sumWrap), 2, '.','')}} </b>
                </td>
                <td class = "tableDataTD tableDataTotal" style = "width27%;">
                     <b> {{ number_format((float)($sumhandle), 2, '.','')}}  </b>
                </td>
            </tr>
        </table>

        <br><br>

        <table class = "tableData tableDataTD tableDataTotal">
            <tr>
                <td class = "tableDataTD tableDataTotal" align="right">
                    <I>*This report is System Generated.</I>
                </td>
            </tr>
        </table>

        @endif 

    </div>
                    
    </form>
        
</div>



