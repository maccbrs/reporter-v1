@extends('Backend.master')


@section('content')

<style>

    .btn-info{

        padding: 2px 4px;

    }

    span{

        font-size: 11px;

    }

    .container-action{

        width:70%;
        height:30px;
        margin-left:0px;
        margin-right: 0px;
    }

</style> 

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Calls using overage load</div>
                    <div class="panel-body">
                        <div class="bs-example" data-example-id="bordered-table">
                            @if($calls)
                            <table class="table table-bordered">

                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>call date</th>
                                        <th>caller id</th>
                                        <th>talk time</th> 
                                        <th>wrap time</th>
                                        <th>total handled</th> 
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($calls as $call)
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>{{ $call->call_date }}</td>
                                        <td>{{ $call->phone_number}}</td>
                                        <td>{{ number_format((float)($call->talk_Sec/60), 2, '.', '') }} </td>
                                        <td>{{ number_format((float)($call->dispo_Sec/60), 2, '.', '') }}</td>
                                        <td>{{ number_format((float)($call->seconds/60), 2, '.', '') }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                 
                            </table>
                           @else
                            <a class="btn btn-app">
                                <i class="fa fa-times"></i> No Record Found!
                            </a>                            
                           @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
