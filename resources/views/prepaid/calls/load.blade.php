@extends('prepaid.master')

<?php $asset = URL::asset('/'); ?> 
@section('content')

<style>

    body{

        color:black;
    }

    .btn-info{

        padding: 2px 4px;
    }

    span{

        font-size: 11px;

    }

    .container-action{

        width:70%;
        height:30px;
        margin-left:0px;
        margin-right: 0px;
    }

    .img-circle.profile_img {
        width: 70%;
        background: #fff;
        margin-left: 15%;
        z-index: 1000; 
        position: inherit;
        margin-top: 20px;
        border: 0px;
        padding: 4px;
    }

    #content{ 

        background:#FFF; min-height:400px; padding:20px;
        border-radius:2px; 
        -moz-border-radius:2px; 
        -webkit-border-radius:2px;
    }

    .divAni{ 
        border-radius:2px; 
        -moz-border-radius:2px; 
        -webkit-border-radius:2px; 
    }

    .tableProd{

        padding: 15px;
        border-color: black;
        border-radius: 10px;
        border: 1px solid black;
        margin: 20px;
    }

    table, th, td {
        border: 1px solid black;
        padding: 5px;
    }

    th {

       background-color: #ADD8E6;
    }

    .dateday{

        border-bottom: 1px solid black;
    }

    .pdfDateRange{
    
    width: 65%;
    display: table;
    margin: 0 auto;
    
    }

    .daterangepdf{

        border-radius: 4px;
        background-color: snow;
    }

    .column-size{
        font-size: 75%;
    }

    /*xxxxxxxxxx*/

    #window {
    width: 500px;
    height: 400px;
    min-width: 100px;
    min-height: 100px;
    border-bottom: 1px solid gray;
    }

    #header {
        height: 25px;
        line-height: 25px;
        background-color: gray;
        color: white;
        text-align: center;
    }
    table {
      
        width: 100%;
        border-collapse: collapse;
        margin-bottom: 25px;
    }
    #tableContainer {
        top: 25px;
        bottom: 0px;
        left: 0px;
        right: 0px;
        margin: 0;
        padding: 0;
        overflow: auto;
    }
    td {
        padding: 5px;
        margin: 0px;
        border: 1px solid gray;
    }
    tr:last-child td {
        border-bottom: none;
    }


</style> 



<div class="row">
    @include('flash::message')

        <div class="x_title">
            <h2><i class="fa fa-bars"></i> Report</h2>
            <ul class="nav navbar-right panel_toolbox">
                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                  @include('alert.errorlist')
                  <div class="x_title" style = "text-align:center;">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">

                        <ul  class="nav nav-tabs bar_tabs" role="tablist">

                          <li role="presentation" class="active">
                            <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Call Details</a>
                          </li>

                          <li role="presentation" class="">
                            <a href="#tab_content2" role="tab" data-toggle="tab" aria-expanded="false">Daily Connectime - Min</a>
                          </li>

                          <li role="presentation" class="">
                            <a href="#tab_content3" role="tab" data-toggle="tab" aria-expanded="false">Daily Connectime - Sec</a>
                          </li>

                          <li role="presentation" class="">
                            <a href="#tab_content4" role="tab" data-toggle="tab" aria-expanded="false">Daily Crc</a>
                          </li> 

                          <li role="presentation" class="">
                            <a href="#tab_content5" role="tab" data-toggle="tab" aria-expanded="false">Daily Crc %</a>
                          </li> 

                        </ul>

                        <div id="myTabContent" class="tab-content">

                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                             <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>AgentName</th>
                                    <th>CallDate</th>
                                    <th>PhoneNum</th>
                                    <th>Campaign</th>
                                    <th>CRC</th>
                                    <th>ConnectTime</th>
                                    <th>TalkTime</th>
                                    <th>WrapTime</th> 
                                  </tr>
                                </thead>
                                <tbody>

                                  @if($result->details2)

                                    @foreach($result->details2 as $data)
                                      @if($data->closer2)
                                      <tr>
                                        <td>{{$data->closer2['user']}}</td>
                                        <td>{{$data->closer2['call_date']}}</td>
                                        <td>{{$data->closer2['phone_number']}}</td>
                                        <td>{{$data->closer2['campaign_id']}}</td>
                                        <td>{{$data->closer2['status']}}</td>

                                        <?php $connect_time = (isset($data->closer2['agent']['talk_sec'])?$data->closer2['agent']['talk_sec']:0)+(isset($data->closer2['agent']['dispo_sec'])?$data->closer2['agent']['dispo_sec']:0)?>
                                        <td>{{$gn->roundsix($connect_time)}}</td>
                                        <td>{{(isset($data->closer2['agent']['talk_sec'])?$data->closer2['agent']['talk_sec']:0)}}</td>
                                        <td>{{(isset($data->closer2['agent']['dispo_sec'])?$data->closer2['agent']['dispo_sec']:0)}}</td>
                                      </tr>
                                      @endif
                                    @endforeach

                                  @endif
                                
                                </tbody>
                              </table>
                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                               <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th>Day</th>
                                      <th>Date</th>
                                      <th>ConnectTime</th>
                                      <th>TalkTime</th>
                                      <th>WrapTime</th>
                                    </tr>
                                  </thead>
                                  <tbody> 
                                    @if($summary)
                                      @foreach($summary as $k => $sum)
                                      <tr>
                                        <td>{{$sum['day']}}</td>
                                        <td>{{$k}}</td>
                                        <td>{{$sum['totalm']}}</td>
                                        <td>{{$sum['talkm']}}</td>
                                        <td>{{$sum['wrapm']}}</td>
                                      </tr>
                                      @endforeach
                                    @endif                                        
                                  </tbody>
                              </table>

                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">

                               <table class="table table-bordered">
                                <thead>
                                  <tr>
                                      <th>CRC</th>
                                    @foreach($dates as $k => $d)
                                      <th>{{$k}}</th>
                                    @endforeach
                                  </tr>
                                  <tr>
                                      <th></th>
                                    @foreach($dates as $k => $d)
                                      <th>{{$d}}</th>
                                    @endforeach
                                  </tr>                                        
                                </thead>
                                <tbody>
                                  @if($crc)
                                    @foreach($crc as $k => $d)
                                    <tr>
                                      <td>{{$k}}</td>
                                      @foreach($d as $v)
                                      <td>{{$v['data']['count']}}</td>
                                      @endforeach
                                    </tr>
                                    @endforeach

                                    <?php $cnt = 0; ?>
                                    @foreach($crc as $crc2)
                                      @if($cnt > 0) <?php break; ?> @endif
                                        <tr>
                                          <td>total</td>
                                          @foreach($crc2 as $k => $d)
                                          <td>{{$d['data']['totalcounts']}}</td>
                                          @endforeach
                                        </tr>
                                      <?php $cnt++; ?>
                                    @endforeach
                                  @endif
                                </tbody>
                              </table>

                          </div>

                          <div role="tabpanel" class="tab-pane fade" id="tab_content4" aria-labelledby="profile-tab">

                               <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Day</th>
                                    <th>Date</th>
                                    <th>ConnectTime</th>
                                    <th>TalkTime</th>
                                    <th>WrapTime</th>
                                  </tr>
                                </thead>
                                <tbody> 
                                  @if($summary)
                                    @foreach($summary as $k => $sum)
                                    <tr>
                                      <td>{{$sum['day']}}</td>
                                      <td>{{$k}}</td>
                                      <td>{{$sum['total']}}</td>
                                      <td>{{$sum['talk']}}</td>
                                      <td>{{$sum['wrap']}}</td>
                                    </tr>
                                    @endforeach
                                  @endif                                        
                                </tbody>
                              </table>

                          </div> 

                          <div role="tabpanel" class="tab-pane fade" id="tab_content5" aria-labelledby="profile-tab">

                               <table class="table table-bordered">
                                <thead>
                                  <tr>
                                      <th>CRC</th>
                                    @foreach($dates as $k => $d)
                                      <th>{{$k}}</th>
                                    @endforeach
                                  </tr>
                                  <tr>
                                      <th></th>
                                    @foreach($dates as $k => $d)
                                      <th>{{$d}}</th>
                                    @endforeach
                                  </tr>    
                                </thead>
                                <tbody>
                                  @if($crc)
                                    @foreach($crc as $k => $d)
                                    <tr>
                                      <td>{{$k}}</td>
                                      @foreach($d as $v)
                                      <td>{{$v['data']['percent']}}%</td>
                                      @endforeach
                                    </tr>
                                    @endforeach
                                  @endif  
                                </tbody>
                              </table>

                          </div> 

                        </div>
                      </div>
                      <br>
                      <div class="clearfix"></div>
                  </div>
            </div>
        </div>
</div>

@endsection
@section('header-scripts')
    <style type="text/css">
    .cchart{
        padding: 5px;
        background-color: #0F9D58;  
        border-radius: 1px;
        color: #fff;     
    }
    .fst{
        background-color: #ee8823;
    }
    .ptd{
        background-color: #589796;
    }
    #calendar{
        margin-top: 11px;
    }
    #tab5:checked ~ #content5 {
      display: block;
    }
    #content4,#content5{
      overflow: scroll;
    }
    </style>
@endsection
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript" src="project.js"></script>
@section('footer-scripts')
<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>
@endsection


