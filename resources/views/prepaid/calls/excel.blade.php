<?php $asset = $_SERVER["DOCUMENT_ROOT"] ."/";?> 


<html>
   
    <table>
        <td></td>
        <td></td>
        <td>
            <span><b>Magellan Solutions</b></span>
        </td>
    </table>

    <table>
        <td></td>
        <td></td>
        <td>
            33rd Floor, Summit One Tower,
        </td>
    </table>   

    <table>
        <td></td>
        <td></td>
        <td>
            Shaw Blvd, Mandaluyong, 1552 Metro Manila
        </td>
    </table> 

    <br>

    <table>
        <td>
            Call Log Information
        </td>
    </table> 

    <br>

    <table>
        <td>
            <b>Client Name : </b> {{ $subscribers['client'] }} <br>
        </td>
    </table> 

    <table>
        <td>
            <b>Caller Id : </b>  {{ $loads[0]->phone_number }} <br>
        </td>
    </table> 

    <table>
        <td>
            <b>Email : </b>  {{ $subscribers['email'] }} <br>
        </td>
    </table> 

    <br>

     <table>
        <td>
            Call List
        </td>
    </table> 

    <br>

    @if(!empty($loads))

        <div >
           <table >
                <thead>
                    <tr>
                        <th align = "center">#</th>
                        <th align = "center">user</th>
                        <th align = "center">call date</th>
                        <th align = "center">phone number</th>
                        <th align = "center">talk time</th> 
                        <th align = "center">wrap time</th>
                        <th align = "center">total handled</th> 
                    </tr>
                </thead>
                <tbody>
                    <?php ?>
                    <?php $ctr = 0; ?>
                    <?php $sumTalk = 0; ?>
                    <?php $sumWrap = 0; ?>
                    <?php $sumhandle = 0; ?>
                    @foreach($loads as $load)

                    <?php $ctr++; ?>
                    <?php $sumTalk = ($load->talk_Sec/60) + $sumTalk; ?>
                    <?php $sumWrap = ($load->dispo_Sec/60) + $sumWrap; ?>
                    <?php $sumhandle = ($load->seconds/60) + $sumhandle; ?>

                    <tr>
                        <th align = "center">{{$ctr}}</th>
                        <td align = "center">{{ $load->user }} </td>
                        <td align = "center">{{ $load->call_date }} </td>
                        <td align = "center">{{ $load->phone_number }} </td>
                        <td align = "center">{{ number_format((float)($load->talk_Sec/60), 2, '.', '') }} </td>
                        <td align = "center">{{ number_format((float)($load->dispo_Sec/60), 2, '.', '') }}</td>
                        <td align = "center">{{ number_format((float)($load->seconds/60), 2, '.', '') }}</td>
                    </tr>

                    @endforeach

                    
                </tbody>
            </table>
        </div>

        @else

        <a>
            <i ></i> No Record Found!
        </a> 

        @endif 

        <br>

        <table >
            
            <td>
                <b>Total : </b>
            </td>

            <td></td>
            <td></td>
            <td></td>

            <td align = "center">
                <b>{{ number_format((float)($sumTalk), 2, '.','')}} </b>
            </td>
            <td align = "center">
                <b>{{ number_format((float)($sumWrap), 2, '.','')}} </b>
            </td>
            <td align = "center">
                <b> {{ number_format((float)($sumhandle), 2, '.','')}}  </b>
            </td>
            
        </table>

        <br><br>

        <table >

            <tr>
                <td></td>
                <td></td>
                <td align="right">
                   <center><I>*This report is System Generated. </I></center>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
</html>