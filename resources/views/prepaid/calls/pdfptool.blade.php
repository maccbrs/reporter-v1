<?php $asset = $_SERVER["DOCUMENT_ROOT"] ."/";?> 

<style>

    .logoimg{

        width: 5000%;
        margin-top: 50px;

    }

    span {

        font-family: "Helvetica Neue", Roboto, Arial, "Droid Sans", sans-serif; 

    }

    .tableData {

        margin: auto;
        padding: 5px;
        width: 90%;
        text-align: center;

    }

    .tableDataTD {

        border: 1px solid;
        border-collapse: collapse;

    }

    .tableDataTotal {

        border: 0px solid;

    }

    .marginThis{

        padding:7px;
    }

    .loadtable { 
        border: 2px solid black 
    }

    .loadtd { 
        border: thin solid black 
    }


</style>

<head>
    <title>Magellan Prepaid | </title>
     <link href="localhost:8000/gentella/css/floatexamples.css" rel="stylesheet" type="text/css" />
</head>

<div class="container">
    <div class="panel panel-default" style = "padding:15px;">
        <div class="row">
            <div class = "container">

                <table>
                    <tr>
                        <td style = "width: 75%;">
                            <div style = " width: 290%; background-color:#0EBFE9 ; padding:9px;  margin-top: 0px; text-align: right;" >
                                <I><span style = "color:white; margin: 20px; ">See The Future Your Way</span></I>
                            </div>
                        </td>
                        <td style = "width: 25%;">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;
                        <img src="{{$asset}}gentella/images/logo2.png" alt="..." class=" logoimg " style ="width: 5000%; margin-top:0px;" >
                            
                        </td>

                    </tr>
                </table>
            </div>
        </div>
        
        <br>
               
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div style = "background-color:black ;  " class = "dd animated bounceInLeft">
                        <I><span style = "color:white; margin: 20px; ">Call Log Information</span></I>
                </div>
            </div>
        </div>

        <br>

        <div class="row">
            <div class="col-xs-12 col-md-8 animated bounceInUp">
                <div style = "  margin-left:20px;">
                    <b>Client Name : </b> {{ $subscribers->client }} <br>
                    <b>Caller Id : </b>  {{ $loads[0]->phone_number }} <br>
                    <b>Email : </b>  {{ $subscribers->email }} <br>
                </div>
            </div>
        </div>

        <br>

        <div style = "background-color:black ;  " class ="dd animated bounceInRight">
            <I><span style = "color:white; margin: 20px; ">Call List</span></I>
        </div>

        <br>

        <div class="row">
            <div class="col-xs-12 col-md-8 animated bounceInUp">
                <div style = "  margin-left:20px;">
                    
                        @foreach($dataHolder as $datakey =>$data)
                            <table class = "loadtable">
                                <tr>

                                <?php $ctr = 0;?>
                                @foreach($data as $datakey2 =>$data2)
                                    @if($ctr == 0)
                                        <td class = "loadtd" align ="center"><b>#</b></td>
                                        <td class = "loadtd" align ="center"><b>CRC</b></td>
                                        <td class = "loadtd" align ="center"><b>Description</b></td>
                                    @endif
                                   <td class = "loadtd" class = "marginThis" align = "center"><b>{{$data2['date']}} <br> {{$data2['day']}}</b></td>                                
                                    <?php $ctr++;?>
                                @endforeach
                                </tr>

                                
                                @foreach($data2['count'] as $count => $countList)
                                    <tr>
                                        
                                        <td class = "loadtd" align ="center" style = "padding:2px;"><b>&nbsp;&nbsp;&nbsp;{{$count + 1}}</b></td>
                                        <td class = "loadtd" align ="center">&nbsp;&nbsp;{{$statusHolder[$count]['status']}} &nbsp;&nbsp;</td>
                                        <td class = "loadtd" align ="center">&nbsp;&nbsp;{{$statusHolder[$count]['status_name']}}&nbsp;&nbsp;</td>
                                        
                                        @foreach($data as $datakey2 =>$data2)
                                            

                                            <td class = "loadtd marginThis" align = "center">{{$data2['count'][$count]}}</td>               
                                            
                                        @endforeach
                                    </tr>
                                    
                                @endforeach

                                <tr>
                                    <?php $ctr3 = 0;?>
                                    @foreach($data as $datakey2 =>$data2)
                                        @if($ctr3 == 0)
                                            <td colspan = "3" align = "center" class = "loadtd"><b>Total</b></td>
                                        @endif
                                       <td class = "marginThis" align = "center" class = "loadtd"><b>{{ array_sum($data2['count']) }}<b></td>                              
                                        <?php $ctr3++;?>
                                    @endforeach
                                    
                                </tr>
                            </table>

                            <br><br>
                        @endforeach
                </div>
            </div>
        </div>

        <div align = "right"><I>*This Report is System Generated</I></div>

        <br>

    </div>
                    
    </form>
        
</div>
<?php// exit;?>


