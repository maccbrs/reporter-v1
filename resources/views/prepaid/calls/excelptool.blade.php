<?php $asset = $_SERVER["DOCUMENT_ROOT"] ."/";?> 

<html>
    
    <table>
        <td>
            <span><b>Magellan Solutions</b></span>
        </td>
    </table>

    <table>
        <td>
            33rd Floor, Summit One Tower,
        </td>
    </table>   

    <table>
        <td>
            Shaw Blvd, Mandaluyong, 1552 Metro Manila
        </td>
    </table> 

    <br>

    <table>
        <td>
            Call Log Information
        </td>
    </table>

    <br>

    <table>
        <td>
            <b>Client Name : </b> {{ $subscribers->client }} <br>
        </td>
    </table> 

    <table>
        <td>
            <b>Caller Id : </b>  {{ $loads[0]->phone_number }} <br>
        </td>
    </table> 

    <table>
        <td>
            <b>Email : </b>  {{ $subscribers->email }} <br>
        </td>
    </table> 

    <br>

     <table>
        <td>
            Call List
        </td>
    </table> 


    <table >

        <tr>
            <td></td>
            <th align = "center">#</th>
            <th align = "center">CRC</th>
            <th align = "center" bg-color = "blue">Description</th>

            @foreach($dates as $date)

            <td align = "center" style = "background-color:#2B60DE;" >
               {{$date}}          
            </td>

            @endforeach

        </tr>
        <tr>
            <td></td>
            <th></th>
            <th></th>
            <th></th>
            @foreach($dates as $date)

            <td align = "center" style = "background-color:#659EC7;">
               {{date('D', strtotime($date))}}         
            </td>

            @endforeach
        </tr>
    </table>

    <table>
        <?php $ctr = 1;?>
        @if(!empty($loadList))
            @foreach($loadList as $key => $load)  
                <tr>
                    <td></td> 
                    <td align = "center">{{$ctr }}</td>
                    <td align = "center">{{$load['status'] }}</td>
                    <td align = "center">{{$load['status_name'] }}</td>
                    @foreach($dates as $key2 => $date)
                        <td align = "center">{{count(array_filter($load['date'][$date])) }}</td> 
                        <?php $sums[$date][$key] = count(array_filter($load['date'][$date]));?>         
                    @endforeach 
                </tr>
            <?php $ctr++;  ?>
            @endforeach


        <tr>
            <td></td>
            <td align = "center" colspan = 3><b>Total</b></td>
            
            <?php foreach($sums as $sumkey => $sum){ ?>

                <td align = "center"><b>{{ array_sum($sum) }}</b></td> 
            <?php } ?>
        </tr>

        @endif
    </table>
   
</html>