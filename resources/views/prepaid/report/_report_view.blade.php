<?php $asset = URL::asset('/'); ?> 

<div class="" role="tabpanel" data-example-id="togglable-tabs">
    
    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Call Log Report</a>
        </li>
        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Productivity Report</a>
        </li>
    </ul>

    <div id="myTabContent" class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
            
            <br>

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6"></div>
                    
                    <a href="{{$restrictions->btnRoute('prepaid.calls.pdf', $parameters = array($load_id,$subscribers,$startdate,$enddate ))}}" class="btn btn-primary pull-right {{$restrictions->btnDisabler('prepaid.calls.pdf')}}" >
                            <i class="glyphicon glyphicon-list-alt"></i> PDF</a> 

                    <a href="{{$restrictions->btnRoute('prepaid.calls.excel', $parameters = array($load_id,$subscribers,$startdate,$enddate))}}" class="btn btn-primary pull-right {{$restrictions->btnDisabler('prepaid.calls.excel')}}" >
                            <i class="glyphicon glyphicon-list-alt"></i> Excel</a>
                </div>
            </div>

            <br> 

             <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default" style = "padding:15px; border-color: black;">
                        <div class="row">
                            <div class = "container" style = "max-width: 100%; height: auto;">
                                <div class="col-xs-12 col-md-8">
                                    <div style = "background-color:#0EBFE9 ; padding:9px;  margin-top: 58px; text-align: right;" class = "dd animated bounceInLeft">
                                        <I><span style = "color:white; margin: 20px; ">See The Future Your Way</span></I>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-md-2 dd animated bounceInRight"><img src="{{$asset}}gentella/images/logo2.png" alt="..." class="img-circle profile_img" style ="width:223%; margin-left: -4px; margin-top: 30px;"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                            </div>
                            <div class="col-xs-6 col-md-6 animated bounceInUp" style = "text-align: right;">
                                <b>Magellan Solutions</b> <br>
                                33rd Floor, Summit One Tower,<br>
                                Shaw Blvd, Mandaluyong, 1552 Metro Manila
                            </div>
                            <br><br>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div style = "background-color:black ;  " class = "dd animated bounceInLeft">
                                        <I><span style = "color:white; margin: 20px; ">Call Log Information</span></I>
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-md-8 animated bounceInUp">
                                <div style = "  margin-left:20px;">
                                    <b>Client Name : </b> &nbsp &nbsp&nbsp&nbsp&nbsp {{ $subscriberData->client }} <br>
                                    <b>Caller Id : </b> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp {{ (isset($loads[0]->phone_number)?$loads[0]->phone_number:'') }} <br>
                                    <b>Email : </b> &nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp {{ $subscriberData->email}} <br>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-xs-12 col-md-12" >
                                <div style = "background-color:black ;  " class ="dd animated bounceInRight">
                                    <I><span style = "color:white; margin: 20px; ">Call List</span></I>
                                </div>

                                <br>

                            <?php if(!empty($loads)){ ?>
                                <div class="bs-example animated bounceInUp " data-example-id="bordered-table" id ="content">
                                   <table class="table table-bordered" style ="border-color: black;">
                                        <thead >
                                            <tr>
                                                <th  style ="border-color: black;">#</th>
                                                <th  style ="border-color: black;">user</th>
                                                <th  style ="border-color: black;">call date</th>
                                                <th  style ="border-color: black;">phone number</th>
                                                <th  style ="border-color: black;">talk time</th> 
                                                <th  style ="border-color: black;">wrap time</th>
                                                <th  style ="border-color: black;">total handled</th> 
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php ?>
                                            <?php $ctr = 0; ?>
                                            <?php $sumTalk = 0; ?>
                                            <?php $sumWrap = 0; ?>
                                            <?php $sumhandle = 0; ?>

                                            <?php foreach($loads as $load){ ?>

                                                <?php $ctr++; ?>
                                                <?php $sumTalk = ($load->talk_Sec/60) + $sumTalk; ?>
                                                <?php $sumWrap = ($load->dispo_Sec/60) + $sumWrap; ?>
                                                <?php $sumhandle = ($load->seconds/60) + $sumhandle; ?>

                                                <tr>
                                                    <td scope="row" style ="border-color: black;"><?php echo $ctr ?></th>
                                                    <td style ="border-color: black;"><?php echo $load->user ?></td>
                                                    <td style ="border-color: black;"><?php echo $load->call_date ?>  </td>
                                                    <td style ="border-color: black;"><?php echo $load->phone_number ?></td>
                                                    <td  style ="border-color: black;"><?php echo number_format((float)($load->talk_Sec/60), 2, '.', '') ?> </td>
                                                    <td  style ="border-color: black;"><?php echo number_format((float)($load->dispo_Sec/60), 2, '.', '') ?></td>
                                                    <td  style ="border-color: black;"><?php echo number_format((float)($load->seconds/60), 2, '.', '') ?></td>
                                                </tr>

                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php }else{ ?>
                                <a class="btn btn-app">
                                    <i class="fa fa-times"></i> No Record Found!
                                </a> 
                            <?php }?>
                            </div>
                        </div>

                        @if(!empty($loads))

                        <div class="row">
                            <div class="col-xs-12 col-md-7">
                                <div style = "text-align:left;">
                                    <b>Total</b><br>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-1">
                                <div style = "text-align:left;">
                                    <b>{{ number_format((float)($sumTalk), 2, '.','')}}</b><br>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div style = "text-align:left;">
                                    <b>{{ number_format((float)($sumWrap), 2, '.','')}}</b><br>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-2">
                                <div style = "text-align:left;">
                                    <b>{{ number_format((float)($sumhandle), 2, '.','')}}</b><br>
                                </div>
                            </div>
                        </div>

                        @endif 

                        <br><br>

                        <div class="row">
                            <div class="col-xs-12 col-md-5">
                                <div style = "text-align:left;">
                                    <b>Contact Us</b><br>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                            <div class="col-xs-6 col-md-4" >
                                <div style = "text-align:left;">
                                    <a href="http://www.magellan-solutions.com/" target="_blank"  class="btn btn-laptop" style ="margin-bottom:0px; ; padding-bottom:0px; color: black;"><i class="fa fa-laptop"></i> 
                                        <span style = "font-style:5px; color: black;">www.magellan-solutions.com<span>
                                    </a>
                                   <br>
                                </div>
                            </div>

                            <div class="col-xs-6 col-md-4" >
                                <div style = "text-align:left;">
                                    <a href="https://www.facebook.com/magellansolutions/" target="_blank" class="btn btn-facebook" style ="margin-bottom:0px; color: black; padding-bottom:0px;"><i class="fa fa-facebook"></i> 
                                        <span style = "font-style:5px; font-style:5px; color: black;">www.facebook.com/magellansolutions<span>
                                    </a> 
                                   <br>
                                </div>
                            </div>

                            <div class="col-xs-6 col-md-4" >
                                <div style = "text-align:left;">
                                    <a href="https://www.facebook.com/magellansolutions/" target="_blank" class="btn btn-facebook" style ="margin-bottom:0px; color: black; padding-bottom:0px;"><i class="fa fa-linkedin-square"></i> 
                                        <span style = "font-style:5px; font-style:3px;color: black;">magellan-solutions-outsourcing-inc<span>
                                    </a>
                                   <br>
                                </div>
                            </div>

                         </div>
                    </div>
                        <br>                     
                        
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
        
        <br>

        <div class="container">
            <div class="row">
                <div class="col-xs-8 col-md-6"></div>

                <a href="{{route('prepaid.calls.pdfptool', $parameters = array($load_id,$subscribers,$startdate,$enddate ))}}" class="btn btn-primary pull-right" style = "margin-right: 15px;">
                    <i class="glyphicon glyphicon-list-alt"></i> PDF</a> 

                <a href="{{route('prepaid.calls.excelptool', $parameters = array($load_id,$subscribers,$startdate,$enddate ))}}" class="btn btn-primary pull-right" >
                    <i class="glyphicon glyphicon-list-alt"></i> Excel</a>
            </div>
        </div>
            
            <br><br>

        <div id="getRequestData"></div>

            <div id="removeData">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-default" style = "padding:15px; border-color: black;">

                            <br>
                            <div class="row">
                                <div class = "container" style = "max-width: 100%; height: auto;">
                                    <div class="col-xs-12 col-md-8">
                                        <div style = "background-color:#0EBFE9 ; padding:9px;  margin-top: 58px; text-align: right;" class = "dd animated bounceInLeft">
                                            <I><span style = "color:white; margin: 20px; ">See The Future Your Way</span></I>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-2 dd animated bounceInRight"><img src="{{$asset}}gentella/images/logo2.png" alt="..." class="img-circle profile_img" style ="width: 195%; margin-left: -4px; margin-top: 6px;"></div>
                                </div>
                            </div>
                            
                            <br>
                            <div class="row">
                                <div class="col-xs-6 col-md-6 animated bounceInUp" >
                                    <b>Magellan Solutions</b> <br>
                                    33rd Floor, Summit One Tower,<br>
                                    Shaw Blvd, Mandaluyong, 1552 Metro Manila
                                </div>
                                <br><br>
                            </div>

                            <br><br>

                            <div class = "container">
                                <div class = "content-loader " id = "tableContainer">
                                    <table >
                                        <tr>
                                            <th>#</th>
                                            <th>CRC</th>
                                            <th>Description</th>

                                            <?php foreach($dates as $date){ ?>

                                            <td>
                                                <table>
                                                    <tr>
                                                        <div class = "column-size">
                                                            <b>
                                                                {{$date}}
                                                            </b>
                                                        </div>
                                                    </tr>
                                                    <tr>
                                                        <b class = "column-size"><center>
                                                        {{date('D', strtotime($date))}}
                                                        </b></center>
                                                    </tr>
                                                </table>
                                            </td>
                                               
                                            <?php } ?>

                                        </tr>

                                        <?php $ctr = 1;?>
                                        <?php if(!empty($loadList)){ ?>
                                        <?php foreach($loadList as $key => $load){ ?>   
                                            <tr>
                                                <td>{{$ctr }}</td>
                                                <td>{{$load['status'] }}</td>
                                                <td>{{$load['status_name'] }}</td>
                                                <?php foreach($dates as $key2 => $date){ ?>
                                                    <td>{{count(array_filter($load['date'][$date])) }}</td> 
                                                    <?php $sums[$date][$key] = count(array_filter($load['date'][$date]));?>         
                                                <?php }  ?>  
                                            </tr>
                                           
                                        <?php $ctr++; }}  ?>

                                        <tr>
                                            <td colspan = 2 align = rights style = "border-right: 1px solid white;"><b>Total :</b><td>
                                            
                                            <?php foreach($sums as $sumkey => $sum){ ?>
                                           
                                                <td>{{ array_sum($sum) }}</td> 

                                            <?php } ?>
                                        </tr>  

                                    </table>
                                </div>
                          
                            </div>

                            <br><br>

                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <div style = "text-align:left;">
                                        <b>Contact Us</b><br>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-xs-6 col-md-4" >
                                    <div style = "text-align:left;">
                                        <a href="http://www.magellan-solutions.com/" target="_blank"  class="btn btn-laptop" style ="margin-bottom:0px; ; padding-bottom:0px; color: black;"><i class="fa fa-laptop"></i> 
                                            <span style = "font-style:5px; color: black;">www.magellan-solutions.com<span>
                                        </a>
                                       <br>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-4" >
                                    <div style = "text-align:left;">
                                        <a href="https://www.facebook.com/magellansolutions/" target="_blank" class="btn btn-facebook" style ="margin-bottom:0px; color: black; padding-bottom:0px;"><i class="fa fa-facebook"></i> 
                                            <span style = "font-style:5px; font-style:5px; color: black;">www.facebook.com/magellansolutions<span>
                                        </a> 
                                       <br>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-md-4" >
                                    <div style = "text-align:left;">
                                        <a href="https://www.facebook.com/magellansolutions/" target="_blank" class="btn btn-facebook" style ="margin-bottom:0px; color: black; padding-bottom:0px;"><i class="fa fa-linkedin-square"></i> 
                                            <span style = "font-style:5px; font-style:3px;color: black;">magellan-solutions-outsourcing-inc<span>
                                        </a>
                                       <br>
                                    </div>
                                </div>

                             </div>
                        </div>
                            <br>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

