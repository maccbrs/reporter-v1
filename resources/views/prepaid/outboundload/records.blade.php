 @extends('prepaid.master')
<?php $asset = URL::asset('/'); ?> 
@section('content')

<div class="row">
    @include('flash::message')

        <div class="x_title">
            <h2><i class="fa fa-bars"></i> Report</h2>
            <ul class="nav navbar-right panel_toolbox">
                 <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>

        <div class="row">



			            <div class="col-md-12 col-sm-12 col-xs-12">
			              <div class="x_panel">
			                <div class="x_content">
			                  <table id="datatable-buttons" class="table table-striped table-bordered">

			                    <thead>
			                      <tr>
			                        <th>Date</th>
			                        <th>Time</th>
			                        <th>Dialed Number</th>
			                        <th>Length(sec)</th>
			                        <th>Length(min)</th>
			                      </tr>
			                    </thead>


			                    <tbody>
				                  @foreach($records->details as $row)

				                      <tr>
				                        <td>{{$carbon->mbtimecreate($row['call_details']['start_time'],'M d, Y')}}</td>
				                        <td>{{$carbon->mbtimecreate($row['call_details']['start_time'],'H:i:s')}}</td>
				                        <td>{{$row['call_details']['number_dialed']}}</td>
				                        <td>{{$row['call_details']['length_in_sec']}}</td>
				                        <td>{{$row['call_details']['length_in_min']}}</td>
				                      </tr>

			                      @endforeach
			                    </tbody>
			                  </table>
			                </div>
			              </div>
			            </div>


        </div>
</div>

@endsection

@section('header-scripts')
  <link href="{{$asset}}gentella/js/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="{{$asset}}gentella/js/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{$asset}}gentella/js/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{$asset}}gentella/js/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="{{$asset}}gentella/js/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
@endsection



@section('footer-scripts')
    
	<script src="{{$asset}}gentella/js/datatables/jquery.dataTables.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/dataTables.bootstrap.js"></script>
	<script src="{{$asset}}gentella/js/datatables/dataTables.buttons.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/buttons.bootstrap.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/jszip.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/pdfmake.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/vfs_fonts.js"></script>
	<script src="{{$asset}}gentella/js/datatables/buttons.html5.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/buttons.print.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/dataTables.fixedHeader.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/dataTables.keyTable.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/dataTables.responsive.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/responsive.bootstrap.min.js"></script>
	<script src="{{$asset}}gentella/js/datatables/dataTables.scroller.min.js"></script>
	<script src="{{$asset}}gentella/js/moment/moment.min.js"></script>

        <script>
          var handleDataTableButtons = function() {
              "use strict";
              0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
                dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm"
                }, {
                  extend: "csv",
                  className: "btn-sm"
                }, {
                  extend: "excel",
                  className: "btn-sm"
                }, {
                  extend: "pdf",
                  className: "btn-sm"
                }, {
                  extend: "print",
                  className: "btn-sm"
                }],
                responsive: !0
              })
            },
            TableManageButtons = function() {
              "use strict";
              return {
                init: function() {
                  handleDataTableButtons()
                }
              }
            }();
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('#datatable').dataTable();
            $('#datatable-keytable').DataTable({
              keys: true
            });
            $('#datatable-responsive').DataTable();
            $('#datatable-scroller').DataTable({
              ajax: "js/datatables/json/scroller-demo.json",
              deferRender: true,
              scrollY: 380,
              scrollCollapse: true,
              scroller: true
            });
            var table = $('#datatable-fixed-header').DataTable({
              fixedHeader: true
            });
          });
          TableManageButtons.init();
        </script>


@endsection


