<?php $asset = URL::asset('/'); 
$user = Auth::user();   ?> 
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row"  >

	        <div class="col-xs-6">

	        	<img src = "{{$asset}}gentella/images/ghost.png" >

	        </div>

	        <div class="col-xs-6">
	        <div style ="margin-top: 120px;">
	        	 @include('flash::message')
	        	<form method="POST" action = "/store">
					<input type="hidden" name="_method" value="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
	        		<input class="form-control" type = "hidden" value = {{ $user['id'] }} name ="user_id">
	        		<center>
	        		<h1>Something went wrong.<br> <br> 
	        		Tell us what Happen.</h1><br><br> <center>
					  <div class="form-group row">
					    <label for="inputEmail3" class="col-sm-2 form-control-label">Details</label>
					    <div class="col-sm-10">
					      <textarea class="form-control" id="exampleTextarea" rows="3" name ="desc"></textarea>
					    </div>
					  </div>
					  <div class="form-group row">
					    <label for="inputPassword3" class="col-sm-2 form-control-label"  >Website</label>
					    <div class="col-sm-10">
					      <select class="form-control" id="sel1" name = "website">
						    <option value = "Prepaid" text ="Prepaid">Prepaid</option>
						    <option value = "Reporter">Reporter</option>
						    <option value = "NOC Central">NOC Central</option>
						    <option value = "Logger">Logger</option>
						  </select>
					    </div>
					  </div>

					  <div class="form-group row">
					    <label class="col-sm-2 form-control-label" >How often?</label>
					    <div class="col-sm-10">
					      <select class="form-control" id="sel1" name = "how_often">
						    <option value = "Once" text ="Prepaid">Once</option>
						    <option value = "Seldom">Seldom</option>
						    <option value = "Always">Always</option>
						  </select>
					    </div>
					  </div>

					  <br><br>

					  <div class="form-group row">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="submit" class="btn btn-primary">Submit</button>
					    </div>
					  </div>
					</form>


	        </div>
	     </div>
            


        
    </div>
</div>
@endsection