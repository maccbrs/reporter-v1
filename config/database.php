<?php
$ip = '192.168.200.28';
$username = 'crusader';
$password = 'c4a1de';

// $ip = 'localhost';
// $username = 'root';
// $password = '';
return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => 'bouquet', 

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        '192.168.200.28.laravel' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.28',
            'database'  => 'laravel',
            'username'  => 'crusader',
            'password'  => 'c4a1de',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        'rhyolite' => [
            'driver' => 'mysql',
            'host' => '192.168.200.123',
            'port' => '3306',
            'database' => 'rhyolite',
            'username' => 'marlon',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],          
        'jasmin' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.97',
            'database'  => 'jasmin',
            'username'  => 'marlon',
            'password'  => 'c4a1de',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],          
        'bouquet' => [ 
            'driver' => 'mysql',
            'host' => $ip,
            'port' => '3306',
            'database' => 'bouquet', 
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ], 
        '192.168.200.132' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.132',
            //'host'      => '192.168.200.132',//132
            'port' => '3306',
            'database'  => 'asterisk',
            'username' => 'cron',
            'password' => '1234',
            // 'username' => 'crusader',
            // 'password' => 'c4a1de',            
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        // 130
        '192.168.200.130' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.132',
            'port' => '3306',
            'database'  => 'asterisk',
            'username' => 'cron',
            'password' => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ], 

        '192.168.200.77' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.77',
            'port' => '3306',
            'database'  => 'asterisk',
            'username' => 'cron',
            'password' => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],                             
        'rosebud' => [
            'driver' => 'mysql',
            'host' => $ip,
            'port' => '3306',
            'database' => 'rosebud', 
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
         'lily' => [
            'driver' => 'mysql',
            'host' => $ip,
            'port' => '3306',
            'database' => 'lily', 
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ], 
        'primrose' => [
            'driver' => 'mysql',
            'host' => $ip,
            'port' => '3306',
            'database' => 'primrose', 
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'dahlia' => [ 
            'driver' => 'mysql',
            'host' => $ip,
            'port' => '3306',
            'database' => 'dahlia', 
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ], 
        'prepaid' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.97',
            'database'  => 'prepaid',
            'username'  => 'marlon',
            'password'  => 'c4a1de',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,   
        ],
        'dummyboard' => [
            'driver' => 'mysql',
            'host' => '192.168.200.28',
            'port' => '3306',
            'database' => 'laravel', 
            'username' => 'crusader',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],
        'mytestdb' => [
            'driver' => 'mysql',
            'host' => '192.168.200.28',
            'port' => '3306',
            'database' => 'mytestdb', 
            'username' => 'crusader',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],  
        'poppy' => [
            'driver' => 'mysql',
            'host' => '192.168.200.28',
            'port' => '3306',
            'database' => 'supportdb', 
            'username' => 'crusader',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci', 
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],                                                                    
        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
        ],
         'vicidial' => [
            'driver'    => 'mysql',
            'host'      => '192.168.200.132',
            'database'  => 'asterisk',
            'username'  => 'cron',
            'password'  => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'vicidial2' => [
            'driver'    => 'mysql',

            'host'      => '192.168.200.132',

            'database'  => 'asterisk',
            'username'  => 'cron',
            'password'  => '1234',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
        
        'gardenia' => [ 
            'driver' => 'mysql',
            'port' => '3306',
            'host' => '54.67.2.80',
            'database' => 'gardenia',
            'username' => 'marlon.bernal',
            'password' => 'c4a1de',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ], 
        'nexus' => [
            'driver' => 'sqlsrv',
            'host' => '192.168.200.133\CICMSSQLSERVER',
            'database' => 'I3_IC',
            'port' => '1433',
            'username' => 'sa',
            'password' => 'M@gell@n$oln!@#$',
            'charset' => 'utf8',
            'prefix' => '',
        ],         

		'jasper' => [
            'driver' => 'mysql',
            'host' => '192.168.201.77',
            'port' => '3306',
            'database' => 'client_portal',
            'username' => 'marc',
            'password' => 'm@rc@db',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'strict' => false,
            'engine' => null,
        ],


    ], 

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host' => env('REDIS_HOST', 'localhost'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],

    ],

];
